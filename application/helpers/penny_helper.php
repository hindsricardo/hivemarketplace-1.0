<?php	

if (!function_exists('get_user_username')) {

    function get_user_username($users_id) {
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$users_data = $CI->model_users->get_user_username($users_id);
		return $users_data;
		}
	}
if (!function_exists('get_user_email')) {

    function get_user_email($users_id) {
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$users_data = $CI->model_users->get_user_email($users_id);
		return $users_data;
		}
	}
if (!function_exists('get_user_paypalemail')) {

    function get_user_paypalemail($users_id) {
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$users_data = $CI->model_users->get_user_paypalemail($users_id);
		return $users_data;
		}
	}
if (!function_exists('credit_val')) {
    function credit_val() {
		return '0.75';
		}
	}
if (!function_exists('withdraw_credits_val')) {
    function withdraw_credits_val() {
		return '0.71';
		}
	}
if (!function_exists('user_available_credit')) {
    function user_available_credit($user_id = ''){
		$CI = & get_instance();
		if($user_id == ''){
			$user_id = $CI->session->userdata('user_id');
		}
		$CI->load->model('model_manage_credits');
		$users_data = $CI->model_manage_credits->user_available_credit($user_id);
		return $users_data;
	}
}
/*if (!function_exists('auction_current_winner')) {
    function auction_current_winner($auction_id) {
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$auction_current_winner = get_user_username($CI->model_list_auction->current_winner($auction_id));
		return $auction_current_winner;
	}
}*/
if (!function_exists('get_admin_username')) {
    function get_admin_username($admin_id) {
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$admin_username = $CI->model_users->get_admin_username($admin_id);
		return $admin_username;
	}
}
if (!function_exists('get_current_winner')) {
    function get_current_winner($auction_id) {
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$bid_details = $CI->model_list_auction->get_current_winner($auction_id);
		//echo '<pre>';print_r($bid_details);die();
		if(!empty($bid_details)){
				$current_winner['winner_credit'] = $bid_details[0]['credits'];
				$current_winner['winner_user_id'] = $bid_details[0]['user_id'];
				$current_winner['winner_username'] = get_user_username($bid_details[0]['user_id']);
			return $current_winner;
		}else{
			return array();
		}
	}
}
if (!function_exists('user_unread_message')) {
    function user_unread_message($user_id) {
		$CI = & get_instance();
		$CI->load->model('message_model');
		$data['unread'] =$CI->message_model->message_unread();
		return count($data['unread']);
	}
}
if (!function_exists('item_shipping_value')) {
    function item_shipping_value($aid) {
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$item_shipping_value =$CI->model_list_auction->auction_shipping_details($aid);
		//echo '<pre>';print_r($item_shipping_value);die();
		if(isset($item_shipping_value[0]['us_price'])){
			return $item_shipping_value[0]['us_price'];	
		}else{
			return '0';
		}
	}
}
if (!function_exists('get_social')) {
    function get_social() {
		$CI = & get_instance();
		$CI->load->model('admin/model_social');
		//$CI->load->model('model_list_auction');
		$get_social = $CI->model_social->get_social();
		//echo '<pre>';print_r($get_social);die();
		return $get_social;
	}
}
if (!function_exists('user_current_hivemp_fee')) {
    function user_current_hivemp_fee($user_id) {
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$CI->load->model('admin/model_users');
		$subscribe = $CI->model_users->check_subscribe($user_id);
		if($subscribe == '1'){
			return $hmp_fee = '1.75';
		}else{
			$user_live_auctions = $CI->model_list_auction->count_user_live_auction($user_id);
			//return $user_live_auctions;
			if($user_live_auctions <= 9){
			$hmp_fee = '7';
			}elseif($user_live_auctions > 9 && $user_live_auctions <= 19){
			$hmp_fee = '3.5';
			}else{
			$hmp_fee = '1.75';
			}
			return $hmp_fee;
		}
	}
}
if (!function_exists('check_subscribe')) {
    function check_subscribe($user_id) {
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$subscribe = $CI->model_users->check_subscribe($user_id);
			return $subscribe;
	}
}
if (!function_exists('get_auction_images')) {
    function get_auction_images($auction_id) {
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$auction_images = $CI->model_list_auction->get_auction_images($auction_id);
		if(!empty($auction_images)){
			return $auction_images;
		}else{
			return '';
		}
	}
}
if (!function_exists('winnerCart')) {
    function winnerCart(){
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
			$get_auctions = $CI->model_list_auction->user_win_add_cart_aid();
			if($get_auctions != ''){
				$i = 0;
				//echo '<pre>';print_r($get_auctions);die();
				$CI->load->model('cart_model');
				$c = $CI->cart->contents();
				//echo '<pre>';print_r($get_auctions);die();die();
				//$CI->cart->destroy();
				foreach($get_auctions as $get_auctions_id){
					$insert_room = array();
					$auction_details = $CI->model_list_auction->get_auction_details($get_auctions_id['ID']);
					if($auction_details[0]['type'] != 'credit_auction'){
						$no_of_bids = $CI->model_list_auction->bid_details($get_auctions_id['ID']);
						$item_price = $no_of_bids*$CI->config->item('pennies');
						//echo '<pre>';print_r($auction_details);
						if($auction_details[0]['type'] == 'credit_auction'){
							$picture = 'HIVEcomb.png';
						}else{
							$picture = $auction_details[0]['images'][0]['image_name'];
						}
						if($auction_details[0]['offer_shipping'] == '1'){
							$delivery_type = 'ship';
						}elseif($auction_details[0]['payat_pickup'] == '1'){
							$delivery_type = 'pick';
						}else{
							$delivery_type = '';
						}
						if(isset($auction_details[0]['buy_type'])){
							$buy_type = $auction_details[0]['buy_type'];
						}else{
							$buy_type = '';
						}
						//echo $auction_details[0]['title'];
						$insert_room = array(
							'id' => $auction_details[0]['id'],
							'user_id' => $auction_details[0]['user_id'],
							'picture' => $picture,
							'name' => $auction_details[0]['title'],
							'price' => $item_price,
							'qty' => '1',
							'tags' => $auction_details[0]['tags'],
							'desc' => $auction_details[0]['description'],
							'credit' => '',
							'offer_shipping' => $auction_details[0]['offer_shipping'],
							'shipping' => $auction_details[0]['us_price'],
							'payat_pickup' => $auction_details[0]['payat_pickup'],
							'delivery_type' => $delivery_type,
							'buy_type' => $buy_type
						);		
						$data['win_add_cart'] = $CI->cart->insert($insert_room);
					}
				}
			}
			//echo count($CI->cart->contents()); die();
		return count($CI->cart->contents());
	}
}
if (!function_exists('is_watchlist')) {
	function is_watchlist($aid, $uid){
		$CI = & get_instance();
		$CI->load->model('model_list_auction');
		$watchlist = $CI->model_list_auction->is_watchlist($aid, $uid);
		return $watchlist;
	}
}
if (!function_exists('is_feedback')) {
	function is_feedback($aid, $uid){
		$CI = & get_instance();
		$CI->load->model('feedback_model');
		$feedback = $CI->feedback_model->is_feedback($aid, $uid);
		return $feedback;
	}
}
if (!function_exists('order_shipped_scheduled_pickup_check')) {
	function order_shipped_scheduled_pickup_check($order_details_id){
		$CI = & get_instance();
		$CI->load->model('model_manage_credits');
		$feedback = $CI->model_manage_credits->order_shipped_scheduled_pickup_check($order_details_id);
		return $feedback;
	}
}
if (!function_exists('is_item_received')) {
	function is_item_received($order_details_id){
		$CI = & get_instance();
		$CI->load->model('model_manage_credits');
		$item_recived = $CI->model_manage_credits->is_item_received($order_details_id);
		//echo '<pre>';print_r($feedback);
		return $item_recived[0]['order_status'];
	}
}
if (!function_exists('report_data')) {
	function report_data($type){
		$CI = & get_instance();
		$CI->load->model('admin/model_report');
		$report = $CI->model_report->report_data($type);
		//echo '<pre>';print_r($feedback);
		return $report[0]['total_hmp_fee'];
	}
}
if (!function_exists('is_reply')) {
	function is_reply($message_id){
		$CI = & get_instance();
		$CI->load->model('message_model');
		$reply_date = $CI->message_model->is_reply($message_id);
		//echo '<pre>';print_r($feedback);
		if(!empty($reply_date)){
			return $reply_date;
		}else{
			return '';
		}
	}
}
if (!function_exists('admin_is_reply')) {
	function admin_is_reply($message_id){
		$CI = & get_instance();
		$CI->load->model('admin/message_model');
		$reply_date = $CI->message_model->admin_is_reply($message_id);
		//echo '<pre>';print_r($feedback);
		if(!empty($reply_date)){
			return $reply_date;
		}else{
			return '';
		}
	}
}



/*For date*/
if ( ! function_exists('to_db_date'))
{
	function to_db_date($time = '')
	{
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$users_id = $CI->session->userdata('user_id');
		$userTZ = $CI->model_users->get_user_timezone($users_id);
		//$userTZ = "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";
		
		if ($time == '') {
			return gmdate('Y-m-d H:i:s');
		}
		$timezone_val = timezones($userTZ);
		$time -= $timezone_val['gmtOffset'] * 60;
		$dst = inDST(date('Y-m-d H:i:s',$time), $timezone_val);
		if ($dst == TRUE){
			$time -= 3600;
		}

		return date('Y-m-d H:i:s',$time);
	}
}
if ( ! function_exists('timezones')){
	function timezones($tz = ''){
		$CI = & get_instance();
		$timezonesArray = $CI->config->item("timezones");
		if ($tz == ''){
			return $timezonesArray;
		}
		return ( ! isset($timezonesArray[$tz])) ? 0 : $timezonesArray[$tz];
	}
}
function inDST($date, $zone){
	$datetime = explode(' ', $date);
	$dateSplit = explode('-', $datetime[0]);
	if(empty($dateSplit[2]))return false;
	$dstRange = getDSTRange($dateSplit[0], $zone);
	/*echo '<pre>';
	print_r($dstRange);
	echo '</pre>';*/
	if(!$dstRange){
		return false;
	}
	$datestamp = strtotime($date);
	$startstamp = strtotime($dstRange['start']);
	$endstamp = strtotime($dstRange['end']);
	if((($datestamp >= $startstamp  || $datestamp < $endstamp) && $startstamp > $endstamp)
		|| ($datestamp >= $startstamp && $datestamp < $endstamp)
	){
		return true;
	}
	return false;
}
function getDSTRange($year, $zone){
	$range = array();
	if(empty($zone['dstOffset'])){
		return false;
	}

	$range['start'] = getDateFromRules($year, $zone['dstMonth'], $zone['dstStartday'], $zone['dstWeekday'],  $zone['dstStartTimeSec']);
	$range['end'] = getDateFromRules($year, $zone['stdMonth'], $zone['stdStartday'], $zone['stdWeekday'],  $zone['stdStartTimeSec']);
	return $range;
}
function getDateFromRules($year, $startMonth, $startDate, $weekday, $startTime ){
	if($weekday < 0)return date( 'Y-m-d H:i:s', strtotime("$year-$startMonth-$startDate") + $startTime);
	$dayname = getWeekDayName($weekday);
	if($startDate > 0)$startMonth--;
	$month = getMonthName($startMonth);
	$startWeek = floor($startDate/7);
	return date( 'Y-m-d H:i:s', strtotime( "$startWeek week $dayname", strtotime( "$month 1, $year" ) ) + $startTime );

}

function getWeekDayName($indexOfDay){
	static $dow = array ( 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday' );
	return $dow[$indexOfDay];
}
function getMonthName($indexMonth){
	static $months = array ( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
	return $months[$indexMonth];
}

if ( ! function_exists('to_display_date_time'))
{
	function to_display_date_time($time = '', $current_user_id='')
	{
		$CI = & get_instance();
		$CI->load->model('admin/model_users');
		$users_id = $CI->session->userdata('user_id');
		$userTZ = $CI->model_users->get_user_timezone($users_id);
		//$userTZ = "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";
		//$userTZ = "(GMT+07:00) Bangkok, Hanoi, Jakarta";
		$userDateFormat = 'Y-m-d';
		$userTimeFormat = 'H:i:s';
		if($time == ''){
			return date($userDateFormat.' '.$userTimeFormat);
		}
		
		$timezone_val = timezones($userTZ);
		$time += $timezone_val['gmtOffset'] * 60;
		$dst = inDST(date('Y-m-d H:i:s',$time), $timezone_val);
		if ($dst == TRUE){
			$time += 3600;
		}

		return date('Y-m-d H:i:s', $time);
	}
}
function get_adjust_time($schedule_endtime){
	$adjust_time = strtotime($schedule_endtime)-6*60*60;
	//$adjust_time = strtotime($schedule_endtime);
	return $adjust_time;
}

function fSecureInput($input) {
   $output = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars(trim($input), ENT_QUOTES))));
   return $output;
}
function get_user_direct_buy_item_count($auction_id, $user_id){
	$CI = & get_instance();
	$CI->load->model('model_list_auction');
	$users_data = $CI->model_list_auction->get_user_direct_buy_item_count($auction_id, $user_id);
	return $users_data;
}
?>