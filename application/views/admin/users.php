<?php $this->load->helper('url'); ?>
<?php $this->load->view('adminnav/nav_header');?>
<?php $this->load->view('adminnav/nav_left');?>
<script type="text/javascript" language="javascript">
function keyword_search_onblur(search_val){
	if(search_val == ''){
		$("#user_search_value").val('Keyword Search');
	}
}
function keyword_search_onfocus(search_val){
	if(search_val == 'Keyword Search'){
		$("#user_search_value").val('');
	}
}
</script>
<?php //echo '<pre>';print_r($data);?>

	<div class="Row95 marTop10">
	<p class="txt16pB">Users</p>
	</div>
	<div class="messagebox_right">
		<div class="Row98 clearfix">
			<div class="flt_lft" style="width:70px;">
				<div class="Row98 txt12B marTop20">Total: <span class="txt11pB_or"><?php echo count($user_list)?></span></div>
			</div>
		<!--Search -->
			<div class="flt_right">
				<div class="AdminsearchWrap">
				<form name="user_search_form" id="user_search_form" class="user_search_form" method="post" action="<?php echo base_url()?>webadmin/users/search">
					<div class="AdminsearchInpWrap"><input name="user_search_value" id="user_search_value" type="text" onblur="keyword_search_onblur(this.value)" onfocus="keyword_search_onfocus(this.value)" value="<?php if(isset($user_search_value)){ echo $user_search_value; }else{ echo 'Keyword Search';}?>"  class="AdminsearchsearchInp"/>
					</div>
					<div class="flt_right"><input value="" type="submit" class="admin_search"  /></div>
				</form>
				</div>
			</div>
		<!--Search -->
		<?php //echo '<pre>';print_r($user_list);?>
		<div class="cls" style="height:10px;"></div>
		<hr />
			<div class="Row">
				<table width="100%" class="admin_comm_table">
					<tr class="admin_topRow">
						<td class="mid center">ID</td>
						<td class="mid center">User Name</td>
						<td class="mid center">Last Login</td>
						<td class="mid center">Credit Value</td>
						<td class="mid center">Email</td>
						<td class="mid center">HIVEMP Fee%</td>
					</tr>
					<?php foreach($user_list as $user_list_val){ ?>
					<tr>
						<td class="mid borderBot"><?php echo $user_list_val->id; ?></td>
						<td class="mid borderBot"><a href="<?php echo base_url()?>webadmin/users/details/<?php echo $user_list_val->id?>" class="txt11pN"><?php echo $user_list_val->username; ?></a></td>
						<?php $timezone='EST';
						date_default_timezone_set($timezone);?>
						<td class="mid borderBot"><?php echo date("d/m/Y  g:i a",strtotime($user_list_val->lost_login))." EST";?><!--1/1/2012 8:59pm EST--></td>
						<?php $user_credit_available = user_available_credit($user_list_val->id);?>
						<td class="mid center borderBot"><?php echo $user_credit_available['total_credit_available'];?></td>
						<td class="mid borderBot"><?php echo $user_list_val->email; ?></td>
						<td class="center borderBot"><?php echo user_current_hivemp_fee($user_list_val->id);?></td>
					</tr>
					<?php }?>
				</table>
			</div>
		</div>
	</div>
     </td>
  </tr>
</table>
</div>
</div>
</div>
<!--BID NOW AREA END-->
<?php $this->load->view('adminnav/nav_footer');?>