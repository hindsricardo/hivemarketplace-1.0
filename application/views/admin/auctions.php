<?php $this->load->helper('url'); ?>
<?php $this->load->view('adminnav/nav_header');?>
<?php $this->load->view('adminnav/nav_left');?>
<script src="<?php echo base_url()?>assetts/js/jquery.countdown.js"></script><div class="messagebox_right">
<script>
function search_auction_list(){
	var type = $('input[name="r1"]:checked').val();
	var search_str = $('.AdminsearchsearchInp').val();
	//alert(redio_val+'>>>>>>>>>'+keyword_search);
	
	$.post("<?php echo base_url();?>webadmin/auctions/auction_filter",{search_str: search_str, type: type}, function(data){
		$("#middel_content").html(data);
	});
	
}
</script>
<style>
.countdownHolder{
	margin:0 auto;
	/*font: 40px/1.5 'Open Sans Condensed',sans-serif;*/
	text-align:center;
	letter-spacing:-3px;
}

.position{
	display: inline-block;
	height: 21px;
	overflow: hidden;
	position: relative;
	width: 15px;
}

.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}

.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}

/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{ /* display:none !important;*/ }
.countDiv0{ /* display:none !important;*/ }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}
.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}

.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}

.countDiv:after{
	top:0.9em;
}
</style>
<?php //echo '<pre>';print_r($list_auction);?>
<div class="Row98 clearfix">
<div class="flt_lft" style="width:70px;">

</div>

<div class="flt_lft" style="width:200px;">
<div class="Row98 txt12B marTop20">
<form action="" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="middle" align="left"><input name="r1" type="radio" value="all" class="redio_all" onClick="search_auction_list()" />&nbsp;ALL</td>
     <td valign="middle" align="left"><input name="r1" type="radio" value="active" class="redio_active" onClick="search_auction_list()" />&nbsp;Active</td>
     <td valign="middle" align="left"><input name="r1" type="radio" value="ended" class="redio_ended" onClick="search_auction_list()" />&nbsp;Ended</td>
  </tr>
</table>
</form>
</div>
</div>
<!--Search -->
<div class="flt_right">
	<div class="AdminsearchWrap">
	<div class="AdminsearchInpWrap"><input name="search_str" type="text" value="" class="AdminsearchsearchInp" placeholder="Keyword Search"/>
	</div>
	<div class="flt_right"><input type="button" class="admin_search" onClick="search_auction_list()" /></div>
	</div>
</div>
    <!--Search -->
<div class="cls"><br clear="all" /></div>
<div id="middel_content">
<?php $this->load->view('admin/auctions_bk');?>
</div>
</div>
</div>
<?php $this->load->view('adminnav/nav_footer');?>