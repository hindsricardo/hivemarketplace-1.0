<?php $this->load->helper('url'); ?>
<?php $this->load->view('adminnav/nav_header');?>
<?php $this->load->view('adminnav/nav_left');?>
<script type="text/javascript" language="javascript">
function add_new_admin_function(){
	var username = $('#add_username').val();
	var password = $('#add_password').val();
	var name = $('#add_name').val();
	var email = $('#add_email').val();
	if(username == ''){
		alert('Please enter User Name');
		$('#add_username').focus();
		return false;
	}
	if(password == ''){
		alert('Please enter Password');
		$('#add_password').focus();
		return false;
	}
	if(name == ''){
		alert('Please enter Name');
		$('#add_name').focus();
		return false;
	}

	if(IsEmail(email)==false){
		alert('Please enter a valid Email');
		$('#add_email').focus();
		return false;
	}
	$("#add_new_admin").submit();
}
	function IsEmail(email) {
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)) {
			return false;
		}else{
			return true;
		}
	}
function delete_admin_function(delete_id){
		$.post("<?php echo base_url()?>webadmin/admin_tools/delete", { id: delete_id},function(data) {
			$(".admin_list_"+delete_id).remove();
		});
}
function edit_admin_function(edit_id){
	$.post("<?php echo base_url()?>webadmin/admin_tools/get_edit_details", { id: edit_id},function(data) {
	var html_code = '<form name="edit_new_admin" id="edit_new_admin" class="edit_new_admin" action="<?php echo base_url();?>webadmin/admin_tools/edit" method="post">';
		 html_code +=	'<td class="mid center borderBot"><input readonly name="edit_id" id="edit_id" type="text" class="comm_inp"  style="width:30px;" value="'+data[0].id+'" /></td>';
		 html_code +=	'<td class="mid center borderBot"><input name="edit_username" id="edit_username" type="text" class="comm_inp"  style="width:100px;" value="'+data[0].username+'" /></td>';
		 html_code +=	'<td class="mid center borderBot"><input name="edit_password" id="edit_password" type="text" class="comm_inp"  style="width:100px;" value="'+data[0].password+'" /></td>';
		 html_code +=	'<td class="mid center center borderBot"><input name="edit_name" id="edit_name" type="text" class="comm_inp"  style="width:100px;" value="'+data[0].name+'" /></td>';
		 html_code +=	'<td class="mid center borderBot"><input name="edit_email" id="edit_email" type="text" class="comm_inp"  style="width:150px;" value="'+data[0].email+'" /></td>';
		 html_code +=	'<td class="mid center borderBot">';
		 html_code +=		'<select name="edit_level" id="edit_level" class="add_level">';
		 html_code +=			'<option value="1" '+(data[0].level == 1 ? 'selected="selected"': '')+'>Admin</option>';
		 html_code +=			'<option value="2" '+(data[0].level == 2 ? 'selected="selected"': '')+'>Manager</option>';
		 html_code +=			'<option value="3" '+(data[0].level == 3 ? 'selected="selected"': '')+'>Support Admin</option>';
		 html_code +=		'</select></td>';
		 html_code +=	'<td colspan="2" class="mid center borderBot"><a href="javascript:void(0);" class="txt11pN" onclick="update_admin_function('+data[0].id+');">Update</a></td>';
		 html_code += '</form>';
		$(".admin_list_"+edit_id).html(html_code);
	},'json');
}
function update_admin_function(update_id){
	var username = $('.admin_list_'+update_id+' #edit_username').val();
	var password = $('.admin_list_'+update_id+' #edit_password').val();
	var name = $('.admin_list_'+update_id+' #edit_name').val();
	var email = $('.admin_list_'+update_id+' #edit_email').val();
	var level = $('.admin_list_'+update_id+' #edit_level').val();
	if(username == ''){
		alert('Please enter User Name');
		$('.admin_list_'+update_id+' #edit_username').focus();
		return false;
	}
	if(password == ''){
		alert('Please enter Password Name');
		$('.admin_list_'+update_id+' #edit_password').focus();
		return false;
	}
	if(IsEmail(email)==false){
		alert('Please enter a valid Email');
		$('.admin_list_'+update_id+' #edit_email').focus();
		return false;
	}
		$.post("<?php echo base_url()?>webadmin/admin_tools/edit", { id: update_id, username: username, password: password, name: name, email: email, level: level},function(data) {
			var html_code ='';
			html_code +=	'<td class="mid center borderBot">'+update_id+'</td>';
			html_code +=	'<td class="mid center borderBot">'+username+'</td>';
			html_code +=	'<td class="mid center borderBot">'+password+'</td>';
			html_code +=	'<td class="mid center center borderBot">'+name+'</td>';
			html_code +=	'<td class="mid center borderBot">'+email+'</td>';
			html_code +=	'<td class="mid center borderBot"><a href="javascript:void(0);" class="btn_edit" onclick="edit_admin_function('+update_id+')"></a></td>';
			html_code +=	'<td class="mid center borderBot"><a href="javascript:void(0);" class="btn_delete_grd" onclick="delete_admin_function('+update_id+');"></a></td>';
			$(".admin_list_"+update_id).html(html_code);
		});
}
</script>
<?php //echo '<pre>';print_r($data);?>

	<div class="Row95 marTop10">
		<p class="txt16pB">Admin Tools</p>
	</div>
	<div class="Row">
	<span class="error"><?php echo validation_errors('<p class="error">'); ?></span>
		<table width="100%" class="admin_comm_table">
			<tr class="admin_topRow">
			<td width="5%" class="mid center">ID</td>
			<td width="17%" class="mid center">User Name</td>
			<td width="15%" class="mid center">Password</td>
			<td width="17%" class="mid center">Name</td>
			<td width="20%" class="mid center">Email</td>
			<td width="10%" class="mid center"></td>
			<td width="10%" class="mid center"></td>
		</tr>
		<?php foreach($admin_list as $admin_list_val){ ?>
		<tr class="admin_list_<?php echo $admin_list_val->id?>">
			<td class="mid center borderBot"><?php echo $admin_list_val->id; ?></td>
			<td class="mid center borderBot"><?php echo $admin_list_val->username; ?></td>
			<td class="mid center borderBot"><?php echo $admin_list_val->password; ?></td>
			<td class="mid center center borderBot"><?php echo $admin_list_val->name; ?></td>
			<td class="mid center borderBot"><?php echo $admin_list_val->email; ?></td>
			<?php if($this->session->userdata('username') != $admin_list_val->username){ ?>
			<td class="mid center borderBot"><a href="javascript:void(0);" class="btn_edit" onclick="edit_admin_function('<?php echo $admin_list_val->id?>')"></a></td>
			<td class="mid center borderBot"><a href="javascript:void(0);" class="btn_delete_grd" onclick="delete_admin_function('<?php echo $admin_list_val->id?>');"></a></td>
			<?php }else{ ?>
			<td class="mid center borderBot"></td>
			<td class="mid center borderBot"></td>
			<?php }?>
			
		</tr>
		<?php }?>
		<tr>
		<form name="add_new_admin" id="add_new_admin" class="add_new_admin" action="<?php echo base_url();?>webadmin/admin_tools/add" method="post">
			<td class="mid center borderBot"><input readonly name="add_id" id="add_id" type="text" class="comm_inp"  style="width:30px;" /></td>
			<td class="mid center borderBot"><input name="add_username" id="add_username" type="text" class="comm_inp"  style="width:100px;" /></td>
			<td class="mid center borderBot"><input name="add_password" id="add_password" type="text" class="comm_inp"  style="width:100px;" /></td>
			<td class="mid center center borderBot"><input name="add_name" id="add_name" type="text" class="comm_inp"  style="width:100px;" /></td>
			<td class="mid center borderBot"><input name="add_email" id="add_email" type="text" class="comm_inp"  style="width:150px;" /></td>
			<td class="mid center borderBot">
				<select name="add_level" id="add_level" class="add_level">
					<option value="1">Admin</option>
					<option value="2">Manager</option>
					<option value="3">Support Admin</option>
				</select>
			</td>
			<td colspan="2" class="mid center borderBot"><a href="javascript:void(0);" class="txt11pN" onclick="add_new_admin_function();">Add New</a></td>
		</form>
		</tr>  
		</table>
	</div>
	</td>
	</tr>
	</table>
</div>
</div>
</div>

<!--BID NOW AREA END-->

<!--BID NOW AREA END-->
<?php $this->load->view('adminnav/nav_footer');?>