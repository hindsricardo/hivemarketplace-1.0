<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('site/model_social');
	}
	
	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$this->load->view('nav/nav_header');
			$this->load->view('nav/nav_left');
			$data['result'] = $this->model_social->get_social();
			$this->load->view('site/social', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	
	function update($id)
	{
		$data['id'] = $id;
		$data['twitter'] = $this->input->post('twitter');
		$data['vimeo'] = $this->input->post('vimeo');
		$data['skype'] = $this->input->post('skype');
		$data['facebook'] = $this->input->post('facebook');
		$data['flickr'] = $this->input->post('flickr');
		$data['linkedin'] = $this->input->post('linkedin');
		$data['rss'] = $this->input->post('rss');
		
		
		$result = $this->model_social->update_privacy_policy($data);
		
		$this->load->view('nav/nav_header');
		$this->load->view('nav/nav_left');
		$data['result'] = $this->model_social->get_social();
		$this->load->view('site/social', $data);
	}
	
}