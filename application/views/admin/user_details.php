<?php $this->load->helper('url'); ?>
<?php $this->load->view('adminnav/nav_header');?>
<?php $this->load->view('adminnav/nav_left');?>
<script src="<?php echo base_url()?>assetts/js/jquery.countdown.js"></script><div class="messagebox_right">
<script type="text/javascript" language="javascript">
function check_password(){
	var password = $(".comm_inp").val();
	if(password != ''){
		return true;	
	}else{
		alert("Enter Password");
		$(".comm_inp").css("border", "1px solid red");
		$(".comm_inp").focus();
		return false;
	}
}
function search_auction_list(auction_type, user_id){
	var type = auction_type;
	//var search_str = $('.AdminsearchsearchInp').val();
	//alert(redio_val+'>>>>>>>>>'+keyword_search);
	
	$.post("<?php echo base_url();?>webadmin/auctions/auction_filter",{type: type, seller_id: user_id}, function(data){
		$("#middel_content").html(data);
	});
	
}
</script>
<style>
.countdownHolder{
	margin:0 auto;
	/*font: 40px/1.5 'Open Sans Condensed',sans-serif;*/
	text-align:center;
	letter-spacing:-3px;
}

.position{
	display: inline-block;
	height: 21px;
	overflow: hidden;
	position: relative;
	width: 15px;
}

.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}

.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}

/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{ /* display:none !important;*/ }
.countDiv0{ /* display:none !important;*/ }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}
.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}

.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}

.countDiv:after{
	top:0.9em;
}
</style>
	 <div class="Row95 marTop10">
     <p class="txt16pB">Users Details</p>
     </div>
     
     
     <div class="messagebox_right">
<div class="Row98 clearfix marTop20">

<div class="Row">

<div class="flt_lft" style="width:430px;">

<div class="Row">
<div class="flt_lft">
<?php 
$user_profile_img = base_url().'assetts/uploads/'.$user_details[0]->photo;
if(file_exists($user_profile_img)){
?>
	<img src="<?php echo base_url()?>assetts/uploads/<?php echo $user_details[0]->photo; ?>" alt="Usre Name" width="57" height="57" border="0" />
<?php }else{ ?>
	<img src="<?php echo base_url()?>assetts/uploads/noimage.jpg" alt="Usre Name" width="57" height="57" border="0" />
<?php }?>


</div>

<div class="flt_lft marLft10">
<p class="txt12B">User ID : <?php echo $user_details[0]->id;?> </p>

<p class="marTop10 txt16pB">List#000</p>

<p class="txt12B marTop10">EMAIL: <a href="#" class="txt12B"><?php echo $user_details[0]->email;?></a></p>
</div>

</div>

<div class="Row marTop20">

<div class="roundBox_comm w428px">

<p class="txt13PB marTop10" align="center">Password Reset</p>

<div class="Row">
<form id="passwordreset" class="passwordreset" method="post" action="<?php echo base_url()?>webadmin/users/passwordreset" onsubmit="return check_password();">
<table width="98%" class="admin_comm_table">
  <tr>
		<td width="46%" class="mid txt11pN">Send Reset password to email address </td>
		
		<td width="37%" class="mid txt11pN"><input name="password" type="text" class="comm_inp" style="" /><input name="id" type="hidden" value="<?php echo $user_details[0]->id;?>" /></td>
		
		<td width="17%" class="mid txt11pN"><input type="submit" value="" class="btn_send" /></td>
  </tr>
</table>
</form>
</div>

</div>

</div>


</div>


<div class="biddingHistory flt_lft marLft40" style="margin-left:10px;">

<div class="bidHistoryHeader">Bidding History</div>

<div class="flt_lft Row biddingHistoryDetails">

<table width="100%" border="0" style="border-left:1px solid #666; border-right:1px solid #666;"  class="txtalignCenter txt11pN" cellspacing="0" cellpadding="0">
<?php
$i= 1;
foreach($bidding_history as $bidding_history_val){
?>
  <tr <?php if($i%2 != 0) echo 'class="odd"';?>>
    <td><img src="<?php echo base_url()?>assetts/images/icon_user.png" width="30" height="30" border="0" /></td>
    <td><?php echo 'List#'.$bidding_history_val['auction_id']?></td>
    <td><?php echo $bidding_history_val['credits']*$this->config->item("pennies");?></td>
    <td>Bid</td>
  </tr>
 <?php $i++; }?>
   <tr>
    <td align="center" colspan="3"><a href="#" class="txt13B">Click to Display More...</a></td>
  </tr>
</table>
</div>
</div>
</div>
<div class="Row marTop10">
<div style="float:left;"><p class="txt13B">Listings</p></div>
<div class="details_select">
<select class="sel" onchange="search_auction_list(this.value,'<?php echo $user_details[0]->id?>')">
<option value="active">Active</option>
<option value="ended">Deactive</option>
</select>
</div>
<br /><br />
<hr />
</div>

<div class="Row">
<div id="middel_content">
<?php $this->load->view('admin/auctions_bk');?>
<!--<div class="caseRow marBot20">

<table width="100%" border="0" class="admin_comm_table">
  <tr>
    <td class="top" width="10%"><img src="<?php echo base_url()?>assetts/adminimages/t1.jpg" width="90" height="90" border="0" /></td>
    
    <td width="2%">&nbsp;</td>
    
     <td  width="63%" class="marLft10 top">
     <table>
  <tr>
    <td class="top" width="152">
    <p class="txt11pN">List# 0</p>
    <p class="txt12B marTop5">Auction Title</p>
    <p class="txt11pN marTop5">Seller : <a href="#" class="txt11pN">user name</a></p>
    
    <p class="txt11pN marTop5 marRlt10">
    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    </p>
    
    </td>
    
    <td class="top" width="71">
   <p class="txt12B grey_txt">BIDS</p>
   <p class="txt13B marTop5">5</p>
   <p class="txt11pN marTop5">QTY : 10</p>

    
    </td>
    
    <td class="top" width="140">
   <p class="txt12B grey_txt">Summery</p>
   <p class="txt13B marTop5">Item value : $100.00</p>
   <p class="txt11pN marTop5">Auction price : $0.05</p>
   <p class="txt11pN marTop5">&nbsp;</p>
   </td>
  </tr>
  
</table>

     </td>
     
      <td class="top" width="25%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="txtalignCenter" style="white-space:nowrap;">
     <h4>00:00:06</h4>
  
    <p class="txt11pN marTop5">Wiiner: Username</p>
    <p class="marTop5 txt11pN">Minimun bids remaining :20</p>
    
    </td>
    <td class="txtalignCenter mid">
    
    
    </td>
  </tr>
</table>

      
      </td>
  </tr>
  
  <tr><td class="mid" colspan="8">
  <div class="flt_lft">
  <?php if($this->session->userdata('level') != 3){?>
 <a href="javascript:void(0)"> <img src="<?php echo base_url()?>assetts/adminimages/btn_reverse_auction.png" width="114" height="22" border="0" align="absmiddle"   /></a>
  <?php }?>
  </div>
  
  <div class="flt_right txt11pB">Status :Active</div> 
  
  </td>
  
  
</table>


</div>-->
</div>
</div>

</div>
</div>
     
     </td>
    
  </tr>

</table>

</div>
</div>
</div>

<!--BID NOW AREA END-->
<?php $this->load->view('adminnav/nav_footer');?>