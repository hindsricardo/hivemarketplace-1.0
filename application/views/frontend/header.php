<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HIVE MARKETPLACE</title>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
<link rel="icon" href="/favicon.ico" type="image/x-icon"/>
<link href="<?php echo base_url();?>assetts/css/site.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assetts/css/slider.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assetts/css/message.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assetts/css/itemSlider.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>assetts/js/jquery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url();?>assetts/js/mocha.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url();?>assetts/js/jquery-1.9.1.min.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){	
	$(".divPayPal").hide();
	$("#showPaypal").mouseover(function() {
	$(".divPayPal").show();
	}).mouseout(function(){
	$(".divPayPal").hide();
	});	
	
	
	$('#user_section').hide();
	$('.hmp').hide();
	$('.hmp1').hide();
	$('.usrName').click(function(){
	   $('.hmp').hide(); 
	   $('.hmp1').hide(); 
	   $('#user_section').toggle('slow');
	});
	$('.flt_right .topRight li:first').click(function(){
	   $('.hmp1').hide(); 
	   $('#user_section').hide(); 
	   $('.hmp').toggle('slow');
	});
	
	
	$('.credit, #total_credit_available').click(function(){
	   $('.hmp').hide(); 
	   $('#user_section').hide(); 
	   $('.hmp1').toggle('slow');
	});	
	
	
	
	
	});	
</script>
<script type="text/javascript">
	function login_username_onblur(search_val){
		if(search_val == ''){
			$("#user_usernmae").val('User Name');
		}
	}
	function login_username_onfocus(search_val){
		if(search_val == 'User Name'){
			$("#user_usernmae").val('');
		}
	}
	function login_password_onblur(search_val){
		if(search_val == ''){
			$("#user_password").val('**********');
		}
	}
	function login_password_onfocus(search_val){
		if(search_val == '**********'){
			$("#user_password").val('');
		}
	}
	function check_user_auth(){
		var username = $.trim($("#user_usernmae").val());
		var password = $("#user_password").val();
		if(username == '' || username == 'User Name'){
			alert('Please enter Your Username');
			$("#user_usernmae").focus();
			return false;
		}
		if(password == '' || password == '**********'){
			alert('Please enter Your Password');
			$("#user_password").focus();
			return false;
		}
		$("#userlogin_form").submit();
	}
	function auction_search_onblur(search_val){
		if(search_val == ''){
			$("#search_str").val('Search , Live Auction, products and more');
		}
	}
	function auction_search_onfocus(search_val){
		if(search_val == 'Search , Live Auction, products and more'){
			$("#search_str").val('');
		}
	}
	function auction_search(){
		var search_str = $.trim($("#search_str").val());
		if(search_str == '' || search_str == 'Search , Live Auction, products and more'){
			alert('Please enter your search');
			$("#search_str").focus();
			return false;
		}else{
			$("#auction_search_form").submit();
		}
	}
	function forgot_password(){
		 var createdDiv='<div id="forgot_password_div" class="forgot_password_div" style="max-width:300px;" align="left">Enter Registered Email<br/><input type="text" id="forgot_email"><input type="button" value="Send Password" onclick="send_forgot_password()"><a class="close_forgot_password" style="cursor:pointer;" onclick="close_forgot_password()">Close</a></div>';
	$('.userLogInwrapper').append(createdDiv);
	}
	document.close_forgot_password = function(){
		$('#forgot_password_div').remove();
	}
	function send_forgot_password(){
		var forgot_email = $("#forgot_email").val();
		if(forgot_email != ''){
			$.post("<?php echo base_url();?>webadmin/users/send_forgot_password",{forgot_email: forgot_email}, function(data){
			var data = "This email is not in our database. Sorry please try again."
                        $('#forgot_password_div').remove();	
			var createdDiv='<div id="suss_pass" class="sucess_mail">'+data+'</div>';
			$('.userLogInwrapper').append(createdDiv);
			setTimeout(function(){
			$("#suss_pass").remove(); 									
			}, 2000);				
			});
		}else{
			alert("Please enter your email");
			$("#forgot_email").focus();
		}

	}
</script>
<script>
function hidehiw(){
    $('.hiwclose').click(function(){
        $('#hiw').hide();
    }
        );
}


</script>
<style>
#user_section, .hmp, .hmp1 {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000; 
	border-radius: 8px 8px 8px 8px;  
    margin-bottom: 7px;   
    padding: 7px !important;
    position: absolute;	
	z-index:1000px;	
		
}
.forgot_password_div {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000; 
	border-radius: 8px 8px 8px 8px;  
    margin-bottom: 15px;   
    padding: 15px !important;
    position: absolute;	
	z-index:1000px; 	
}
a.close_forgot_password {
    background: url("<?php echo base_url();?>assetts/images/fancy_closebox.png") no-repeat scroll 0 0 transparent;
    height: 30px;
    overflow: hidden;
    position: absolute;
    right: -10px;
    text-indent: -1000px;
    top: -10px;
    width: 30px;
}
</style>

</head>
<body>
    <div id="hiw"style='position:fixed; height:150px;width:20px;background:#ffcc00;border:1px solid #363636; border-top-right-radius:15px; border-bottom-right-radius:15px;box-shadow:1px 1px 1px #363636;bottom:40px; color:#363636;font-family:verdana;font-weight:bold;font-size: 16px;'><p style='position:relative;white-space:nowrap;writing-mode:tb-rl;bottom:-26px;-webkit-transform: rotate(90deg);'><a href="<?php echo base_url();?>about">How it works</a></p></div>
<div id="credit_val" style="display:none"><?php echo credit_val();?></div>
<div id="withdraw_credits_val" style="display:none"><?php echo withdraw_credits_val();?></div>
<div class="wrapper_center clearfix">
<div class="Row">
        
	<?php if($this->session->userdata('user_logged_in')){ ?>
	<div class="flt_lft usrName1"><span class="txt13B">Good Luck</span><a href="javascript:void(0);" class="usrName txt13B"><?php echo $this->session->userdata('fname')?></a> <!-- //insert after fname) to add users last name//.' '.$this->session->userdata('lname') -->
	
			<div id="user_section" style="display:none;">
				<ul style="list-style:none; padding-left: 10px;">
                                    <!-- The if statement in the li below sets default image if user has not uploaded photo -->
				<li> <?php if($this->session->userdata('photo') == NULL){ ?>
                                <img src="<?php echo base_url();?>assetts/images/icon_user.png" width="40" height="40" align="absmiddle"/>    
                                <?php } else{ ?>  <img src="<?php echo $this->session->userdata('photo')?> " width="40" height="40" align="absmiddle" style="border-radius:5px;"/><?php } ?>
                                        <?php echo $this->session->userdata('fname').' '.$this->session->userdata('lname') ?>
                                </li>
				<li><a href="<?php echo base_url();?>users/account_settings">Account Settings</a></li>
				<li><a href="<?php echo base_url();?>home/logout">Sign Out</a></li>           
				</ul>  
			</div>
			</div>
	<?php //echo '<pre>';print_r($this->session->all_userdata());?>
	<div class="flt_right">
		<ul class="topRight">
			<li><a style="cursor:pointer;">My HMP</a>
			
			
			
        <div class="hmp" style="max-width:138px; display:none;">
         <ul style="list-style:none; padding-left: 10px;">
           <li><a href="<?php echo base_url();?>users/activity">Summary</a></li>
           <li><a href="<?php echo base_url();?>users/activity/bids_and_offers">Bids/Offers</a></li>
           <li><a href="<?php echo base_url();?>users/activity/watchlist">Watchlist</a></li>   
           <li><a href="<?php echo base_url();?>users/activity/paid">Sell an item</a></li>   
           <!--<li><a href="">All Selling</a></li>-->
           <li><a href="<?php echo base_url();?>users/activity/recent_search">Recent Searches</a></li>   
           <li><a href="<?php echo base_url();?>users/messages">Messages(<?php echo user_unread_message($this->session->userdata('user_id'));?>)</a></li>        
          </ul>  
        </div> 
			
			
			
			</li>
			<?php $available_credit = user_available_credit();?>
			<li><a style="cursor:pointer;"><div style="float:left" class="credit">Bids:</div><div id="total_credit_available" style="display:block; float:left;"><?php echo $available_credit['total_credit_available'];?></div></a>
			<div id="hive_credit_available" style="display:none"><?php echo $available_credit['hive_credit_available'];?></div>
			<div id="bonus_credit_available" style="display:none"><?php echo $available_credit['bonus_credit_available'];?></div>
			
			
        <div class="hmp1" style="max-width:142px; top:28px; display:none;">
         <ul style="list-style:none; padding-left: 10px;">
           <li><a href="<?php echo base_url();?>users/manage_credits">Manage Bids</a></li>
           <li><a href="<?php echo base_url();?>users/buy_credit">Buy Bids</a></li>                  
          </ul>  
        </div> 
			
			
			
			
			</li>
			<li><div style="float:left">Value:$</div><div id="total_credit_value_available" style="float:left"><?php echo $available_credit['total_credit_available']*$this->config->item("credit_val")?></div></li>
			<li style="margin-right:3px; margin-top:-4px;"><a href="<?php echo base_url();?>checkout"><img src="<?php echo base_url();?>assetts/images/icon_cart.png" width="26" height="21" border="0" /></a></li>
			<li style="margin-right:0px; margin-top:-1px;">Cart</li>
			<li style="margin-right:0px; margin-top:-6px;" class="cart"><?php echo winnerCart();?></li>
		</ul>
	</div>
	<?php }else{?>
	<div class="topRow"><h2>New to the market place? <a href="<?php echo base_url();?>registration" class="joinNow">Join Now</a></h2></div>
	<?php } ?>
</div>
<div class="logoWrapper"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assetts/images/deerbeelogo.png" alt="HIVE" width="523" height="189" border="0" /></a></div>
<!--menu + uesr log in -->
<div class="Row clearfix">
	<div class="menuWrapper clearfix">
		<ul class="menu">
			<li><a href="<?php echo base_url();?>" class="highlight"><span>Auctions</span></a></li>
			<li><a href="<?php echo base_url();?>users/buy_credit"><span>Buy Bids</span></a></li>
			<li><a href="<?php echo base_url();?>users/sell"><span>Sell</span></a></li>
			<li><a href="<?php echo base_url();?>faqs"><span>FAQ’s</span></a></li>
			<li><a href="<?php echo base_url();?>users/activity"><span>Activity</span></a></li>
		</ul>
	</div>
	<?php //echo '<pre>';print_r($this->uri->segment(1));?>
	<div class="userLogInwrapper">
	<?php if($this->session->userdata('user_logged_in')){ ?>
		<!--<a style="float:right;" href="<?php echo base_url();?>home/logout"><img src="<?php echo base_url();?>assetts/images/btn_logOut.png" alt="" /></a>-->
	<?php }else{?>
	<a id="forgot_password" href="javascript:void(0);" onclick="forgot_password();">Forgot Password</a>
		<form name="userlogin_form" id="userlogin_form" class="userlogin_form" action="<?php echo base_url()?>home/authentication" method="post">
			<div class="userInpDiv"><input name="username" id="user_usernmae" type="text" placeholder='User Name' class="inp" onblur="login_username_onblur(this.value)" onfocus="login_username_onfocus(this.value)" /></div>
			<div class="userInpDiv"><input name="password" id="user_password" type="password" value="**********" class="inp" onblur="login_password_onblur(this.value)" onfocus="login_password_onfocus(this.value)"/></div>
			<div><input name="" value="" type="button" class="btn_go" onclick="return check_user_auth();" /></div>
		</form>
	<?php }?>
	</div>
</div>
<!--menu + uesr log in -->
<!--search areaa -->
<div class="search_bg Row" style='border-radius:5px;'>
	<div class="searchWrap">
	<form name="auction_search_form" id="auction_search_form" action="<?php echo base_url();?>home/search" method="post" onsubmit="return auction_search()">
		<div class="searchInpWrap">
			
			<input name="search_str" id="search_str" placeholder="Search by auction title, list #, or tags" <?php if(isset($search_str)){ ?> value="<?php echo $search_str;?>" <?php }?> type="text"  class="searchInp" />
		</div>
		<div class="flt_right">
			<input name="auction_search_submit" type="submit" class="btn_search" value="" />
		</div>
	</form>
	</div>
</div>
<!--search areaa -->