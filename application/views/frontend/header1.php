<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HIVE MARKETPLACE</title>
<link href="<?php echo base_url();?>assetts/css/site.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assetts/css/slider.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assetts/css/message.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assetts/css/itemSlider.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url();?>assetts/js/jquery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url();?>assetts/js/mocha.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url();?>assetts/js/jquery-1.9.1.min.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url();?>assetts/js/easySlider1.7.js" language="javascript" type="text/javascript"></script>



<script type="text/javascript">
	$(document).ready(function(){	
		$("#slider").easySlider({
			auto: true, 
			continuous: true
		});
	});	
</script>
<script type="text/javascript">
	$(document).ready(function(){	
		
	$("#filter").hide();
	$("#sort").hide();
	
	$("#clickFilter").click(function()
	
	{
	$("#sort").hide();
	$("#filter").slideToggle("slow");
	
	})
		
	
	
	$("#filter_go").click(function()
	
	{
		
	$("#filter").slideUp("slow");
	
	})
	
		
	$("#clickSort").click(function()
	
	{
	$("#filter").hide();
	$("#sort").slideToggle("slow");
	
	})
		
	});	
</script>

<script type="text/javascript">
	$(document).ready(function(){	
		
	$(".divPayPal").hide();
	
	$("#showPaypal").mouseover(function() {
	
	$(".divPayPal").show();
	
	}).mouseout(function(){
	$(".divPayPal").hide();
	
	});	
			
	});	
</script>
<script type="text/javascript">

	function login_username_onblur(search_val){
		if(search_val == ''){
			$("#user_usernmae").val('User Name');
		}
	}
	function login_username_onfocus(search_val){
		if(search_val == 'User Name'){
			$("#user_usernmae").val('');
		}
	}
	
	function login_password_onblur(search_val){
		if(search_val == ''){
			$("#user_password").val('**********');
		}
	}
	function login_password_onfocus(search_val){
		if(search_val == '**********'){
			$("#user_password").val('');
		}
	}

	function check_user_auth(){
		var username = $.trim($("#user_usernmae").val());
		var password = $("#user_password").val();
		if(username == '' || username == 'User Name'){
			alert('Please enter Your Username');
			$("#user_usernmae").focus();
			return false;
		}
		if(password == '' || password == '**********'){
			alert('Please enter Your Password');
			$("#user_password").focus();
			return false;
		}
		$("#userlogin_form").submit();
	}
</script>
</head>
<body>
<div id="creadit_val" style="display:none"><?php echo credit_val();?></div>
<div id="withdraw_credits_val" style="display:none"><?php echo withdraw_credits_val();?></div>

<div class="wrapper_center clearfix">
<div class="Row">
	<?php if($this->session->userdata('user_logged_in')){ ?>
	<div class="flt_lft usrName"><span class="txt13B">Happy bidding</span><a href="javascript:void(0);" class="usrName txt13B"><?php echo $this->session->userdata('fname').' '.$this->session->userdata('lname') ?></a></div>
	<div class="flt_right">
		<ul class="topRight">
			<li><a href="<?php echo base_url();?>">My HMP</a></li>
			<li>Credits:1000</li>
			<li>Value:$650.00</li>
			<li style="margin-right:3px; margin-top:-4px;"><img src="<?php echo base_url();?>assetts/images/icon_cart.png" width="26" height="21" border="0" /></li>
			<li style="margin-right:0px; margin-top:-1px;">Cart</li>
			<li style="margin-right:0px; margin-top:-6px;" class="cart">2</li>
		</ul>
	</div>
	<?php }else{?>
	<div class="topRow"><h2>New to the market place? <a href="#" class="joinNow">Join Now</a></h2></div>
	<?php } ?>
</div>

<div class="logoWrapper"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assetts/images/logo.png" alt="HIVE" width="593" height="79" border="0" /></a></div>

<!--menu + uesr log in -->
<div class="Row clearfix">
	<div class="menuWrapper clearfix">
		<ul class="menu">
			<li><a href="<?php echo base_url();?>" <?php if($this->uri->segment(1) == ''){ echo 'class="highlight"';}?>><span>Auction</span></a></li>
			<li><a href="#"><span>Buy Credits</span></a></li>
			<li><a href="<?php echo base_url();?>users/sell"<?php if($this->uri->segment(2) == 'sell'){ echo 'class="highlight"';}?>><span>Sell</span></a></li>
			<li><a href="<?php echo base_url();?>faqs" <?php if($this->uri->segment(1) == 'faqs'){ echo 'class="highlight"';} ?>><span>FAQ’s</span></a></li>
			<li><a href="<?php echo base_url();?>users/account_settings"<?php if($this->uri->segment(2) == 'account_settings'){ echo 'class="highlight"';}?>><span>Accounts</span></a></li>
		</ul>
	</div>
	<?php //echo '<pre>';print_r($this->uri->segment(1));?>
	<div class="userLogInwrapper">
	<?php if($this->session->userdata('user_logged_in')){ ?>
		<a style="float:right;" href="<?php echo base_url();?>home/logout"><img src="<?php echo base_url();?>assetts/images/btn_logOut.png" alt="" /></a>
	<?php }else{?>
		<form name="userlogin_form" id="userlogin_form" class="userlogin_form" action="<?php echo base_url()?><?php base_url();?>home/authentication" method="post">
			<div class="userInpDiv"><input name="username" id="user_usernmae" value="User Name" type="text"  class="inp" onblur="login_username_onblur(this.value)" onfocus="login_username_onfocus(this.value)"/></div>
			<div class="userInpDiv"><input name="password" id="user_password" type="password" value="**********" class="inp" onblur="login_password_onblur(this.value)" onfocus="login_password_onfocus(this.value)"/></div>
			<div><input name="" value="" type="button" class="btn_go" onclick="return check_user_auth();" /></div>
		</form>
	<?php }?>
	</div>
</div>
<!--menu + uesr log in -->



<!--search areaa -->
<div class="search_bg Row">
	<div class="searchWrap">
		<div class="searchInpWrap">
			<input name="" onblur="this.value='Search , Live Auction, products and more'" onfocus="this.value=''" value="Search , Live Auction, products and more" type="text"  class="searchInp"/>
		</div>
		<div class="flt_right">
			<input name="" value="" type="submit" class="btn_search" />
		</div>
	</div>
</div>
<!--search areaa -->