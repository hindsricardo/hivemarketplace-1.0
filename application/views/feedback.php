<?php $this->load->helper('url'); ?>
<?php $this->load->view('frontend/header');?>
<?php $this->load->view('frontend/slider');?>
<script language="javascript">
$(document).ready(function(){

	$('.btn_reset').click(function(){	
	$('input[name=response]').val('');
	$('input:checkbox[name="positive"]').attr('checked',false);
	$('input:checkbox[name="negative"]').attr('checked',false);
	$('#feedback').val('');
	});
	
	$('.btn_submit').click(function(){	
	if($('input[name=response]').val()==''){
	alert('Please select your reasion');	
	return false;
	}
	if($('#feedback').val()==''){
	alert('Feedback boby is blank');
	$('#desc').focus();
	return false;
	
	}
	});
	
	$('input:checkbox[name="positive"]').click(function(){
	if($(this).is(':checked')){
		$('input[name=response]').val('positive');
		$('input:checkbox[name="negative"]').attr('checked',false);
	}
	});
	
	$('input:checkbox[name="negative"]').click(function(){
	if($(this).is(':checked')){
		$('input[name=response]').val('negative');
		$('input:checkbox[name="positive"]').attr('checked',false);
	}
	});
	
	var rate_desc=0;
	var rate_speed=0;
	var rate_comm=0;	
	document.rate_desc = function(id){	
		$('input[name=desc_rate]').val(id);
		rate_desc = $('input[name=desc_rate]').val();
		for(var i=1;i<=5;i++){
			$('#desc'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=id;i++){
			$('#desc'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		var total_rate= parseInt(rate_desc)+parseInt(rate_speed)+parseInt(rate_comm);
		var avg_rate = total_rate/3;
		for(var i=1;i<=5;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=avg_rate;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		
		if(total_rate%3 > 0){
			var star=parseInt(avg_rate)+1;			
			$('#avg_rate'+star).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_half.png');
		}
	
	}


	document.rate_speed = function(id){	
		$('input[name=speed_rate]').val(id);
		rate_speed = $('input[name=speed_rate]').val();
		for(var i=1;i<=5;i++){
			$('#speed'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=id;i++){
			$('#speed'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		var total_rate= parseInt(rate_desc)+parseInt(rate_speed)+parseInt(rate_comm);
		var avg_rate = total_rate/3;
		for(var i=1;i<=5;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=avg_rate;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		if(total_rate%3 > 0){
		var star=parseInt(avg_rate)+1;		
			$('#avg_rate'+star).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_half.png');
		}
	}


   document.rate_comm = function(id){	
		$('input[name=comm_rate]').val(id);
		rate_comm = $('input[name=comm_rate]').val();
		for(var i=1;i<=5;i++){
			$('#comm'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=id;i++){
			$('#comm'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		var total_rate= parseInt(rate_desc)+parseInt(rate_speed)+parseInt(rate_comm);
		var avg_rate = total_rate/3;
		for(var i=1;i<=5;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_blank.png');
		}
		for(var i=1;i<=avg_rate;i++){
			$('#avg_rate'+i).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_full.png');
		}
		if(total_rate%3 > 0){
			var star=parseInt(avg_rate)+1;		
			$('#avg_rate'+star).find('img').attr('src','<?php echo base_url();?>assetts/images/rate_half.png');
		}
	}	
});
</script>
<div class="Row marTop10 marBot10">
<div class="Row flt_lft">
<p class="auctionTitle_b">Provide Seller Feedback</p>
</div>
<div class="cls"><br clear="all" /></div>
<div class="greyRow">
<p class="txt13B marLft10">Seller : User name <span class="marLft10">List #<?php echo $auction_id;?></span></p>
</div>
<div class="cls"><br clear="all" /></div>

<div class="div750Center">
<div class="ratingsBox clearfix Row">
<div class="flt_lft Row">
<table class="comm_table">
  <tr>
    <td width="60%" class="mid txt12B">How well did product match seller description</td>
    <td width="40%" class="mid txt12B"><?php for($i=1; $i<=5; $i++){ ?>
	<a id="desc<?php echo $i;?>" style="cursor:pointer;" onclick="rate_desc(<?php echo $i;?>)"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle" title="rate <?php echo $i;?>"/></a>
    <?php }?>
   </td>
    
  </tr>  
  <tr><td class="mid txt12B" colspan="2">&nbsp;</td></tr>  
  <tr>
    <td width="60%" class="mid txt12B">How would you rate the speed of delivery?</td>
    <td width="40%" class="mid txt12B">
    <?php for($i=1; $i<=5; $i++){ ?>
    <a id="speed<?php echo $i;?>" style="cursor:pointer;" onclick="rate_speed(<?php echo $i;?>)"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle" title="rate <?php echo $i;?>"/></a>
    <?php }?>
    </td>
    
  </tr>  
  <tr><td class="mid txt12B" colspan="2">&nbsp;</td></tr>  
  <tr>
    <td width="60%" class="mid txt12B">How was the seller's communication</td>
    <td width="40%" class="mid txt12B">
    <?php for($i=1; $i<=5; $i++){ ?>
    <a id="comm<?php echo $i;?>" style="cursor:pointer;" onclick="rate_comm(<?php echo $i;?>)"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle" title="rate <?php echo $i;?>"/></a>
    <?php }?>
    </td>
    
  </tr>  
  <tr><td class="mid txt12B" colspan="2"><hr /></td></tr>  
  <tr>
    <td width="60%" class="mid txt12B">As a result of your ratings (Overall Rating:)</td>
    <td width="40%" class="mid txt12B"> 
    <?php for($i=1; $i<=5; $i++){ ?>
    <a id="avg_rate<?php echo $i;?>" style="cursor:pointer;"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
    <?php }?>
    
    </td>    
  </tr>  
</table>

</div>
</div>
<div class="cls"><br clear="all" /></div>

<div class="Row flt_lft">
<p class="support_notes marLft10">Comments</p>

<form action="feedback/add" method="post" class="marTop10" >
<input type="hidden" name="seller_id" value="<?php echo $seller_id;?>" />
<input type="hidden" name="auction_id" value="<?php echo $auction_id;?>" />
<input type="hidden" name="desc_rate" value="" />
<input type="hidden" name="speed_rate" value="" />
<input type="hidden" name="comm_rate" value="" />
<input type="hidden" name="response" value="" />

<div class="flt_lft marLft10">
<input name="positive" type="checkbox" value="" /> <span class="txt12B">Postive</span>
<input name="negative" type="checkbox" value="" /> <span class="txt12B">Negative</span>
</div>

<textarea name="feedback" id="feedback" cols="88" class="comm_txtArea" rows="15"></textarea>
<div class="flt_lft clearfix" style="margin-left:250px; margin-top:10px;">
    <input name="" style="float:left;" type="submit" value="" class="btn_submit" />     
    <input name="" type="button" value="" style="float:left; margin-left:10px;" class="btn_reset" />
</div>
</form>


<div class="cls"><br clear="all" /></div>

</div></div>
</div>

<? $this->load->view('frontend/footer');?>