<?php $this->load->helper('url'); ?>
<?php $this->load->view('frontend/header');?>
<?php $this->load->view('frontend/slider');?>
<script language="javascript">
/*function clear_cart() {
	var result = confirm('Are you sure want to clear all bookings?');
	
	if(result) {
		window.location = "<?php echo base_url(); ?>checkout/remove/all";
	}else{
		return false; // cancel button
	}
}*/
document.magbox_open = function(id){
$('#msgbox_'+id).show();
}
document.send_msg = function(id){
var usar_id = $('input[name=user_id]').val();
var auction_id = $('input[name=auction_id]').val();
var subject = $('input[name=subject_'+id+']').val();
var msg = $('#message_'+id).val();
$.post("<?php echo base_url();?>checkout/sendMail",{ auction_usar_id: usar_id, auction_id:auction_id, subject: subject, msg: msg, type:'cart' }, function(response){
               $('.greyRow_full').hide();
				alert(response);
				});
//$('#send_'+id).submit();
}
$(document).ready(function(){

	$('.btn_cancel').click(function(){
	$('.greyRow_full').hide();
	});
	
});
function add_ship(rowid, amount){
	$("#shipping_value_"+rowid).text(amount);
	var total = parseFloat($("#total").text());
	total = parseFloat(total)+parseFloat(amount);
	$("#total").text(total);
	var pick_up	= parseFloat($("#pick_up").text());
	pick_up = parseFloat(pick_up)+parseFloat(amount);
	$("#pick_up").text(pick_up);
	var delivery_type = 'ship';
	$.post("<?php echo base_url();?>checkout/update_shipping",{ rowid: rowid, amount:amount, delivery_type: delivery_type}, function(response){
		//alert(response);
	});
}
function add_pick(rowid, amount){
	var shipping_value = parseFloat($("#shipping_value_"+rowid).text());
	$("#shipping_value_"+rowid).text(amount);
	var total = parseFloat($("#total").text());
	total = parseFloat(total)-parseFloat(shipping_value);
	$("#total").text(total);
	var pick_up	= parseFloat($("#pick_up").text());
	pick_up = parseFloat(pick_up)-parseFloat(shipping_value);
	$("#pick_up").text(pick_up);
	var delivery_type = 'pick';
	$.post("<?php echo base_url();?>checkout/update_shipping",{ rowid: rowid, amount:amount, delivery_type:delivery_type}, function(response){
		//alert(response);
	});
}
</script>
<div class="Row borderbox">
	<div class="Row98">
	<h2 class="txt16pB marTop10">Checkout : Review Order</h2>

	<br clear="all" />
		<div class="Row95 borderbox_mid clearfix pad10">
			<div class="Row">
                       
                <hr />        
                <br clear="all" />
               
                <p class="txt12B">Note: if you select In-store/In-person pick up the item will be removed from your checkout. You will need to coordinate with the seller for payment and pickup.</p>
                
                <br />
                <div class="faq_comments_wrap clearfix marTop10">
                <p class="txt12B">Tip: The quicker you pay, The quicker you receive your items</p>
             </div>        
             <hr />
          </div>
<!--LOOP STARTS  -->
<div style="color:#F00"><?php //echo $message?></div>
<?php if ($cart = $this->cart->contents()): ?>
<?php 
//echo '<pre>';print_r($cart);
$grand_total = 0; $shipping_total = 0; $i = 1;
foreach ($cart as $item):?>
<div class="Row marTop10">
	<div class="flt_lft txt11pN marRlt10">Seller : <a href="#" class="txt11pN"><?php echo get_user_username($item['user_id'])?></a></div>
		<div class="flt_lft txt11pN marRlt10" onclick="magbox_open(<?php echo $item['id']?>);"><img src="<?php echo base_url();?>assetts/images/icon_mail_sm.png" width="25" height="25" border="0" align="absmiddle"   />
Message Seller
		</div>
		<div class="flt_right"><a href="<?php echo base_url();?>users/buy_credit/paypal_single_item/<?php echo $item['rowid']?>"><img src="<?php echo base_url();?>assetts/images/btn_payOnly.png" width="130" height="22" border="0" /></a></div>
		<br clear="all" /><br clear="all" />
		<div class="Row">
			<div class="greyRow_full" id="msgbox_<?php echo $item['id'];?>" style="display:none;">
			
            <input type="hidden" name="user_id" value="<?php echo $item['user_id'];?>" />
            <input type="hidden" name="auction_id" value="<?php echo $item['id'];?>" />
                <table width="100%" cellpadding="8">
                  <tr>
                    <td colspan="2" valign="middle"><input name="subject_<?php echo $item['id'];?>" type="text" class="comm_inp" value="<?php echo $item['name']." # ".$item['id']; ?>" style="width:870px; height:25px;border-radius:10px;padding:5px; background:#F7F7F8;" readonly="readonly" /></td>
                  </tr>
                  
                    <tr>
                    <td colspan="2" valign="top"><textarea name="message_<?php echo $item['id'];?>" id="message_<?php echo $item['id'];?>" class="comm_txtArea" style="width:870px;max-width:870px; height:55px; border-radius:10px;padding:10px; background:#F7F7F8;" >Message goes here...</textarea></td>
                  </tr>  
                   <tr>
                    <td colspan="2" align="left" valign="top">
                        <table width="50%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="29%" align="left" valign="top"><a href="javascript:void(0);" class="btn_send_msg" onclick="send_msg(<?php echo $item['id'];?>);"></a></td>
                            <td width="71%" align="left" valign="top">  <a href="javascript:void(0);" class="btn_cancel"></a></td>
                          </tr>
                        </table>
                    </td>
                  </tr>  
                </table>
			</div>
			<br clear="all" />
			<div class="div850Center borderbox_mid">
			<table width="100%" class="comm_table">
  <tr>
    <td width="10%" class="mid"><!--<img src="<?php echo base_url();?>assetts/images/userPi-cs.jpg" width="100" border="0" />-->
    <?php if($item['picture']=='HIVEcomb.png'){
		echo '<img src="'.base_url().'assetts/images/'.$item['picture'].'" width="90" height="90" border="0" />';
	}else if($item['picture']=='userPi-cs.jpg'){
		echo '<img src="'.base_url().'assetts/images/'.$item['picture'].'" width="90" height="90" border="0" />';
	}else{
		echo '<img src="'.base_url().'upload/'.$item['picture'].'" width="120"  border="0" />';
	}
	?>
    <!--<img src="<?php echo base_url();?>upload/<?php echo $item['picture']?>" width="120"  border="0" />-->
	</td>
    <td width="90%" class="top">
    <table width="100%" class="comm_table">
  <tr>
    <td width="54%">
    <p class="txt11pN">List# <?php echo $item['id']; ?></p>
    
    <p class="txt12B marTop10"><?php echo $item['name']; ?><!--Auction Title--></p>
    <?php if($item['tags']!=''){ echo '<p class="txt11pN marTop10">Tags: '.$item['tags'].'</p>'; }?>
    
    </td>
    
    
    <td width="35%">
    
    <table width="100%" class="comm_table">
  <tr>
    <td class="top"><p class="txt11pN">
    <?php if($item['credit']!=''){
	 echo 'Credit : '.$item['credit'];
	}else{
	echo 'Quantity : '.$item['qty'];
	}
	?>
   <!-- Quantity : <?php echo $item['qty']; ?>-->
    
    </p>
    <p class="txt11pN marTop10" id="domestic_shipping_<?php echo $item['rowid']?>">Shipping</p></td>
  </tr>
  
  <tr>
    <td class="top">
		<?php if($item['credit']==''){?>
		<table width="100%" class="comm_table">
			<tr>
				<?php if($item['offer_shipping'] == '1'){ ?>
					<td width="14%" class="mid"><input id="ship_<?php echo $item['rowid'];?>" name="ship_<?php echo $item['id'];?>" type="radio" onclick="add_ship('<?php echo $item['rowid'];?>','<?php echo item_shipping_value($item['id']);?>')" <?php echo($item['offer_shipping'] == 1?'checked="checked"':'')?> value="yes" /></td>
					<td width="19%" class="mid txt12B">Ship</td>
				<?php }else{ ?>
						<td width="14%" class="mid"></td>
						<td width="19%" class="mid txt12B"></td>
				<?php }?>
				<?php if($item['payat_pickup'] == '1'){ ?>
					<td width="15%" class="mid"><input id="pick_<?php echo $item['rowid'];?>" name="ship_<?php echo $item['id'];?>" type="radio" onclick="add_pick('<?php echo $item['rowid'];?>','0')" <?php if($item['offer_shipping'] == 0 && $item['payat_pickup'] == 1) echo 'checked="checked"';?> value="no" /></td>
					<td width="52%" class="mid txt12B">In-store/In-person Pickup and Payment</td>
				<?php }else{ ?>
						<td width="15%" class="mid"></td>
						<td width="52%" class="mid txt12B"></td>
				<?php }?>
			</tr>
		</table>
		<?php } ?>
    </td>
  </tr>
</table>
    <?php $grand_total = $grand_total + number_format($item['price'],2); 
	      $shipping_total = $shipping_total + number_format($item['shipping'],2);?>
    </td>
    <td width="11%" align="right" >
    <p class="txt12B"><!--1.67--><?php echo number_format($item['price'],2); ?></p>
    
    <p class="txt11pN marTop10" id="shipping_value_<?php echo $item['rowid']?>"><!--5.00--><?php echo number_format($item['shipping'],2); ?></p>
    </td>
  </tr>
</table>
    </td>
  </tr>
</table>
			</div>
		</div>
		<br clear="all" />
		<hr />
	</div>
<?php endforeach; ?>
<!--<input type="button" value="Clear Cart" onclick="clear_cart()" >&nbsp;&nbsp; 
<input type="button" value="Continue Shopping" onclick="window.location='users/activity'" />-->
<!--LOOP ENDS -->
<div class="Row">
<div class="flt_right" style="margin-right:22px; width:300px;">
  <table width="100%" class="comm_table" style='font-size:14px;'>
      <tr>
        <td colspan="2"><hr /></td>
      </tr>
      <tr>
        <td class="mid">Sub Total (<?php echo $msg_head?> Auctions)</td>
        <td>$<?php echo number_format($grand_total,2); ?></td>
      </tr>  
      <tr>
        <td colspan="2" />&nbsp;</td>
      </tr>  
      <tr>
        <td class="mid">Shipping Total</td>
        <td id="pick_up"><?php echo number_format($shipping_total,2); ?></td>
      </tr>  
      <tr>
        <td colspan="2"><hr /></td>
      </tr>  
      <tr>
        <td class="mid">Total</td>
        <td id="total"><?php echo number_format($grand_total,2)+number_format($shipping_total,2);?></td>
      </tr>  
      <tr>
        <td colspan="2" />&nbsp;</td>
      </tr>  
      <tr>
        <td colspan="2" class="mid txt11pN">Continue to PayPal to review payment Option</td>
      </tr>  
      <tr>
        <td colspan="2" />&nbsp;</td>
      </tr>  
      <tr>
        <td align="left" colspan="2">
        <form name="buycredit"  id="buycredit" action="<?php echo base_url();?>users/buy_credit/paypal" method="post">
        <?php ?>
       
        <input name="" type="submit" value="" class="btn_continue" />
        </form>
        </td>
      </tr>  
  </table>
</div>
</div>
<?php endif; ?>
</div>
</div>


<div class="cls"><br clear="all" /></div>
</div>
<? $this->load->view('frontend/footer');?>