<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HIVE MARKETPLACE</title>
<link href="<?php echo base_url();?>assetts/css/site.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assetts/css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>assetts/js/jquery.js"></script>
</head>
<body>
<div class="wrapper_center clearfix">
<div class="Row">
<!--<div class="topRow"><h2>New to the market place? <a href="#" class="joinNow">Join Now</a></h2>
</div>-->
<div class="logoWrapper"><img src="<?php echo base_url();?>assetts/images/logo.png" alt="HIVE" width="593" height="79" border="0" /></div>
<!--LIVE AUCTION AREA -->
<div class="flt_lft Row liveAunctionWrap">
<div class="top">
<div class="flt_lft">
<h2>Log In</h2>
</div>
</div>
</div>
<!--BID NOW AREA SRT-->
<div class="Row bid_now">
<div class="log-in">
<table width="496" border="0"  cellspacing="0" cellpadding="0">
<tr><td class="log-in_box_top"></td></tr>
<tr><td class="log-in_box_mid">
<?php if(isset($area) && $area=="loginarea"){?>
<div id="showlogin">
<form name="loginform" id="loginform" action="<?php echo base_url();?>admin/authentication" method="post">
<table width="70%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="2" class="log-in-text"><img src="<?php echo base_url();?>assetts/images/admin_img.png" align="absmiddle" width="32" height="32" alt="" />&nbsp;LOG IN: Step 2</td>
  </tr>
  <?php if(isset($error)){ ?>
  <tr>
    <td colspan="2" align="center"><span class="error"><?php if(isset($error)) echo $error; ?></span></td>
  </tr>     
  <tr>
 <td colspan="2" style="height:10px;"></td>
  </tr>
  <?php } ?>
  <tr>
   <td width="24%" class="welcome_admin_text">User Name</td>
    <td width="39%"><input name="username" type="text" value="USERNAME" onfocus="if(this.value=='USERNAME') this.value='';" onblur="if(this.value=='') this.value='USERNAME';" class="welcome_admin_input" autocomplete="off" /></td>
  </tr>
   <tr>
 <td colspan="2" style="height:10px;"></td>
  </tr>
   <tr>
 <td width="24%" class="welcome_admin_text">Password</td>
    <td width="39%"><input name="password" type="password" value="PASSWORD" onfocus="if(this.value=='PASSWORD') this.value='';" onblur="if(this.value=='') this.value='PASSWORD';" class="welcome_admin_input" /></td>
  </tr>
    <tr>
 <td colspan="2" style="height:10px;">&nbsp;</td>
  </tr>
   <tr>
 <td colspan="2" align="center"><input id="submitlogin" type="submit" class="log-in_bttm" style="text-indent:-9999px" /></td>
  </tr>
    <tr>
 <td colspan="2">&nbsp;</td>
  </tr>
</table>
</form>
</div>
<?php }else if(isset($area) && $area=="passcodearea"){ ?>
<div id="showpasscode">
<form name="passcodeform" id="passcodeform" action="<?php echo base_url();?>admin/passcode" method="post"> 
 <table width="70%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td colspan="2" align="center"><span class="success"><?php if(isset($error)) echo $error; ?></span></td>    
  </tr>
    <tr>
 <td colspan="2" style="height:10px;"></td>
  </tr>
  <tr>
 <td width="24%" class="welcome_admin_text">Enter Passcode</td>
    <td width="39%"><input name="passcode" type="password" value=""  class="welcome_admin_input" /></td>
  </tr>
  <tr>
 <td colspan="2" style="height:10px;">&nbsp;</td>
  </tr>
  <tr>
 <td colspan="2" align="center"><input id="submitcode" type="submit" class="log-in_bttm" style="text-indent:-9999px"/></td>
  </tr>
  </table>
  </form>
  </div>
<?php } ?>
</td></tr>
<tr><td class="log-in_box_bot"></td></tr> 
</table>
</div>
</div>
<!--BID NOW AREA END-->
<!--FOOTER SRT-->
<div class="Row" id="footer">          
<p>HIVE MarketPlace LLC. All rights reserved
</p>        
</div>
<!--FOOTER END-->
</div>
</div><!--end of wrapper center -->
</body>
</html>