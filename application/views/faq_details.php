<?php $this->load->view('frontend/header');?>
<script type="text/javascript">
function answer_div(){
	$("#answer_form_section").css("display","block");
}
function unanswered_answred_click(question_id){
	var html_value = ''
	html_value +='<table class="comm_table" >';
	html_value +='	<tr>';
	html_value +='		<td class="vert">';
	html_value +='			<textarea name="unanswered_answered_value" id="unanswered_answered_value_'+question_id+'" style="width:255px; height:70px;"  class="comm_txtArea"></textarea>';
	html_value +='		</td>';
	html_value +='	</tr>';
	html_value +='	<tr>';
	html_value +='		<td class="vert marLft10">';
	//html_value +='			<input name="unanswered_question_id" type="text" value="'+question_id+'" />';
	html_value +='			<input type="button" class="btn_submit"  value="" onclick="unanswered_answered_submit('+question_id+')" />';
	html_value +='		</td>';
	html_value +='	</tr>';
	html_value +='</table>';
	$("#unanswred_answered_id_"+question_id).html(html_value);
}
function unanswered_answered_submit(questions_id){
	var unanswered_answer = $("#unanswered_answered_value_"+questions_id).val();
	if(unanswered_answer != ''){
		$.post("<?php echo base_url()?>faqs/unanswered_answered_submit", { unanswered_answer : unanswered_answer, questions_id : questions_id },function(data) {
			alert('your question has been submitted');
			$("#unanswered_sections").html(data);
		});
	}else{
		alert('Please enter your Answer');
		$("#unanswered_answered_value_"+questions_id).focus();
		return false;
	}
}
function pagination(page,limit){
	$.post("<?php echo base_url()?>faqs/unanswred_pagination", { page : page, limit : limit },function(data) {
		$("#unanswered_sections").html(data);
	});
}
function rate_mouseover(rate_val, answer_id){
	//alert(rate_val+'>>>>>>>>>'+answer_id);
}
function rate_it(rate_val, answer_id){
	$.post("<?php echo base_url()?>faqs/rate_it", { rate_val : rate_val, answer_id : answer_id },function(data) {
		$("#rate_summary_"+answer_id).html(data);
		$("#rate_answer_"+answer_id).html('');
	});
}
</script>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">

<div class="Row98 faq_wrap clearfix">

<div class="faq_left">

<div class="flt_lft txt11pN mar10">Welcome <a href="javascript:void(0)" class="txt12B"><?php echo $this->session->userdata('username')?></a></div>
<div style="float:right;margin-top: 10px;"><a href="<?php echo base_url()?>faqs"><img src="<?php echo base_url();?>assetts/images/btn_back_search_result.png" /></a></div>
<br clear="all" />
<h2>Answer Center</h2>

<div class="Row">

<div class="faq_info_box">
<p class="faq26w">Question</p>
<p><?php echo $faq_details['questions']?></p>
</div>

<div class="Row marTop10">

<div class="flt_lft marTop10"><p class="txt11pN">Answers by highest rated</p></div>
<div class="flt_right marTop10">

<p class="flt_lft txt11pN marLft10"><a href="javascript:void(0)" class="txt11pB_or">Answers</a><?php echo '('.count($faq_details['answer_details']).')';?>&nbsp;</p>
				<?php if($faq_details['user_id'] != $this->session->userdata('user_id')){?>
				<p class="flt_lft marTop10 txt11pN" style="margin-top:-4px; margin-right:15px;"><?php if($this->session->userdata('user_logged_in')){?><a href="javascript:void(0)" onclick="answer_div()"><img src="<?php echo base_url();?>assetts/images/btn_post_an_answers.png"   border="0" /></a><?php }?></p>
				<?php }?>

</div>
<br clear="all" /><br clear="all" />
<div id="answer_form_section" style="display:none;">
	<form id="answer_form" name="answer_form"  action="<?php echo base_url()?>faqs/answer_submit" method="post">
		<input type="hidden" name="questions_id" id="questions_id" value="<?php echo $faq_details['id']?>" />
		<textarea name="answers" style="width:652px; height:110px;" class="comm_sel"></textarea>
		<input type="submit" value="" class="btn_submit" style="margin-top:10px;" />
	</form>
</div>

<br clear="all" />
<hr />
</div>
	<?php if(is_array($faq_details['answer_details'])){ ?>
		<?php $answer_count = 1;?>
		<?php foreach($faq_details['answer_details'] as $answer_details_val){?>
		<div class="Row marTop10">
			<div class="flt_lft num"><p class="txt11pN"><?php echo $answer_count.'.';?></p></div>
			<div class="flt_lft w600">
				<p class="flt_lft txt11pN" id="rate_summary_<?php echo $answer_details_val['id']?>">
					<?php
						$count_rate = 0;
						$sumArray = 0;
						$rate_round= 0;
						$rating_num = 0;
						foreach($answer_details_val['rate_details'] as $rate_val){
							$rating_num += $rate_val['rating_num'];
							$count_rate++;
						}
						if($rating_num != 0){
							$rate_round = round($rating_num/$count_rate);
						}else{
							$rate_round = 0;
						}
					?>
					<?php
						for($i=1; $i<= $rate_round; $i++){
						?>
						<img src="<?php echo base_url();?>assetts/images/rate_full.png" width="16" height="16" align="absmiddle"/>
					<?php
						}
						for($i=$rate_round; $i< 5; $i++){
						?>
						<img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/>
					<?php	
						}
					?>
					<?php echo '('.count($answer_details_val['rate_details']).')';?>
				</p>
				<p class="flt_lft txt11pN marLft10"><a href="javascript:void(0)" class="txt11pB_or">Commnents</a><?php echo '('.count($faq_details['answer_details'][$answer_count-1]['comment_details']).')';  ?></p>
				<br clear="all" /> 
				<p class="txt11pN marTop10" style="font-size:15px;"><?php echo $answer_details_val['answers']?></p>
				<!--Comments section -->
				<?php if(is_array($answer_details_val['comment_details'])){?>
				<?php foreach($answer_details_val['comment_details'] as $comment_details_val){ ?>
				<div class="faq_comments_wrap clearfix marTop10" >
					<p class="txt11pN" style="font-size:14px;"><?php echo $comment_details_val['comments'];?></p>
					<div class="flt_right txt12B marBot10">Comment by : <a href="#" class="txt11pB"><?php echo get_user_username($comment_details_val['user_id']); ?></a></div>
				</div>
				<?php } }?>
				<!--Comments section -->
				<hr color="#CCCCCC" />
				<?php if($this->session->userdata('user_logged_in')){?>
				<div class="Row98 marTop10" id="rate_answer_<?php echo $answer_details_val['id']?>">
				<?php
				$user_rate_flag = '';
				foreach($answer_details_val['rate_details'] as $rate_details){
						if($rate_details['user_id'] == $this->session->userdata('user_id')){
							$user_rate_flag = 1;
						}
				?>
				<?php }?>
				<?php if($user_rate_flag == ''){?>
					<p class="flt_lft txt12B">Helpful?</p>
					<p class="flt_lft txt11pN marLft10">
					<a href="javascript:void(0)" class="rate_hover_img_1" onmouseover="rate_mouseover(1,'<?php echo $answer_details_val['id']?>');" onclick="rate_it(1,'<?php echo $answer_details_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
					<a href="javascript:void(0)" class="rate_hover_img_2" onmouseover="rate_mouseover(2,'<?php echo $answer_details_val['id']?>');" onclick="rate_it(2,'<?php echo $answer_details_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
					<a href="javascript:void(0)" class="rate_hover_img_3" onmouseover="rate_mouseover(3,'<?php echo $answer_details_val['id']?>');" onclick="rate_it(3,'<?php echo $answer_details_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
					<a href="javascript:void(0)" class="rate_hover_img_4" onmouseover="rate_mouseover(4,'<?php echo $answer_details_val['id']?>');" onclick="rate_it(4,'<?php echo $answer_details_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
					<a href="javascript:void(0)" class="rate_hover_img_5" onmouseover="rate_mouseover(5,'<?php echo $answer_details_val['id']?>');" onclick="rate_it(5,'<?php echo $answer_details_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/></a>
					</p>
					<p class="flt_lft txt11pN marLft10"><a href="javascript:void(0)"><img src="<?php echo base_url();?>assetts/images/btn_rate_it.png" style="margin-top:-3px;" border="0" /></a></p>
					<?php }?>
				</div>
				<div class="Row98 marTop10">
				<form name="submit_comment_form" id="submit_comment_form" action="<?php echo base_url()?>faqs/submit_comment" method="post">
					<table width="100%" class="comm_table">
						<tr>
							<td class="vert">
							<input type="hidden" name="questions_id" id="questions_id" value="<?php echo $faq_details['id']?>" />
							<input type="hidden" name="answers_id" id="answers_id" value="<?php echo $answer_details_val['id']?>" />
							<textarea name="comments" style="width:565px; max-width:565px; max-height:110px; height:110px;border-radius:5px;" class="comm_sel"></textarea>
							</td>
						</tr>
						<tr>
							<td class="vert" align="center">
							<input type="submit" class="btn_submit_comm" value="" />
							</td>
						</tr>
					</table>
				</form>
				</div>
				<?php }?>
				<br clear="all" />
				<hr color="#CCCCCC" />
			</div>
		</div>
		<?php 
		$answer_count++;
		}
	}
	?>
</div>
</div>
<div class="faq_Right marTop10">
<div class="faq_Right_inner clearfix" id="unanswered_sections">
<div class="Row"><h2>Unanswered Questions</h2></div>
<br class="cls" clear="all" />
<div class="Row marBot20">
	<?php $unanswered_questions_list_count = 1;?>
	<?php foreach($unanswered_questions_list as $unanswered_questions_list_val){?>
	<p class="txt11pN marTop10 marLft10"><?php echo $unanswered_questions_list_count.'.'; ?> <?php echo get_user_username($unanswered_questions_list_val->user_id); ?> : <?php echo $unanswered_questions_list_val->questions;?>
	<br />
	<?php if($this->session->userdata('user_logged_in')){?>
		<?php if($unanswered_questions_list_val->user_id != $this->session->userdata('user_id')){?>
			<a href="javascript:void(0)" onclick="return unanswered_answred_click('<?php echo $unanswered_questions_list_val->id;?>');"><img src="<?php echo base_url()?>assetts/images/btn_answer.png" class="marLft10" border="0" /></a>
		<?php }?>
	<?php }?>
	<div class="flt_lft" id="unanswred_answered_id_<?php echo $unanswered_questions_list_val->id;?>">
	</div>
	</p>
	<?php $unanswered_questions_list_count++;
	}?>
</div>


<!--pagination -->
<div class="flt_lft Row">
		<ul class="pagination">
			<?php
			$adjacents = 1;
			if ($page == 0) $page = 1;					//if no page var is given, default to 1.
			$prev = $page - 1;							//previous page is page - 1
			$next = $page + 1;							//next page is page + 1
			$lastpage = ceil($total_rows/$limit);		//lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;						//last page minus 1
			$pagination = "";
			if($lastpage > 1)
			{	
				//previous button
				if ($page > 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($prev,$limit)\">Previous</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Previous</li>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active_page\">$counter</li>";
						else
							$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($next,$limit)\">Next</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Next</li>";
					
				echo $pagination;
			}
			?>
		</ul> 		
		</div>

<!--pagination -->

</div>
</div>


</div>

<div class="cls"><br clear="all" /></div>
</div>
<!--REGISTRATIOM AREA-->
<?php $this->load->view('frontend/footer');?>