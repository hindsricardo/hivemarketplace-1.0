<?php $this->load->view('frontend/header');?>
<script type="text/javascript">
function submit_question(){
	var faq_question = $("#faq_search").val();
		if(faq_question != ''){
			$.post("<?php echo base_url()?>faqs/question_submit", { faq_question : faq_question },function(data) {
				$("#unanswered_sections").html(data);
			});
		}else{
			alert('Please enter your Question');
			$("#faq_search").focus();
			return false;
		}
}
function faq_search_click(){
	var faq_question = $("#faq_search").val();
	if(faq_question != ''){
		$("#faq_search_form").submit();
	}else{
		alert('Please enter your Question');
		$("#faq_search").focus();
		return false;
	}
}
function unanswered_answred_click(question_id){
	var html_value = ''
	html_value +='<table class="comm_table" >';
	html_value +='	<tr>';
	html_value +='		<td class="vert">';
	html_value +='			<textarea name="unanswered_answered_value" id="unanswered_answered_value_'+question_id+'" style="width:255px; height:70px;"  class="comm_txtArea"></textarea>';
	html_value +='		</td>';
	html_value +='	</tr>';
	html_value +='	<tr>';
	html_value +='		<td class="vert marLft10">';
	//html_value +='			<input name="unanswered_question_id" type="text" value="'+question_id+'" />';
	html_value +='			<input type="button" class="btn_submit"  value="" onclick="unanswered_answered_submit('+question_id+')" />';
	html_value +='		</td>';
	html_value +='	</tr>';
	html_value +='</table>';
	$("#unanswred_answered_id_"+question_id).html(html_value);
}
function unanswered_answered_submit(questions_id){
	var unanswered_answer = $("#unanswered_answered_value_"+questions_id).val();
	if(unanswered_answer != ''){
		$.post("<?php echo base_url()?>faqs/unanswered_answered_submit", { unanswered_answer : unanswered_answer, questions_id : questions_id },function(data) {
			alert('your question has been submitted');
			$("#unanswered_sections").html(data);
		});
	}else{
		alert('Please enter your Answer');
		$("#unanswered_answered_value_"+questions_id).focus();
		return false;
	}
}
function pagination(page,limit){
	$.post("<?php echo base_url()?>faqs/unanswred_pagination", { page : page, limit : limit },function(data) {
		$("#unanswered_sections").html(data);
	});
}
</script>
<div class="Row borderbox">

<div class="Row98 faq_wrap clearfix">

<div class="faq_left">
<?php if($this->session->userdata('user_logged_in')){?>
<div class="flt_lft txt11pN mar10">Welcome <a href="javascript:void(0)" class="txt12B"><?php echo $this->session->userdata('username')?></a></div>
<?php } ?>
<br clear="all" />
<h2>Answer Center</h2>

<div class="Row">
<div class="faq_info_box flt_lft">
<p class="mar10">
The Answer center is the place for HIVE MarketPlace buyers and sellers to ask fellow members as well as HIVE MarketPlace questions about buying and selling in the MarketPlace. You can also share tips and information that you feel will be useful to fellow members

</p>
</div>
<div class="cls"><br clear="all" /></div>
<p class="txt13pB">What is your Question?</p>
<div class="Row98">
<div class="searchWrap">
<form name="faq_search_form" id="faq_search_form" class="faq_search_form" method="post" action="<?php echo base_url()?>faqs/search">
	<div class="faqsearchInpWrap"><input name="faq_search" type="text"  class="faqsearchInp" id="faq_search" value="<?php echo ($this->session->userdata('faq_search') == ''? '':$this->session->userdata('faq_search'));?>"/></div>
	<div class="flt_right"><input type="button" class="btn_faq_search" onclick="return faq_search_click()" /></div>
</form>
</div>
</div>
<div class="Row">
<div class="faq_result_box">
Results
</div>
<div class="flt_lft marTop10">
	<p class="txt12B">Did you find what you were looking for?&nbsp;&nbsp;
		<?php if($this->session->userdata('user_logged_in')){?><a href="javascript:void(0)" onclick="return submit_question();"><img src="<?php echo base_url()?>assetts/images/submit_q.png" align="absmiddle" border="0" /></a><?php } ?>
	</p>
</div>
<br clear="all" />
<hr />
<?php if(isset($faq_list)){?>
	<?php if(!empty($faq_list)){ ?>
	<p class="txt11pN marTop10">Click on one of the anewsered questions below that mostly closely matches your question</p>
		<?php foreach($faq_list as $faq_list_val){ ?>
			<p class="txt13B marTop10">Q. <a href="<?php echo base_url()?>faqs/details/<?php echo $faq_list_val->id?>" class="txt12B"><?php echo $faq_list_val->questions?></a></p>
		<?php }?>
	<?php }else{?>
		<p class="txt11pN marTop10">Sorry Question is not match</p>
		<?php }?>
<?php }?>


</div>

</div>

</div>


	<div class="faq_Right marTop10">
		<div class="faq_Right_inner clearfix" id="unanswered_sections">
		<div class="Row"><h2>Unanswered Questions</h2></div>
		<br class="cls" clear="all" />
			<div class="Row marBot20">
				<?php $unanswered_questions_list_count = 1;?>
				<?php foreach($unanswered_questions_list as $unanswered_questions_list_val){?>
				<p class="txt11pN marTop10 marLft10"><?php echo $unanswered_questions_list_count.'.'; ?> <?php echo get_user_username($unanswered_questions_list_val->user_id); ?> : <?php echo $unanswered_questions_list_val->questions;?>
				<br />
				<?php if($this->session->userdata('user_logged_in')){?>
					<?php if($unanswered_questions_list_val->user_id != $this->session->userdata('user_id')){?>
				<a href="javascript:void(0)" onclick="return unanswered_answred_click('<?php echo $unanswered_questions_list_val->id;?>');"><img src="<?php echo base_url()?>assetts/images/btn_answer.png" class="marLft10" border="0" /></a>
					<?php }?>
				<?php }?>
				<div class="flt_lft" id="unanswred_answered_id_<?php echo $unanswered_questions_list_val->id;?>">
				</div>
				</p>
				<?php $unanswered_questions_list_count++;
				}?>
			</div>
		<!--pagination -->
		<div class="flt_lft Row">
		<ul class="pagination">
			<?php
			$adjacents = 1;
			if ($page == 0) $page = 1;					//if no page var is given, default to 1.
			$prev = $page - 1;							//previous page is page - 1
			$next = $page + 1;							//next page is page + 1
			$lastpage = ceil($total_rows/$limit);		//lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;						//last page minus 1
			$pagination = "";
			if($lastpage > 1)
			{	
				//previous button
				if ($page > 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($prev,$limit)\">Previous</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Previous</li>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active_page\">$counter</li>";
						else
							$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($next,$limit)\">Next</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Next</li>";
					
				echo $pagination;
			}
			?>

		</ul> 		
		</div>
		
		<!--pagination -->
		
		</div>
	</div>


</div>

<div class="cls"><br clear="all" /></div>
</div>
<?php $this->load->view('frontend/footer');?>