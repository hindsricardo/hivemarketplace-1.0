<?php $this->load->view('frontend/header');?>
<script>
document.feedback_submit = function(id){
 $("#feedback"+id).submit();
}
function feedback_already_send(){
	alert("you've already provided feedback for this item");
}
function check_payment_received(payment_date){
	var get_date = payment_date.split(' ');
	alert("Buyer paid on "+get_date[0]);
}
function shipped_scheduled_pickup_details(delivery_method, delivery_date){
	if(delivery_method == 'pickup'){
		alert("Item have been chosen to coordinate with the seller to pick up the item locally.");
	}else{
		var get_date = delivery_date.split(' ');
		alert("Item have been shipped on "+get_date[0]);
	}
}
function check_subscribe(user_id, item_id){
	$.post("<?php echo base_url();?>webadmin/users/check_subscribe",{ user_id: user_id}, function(response){
		$('#subscribe').remove();
		if(response == 'unsubscribe'){
			var createdDiv='<div id="subscribe" class="sucess_subscribe"><div class="txt11pN">Your HMP Fee is based on how many live<br>auctions you have at the time of the<br><br>auction.<br>20 or more auctions 5%<br><br>10 - 19 auctions 10%<br><br>1 - 9 auctions 15%<br><br>Become a Premium Bee and have a<br>steady 5% HMP fee for only $20/Month</div><br><div><a href="javascript:void(0)" onclick="subscribenow('+item_id+');"><img src="<?php echo base_url();?>assetts/images/btn_subscribenow.png" width="105" height="30" align="absmiddle"/></a></div><a class="subscribe_close" style="cursor:pointer;" onclick="subscribe_close_userpop()">Close</a></div>';
			$('#check_subscribe_'+item_id).append(createdDiv);
		}else{
			var createdDiv='<div id="subscribe" class="sucess_subscribe">'+response+'<a class="subscribe_close" style="cursor:pointer;" onclick="subscribe_close_userpop()">Close</a></div>';

		}
	});
}
document.subscribe_close_userpop = function(){
   $('#subscribe').remove();
}
function subscribenow(item_id){
	$.post("<?php echo base_url();?>users/account_settings/subscribe",{}, function(response){
		$('#subscribe').remove();
		var createdDiv='<div id="subscribe" class="sucess_subscribe">'+response+'<a class="subscribe_close" style="cursor:pointer;" onclick="subscribe_close_userpop()">Close</a></div>';
		$('#check_subscribe_'+item_id).append(createdDiv);
	});
}
</script>
<style>
.sucess_subscribe {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000;
	border-radius: 8px 8px 8px 8px;
    margin-bottom: 15px;
    padding: 15px !important;
    position: absolute;
	z-index:1000;
}
a.subscribe_close {
    background: url("<?php echo base_url();?>assetts/images/fancy_closebox.png") no-repeat scroll 0 0 transparent;
    height: 30px;
    overflow: hidden;
    position: absolute;
    right: -10px;
    text-indent: -1000px;
    top: -10px;
    width: 30px;
}
</style>
<div class="Row borderbox">
    <div class="Row marTop10 marBot10">
        <div class="Row">
        <div class="flt_lft marLft10"><p class="txt13B">Case # <span class="caseNo"><?php echo $details[0]['case_id'];?></span> </p></div>
        <div class="flt_right marRlt10"><p class="txt13B">Status: <span class="caseStatus"><?php echo $details[0]['status'];?></span></p></div>
        </div>    
        <div class="cls"><br clear="all" /></div>    
        <div class="Row marLft10">
        <p class="txt12B">Reason for Case: <?php echo $details[0]['purpose'];?></p>
        <p class="txt12B marTop10">Result: </p>
        </div>    
        <div class="cls"><br clear="all" /></div>    
        <div class="Row marLft10 marRlt10">        
        <p class="support_notes">Support Notes</p>  
         <?php if(isset($notes)){ ?>
			 <?php foreach($note as $notes){?>      
			<div class="flt_lft Row_borBot marTop10">
			<?php $note_date=explode(' ', $notes->date);?>
			<p class="txt12B"><?php echo "Date: ".$note_date[0]." And Time: ".$note_date[1];?></p>
			<p class="txt11pN marTop10"><?php echo $notes->description;?></p>
			</div>     
			<?php } ?>
		<?php }?>
        
        </div>    
        <?php //echo '<pre>';print_r($details);?>
        <div class="cls"><br clear="all" /></div>    
        <div class="Row">
          <div class="caseRow">
            <table width="100%" border="0" class="comm_table">
              <tr>
                <td width="10%"><img src="<?php echo base_url();?>assetts/images/thumbs/t1.jpg" width="90" height="90" border="0" /></td>                
                <td width="2%">&nbsp;</td>                
                <td width="63%" class="marLft10">
                     <table>
                          <tr>
                            <td width="273">
								<?php if($details[0]['buy_type'] == 'direct_buy'){ ?>
                                <p class="txt11pN">List#: <?php echo $details[0]['order_title']?></p>
                                <?php }else{ ?>
                                <p class="txt11pN">List#: <?php echo $details[0]['id']?></p>
                                <?php }?>
                                <p class="txt12B marTop5"><a href="<?php echo base_url();?>users/item/details/<?php echo $details[0]['id']?>"><?php echo $details[0]['title'];?></a></p>
                                <p class="txt11pN marTop5">Seller : <a href="#" class="txt11pN"><?php echo get_user_username($details[0]['seller_id']);?></a></p>                     
                                <p class="txt11pN marTop5 marRlt10"><?php echo $details[0]['description'];?></p>                            
                           </td>
                            
                           <td width="104">
                               <p class="txt12B grey_txt">Revenue</p>
                               <p class="txt13B marTop5">$3.25</p>
                               <p class="txt11pN marTop5">QTY : <?php echo $details[0]['qty'];?></p>
                            
                           </td>                            
                           <td width="181">
                               <p class="txt12B grey_txt">Summery</p>
                               <p class="txt13B marTop5">Item value : <?php echo $details[0]['item_value'];?></p>
                               <p class="txt11pN marTop5">Auction price : <?php echo $this->config->item("pennies")*$details[0]['no_of_bids'];?></p>
								<?php
									if($details[0]['seller_id'] == $this->session->userdata('user_id')){ ?>
										<p class="txt13B marTop5" id="check_subscribe_<?php echo $details[0]['id'];?>">HMP Fee : <a href="javascript:void(0);" onclick="check_subscribe('<?php echo $this->session->userdata('user_id')?>','<?php echo $details[0]['id']?>');" class="txt11pN"><?php echo $details[0]['HMP_fee'].'%'?></a></p>
									<?php } ?>
                           	   <p class="txt11pN marTop5">Profit / Loss : $96.75</p>                            
                            </td>
                       </tr>
                       <tr>
                            <td colspan="3">
                            <!--<ul class="case_Details_plus">
                            <li><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /><span>Payment Received</span></li>
                            <li><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /><span>Shipped</span></li>
                            <li><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /><span>Buyer Received</span></li>
                            <li><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /><span>Feedback</span></li>                            
                            <li class="case_stat">Open Case</li>
                            
                            </ul>-->
							<div class="flt_lft">
							<!--<a href="#"> <img src="<?php echo base_url();?>assetts/images/btn_reverse_auction.png" width="114" height="22" border="0" align="absmiddle"/></a>-->
								
								<div class="paid_option"><a id="payment_received_<?php echo $details[0]['order_id'];?>" href="javascript:void(0);" onclick="check_payment_received('<?php echo $details[0]['payment_date'];?>');"><img src="<?php echo base_url();?>assetts/images/icon_plus_c.png" width="18" height="18" border="0" align="absmiddle" /></a>Payment Received</div>
								
								
								<div class="paid_option" id="shipped_scheduled_pickup_<?php echo $details[0]['order_details_id']?>">
								<?php
								$get_order_shipped_scheduled = order_shipped_scheduled_pickup_check($details[0]['order_details_id']);
								if($get_order_shipped_scheduled[0]['shipping_or_pickup'] == ''){ ?>
								<a href="javascript:void(0);" onclick="shipped_scheduled_pickup('<?php echo $details[0]['order_details_id']?>');">
								
									<img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" />
									</a>
								<?php }else{ ?>
									<a href="javascript:void(0);" onclick="shipped_scheduled_pickup_details('<?php echo $get_order_shipped_scheduled[0]['shipping_or_pickup']?>','<?php echo $get_order_shipped_scheduled[0]['payment_scheduled_date']?>');">
									<img src="<?php echo base_url();?>assetts/images/icon_plus_c.png" width="18" height="18" border="0" align="absmiddle" />
								</a>
								<?php }?> Shipped/scheduled pickup</div>
								
								<div class="paid_option">
								<?php
								if($get_order_shipped_scheduled[0]['shipping_or_pickup'] == ''){?>
									<a id="buyer_received_<?php echo $details[0]['order_id']?>" href="javascript:void(0);" ><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /></a>
								<?php }else{ 
									if(is_item_received($details[0]['order_details_id']) == 'Pending'){ ?>
										<a id="buyer_received_<?php echo $details[0]['order_details_id']?>" href="javascript:void(0);" ><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /></a>
									<?php }else{ ?>
									<a id="buyer_received_<?php echo $details[0]['order_details_id']?>" href="javascript:void(0);"><img src="<?php echo base_url();?>assetts/images/icon_plus_c.png" width="18" height="18" border="0" align="absmiddle" /></a>
									<?php }
									}?>
									Buyer Received</div>								
								
								<div class="paid_option">
								 <div>
								<?php if($details[0]['buyer_id'] == $this->session->userdata('user_id')){
										if(is_feedback($details[0]['id'], $this->session->userdata('user_id')) == false){?>
											<a class="sign_in" style="cursor:pointer;" onclick="feedback_submit(<?php echo $details[0]['id']?>)"><img src="<?php echo base_url();?>assetts/images/icon_plus.png" width="18" height="18" border="0" align="absmiddle" /> Feedback</a> 
											<form id="feedback<?php echo $details[0]['id']?>" method="post" action="<?php echo base_url().'feedback'?>">
											<input type="hidden" name="auction_id" value="<?php echo $details[0]['id'];?>" />
											<input type="hidden" name="seller_id" value="<?php echo $details[0]['seller_id'];?>" />
											</form>
									<?php }else{ ?>
											<a class="sign_in" style="cursor:pointer;" onclick="feedback_already_send();" ><img src="<?php echo base_url();?>assetts/images/icon_plus_c.png" width="18" height="18" border="0" align="absmiddle" /> Feedback</a>
									<?php }
									}?>
									<?php if($get_order_shipped_scheduled[0]['shipping_or_pickup'] == ''){ ?>
									<script>
									$(document).ready(function(){
									$('#shipped_scheduled_pickup_<?php echo $details[0]['order_details_id']?>').click(function(){
											   window.open('<?php echo base_url();?>users/activity/popup_shipping/<?php echo $details[0]['order_details_id']?>','msgWindow','toolbar=no,status=no,menubar=No,scrollbars=yes,width=500,height=300');
									   });
									});
									   </script>
									<?php }?>
								 </div>
								</div>
							</div>
                            </td>
                     </tr>
                </table>            
                </td>                 
                <td width="25%">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="txtalignCenter">
                     <h4>00:00:06</h4>
                    <p class="marTop5"><img src="<?php echo base_url();?>assetts/images/btn_click_for_details_disabled.png" width="127" height="28" border="0" /></p>
                    <p class="txt11pN marTop5">Wiiner: <?php echo get_user_username($details[0]['buyer_id']);?></p>
                    <p class="req marTop5"><?php echo $details[0]['purpose']; ?></p>
                    
                    </td>
                    <td class="txtalignCenter mid">
                    <p class="txtalignCenter"><img src="<?php echo base_url();?>assetts/images/icon_fb.png" width="25" height="25" border="0" /></p>
                    <p class="txtalignCenter marTop10"><img src="<?php echo base_url();?>assetts/images/icon_twitter.png" width="25" height="25" border="0" /></p>
                    
                    </td>
                  </tr>
                </table>                  
                </td>
              </tr>
            </table>
            
            
            </div>
        </div>
        <div class="cls"><br clear="all" /></div>
        <div class="Row marLft10 marRlt10">
                <p class="support_notes">Message About Case</p> 
                <?php foreach($msg as $message){?>      
                <div class="flt_lft Row_borBot marTop10">
                <?php $date=explode(' ', $message->date);?>
                <p class="txt12B"><?php echo "Date: ".$date[0]." And Time: ".$date[1];?></p>
              
                <p class="txt12B marTop10">From: <?php echo $message->msg_from;?> </p>
                
                <p class="txt12B marTop10">To: <?php echo $message->msg_to?> </p>
                
                <p class="txt11pN marTop10"><?php echo $message->msg;?></p>
                </div>
                <?php } ?>
        </div>    
    	<div class="cls"><br clear="all" /></div>
    </div>
</div>
<? $this->load->view('frontend/footer');?>