<?php $this->load->view('frontend/header');?>
<div class="Row borderbox">
<div class="Row flt_lft">
<br/>
<p class=" txt38pb_y"style="text-align:center;background:#ffcc00;border-top-left-radius:15px;border-top-right-radius:15px;color:#000;font-family:Verdana;font-size:33px;padding:5px;">About HIVE MARKETPLACE</p>
<p class="txt11pN mar20">
    <p style="font-weight:bold;color:#0066cc;">What is HIVE Marketplace?</p>
    <br/>
<span style="color:#4D4D4D;">Simply put, HIVE Marketplace is tool to provide better prices for buyers and better profits for sellers. </span>
<br/>
</p>

    <br>
<p style="font-weight:bold;color:#0066cc;">What we believe?</p> 
<br clear="all" />

<span style="color:#4D4D4D;">The rules of supply and demand are too heavily weighed in favor of corporate supply. 
    The customer's demand should be reflected in the price of the individual sale. </span>
<br clear="all" /><br clear="all" />
<br clear="all" /><br clear="all" />
<p style="font-weight:bold;color:#0066cc;">How Does it work?</p>
<br clear="all" /><br clear="all" />
<div>
    <span><div class='topcontainer'style='position:relative'>
    <img src="<?php echo base_url();?>assetts/images/cash_for_credit.png" width="340" height="240" align="absmiddle" style='border-radius:100px;'/>
    
    <p style="color:#4D4D4D;text-align: left;">1. Buyers deposit money for bids to be used in <br>auctions. Each bid is worth 75 cents and can<br>be redeemed at any time. </p></span>

    <span style='float:right;'><img src="<?php echo base_url();?>assetts/images/bids_to_auction.png" width="370" height="250" align="absmiddle" style='border-radius:100px;'/>
    
    <p style="color:#4D4D4D;text-align: left;">2. Bids are spent in auctions to raise the auction <br>price by 1 cent. Whom ever has spent the most bids <br> when the clock expires wins! Sellers can set a<br> minimum number of bids and if that minimum is not met<br> All bids are returned to the bidders like the auction <br>never happened. Risk free for both bidders and the seller. </p></span>
    
    
    
     <div style='float:left;position:relative;margin-top:290px;'><img src="<?php echo base_url();?>assetts/images/after_auction.png" width="370" height="250" align="absmiddle" style='border-radius:100px;'/>
    
    <p style="color:#4D4D4D;text-align: left;">3. When the auction ends the winner pays the <br>pennies of the final auction price and the <br>seller delivers the item(s). The winner has saved<br> as much as 90% of the regular price.</p></div>
    
    
    <div style='float:right;position:relative;margin-top:200px;'><img src="<?php echo base_url();?>assetts/images/pay_seller.png" width="370" height="250" align="absmiddle" style='border-radius:100px;'/>
    
    <p style="color:#4D4D4D;text-align: left;">4. When the buyer confirms that the item is received<br> we pay the seller the total value of all the bids spent<br> in their auction, earning the seller up to 10 times<br> the original value of the item. </p></div>

    <div style='float:left;position:relative;margin-top:240px;'><img src="<?php echo base_url();?>assetts/images/auction_discount.png" width="370" height="250" align="absmiddle" style='border-radius:100px;'/>
    
    <p style="color:#4D4D4D;text-align: left;">5. If you don't win an auction don't worry.<br> The seller can set aside an inventory of the<br> auctioned item. Those who did not win can <br>purchase  from this inventory and use the <br>value of the bids spent in the sellers auction <br>to discount the price. </p></div>

<br>
<br>
<br>
<br>
<br>
<br>



</div>
    
    

</div>



       
<div class="cls">&nbsp;&nbsp;</div>

    </div>
</div>

<?php $this->load->view('frontend/footer');?>