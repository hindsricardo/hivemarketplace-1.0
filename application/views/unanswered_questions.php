		<div class="Row"><h2>Unanswered Questions</h2></div>
		<br class="cls" clear="all" />
			<div class="Row marBot20">
				<?php $unanswered_questions_list_count = 1;?>
				<?php foreach($unanswered_questions_list as $unanswered_questions_list_val){?>
				<p class="txt11pN marTop10 marLft10"><?php echo ($limit*($page-1))+$unanswered_questions_list_count.'.'; ?> <?php echo get_user_username($unanswered_questions_list_val->user_id); ?> : <?php echo $unanswered_questions_list_val->questions;?>
				<br />
				<?php if($this->session->userdata('user_logged_in')){?>
					<?php if($unanswered_questions_list_val->user_id != $this->session->userdata('user_id')){?>
						<a href="javascript:void(0)" onclick="return unanswered_answred_click('<?php echo $unanswered_questions_list_val->id;?>');"><img src="<?php echo base_url()?>assetts/images/btn_answer.png" class="marLft10" border="0" /></a>
					<?php }?>
				<?php }?>
				<div class="flt_lft" id="unanswred_answered_id_<?php echo $unanswered_questions_list_val->id;?>">
				</div>
				</p>
				<?php $unanswered_questions_list_count++;
				}?>
			</div>
		<!--pagination -->
		<div class="flt_lft Row">
		<ul class="pagination">
			<?php
			$adjacents = 1;
			if ($page == 0) $page = 1;					//if no page var is given, default to 1.
			$prev = $page - 1;							//previous page is page - 1
			$next = $page + 1;							//next page is page + 1
			$lastpage = ceil($total_rows/$limit);		//lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;						//last page minus 1
			$pagination = "";
			if($lastpage > 1)
			{	
				//previous button
				if ($page > 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($prev,$limit)\">Previous</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Previous</li>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active_page\">$counter</li>";
						else
							$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($next,$limit)\">Next</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Next</li>";
					
				echo $pagination;
			}
			?>
			<input type="hidden" name="contact_page_no" id="contact_page_no" value="<?php echo $page; ?>" />

		</ul> 		
		</div>
		
		<!--pagination -->
