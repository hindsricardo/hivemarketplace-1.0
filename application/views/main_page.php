<?php $this->load->helper('url'); ?>
<?php $this->load->view('adminnav/nav_header');?>
<?php $this->load->view('adminnav/nav_left');?>

<!--Icon set -->
    <div class="Row marTop20">
    <div class="centerDiv w680">
	
    <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>webadmin/auctions"><img src="<?php echo base_url();?>assetts/adminimages/iconAuction.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/auctions" class="txt13B">Auction</a></p>
    </div>    
    
    <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>webadmin/users"><img src="<?php echo base_url();?>assetts/adminimages/icon_bidder.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/users" class="txt13B">Users</a></p>
    </div>  
    
     <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>webadmin/admin_account"><img src="<?php echo base_url();?>assetts/adminimages/icon_seller.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/admin_account" class="txt13B">Account Settings</a></p>
    </div>  
    
     <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>webadmin/admin_tools"><img src="<?php echo base_url();?>assetts/adminimages/icon_account_settings.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/admin_tools" class="txt13B">Admin Tools</a></p>
    </div>  
    
     <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>webadmin/messages"><img src="<?php echo base_url();?>assetts/adminimages/icon_message.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/messages" class="txt13B">Message Center</a></p>
    </div>  
    
     <div class="flt_lft marLft40 marBot40"><a href="<?php echo base_url();?>faqs"><img src="<?php echo base_url();?>assetts/adminimages/icon_faq.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>faqs" class="txt13B">FAQ Center</a></p>
    </div>  
    
     <div class="flt_lft marLft40"><a href="<?php echo base_url();?>webadmin/case_queue"><img src="<?php echo base_url();?>assetts/adminimages/icon_myHive.png" class="iconSet" width="115" height="114" border="0" /></a>
    <br />
    <p align="center" class="txt13B"><a href="<?php echo base_url();?>webadmin/case_queue" class="txt13B">Case Queue</a></p>
    </div>  
    </div>
    </div>
<!--Icon set -->
<!--    5Running Auctions -->

   <!--<div class="Row98">
   
   <div class="admin_greyRow">
   <div class="flt_lft"><h2 class="heading">5 Running Auctions</h2></div>
   <div class="flt_right"><a href="#" class="view_all_lnk">View All</a></div>
   </div>
   
    
  
	<table width="98%" class="admin_comm_table" cellpadding="3" style="border-collapse:collapse;" bordercolor="#262626" border="1" >
  <tr class="admin_topRow">
    <td>Auction Title</td>
     <td>Auction Value</td>
      <td>Seller Name</td>
       <td>Last Bider</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
</table>
	
	</div>-->
<!--5Running Auctions --> 
<br clear="all" /><br clear="all" />
   <!-- 
    <div class="Row98">
   
   <div class="admin_greyRow">
   <div class="flt_lft"><h2 class="heading">Latest Closed Auctions</h2></div>
   <div class="flt_right"><a href="#" class="view_all_lnk">View All</a></div>
   </div>
   
    
  
	<table width="98%" class="admin_comm_table" cellpadding="3" style="border-collapse:collapse;" bordercolor="#262626" border="1" >
  <tr class="admin_topRow">
    <td>Auction Title</td>
     <td>Auction Value</td>
      <td>Seller Name</td>
       <td>Winner Name</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Harry</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Mathew</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Ken</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Rick</td>
  </tr>
  
   <tr>
    <td>Auction Title</td>
     <td>$0.30</td>
      <td>John</td>
       <td>Willis</td>
  </tr>
  
</table>
	
	</div>
    
    <br clear="all" /><br clear="all" />
    
    <div class="Row98">
   
   <div class="admin_greyRow">
   <div class="flt_lft"><h2 class="heading">5 latest Users</h2></div>
   <div class="flt_right"><a href="#" class="view_all_lnk">View All</a></div>
   </div>
   
    
  
	<table width="98%" class="admin_comm_table" cellpadding="3" style="border-collapse:collapse;" bordercolor="#262626" border="1" >
  <tr class="admin_topRow">
    <td>User Name</td>
     <td>Date of Joining</td>
      <td>Email Id</td>
       <td>Status</td>
  </tr>
  
   <tr>
    <td>John</td>
     <td>10-04-2013</td>
      <td>john808@gmail.com</td>
       <td>Pending</td>
  </tr>
  
   <tr>
    <td>Rick</td>
     <td>08-04-2013</td>
      <td>rick.shen@yahoo.com</td>
       <td>Active</td>
  </tr>
  
  <tr>
    <td>Bob</td>
     <td>06-04-2013</td>
      <td>bob.roy@gmail.com</td>
       <td>Active</td>
  </tr>
  
  <tr>
    <td>Charles</td>
     <td>04-04-2013</td>
      <td>willcharles@gmail.com</td>
       <td>Pending</td>
  </tr>
  
  <tr>
    <td>Ken</td>
     <td>28-03-2013</td>
      <td>kenroger@msn.com</td>
       <td>Active</td>
  </tr>
  
    
</table>
	
	</div>
    
    <br clear="all" /><br clear="all" />
    
    <div class="Row98">
   
   <div class="admin_greyRow">
   <div class="flt_lft"><h2 class="heading">5 latest FAQ</h2></div>
   <div class="flt_right"><a href="#" class="view_all_lnk">View All</a></div>
   </div>
   
    
  
	<table width="98%" class="admin_comm_table" cellpadding="3" style="border-collapse:collapse;" bordercolor="#262626" border="1" >
  <tr class="admin_topRow">
    <td>User Name</td>
     <td>FAQ</td>
      <td>Email Id</td>
  </tr>
  
   <tr>
    <td>John</td>
     <td>How do I list an item?</td>
      <td>john808@gmail.com</td>
    </tr>
  
   <tr>
    <td>Rick</td>
     <td>How long does it take to receive my credits?</td>
      <td>rick.shen@yahoo.com</td>
  
  </tr>
  
  <tr>
    <td>Bob</td>
     <td>How does it works?</td>
      <td>bob.roy@gmail.com</td>
      
  </tr>
  
  <tr>
    <td>Charles</td>
     <td>How do I file a case?</td>
      <td>willcharles@gmail.com</td>
     
  </tr>
  
  <tr>
    <td>Ken</td>
     <td>What is My HMP</td>
      <td>kenroger@msn.com</td>
      
  </tr>
  
    
</table>
	
	</div>
    
    <br clear="all" /><br clear="all" />-->
  
  
    </td>
  </tr>

</table>

</div> 

</div>
</div>

<!--BID NOW AREA END-->

<!--FOOTER SRT-->

<?php $this->load->view('adminnav/nav_footer');?>
