<?php $this->load->view('frontend/header');?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link href="<?php echo base_url()?>assetts/css/site.css" rel="stylesheet" type="text/css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script src="<?php echo base_url()?>assetts/js/jquery.price_format.1.8.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assetts/js/jquery.timeentry.js"></script>
  <script>
 $(function() {
    $( "#slider-range-min" ).slider({
      range: "min",
      value: <?php echo $auction_details[0]['auction_endtime'];?>,
      min: 1,
      max: 168,
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.value);
      }
    });
    //$( "#amount" ).val( "(" + $( "#slider-range-min" ).slider( "value" ) + ")" );
	$( "#amount" ).val( $( "#slider-range-min" ).slider( "value" ) );
  });
  </script>
  
<script language="javascript">
$(document).ready(function(){
$('.count_num').text('(Your title has 80 characters remaining)');
$('input[name=title]').keyup(function () {
    var max = 80;
    var len = $(this).val().length;
    if (len >= max) {
        $('.count_num').text(' you have reached the limit');
    } else {
        var ch = max - len;
        $('.count_num').text('(Your title has '+ ch + ' characters remaining)');
    }
	$('input[name=title]').attr('maxlength','80');
});
	$('input[name=tags]').keyup(function () {
	var a = $('#group_tag #single_tag').size() + 1;
	if(a < 5){
		myString = $(this).val();
		if(myString.indexOf(' ') === -1){
				
		}else{
			var tag = $.trim(myString);
			var group_tag = $('#group_tag');
			var a = $('#group_tag #single_tag').size() + 1;
			var tag_html = $('<div id="single_tag" class="mar10" style="background:#CCCCCC; float:left;"><input type="hidden" name="tag_hidden[]" value="'+tag+'" /><span class="single_tag_val_'+a+'" style="font-size: 14px;">'+tag+'</span><span id="remove_tag_'+a+'" onclick="remove_tag('+a+')" style="font-size: 14px;cursor:pointer; color:red; border:1px solid;margin-left: 5px;" title="Remove">x</span></div>').appendTo(group_tag);
			$("#tags").val('');
			var tags_hidden = $("#tags_hidden").val();
			tags_hidden += ' '+tag;
			$("#tags_hidden").val(tags_hidden);
		}
	}else{
		$("#tags").attr("disabled", true);
		alert('Sorry no more tags will be avilable');
	}
	});
	
	var us_paytype = $('input:checkbox[name=us_paytype]');
	us_paytype.removeAttr('checked');
	us_paytype.click(function(){
		if($(this).is(':checked')){
			$('input[name=us_price]').val('');
			$('input[name=us_price]').attr('readonly', 'readonly');
		}else{
			$('input[name=us_price]').removeAttr('readonly');
		}
	});
	var ww_paytype = $('input:checkbox[name=ww_paytype]');
	ww_paytype.click(function(){
		if($(this).is(':checked')){
			$('input[name=ww_price]').val('');
			$('input[name=ww_price]').attr('readonly', 'readonly');
		}else{
			$('input[name=ww_price]').removeAttr('readonly');
		}
	});
	
	var auction_public = $('#auction_public');
	var auction_private = $('#auction_private');
	var passcode= $('input[name=passcode]');
	var pass_val='';
	auction_public.click(function(){
		if($(this).is(':checked')){
		  pass_val=passcode.val();
		  auction_private.attr('disabled','disabled');
		  passcode.attr('readonly', 'readonly');
		  passcode.val('');
		}else{
			auction_private.removeAttr('disabled');
		    passcode.removeAttr('readonly');
			passcode.val(pass_val);
		}
	});
	
	auction_private.click(function(){
		if($(this).is(':checked')){
		  auction_public.attr('disabled','disabled');
		 }else{
			auction_public.removeAttr('disabled');
		    passcode.removeAttr('readonly');
		}
	});
	 var auction_now = $('#auction_now');
	 var auction_later = $('#auction_later');
	 auction_now.click(function(){
		if($(this).is(':checked')){
		  auction_later.attr('disabled','disabled');
		  $('#datepicker_active').hide();
		}else{
			auction_later.removeAttr('disabled');
		     $('#datepicker_active').show();
		}
	});
	auction_later.click(function(){
		if($(this).is(':checked')){
		  auction_now.attr('disabled','disabled');
		 }else{
			auction_now.removeAttr('disabled');
		 }
	});
	$('#auction_list_form_submit').on('click', function() {
		var list_title = $("#list_title").val();
		
		if($("#list_title").val() == ''){
			alert("Please enter title");
			$("#list_title").focus();
			return false;
		}else if($("#item_value").val() =='USD$ 0.00'){
			alert("Please Enter Item Value");
			return false;
		}else if($("#no_edit_image").val() == '' || $("#no_edit_image").val() == 0){
			alert("Please choose images");
			return false;
		}
		if($("#auction_public").is(':checked') || $("#auction_private").is(':checked')){
			if($("#auction_now").is(':checked') || $("#auction_later").is(':checked')){
				$("#auction_list_form").submit();
			}else{
				alert("Decide when you want your auction to start");
				return false;
			}
		}else{
			alert("Decide whether to make you auction public or private");
			return false;
		}
		
	});
});

function remove_tag(a){
	$("#remove_tag_"+a).parents("#single_tag").remove();
	//var tags_hidden = $("#tags_hidden").val();
	//var single_tag_val = $(".single_tag_val_"+a).text();
	//alert(single_tag_val);
	$("#tags").attr("disabled", false);
}
</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo base_url()?>assetts/images/icon_calendar.png",
			buttonImageOnly: true
		});
		$( "#datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		$('.ui-datepicker-trigger').attr('align','absmiddle');
		$('.ui-datepicker-trigger').css('padding-left','4px');
		//alert($("#datepicker").attr('rel'));
		$( "#datepicker" ).val($("#datepicker").attr('rel'));
	});
</script>
<script>
$(function(){
	$('#item_value').priceFormat({
		prefix: 'USD$ ',
		centsSeparator: '.',
		thousandsSeparator: ''
	});
	$('#ww_price').priceFormat({
		prefix: '',
		centsSeparator: '.',
		thousandsSeparator: ''
	});
	$('#us_price').priceFormat({
		prefix: '',
		centsSeparator: '.',
		thousandsSeparator: ''
	});
	
var offer_pickup = $('input:checkbox[name=offer_pickup]');
offer_pickup.click(function(){
		if($(this).is(':checked')){
		}else{
			$('input[name=payat_pickup]').removeAttr('checked');
		}
	});
var payat_pickup = $('input:checkbox[name=payat_pickup]');
payat_pickup.click(function(){
		if($('input[name=offer_pickup]').is(':checked')){
			$('input[name=payat_pickup]').attr('checked');
		}else{
			$('input[name=payat_pickup]').removeAttr('checked');
			alert('Please Choose Offer pickup as a delivery method');
		}
	});
	$("#offer_shipping_delivery_method").click(function(){
	if ($("#offer_shipping_delivery_method").is(":checked")) {
		$("#us_company_name").prop("disabled", false);
		$("#ww_company_name").prop("disabled", false);
	} else {
		$("#shipping_destination_us").prop("disabled", true);
		$("#us_company_name").prop("disabled", true);
		$("#ww_company_name").prop("disabled", true);
		$("#us_handling_time").prop("disabled", true);
		$("#ww_handling_time").prop("disabled", true);
	}
	});
});

function quantity_isnumeric(quantity){
	if($.isNumeric(quantity)){
		return true
	}else{
		alert("Only numeric value is accepted");
		return false;
	}
}
function get_min_bids(bids){
	var credit_value = 0.75;
	if($.isNumeric(bids)){
		var minimun_profit = (bids*credit_value);
		//alert(minimun_profit);
		minimun_profit = parseFloat(minimun_profit).toFixed(2);
		$("#min_profit").val(minimun_profit);
	}else{
		alert("Only numeric value is accepted");
		return false;
	}
}
function change_us_handling_time(us_value){
	if(us_value == ''){
		$("#us_handling_time").prop("disabled", true);
	}else{
		$("#us_handling_time").prop("disabled", false);
	}
		
}
function change_ww_handling_time(ww_value){
	//alert(ww_value);
	if(ww_value == ''){
		$("#ww_handling_time").prop("disabled", true);
	}else{
		$("#ww_handling_time").prop("disabled", false);
	}
}
$(function () {
	$('#starting_with_time').timeEntry();
});

</script>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<div class="comm_yellow_box">Create You Auction Listing</div>
<div class="flt_right marTop10 marRlt10"><a href="#" class="txt11pB">Click Here to duplicate previous listing</a></div>
<div class="cls" style="height:10px;"></div>
<form name="auction_list_form" id="auction_list_form" class="auction_list_form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>users/sell/auction_list_edit">
<input type="hidden" id="auction_id" name="id" value="<?php echo $auction_details[0]['id'];?>">
<!--Loop -->
<div class="listing_block clearfix">
<div class="Row marBot40 clearfix">
<div class="list_black_row">
<div class="numBox_black"><h1>1</h1></div>
<h1 class="List_header">Create a descriptive title for your auction listing</h1>
</div> 
<div class="Row Listing_greyBox marTop20">
<div class="flt_lft mar20"><input name="title" type="text" id="list_title" style="width:575px; height:25px;" value="<?php echo $auction_details[0]['title'];?>" />
<p class="txt11pN marTop5 marleftIE15 count_num">(Your title has 80 characters remaining)</p>
<div class="under"></div>
</div> 
</div>
</div>
<?php //echo '<pre>';print_r($auction_details);?>
<div class="Row marBot40 clearfix">
	<div class="list_black_row">
	<div class="numBox_black"><h1>2</h1></div>
	<h1 class="List_header">Bring your item to life with pictures</h1>
	</div> 
	<div class="Row Listing_greyBox marTop20">
	<p class="txt11pN mar10">Click Add a photo and select the photo you want to upload</p>
	<div class="Row95 marLft10 marTop10">
	<input type="hidden" id="no_edit_image" value="<?php echo count($auction_details[0]['images']);?>" />
	<?php
	$left = '186';
	for($i = 1; $i<=count($auction_details[0]['images']); $i++){ 
	
	?>
		<div class="listingPicswrap flt_lft">
			<div class="listingPicInner" id="preview-area<?php echo $i;?>">
				<img src="<?php echo base_url()?>upload/<?php echo $auction_details[0]['images'][$i-1]['image_name'];?>" width="100" border="0" />
			</div>
			<div class="btn_add_photo_wrap">
				<input type="button" id="button_<?php echo $i;?>" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:75px;height:25px;font-size:10px;margin-left: 11px;" value="Remove" onclick="remove_edit_photo('<?php echo $i;?>','<?php echo $auction_details[0]['id'];?>', '<?php echo $auction_details[0]['images'][$i-1]['image_name'];?>')" />
				<input type="file" id="upload_filename<?php echo $i;?>" name="filename[]" multiple style="width:75px; margin-left: 10px; position:absolute;left: <?php echo $left;?>px;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2; height:0px; display:none;" />
			</div>
			<div class="btn_add_photo_wrap"></div>
		</div>
	<?php
	$left +=162; 
	}
	for($j = $i; $j<=5; $j++){ ?>
		<div class="listingPicswrap flt_lft">
			<div class="listingPicInner" id="preview-area<?php echo $j;?>">
				<img src="<?php echo base_url()?>assetts/images/item.jpg" width="100" border="0"  />
			</div>
			<div class="btn_add_photo_wrap">
				<input type="button" id="button_<?php echo $j;?>" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:75px;height:25px;font-size:10px;margin-left: 11px;" />
				<input type="file" id="upload_filename<?php echo $j;?>" name="filename[]" multiple style="width:75px; margin-left: 10px; position:absolute;left: <?php echo $left;?>px;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" />
			</div>
			<div class="btn_add_photo_wrap"></div>
		</div>
	<?php 
	$left +=162;
	}?>
	
	</div>
	<div class="under"></div>
	</div> 
</div>
<!--Describe the item(s) you are selling -->

<div class="Row marBot40 clearfix">
<div class="list_orange_row">
<div class="numBox_orange"><h1>3</h1></div>
<h1 class="List_header">Describe the item(s) you are selling</h1>
</div> 

<div class="Row Listing_greyBox marTop20">

<p class="txt11pN mar10">Give your listing more details that will help it stand out to buyers</p>

<div class="Row98 marLft10 marTop10">
<div class="flt_lft Row">
<p class="txt11pN marBot8">Add Tags</p>
<div id="group_tag">

</div>
<input name="tags" type="text" id="tags" style="width:450px; height:25px;" value="<?php echo $auction_details[0]['tags'];?>"/>
<p class="txt11pN marTop10">To Create a tag type a word and then press space (Limit 4 tag per listing)</p>

<hr />
</div>
<div class="marTop10 Row">
<p class="txt11pN marBot8">Item(s) Condition</p>
<select name="item_condition">
<option value="New" <?php if($auction_details[0]['item_condition'] == 'New'){ echo 'selected="selected"';}?>>New</option>  
<option value="Like New" <?php if($auction_details[0]['item_condition'] == 'Like New'){ echo 'selected="selected"';}?>>Like New</option>  
<option value="Used" <?php if($auction_details[0]['item_condition'] == 'Used'){ echo 'selected="selected"';}?>>Used</option>  
<option value="Never Used" <?php if($auction_details[0]['item_condition'] == 'Never Used'){ echo 'selected="selected"';}?>>Never Used</option>  
<option value="Some Damage" <?php if($auction_details[0]['item_condition'] == 'Some Damage'){ echo 'selected="selected"';}?>>Some Damage</option>  
</select><hr />
</div>
<div class="marTop10 Row">
<p class="txt11pN marBot8">Quantity</p>
<div class="flt_lft">
<input name="qty" type="text" class="quantity" onkeyup="quantity_isnumeric(this.value)" style="width:80px; height:20px;" value="<?php echo $auction_details[0]['qty'];?>" /></div>
<div class="flt_lft marLft10 tips_horizontal">
<div class="tips_head">Tips</div>
<div class="flt_lft txt11pN marLft10" style="width:737px;">
<p>
Offering multi of an item in an auction can differentiate your auction even if someone has the same item listed. After all, two is better than one
</p>
</div>

</div>
</div>
<div class="Row marTop10">

<div class="flt_lft">
<div class="open_txt_filed">
<div class="topRow">Description</div>
<div class="flt_lft Row white_bg" style="height:200px;">
<textarea name="description" style="width:644px; height:194px; border:none;" ><?php echo $auction_details[0]['description'];?></textarea>
</div>
</div>

<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft txt11pN">
<p class="mar10">
Help buyers find what they're looking for by providing detaild, easy-to-read information such as the following :
<br />
<br />

Color,size,quantity and measurements, Item conditoon details. Notable features or a good story associated with the item. 
<br />
<br />

Whether you allow returns. <br />

Be sure to check your spelling and grammer. 
</p>
</div>

</div>
</div>
</div>

</div>
<div class="under"></div>
</div> 
</div>
<!--Describe the item(s) you are selling -->
<!--Set item(s) value price and shipping / pickup details -->
<div class="Row marBot40 clearfix">
<div class="list_orange_row">
<div class="numBox_orange"><h1>4</h1></div>
<h1 class="List_header">Set item(s) value price and shipping / pickup details</h1>
</div> 
<div class="Row Listing_greyBox marTop20">
<div class="Row marTop10">
<div class="flt_lft w650">

<div class="Row95">
<div class="flt_lft"><p class="txt11pN">Set item value at: </p></div>

<div class="flt_lft marLft10"><input type="text" name="item_value" id="item_value" style="width:120px;" value="<?php echo($auction_details[0]['item_value'] ? 'USD$ '.$auction_details[0]['item_value'] : 'USD$ 0.00');?>" /></div>
</div>

<div class="Row95 marTop10">

<div class="flt_lft txt11pN ">Auction will be last for</div>
<div class="flt_lft marLft10" style="width:350px; padding-left:10px;"><div id="slider-range-min"></div></div>
<div class="flt_lft txt11pN" style="padding-left:18px;"><input name="auction_endtime" type="text" id="amount" value="<?php echo $auction_details[0]['auction_endtime'];?>" style="border: 0; width:30px; background:#eeeeee; color: #f6931f; font-weight: bold;" />&nbsp;hours
</div>

</div>
<div class="Row95  marTop10">
<div class="flt_lft"><p class="txt11pN">The minimum number of bids for this auctin is </p></div>

<div class="flt_lft marLft10"><input type="text" name="min_bids" onkeyup="get_min_bids(this.value)" value="<?php echo $auction_details[0]['min_bids'];?>" style="width:70px;"/></div>

<div class="flt_lft marLft10"><p class="txt11pN">your minimun profit is $#</p></div>

<div class="flt_lft marLft10"><input type="text" readonly="readonly" name="min_profit"  style="width:70px;" value="<?php echo($auction_details[0]['min_profit'] ? $auction_details[0]['min_bids']:'0.00');?>" id="min_profit"/></div>

</div>

<div class="Row98  marTop10">

<div class="flt_lft marTop5" style="width:70px"><input name="for_bidders"  style="width:70px;" type="text" value="<?php echo $auction_details[0]['for_bidders'];?>" /></div>

<div class="marLft10 flt_right" style="width:556px"><p class="txt11pN">Item will be available for bidders who did not win to purchase directly at the item value minus what they spent in the auction </p></div>

</div>
<div class="Row95 marTop10">
<div class="white_bg_a txt11pN">
<p>
There is no fee for using this service. Direct Sales are a good means boosting participation for your auction as well as sales.
</p>
</div>
</div>
</div>
<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft mar10 txt11pN">
Misrepresented item values can deter some bidders<br /><br />
-Most bidding occurs in the final hours of the auction<br />
<br />
Be careful of setting a minimun above the item value as this may result in profitable auctions not meeting their minimum and cancelling at the end. It is best to aim for minimum number of bids just below the item value
</div>

</div>

</div>
<div class="under"></div>

</div> 
<div class="Row Listing_greyBox marTop20">

<div class="greyBox_inner clearfix">

<div class="faq_left w600">

<div class="Row">
<div class="flt_lft"><input name="offer_shipping_delivery_method" id="offer_shipping_delivery_method" type="checkbox" checked="checked" value="" onclick="offer_shipping_delivery_method1()" /></div>
<div class="flt_lft txt11pN marLft10">Offer Shipping as delivery method</div>
</div>
<div class="Row marTop10">
<div class="flt_lft txt11pN marLft10">Shipping Destination Services</div>
</div>
<div class="Row marTop10">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="txt11pN">
		<tr>
			<td valign="middle" align="left">United States</td>
			<td valign="middle" align="left">
				<select name="us_company_name" id="us_company_name" onchange="change_us_handling_time(this.value)">
					<option value="">Select Carrier</option>
					<option value="UPS" <?php if($auction_details[0]['us_company_name'] == 'UPS'){ echo 'selected="selected"';}?>>UPS</option>
					<option value="USPS" <?php if($auction_details[0]['us_company_name'] == 'USPS'){ echo 'selected="selected"';}?>>USPS</option>
					<option value="FEDEX" <?php if($auction_details[0]['us_company_name'] == 'FEDEX'){ echo 'selected="selected"';}?>>FEDEX</option>
					<option value="Other Service Inside the United States" <?php if($auction_details[0]['us_company_name'] == 'Other Service Inside the United States'){ echo 'selected="selected"';}?>>Other Service Inside the United States</option>
					<option value="Other Service Outside the United States" <?php if($auction_details[0]['us_company_name'] == 'Other Service Outside the United States'){ echo 'selected="selected"';}?>>Other Service Outside the United States</option>
				</select>
			</td>
			<td valign="middle" align="left">Handling Time</td>
			<td valign="middle" align="left">
				<select name="us_handling_time" id="us_handling_time">
					<option value="1" <?php if($auction_details[0]['us_handling_time'] == '1'){ echo 'selected="selected"';}?>>1day</option>
					<option value="2" <?php if($auction_details[0]['us_handling_time'] == '2'){ echo 'selected="selected"';}?>>2days</option>
					<option value="3" <?php if($auction_details[0]['us_handling_time'] == '3'){ echo 'selected="selected"';}?>>3days</option>
					<option value="4" <?php if($auction_details[0]['us_handling_time'] == '4'){ echo 'selected="selected"';}?>>4days</option>
					<option value="5" <?php if($auction_details[0]['us_handling_time'] == '5'){ echo 'selected="selected"';}?>>5days</option>
					<option value="6" <?php if($auction_details[0]['us_handling_time'] == '6'){ echo 'selected="selected"';}?>>6days</option>
					<option value="7" <?php if($auction_details[0]['us_handling_time'] == '7'){ echo 'selected="selected"';}?>>7days</option>
					<option value="8" <?php if($auction_details[0]['us_handling_time'] == '8'){ echo 'selected="selected"';}?>>8days</option>
					<option value="9" <?php if($auction_details[0]['us_handling_time'] == '9'){ echo 'selected="selected"';}?>>9days</option>
					<option value="10" <?php if($auction_details[0]['us_handling_time'] == '10'){ echo 'selected="selected"';}?>>10days</option>
				</select>
			</td>
			<td valign="middle" align="left">$</td>
			<td valign="middle" align="left">
				<input name="us_price" type="text" style="width:50px;" value="<?php echo ($auction_details[0]['us_price'] > 0 ? $auction_details[0]['us_price']: '0.00');?>" id="us_price" />
			</td>
			<td valign="middle" align="left"><input name="us_paytype" type="checkbox" checked="checked" value="<?php echo $auction_details[0]['us_paytype'];?>" /></td>
			<td valign="middle" class="txt11pN" align="left">FREE</td>
		</tr>
		<tr>
			<td valign="middle" colspan="10" style="height:10px;" align="left"></td>
		</tr>
		<tr>
			<td valign="middle" align="left">World Wide</td>
			<td valign="middle" align="left">
				<select name="ww_company_name" id="ww_company_name" onchange="change_ww_handling_time(this.value)">
					<option value="">Select Carrier</option>
					<option value="UPS" <?php if($auction_details[0]['ww_company_name'] == 'UPS'){ echo 'selected="selected"';}?>>UPS</option>
					<option value="USPS" <?php if($auction_details[0]['ww_company_name'] == 'USPS'){ echo 'selected="selected"';}?>>USPS</option>
					<option value="FEDEX" <?php if($auction_details[0]['ww_company_name'] == 'FEDEX'){ echo 'selected="selected"';}?>>FEDEX</option>
					<option value="Other Service Inside the United States" <?php if($auction_details[0]['ww_company_name'] == 'Other Service Inside the United States'){ echo 'selected="selected"';}?>>Other Service Inside the United States</option>
					<option value="Other Service Outside the United States" <?php if($auction_details[0]['ww_company_name'] == 'Other Service Outside the United States'){ echo 'selected="selected"';}?>>Other Service Outside the United States</option>
					<option value="No International Shipping Avilabel" <?php if($auction_details[0]['ww_company_name'] == 'No International Shipping Avilabel'){ echo 'selected="selected"';}?>>No International Shipping Avilabel</option>
				</select>
			</td>
			<td valign="middle" align="left">Handling Time</td>
			<td valign="middle" align="left">
				<select name="ww_handling_time" id="ww_handling_time">
					<option value="1" <?php if($auction_details[0]['ww_handling_time'] == '1'){ echo 'selected="selected"';}?>>1day</option>
					<option value="2" <?php if($auction_details[0]['ww_handling_time'] == '2'){ echo 'selected="selected"';}?>>2days</option>
					<option value="3" <?php if($auction_details[0]['ww_handling_time'] == '3'){ echo 'selected="selected"';}?>>3days</option>
					<option value="4" <?php if($auction_details[0]['ww_handling_time'] == '4'){ echo 'selected="selected"';}?>>4days</option>
					<option value="5" <?php if($auction_details[0]['ww_handling_time'] == '5'){ echo 'selected="selected"';}?>>5days</option>
					<option value="6" <?php if($auction_details[0]['ww_handling_time'] == '6'){ echo 'selected="selected"';}?>>6days</option>
					<option value="7" <?php if($auction_details[0]['ww_handling_time'] == '7'){ echo 'selected="selected"';}?>>7days</option>
					<option value="8" <?php if($auction_details[0]['ww_handling_time'] == '8'){ echo 'selected="selected"';}?>>8days</option>
					<option value="9" <?php if($auction_details[0]['ww_handling_time'] == '9'){ echo 'selected="selected"';}?>>9days</option>
					<option value="10" <?php if($auction_details[0]['ww_handling_time'] == '10'){ echo 'selected="selected"';}?>>10days</option>
				</select>
			</td>
			<td valign="middle" align="left">$</td>
			<td valign="middle" align="left">
				<input name="ww_price" type="text" value="<?php echo ($auction_details[0]['ww_price'] > 0 ? $auction_details[0]['ww_price']: '0.00');?>" id="ww_price" style="width:50px;"/>
			</td>
			<td valign="middle" align="left"><input name="ww_paytype" type="checkbox" value="<?php if($auction_details[0]['ww_paytype'] == 'free'){ echo 'selected="selected"';}?>" /></td>
			<td valign="middle" class="txt11pN" align="left">FREE</td>
		</tr>
		<tr>
			<td valign="middle" colspan="10" style="height:10px;" align="left"></td>
		</tr>
		<tr>
			<td valign="middle" align="left" colspan="6">
				<table class="comm_table" >
					<tr>
						<td width="5%" align="left" valign="middle">
						<input name="offer_pickup" type="checkbox" checked="checked" id="offer_pickup" value="1" />
						</td>
						<td width="40%" align="left" valign="middle">Offer pickup as a delivery method</td>
						<td width="5%" align="left" valign="middle">
							<input name="payat_pickup" type="checkbox" id="payat_pickup1" value="1" />
						</td>
						<td width="50%" align="left" valign="middle">Pay at pickup</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>


<div class="Row98 marTop10">
<div class="white_bg_a txt11pN">
<p>
This pickup option will require you to coordinate with the buyer for a time and location to pick up the item(s). You will not receive your auction funds until the item is reported delivered by the seller or 14 days from seller payment, whichever comes first.
</p>
</div>
</div>

</div>

<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft mar10 txt11pN">
<p>
Free shipping can set your auction apart from similar listing.
</p>
</div>

</div>

</div>

<div class="Row95 marLft10 marTop20">
<div class="flt_lft"><input name="return_policy" type="checkbox" value="" /></div>
<div class="flt_lft marLft10">
<p class="txt11pN" style="width: 825px;">All returns accepted. Your buyers has to contact you within 14 days after the item was recoverd for a refund. The buyer would pay for return shipping</p>
</div>
</div>
<div class="under"></div>

</div>

</div>
<!--Set item(s) value price and shipping / pickup details -->
<!--Decide how you would like to get paid -->
<div class="Row marBot40 clearfix">
<div class="list_yellow_row">
<div class="numBox_yellow"><h1>5</h1></div>
<h1 class="List_header">Decide how you would like to get paid </h1>
</div> 

<div class="Row Listing_greyBox marTop20">


<div class="Row marTop10">

<div class="flt_lft">

<div class="Row98">
<div class="flt_lft"><p class="txt11pN">Accept Payment with PayPal Account </p></div>
<?php echo get_user_paypalemail($this->session->userdata('user_id'));?>
<div class="flt_lft marLft10"><input name="paypal_account" style="width:200px;" type="text" value="<?php echo (get_user_paypalemail($this->session->userdata('user_id')) != ''?get_user_paypalemail($this->session->userdata('user_id')):get_user_email($this->session->userdata('user_id')));?>" /></div>
</div>


<div class="Row98 marTop10">
<div class="white_bg_a txt11pN">
<p>
if you're already a PayPal customer, please enter the email address you used to register with PayPal. if you don't have a PayPal account, you can wait until your item sells to create one; enter your email address in the box and sign up later. You will receive an email asking you to sign up to collect your payment, so an accurate email is vital to receiving your payment.</p>
</div>
</div>

</div>
</div>
<div class="under"></div>

</div> 
</div>

<!--Decide how you would like to get paid -->


<!--Decide whether to make you auction public or private-->
<div class="Row marBot40 clearfix">
<div class="list_red_row">
<div class="numBox_red"><h1>6</h1></div>
<h1 class="List_header">Decide whether to make you auction public or private</h1>
</div> 

<div class="Row Listing_greyBox marTop20">
<div class="Row marTop10">

<div class="flt_lft">

<div class="Row98">
<div class="flt_lft"><input name="auction_type[]" type="checkbox" value="Public" id="auction_public" <?php if($auction_details[0]['auction_type'] == 'Public'){ echo 'checked="checked"';}?> /></div>
<div class="flt_lft marLft10"><p class="txt11pN">Public : make auction open and visible for every one</p></div>
</div>

<div class="Row98 marTop10">
<div class="flt_lft"><input name="auction_type[]" type="checkbox" value="Private" id="auction_private" <?php if($auction_details[0]['auction_type'] == 'Private'){ echo 'checked="checked"';}?> /></div>
<div class="flt_lft marLft10"><p class="txt11pN">Private : your auction can only be found by searching for the item # it can only be accessed them with the passcode you create.</p></div>
<br clear="all" />

<div class="flt_lft"><p class="txt11pN">Enter the passcode you would like to secure the auction with</p></div>

<div class="flt_lft marLft10"><input name="passcode" type="text"  style="width:200px;" value="<?php echo $auction_details[0]['passcode']?>" /></div>

</div>
</div>
</div>
<div class="under"></div>
</div> 
</div>
<!--Decide whether to make you auction public or private-->
<!--Decide when you want your auction to start-->
<div class="Row marBot20 clearfix">
<div class="list_white_row">
<div class="numBox_white"><h1>7</h1></div>
<h1 class="List_header">Decide when you want your auction to start</h1>
</div> 

<div class="Row Listing_greyBox marTop20">
	<div class="Row marTop10">
	
		<div class="flt_lft w600">
			<div class="Row98">
				<div class="flt_lft"><input name="starting_with[]" type="checkbox" value="now" id="auction_now" <?php if($auction_details[0]['starting_with'] == 'now'){ echo 'checked="checked"';}?> /></div>
				<div class="flt_lft marLft10"><p class="txt11pN">Start it right now</p></div>
			</div>
			<div class="Row98 marTop10">
				<div class="flt_lft"><input name="starting_with[]" type="checkbox" value="later" id="auction_later" <?php if($auction_details[0]['starting_with'] == 'later'){ echo 'checked="checked"';} ?> /></div>
				<div class="flt_lft marLft10"><p class="txt11pN">Schedule your auction for a later time</p></div>
				<br clear="all" />
				<div class="Row98 marTop10" id="datepicker_active">
				<div class="flt_lft txt11pN marTop5">Date</div>
				<?php $start_time = explode(" ",$auction_details[0]['schedule_time']);
				//echo '<pre>';print_r($start_time);
				?>
				 <?php if($auction_details[0]['starting_with'] == 'later'){ ?>
				<div class="flt_lft marLft10"><input name="starting_with_date" type="text" id="datepicker" value="" rel="<?php echo $start_time[0]?>" /></div>
				<div class="flt_lft txt11pN marTop5 marLft10">Time</div>
				<div class="flt_lft marLft10"><input name="starting_with_time" type="text" id="starting_with_time" style="100px;" value="<?php echo $start_time[1]?>" /></div>
				</div>
				<?php }?>
			</div>
		</div>
		<div class="marLft10 tips_vertical clearfix">
			<div class="tips_vertical_head">Tips</div>
			<div class="flt_lft txt11pN">
			<p class="mar10">Private auctions are a good way to host your own penny auction to a niche consumer base without unwanted attention. You will have an opportunity to edit start time later if need be.</p>
			</div>
		</div>
		<div class="under"></div>
	</div> 
</div>
<!--Decide when you want your auction to start-->
</div>
<div class="Row">
<p align="center" class="marBot20"><input name="" type="button" class="btn_list_auction" id="auction_list_form_submit" value=""/></p>
</div>
</div>
</form>
<!--Loop -->

</div>
<!--REGISTRATIOM AREA-->

<!--FOOTER SRT-->
<script>
/*	$(function() {
			var uploadDiv = $('#upload_div');
			var i = $('#upload_div #upload_middle_span').size() + 1;
			$('#upload_add').on('click', function() {
					$('<span id="upload_middle_span"><span id="upload_span"><input type="file" name="image'+i+'" /></span><span id="rem_upload"><img src="<?php echo base_url();?>assetts/images/close.png" alt="" /></span></span><br>').appendTo(uploadDiv).slideDown("slow");
					i++;
					return false;
			});
	
			$('#rem_upload').on('click', function() {
			alert('hello');
					if( i > 2 ) {
							$(this).parents('#upload_middle_span').slideUp("slow",function () {
							$(this).remove("slow");
							});
							i--;
					}
					return false;
			});
	});*/

</script>
<script>
  function handleFileSelect1(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area1').innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_1").val('Remove');
		$("#upload_filename1").css("height","0px");
		$("#upload_filename1").css("display","none");
		$("#button_1").attr('onclick','remove_photo(1)');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image+1);

        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect2(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area2').innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_2").val('Remove');
		$("#upload_filename2").css("height","0px");
		$("#upload_filename2").css("display","none");
		$("#button_2").attr('onclick','remove_photo(2)');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image+1);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect3(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area3').innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_3").val('Remove');
		$("#upload_filename3").css("height","0px");
		$("#upload_filename3").css("display","none");
		$("#button_3").attr('onclick','remove_photo(3)');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image+1);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect4(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();
      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area4').innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_4").val('Remove');
		$("#upload_filename4").css("height","0px");
		$("#upload_filename4").css("display","none");
		$("#button_4").attr('onclick','remove_photo(4)');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image+1);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect5(evt) {
  var button_5 = $("#button_5").val();
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area5').innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_5").val('Remove');
		$("#upload_filename5").css("height","0px");
		$("#upload_filename5").css("display","none");
		$("#button_5").attr('onclick','remove_photo(5)');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image+1);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  document.getElementById('upload_filename1').addEventListener('change', handleFileSelect1, false);
  document.getElementById('upload_filename2').addEventListener('change', handleFileSelect2, false);
  document.getElementById('upload_filename3').addEventListener('change', handleFileSelect3, false);
  document.getElementById('upload_filename4').addEventListener('change', handleFileSelect4, false);
  document.getElementById('upload_filename5').addEventListener('change', handleFileSelect5, false);
  function remove_photo(id){
  	$("#upload_filename"+id).val('');
	$("#button_"+id).val('Add a photo');
	$("#upload_filename"+id).css("height","25px");
	$("#upload_filename"+id).css("display","inline");
	$("#button_"+id).attr('onclick','').unbind('click');
	$("#preview-area"+id).html('<img src="<?php echo base_url()?>assetts/images/item.jpg" width="100" border="0"  />');
	var no_edit_image = $("#no_edit_image").val();
	$("#no_edit_image").val(no_edit_image-1);
  }
  function remove_edit_photo(id,aid, image_name){
	$.post("<?php echo base_url();?>users/list_auction/update_list_image",{ auction_id: aid, image_name: image_name}, function(data){
		$("#upload_filename"+id).val('');
		$("#button_"+id).val('Add a photo');
		$("#upload_filename"+id).css("height","25px");
		$("#upload_filename"+id).css("display","inline");
		$("#button_"+id).attr('onclick','').unbind('click');
		$("#preview-area"+id).html('<img src="<?php echo base_url()?>assetts/images/item.jpg" width="100" border="0"  />');
		var no_edit_image = $("#no_edit_image").val();
		$("#no_edit_image").val(no_edit_image-1);
	});	
  }
</script>
<?php $this->load->view('frontend/footer');?>