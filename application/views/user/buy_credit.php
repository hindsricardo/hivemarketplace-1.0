<?php $this->load->view('frontend/header');?>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script src="<?php echo base_url()?>assetts/js/jquery.price_format.1.8.js"></script>
<script type="text/javascript">
function checkcredit(){
	var credit = $("#amount").val();
	if(credit<20){
	alert("You must purchase a minimum of 20 BIDS.");
	return false;
}else
	return true;
}

$(document).ready(function(){
	$("#amount").keyup(function(){
		var credit_val = $("#credit_val").text();
		if(!isNaN($("#amount").val())){
			var amount = $("#amount").val();
			var total_credit = parseFloat(amount*parseFloat(credit_val)).toFixed(2);
			var value = "Value : USD$ "+total_credit;"";
			$("#value").text(value);
			$("#newamount").val(total_credit);
			$('#credit').val(amount);
		}else{
			$("#amount").val('0');
			$("#newamount").val('0');
			var value = "Value : 0.00 (1 credit= "+credit_val+")";
			$("#value").text(value);
			
		}
	});
});
</script>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<div class="Row clearfix marTop10">
	<div class="Row">
		<div class="comm_black_box">BUY BIDS</div>
		<div class="comm_rounded_grey noBorder Row">
			<div class="Row95">
				<div class="div850Center">
				<p class="txt11pN mar10" style="text-align:center;font-style:italic;color:#F5F5F5;font-size:15px;">Worried about getting stuck with useless bids? Don't be! You'll always have the option to auction your bids to potentially earn more than the value or redeem them for 71 cents each!</p>
				</div>
			</div>
		</div>
		<br clear="all" />  
		<form name="buycredit"  id="buycredit" action="<?php echo base_url();?>checkout/add" method="post" onsubmit="return checkcredit();">
			<input type="hidden" name="price" id="newamount" value="" />
            <input type="hidden" name="picture" value="userPi-cs.jpg" />
            <input type="hidden" name="id" value="<?php echo $this->session->userdata('user_id').'-02';?>" />
            <input type="hidden" name="name" value="buy credit" />
            <input type="hidden" name="qty" value="1" />
            <input type="hidden" name="credit" id="credit" value="" />
			<div class="centerDiv w658 clearfix">
				<p align="center" class="padTop20"><img src="<?php echo base_url();?>assetts/images/hivecomb.png" width="300" height="300" border="0"  /></p>
				<br clear="all" />  
				<div class="flt_lft w600">
					<table width="100%" class="comm_table">
					  <tr>
						<td width="42%" nowrap="nowrap" class="txt13B top" style="padding-top:15px;">How many bids do you want?</td>
						   <td width="58%" class="top"><input name="amount" id="amount" value="" type="text" style="width:120px; height:35px; font-size:16px;" />
						   <br />
						<p class="txt12B marTop10">You must buy a minimum of 20 bids</p>
                                                <p style='font-family:verdana; font-size:12px; font-style:italic;color:#939393;text-align:center;'>You must pay for items in checkout before buying bids</p>
							<br />
						<p class="txt16pB padTop20" style="margin-left:50px;"><div id="value" style='font-family:Verdana;font-size:18px;'>Value : 0.00 </div></p>
						   </td>
					  </tr>
					</table>
				</div>
				<br clear="all" />  
				<p align="center">
				<input name="submit" type="submit" value="submit" class="btn_checkOut" style="text-indent:-9999px;" />
				</p>
			</div>
		</form>
	</div>
</div>
<div class="cls"><br clear="all" /></div>
</div>
<!--REGISTRATIOM AREA-->
<? $this->load->view('frontend/footer');?>