<?php $this->load->helper('url'); ?>
<?php $this->load->view('frontend/header');?>
<?php $this->load->view('frontend/slider');?>
<script src="<?php echo base_url()?>assetts/js/jquery.countdown.js"></script>
<script>
window.get_fb_complete = function(aid){
      var data = {aid:aid}
      var feedback = $.ajax({
       type: "POST",
	   data:data,
       url: "<?php echo base_url(); ?>users/manage_credits/check_bids",
       async: false,
	   dataType: JSON,
   }).complete(function(){
       setTimeout(function(){get_fb_complete(aid);}, 10000);
   }).responseText;
   
   	var result = JSON.parse(feedback);	
	var time_day1 = $( "#countdown_"+aid+" .countDays" ).find('.position').first();
	var time_day2 = $( "#countdown_"+aid+" .countDays" ).find('.position').last();
	var time_hr1 = $( "#countdown_"+aid+" .countHours" ).find('.position').first();
	var time_hr2 = $( "#countdown_"+aid+" .countHours" ).find('.position').last();
	var time_min1 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').first();
	var time_min2 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').last();
	var time_sec1 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').first();
	var time_sec2 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').last();
	var total_tm = parseInt(time_day1.text())+parseInt(time_day2.text())+parseInt(time_hr1.text())+parseInt(time_hr2.text())+parseInt(time_min1.text())+parseInt(time_min2.text())+parseInt(time_sec1.text())+parseInt(time_sec2.text());
	if(total_tm > 0){
		//alert(total_tm+" is > 0"+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('<p font-family:Verdana font-size:16px font-weight:bold>NO BIDS YET</p>');
		}
                if(typeof result.winner.winner_username !== "undefined"){$('#buy_in_'+aid).html(parseInt($('#winner_contribution_bid_'+aid).text())+1);}
                else{$('#buy_in_'+aid).html('<p font-family:Verdana font-size:12px>1bid</p>');}
                
                 
                
                
	}else{
	//alert(total_tm+" is < 0"+">>>>>>>>>"+aid+" user name::::::"+result.winner.winner_username);
		if ($('#user_bid_section_'+aid).css('display') == 'block'){
			
			$('#user_bid_section_'+aid).css("display","none");
		}
		//alert(result.winner.winner_username+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('<p font-family:Verdana font-size:16px font-weight:bold>NO BIDS YET</p>');
		}
	}
         if(total_tm == 0){
            $('.countDays,.countDiv0,.countHours,.countDiv1,.countMinutes,.countDiv2,.countSeconds,.timerhead').hide();
            $('.ENDED').show();
        }
		$('#auction_price_'+aid).html((result.bid*0.01).toFixed(2));
		$('#winner_contribution_bid_'+aid).html(result.winner.winner_credit);
		$('#winner_id_'+aid).html(result.winner.winner_user_id);
}
function bid_action(aid){
	var user_id = '<?php echo $this->session->userdata('user_id');?>';
	if(user_id){
		var credit_val = $("#credit_val_"+aid).val();
		if(!isNaN(credit_val)){
			var your_contribution_bid = parseInt($("#your_contribution_bid_"+aid).text());
			//alert(your_contribution_bid);
			var your_total_contribution_bid = parseInt(credit_val)+parseInt(your_contribution_bid);
			var winner_contribution_bid = parseInt($('#winner_contribution_bid_'+aid).text());
			var winner_id = parseInt($('#winner_id_'+aid).text());
			if(winner_id != user_id){
				if(your_total_contribution_bid > winner_contribution_bid){
					var total_credit_available = parseInt($("#total_credit_available").text());
					if(parseInt(credit_val) <= total_credit_available){
						var hive_credit_available = parseInt($("#hive_credit_available").text());
						var bonus_credit_available = parseInt($("#bonus_credit_available").text());
						$.post("<?php echo base_url();?>users/manage_credits/bid",{ auction_id: aid, credit_val: credit_val, bonus_credit_available: bonus_credit_available, hive_credit_available: hive_credit_available, total_credit_available: total_credit_available }, function(data){
							if(data.logout == 'logout'){
								window.location.href = "<?php echo base_url();?>registration";
							}else if(data.not_enough_credit == 'not_enough_credit'){
								alert("Please Enter More Than "+(data.need_credit)+"Bids");
							}else{
								$("#credit_val_"+aid).val('');
								$("#total_credit_available").text(data.total_credit_available);
								$("#hive_credit_available").text(data.hive_credit_available);
								$("#bonus_credit_available").text(data.bonus_credit_available);
								var total_credit_value_available = parseFloat(data.total_credit_available*0.75).toFixed(2);
								$("#total_credit_value_available").text(total_credit_value_available);
								//$("#no_of_bids_"+aid).text(data.auction_bid_no);
								$("#auction_price_"+aid).text(parseFloat(data.auction_bid_no*0.01).toFixed(2));
								
								$("#your_contribution_bid_"+aid).text(data.your_contribution_bid);
								$('#current_winner_'+aid).html(data.winner.winner_username);
								$('#winner_contribution_bid_'+aid).html(data.winner.winner_credit);
								$('#winner_id_'+aid).html(data.winner.winner_user_id);
							}
						},'json');
					}else{
						alert("You do not have enough bids");
					}
				}else{
					var need_credit = (winner_contribution_bid-your_contribution_bid);
					alert("Please Enter More Than "+(need_credit)+"Bids");
				}
			}else{
				alert("You're Winning !!!");
				$("#credit_val_"+aid).val('');
			}
		}
	}else{
		window.location.href = "<?php echo base_url();?>registration";
	}    
        
}
function auction_filter(){		
	var search_str = $.trim($("#search_str").val());
	if(search_str == '' || search_str == 'Search , Live Auction, products and more'){
		search_str = '';
	}
	var auction_sort = $('input[name="auction_sort"]:checked').val();
	if($('input:checkbox[name=price_0_50]').is(':checked')){
		var price_0_50 = '0_50';
	}
	if($('input:checkbox[name=price_50_100]').is(':checked')){
		var price_50_100 = '50_100';
	}
	if($('input:checkbox[name=price_100_500]').is(':checked')){
		var price_100_500 = '100_500';
	}
	if($('input:checkbox[name=price_500]').is(':checked')){
		var price_500 = '500';
	}
	if($('input:checkbox[name=features_no_min_bid]').is(':checked')){
		var features_no_min_bid = 'features_no_min_bid';
	}
	if($('input:checkbox[name=features_credit_auction]').is(':checked')){
		var features_credit_auction = 'features_credit_auction';
	}
	var display_auction_within = $("#display_auction_within").val();
	var miles_of_zipcode = $("#miles_of_zipcode").val();
	
	$.post("<?php echo base_url();?>home/auction_filter",{search_str: search_str, auction_sort: auction_sort, price_0_50: price_0_50, price_50_100: price_50_100, price_100_500: price_100_500, price_500: price_500,features_no_min_bid: features_no_min_bid, features_credit_auction: features_credit_auction, display_auction_within: display_auction_within, miles_of_zipcode:miles_of_zipcode}, function(data){
		$("#middel_content").html(data);
	});
}
function pagination(page,limit){
	var search_str = $.trim($("#search_str").val());
	if(search_str == '' || search_str == 'Search , Live Auction, products and more'){
		search_str = '';
	}
	var auction_sort = $('input[name="auction_sort"]:checked').val();
	if($('input:checkbox[name=price_0_50]').is(':checked')){
		var price_0_50 = '0_50';
	}
	if($('input:checkbox[name=price_50_100]').is(':checked')){
		var price_50_100 = '50_100';
	}
	if($('input:checkbox[name=price_100_500]').is(':checked')){
		var price_100_500 = '100_500';
	}
	if($('input:checkbox[name=price_500]').is(':checked')){
		var price_500 = '500';
	}
	if($('input:checkbox[name=features_no_min_bid]').is(':checked')){
		var features_no_min_bid = 'features_no_min_bid';
	}
	if($('input:checkbox[name=features_credit_auction]').is(':checked')){
		var features_credit_auction = 'features_credit_auction';
	}
	var display_auction_within = $("#display_auction_within").val();
	var miles_of_zipcode = $("#miles_of_zipcode").val();

	$.post("<?php echo base_url();?>home/auction_filter",{page : page, limit : limit, search_str: search_str, auction_sort: auction_sort, price_0_50: price_0_50, price_50_100: price_50_100, price_100_500: price_100_500, price_500: price_500,features_no_min_bid: features_no_min_bid, features_credit_auction: features_credit_auction, display_auction_within: display_auction_within, miles_of_zipcode:miles_of_zipcode}, function(data){
		$("#middel_content").html(data);
	});
}

</script>
<style>
.countdownHolder{
	margin:0 auto;
	/*font: 40px/1.5 'Open Sans Condensed',sans-serif;*/
	text-align:center;
	letter-spacing:-3px;
}
.position{
	display: inline-block;
	height: 21px;
	overflow: hidden;
	position: relative;
	width: 15px;
}
.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}
.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}
/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{ /* display:none !important;*/ }
.countDiv0{ /* display:none !important;*/ }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}
.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}
.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}
.countDiv:after{
	top:0.9em;
}
</style>
<!--Filter Area -->
<div class="flt_lft Row liveAunctionWrap">
<div class="top">
<div class="flt_lft">
<h2>Auctions</h2>
</div>
<div class="filter"><img src="<?php echo base_url();?>assetts/images/filter.png" id="clickFilter" width="99" height="28" border="0" />

<div class="filterDiv" id="filter">
<form action="" id="filter_form" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="8">
   <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td class="bold" colspan="2">Price</td>
  </tr>
  <tr>
    <td width="12%"><input name="price_0_50" type="checkbox" value="0_50" /></td>
     <td width="88%">$0-$50</td>
  </tr>
  
  <tr>
    <td><input name="price_50_100" type="checkbox" value="50_100" /></td>
     <td>$50-$100</td>
  </tr>
  <tr>
    <td><input name="price_100_500" type="checkbox" value="100_500" /></td>
     <td>$100-$500</td>
  </tr>
  <tr>
    <td><input name="price_500" type="checkbox" value="500" /></td>
     <td>$500+</td>
  </tr>
</table>
    </td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td class="bold" colspan="2">Features</td>
  </tr>
  <tr>
    <td width="12%"><input name="features_no_min_bid" type="checkbox" value="" /></td>
     <td width="88%">No Minimum Bid</td>
  </tr>
  <tr>
    <td><input name="features_credit_auction" type="checkbox" value="" /></td>
     <td>Credit Auctions</td>
  </tr>
</table>
    </td>
  </tr>
</table>
    </td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="35%">Display auctions within</td>
    <td width="22%">
    <select name="display_auction_within" id="display_auction_within">
		<option value="10">10</option>
		<option value="20">20</option>
		<option value="30">30</option>
		<option value="40">40</option>
		<option value="50" selected="selected">50</option>
		<option value="60">60</option>
		<option value="70">70</option>
		<option value="80">80</option>
		<option value="90">90</option>
		<option value="100">100</option>
    </select>
    </td>
    <td width="19%">Miles of Zipcode</td>
    <td width="24%"><input name="miles_of_zipcode" id="miles_of_zipcode" style="width:50px;" class="frm_inp" type="text" /></td>
  </tr>
</table>
    </td>
  </tr>
  <tr>
    <td class="right" colspan="2"><input id="filter_go" type="button" class="btn_go" onclick="auction_filter();" /></td>
  </tr>
</table>
</form>
</div>
</div>
<?php  ?>
<div class="shortby"><img src="<?php echo base_url();?>assetts/images/short_by.png" id="clickSort" width="186" height="28" border="0" />
<div class="SortDiv" id="sort">
<form action="<?php echo base_url();?>" id="sort_form" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="8">
  <tr>
    <td width="4%"><input  type="radio" name="auction_sort" value="ending_soon" onclick="auction_filter()" /></td>
    <td width="96%">Ending Soon</td>
  </tr>
    <tr>
    <td><input type="radio" name="auction_sort" value="hightest_value" onclick="auction_filter()"/></td>
    <td>Hightest Value</td>
  </tr>
   <tr>
    <td><input  type="radio" name="auction_sort" value="lowest_value" onclick="auction_filter()"/></td>
    <td>Lowest Value</td>
  </tr>
  <tr>
    <td class="right" colspan="2"><!--<input name="" value="" id="filter_go" type="submit" class="btn_go" />--></td>
  </tr>
</table>
</form>
</div>
</div>
</div>
</div>
<!--LIVE AUCTION AREA -->
<div id="middel_content">
<?php $this->load->view('user/home_bk');?>
</div>
<script>
$(document).ready(function(){
	var credit_val = $("#credit_val").text();
	var aids = $("#auction_ids").val();
	var aids_array = aids.split(',');
	for(i=0;i<aids_array.length;i++){
		if(aids_array[i] != ''){
			get_fb_complete(aids_array[i]);
		}
		
	}
});

</script>
<!--BID NOW AREA END-->
<?php $this->load->view('frontend/footer');?>