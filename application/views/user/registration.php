<?php $this->load->helper('url'); ?>
<?php $this->load->view('frontend/header');?>
<?php $this->load->view('frontend/slider');?>
<script>
$("#agreement").click(function(event) 
{
  $(".dialog").dialog(
  {
     autoOpen: false,
    show: 'slide',
    resizable: false,
    position: 'center',
    stack: true,
    height: 'auto',
    width: '600',
    modal: true 
      }
  
);});

</script>
<div class="flt_lft Row liveAunctionWrap">
<div class="top"><div class="flt_lft"><h2>User Registration</h2></div>
</div></div>

<!--REGISTRATIOM AREA-->
<div class="Row borderbox">

<div class="regiLeft flt_lft">
<div class="regiLeftUserPicsBox">
<p>You're photo goes here<div id="user_upload_pic"></div></p>
</div>

</div>

<div class="commFrom_box flt_lft">
<div style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color:#FF0000">
<?php echo validation_errors('<p class="error">'); ?>
</div>
<!--<form action="registration/save" method="post">-->
<?php echo form_open_multipart("registration/save"); ?>
<table width="100%" border="0" style="border-collapse:collapse;" bordercolor="#0d0d0d" cellspacing="0" cellpadding="5">

<tr class="TopRow">
    <td  class="left"colspan="2" >User Registration</td>
  </tr>
  
  <tr>
    <td class="left" colspan="2"><span class="req">* Mandatory Fields</span></td>
  </tr>

  <tr>
    <td width="27%">User Name <span class="req">*</span></td>
    <td class="left" width="73%"><input name="username" id="usename" class="frm_inp" type="text" /></td>
  </tr>
  
  <tr>
    <td>Password <span class="req">*</span></td>
    <td class="left"><input name="pass" id="pass" class="frm_inp" type="password" />
    <div id="complexity" class="default">Enter secure password</div>
    </td>
  </tr>
  
  <tr>
    <td>Re-enter Password <span class="req">*</span></td>
    <td class="left"><input name="confirmpass" id="confirmpass" class="frm_inp" type="password" /></td>
  </tr>
  
  <tr>
    <td>First Name <span class="req">*</span></td>
    <td class="left"><input name="firstname" id="firstname" class="frm_inp" type="text" /></td>
  </tr>
  
   <tr>
    <td>Last Name <span class="req">*</span></td>
    <td class="left"><input name="lastname" id="lastname"  class="frm_inp" type="text" /></td>
  </tr>
  
    <tr>
    <td>Email <span class="req">*</span></td>
    <td class="left"><input name="email" id="email"  class="frm_inp" type="text" /></td>
  </tr> 
  
   <tr>
    <td>
	PayPal Email <span class="req" id="showPaypal" style="text-decoration:underline">(Don't have PayPal?)</span>
	</td>
    <td class="left"><div class="divPayPal">
	If you do not have a PayPal account you can always set one up later. Payments will be sent to your registered email. You will have to create and verify your PayPal account with that email to collect your Payments."
	</div><input name="paypal" id="paypal"  class="frm_inp" type="text" /></td>
  </tr>
  <tr>
    <td>Zip <span class="req">*</span></td>
    <td class="left"><input name="zipcode" id="zipcode" class="frm_inp" type="text" /></td>
  </tr>
  <tr>
    <td>Upload Image</td>
    <td class="left"><input name="profilepic" type="file" id="profilepic" /></td>
  </tr>
   <tr>
    <?php $timezonesArray = $this->config->item("timezones");?>
    <td>Timezone <span class="req">*</span></td>
    <td class="left">
        <select name="timezone" id="user_country" >
            <?php  foreach($timezonesArray as $key=>$val){ ?>
                    <option value="<?php echo $key;?>"><?php echo $key;?></option>
                <?php }
              ?>
        </select>
  </tr>
   <tr>
    <td  colspan="2" align="center" valign="middle" class="center">
        <br>
         <textarea name='User Agreement' title='User Agreement' readonly='readonly' style='width:540px;;max-width:540px;min-width:540px;max-height:110px;max-height:110px;min-height:110px;border:1px solid #cccccc;border-radius:5px;box-shadow:-1px -1px -1px #cccccc;padding:20px;'> 
      User Agreement Terms and Conditions HIVE Marketplace, LLC


In using this website you are deemed to have read and agreed to the following terms and conditions:


The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We” and "Us", refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing English Law. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.


Privacy Statement:
We are committed to protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible service to our customers. Parliament has created specific offences for unauthorised actions against computer systems and data. We will investigate any such actions with a view to prosecuting and/or taking civil proceedings to recover damages against those responsible


Confidentiality:
We are registered under the Data Protection Act 1998 and as such, any information concerning the Client and their respective Client Records may be passed to third parties. However, Client records are regarded as confidential and therefore will not be divulged to any third party, other than [our manufacturer/supplier(s) and] if legally required to do so to the appropriate authorities. Clients have the right to request sight of, and copies of any and all Client Records we keep, on the proviso that we are given reasonable notice of such a request. Clients are requested to retain copies of any literature issued in relation to the provision of our services. Where appropriate, we shall issue Client’s with appropriate written information, handouts or copies of records as part of an agreed contract, for the benefit of both parties.

We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any emails sent by this Company will only be in connection with the provision of agreed services and products.


Disclaimer:
Exclusions and Limitations 
The information on this web site is provided on an "as is" basis. To the fullest extent permitted by law, this Company:
•	excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or omissions in this website and/or the Company’s literature; and 
•	excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages. 
This Company does not however exclude liability for death or personal injury caused by its negligence. The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer are affected. 


Payment:
We accept no payments for goods. All products sold on HIVE Marketplace are contractual obligations between the buyer and the seller. HIVE Marketplace accepts payments only for the hosting services provided to users. Acceptable payment methods are to be determined by HIVE Marketplace and can only take place through the website. Exceptions to these conditions are legal suits resulting in court ordered payments to HIVE Marketplace. 

Purchase of a bid:
The purchase of an online credit being called a bid by HIVE Marketplace does not constitute a sale of a product of service. The purchase of credits is a deposit of funds. These deposits are subject to fees assessed by HIVE Marketplace and therefore the deposited amount is not guaranteed to the user. The financial value of each bid is to be determined and published by HIVE Marketplace. The fees assessed on each deposit are only 





Cancellation Policy:
User accounts are not deleted, closed, or cancelled. Your email can only be used for one account. User accounts can only be rendered inactive at our discretion. 


Termination of Agreements and Refunds Policy:
Both the Client and ourselves have the right to terminate any Services Agreement for any reason, including the ending of services that are already underway. No refunds shall be offered, where a Service is deemed to have begun and is, for all intents and purposes, underway. Any monies that have been paid to us which constitute payment in respect of the provision of unused Services, shall be refunded.


Availability:
Unless otherwise stated, the services featured on this website are only available within the United States, or in relation to postings from the United States. All advertising is intended solely for the United Kingdom market. You are solely responsible for evaluating the fitness for a particular purpose of any downloads, programs and text available through this site. Redistribution or republication of any part of this site or its content is prohibited, including such by framing or other similar or any other means, without the express written consent of the Company. The Company does not warrant that the service from this site will be uninterrupted, timely or error free, although it is provided to the best ability. By using this service you thereby indemnify this Company, its employees, agents and affiliates against any loss or damage, in whatever manner, howsoever caused.


Log Files:
We use IP addresses to analyse trends, administer the site, track user’s movement, and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information. Additionally, for systems administration, detecting usage patterns and troubleshooting purposes, our web servers automatically log standard access information including browser type, access times/open mail, URL requested, and referral URL. This information is not shared with third parties and is used only within this Company on a need-to-know basis. Any individually identifiable information related to this data will never be used in any way different to that stated above without your explicit permission.


Cookies:
Like most interactive web sites this Company’s website [or ISP] uses cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate partners may also use cookies. [If you do not use cookies, delete this clause]


Links to this website:
You may not create a link to any page of this website without our prior written consent. If you do create a link to a page of this website you do so at your own risk and the exclusions and limitations set out above will apply to your use of this website by linking to it.


Links from this website:
We do not monitor or review the content of other party’s websites which are linked to from this website. Opinions expressed or material appearing on such websites are not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site & to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.


Copyright Notice:
Copyright and other relevant intellectual property rights exists on all text relating to the Company’s services and the full content of this website. 

This Company’s logo is a registered trademark of this Company in the United Kingdom and other countries. The brand names and specific services of this Company featured on this web site are trade marked [delete this paragraphed clause if no registered trademark exists]. 




Communication:
We have several different e-mail addresses for different queries. These, & other contact information, can be found on our Contact Us link on our website or via Company literature or via the Company’s stated telephone, facsimile or mobile telephone numbers.

This company is registered in England and Wales, Number 11111111, registered office 123 Any Street, Anytown AB2 3CD.  [Only need to state this if Limited Company, otherwise proprietors’/partners’ home/trading address must be shown, without use of the term: registered]


Force Majeure:
Neither party shall be liable to the other for any failure to perform any obligation under any Agreement which is due to an event beyond the control of such party including but not limited to any Act of God, terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or man made eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavours to comply with the terms and conditions of any Agreement contained herein.


Waiver:
Failure of either Party to insist upon strict performance of any provision of this or any Agreement or the failure of either Party to exercise any right or remedy to which it, he or they are entitled hereunder shall not constitute a waiver thereof and shall not cause a diminution of the obligations under this or any Agreement. No waiver of any of the provisions of this or any Agreement shall be effective unless it is expressly stated to be such and signed by both Parties.


General:
The laws of England and Wales govern these terms and conditions. By accessing this website [and using our services/buying our products] you consent to these terms and conditions and to the exclusive jurisdiction of the English courts in all disputes arising out of such access. If any of these terms are deemed invalid or unenforceable for any reason (including, but not limited to the exclusions and limitations set out above), then the invalid or unenforceable provision will be severed from these terms and the remaining terms will continue to apply. Failure of the Company to enforce any of the provisions set out in these Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be construed as waiver of such provisions and shall not affect the validity of these Terms and Conditions or of any Agreement or any part thereof, or the right thereafter to enforce each and every provision. These Terms and Conditions shall not be amended, modified, varied or supplemented except in writing and signed by duly authorised representatives of the Company.


Notification of Changes:
The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. If there are any changes to our privacy policy, we will announce that these changes have been made on our home page and on other key pages on our site. If there are any changes in how we use our site customers’ Personally Identifiable Information, notification by e-mail or postal mail will be made to those affected by this change. Any changes to our privacy policy will be posted on our web site 30 days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis


These terms and conditions form part of the Agreement between the Client and ourselves. Your accessing of this website and/or undertaking of a booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer Notice and the full Terms and Conditions contained herein. Your statutory Consumer Rights are unaffected.    

© HIVE MARKETPLACE, LLC 2014 All Rights Reserved


         </textarea>
        <br>
        <br>
        <div><input type="checkbox" name="accept_terms" value="yes"/> <span style='font-weight:bold;'>I Accept User Agreement</span>
            </div>
       
        
        <br>
        <br>
    <div class="flt_lft clearfix" style="margin-left:250px;">
    <input name="submit" style="float:left;" type="submit" value="" class="btn_submit" /><input name="" type="reset" value="" style="float:left; margin-left:10px;" class="btn_reset" />
    </div>
    </td>
  </tr>
</table>
<!--</form>-->
 <?php echo form_close(); ?>
</div>

</div>

<script>
function handleFileSelect1(evt) {

    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
		  
          span.innerHTML = ['<img height="286" width="300" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('user_upload_pic').innerHTML = ['<img height="286" width="300" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  document.getElementById('profilepic').addEventListener('change', handleFileSelect1, false);

</script>

<!--REGISTRATIOM AREA-->
<?php $this->load->view('frontend/footer');?>
