<?php $this->load->view('frontend/header');?>
<script src="<?php echo base_url()?>assetts/js/jquery.ad-gallery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo base_url()?>assetts/js/jquery.countdown.js"></script>
<script type="text/javascript">
  $(function() {
    var galleries = $('.ad-gallery').adGallery();
    $('#switch-effect').change(
      function() {
        galleries[0].settings.effect = $(this).val();
        return false;
      }
    );
    $('#toggle-slideshow').click(
      function() {
        galleries[0].slideshow.toggle();
        return false;
      }
    );
    $('#toggle-description').click(
      function() {
        if(!galleries[0].settings.description_wrapper) {
          galleries[0].settings.description_wrapper = $('#descriptions');
        } else {
          galleries[0].settings.description_wrapper = false;
        }
        return false;
      }
    );
		//var aids = $("#auction_ids").val();
		//get_fb_complete(aids);
  });
  
  window.get_fb_complete = function(aid){
      var data = {aid:aid}
      var feedback = $.ajax({
       type: "POST",
	   data:data,
       url: "<?php echo base_url(); ?>users/manage_credits/check_bids",
       async: false
   }).complete(function(){
       setTimeout(function(){get_fb_complete(aid);}, 10000);
   }).responseText;
   var result = JSON.parse(feedback);
   //alert(result.bid);
   
   
	var time_day1 = $( "#countdown_"+aid+" .countDays" ).find('.position').first();
	var time_day2 = $( "#countdown_"+aid+" .countDays" ).find('.position').last();
	var time_hr1 = $( "#countdown_"+aid+" .countHours" ).find('.position').first();
	var time_hr2 = $( "#countdown_"+aid+" .countHours" ).find('.position').last();
	var time_min1 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').first();
	var time_min2 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').last();
	var time_sec1 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').first();
	var time_sec2 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').last();
	var total_tm = parseInt(time_day1.text())+parseInt(time_day2.text())+parseInt(time_hr1.text())+parseInt(time_hr2.text())+parseInt(time_min1.text())+parseInt(time_min2.text())+parseInt(time_sec1.text())+parseInt(time_sec2.text());
	if(total_tm > 0){
		//alert(total_tm+" is > 0"+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('NO BIDS YET');
		}
                if(typeof result.winner.winner_username !== "undefined"){$('#buy_in_'+aid).html(parseInt($('#winner_contribution_bid_'+aid).text())+1);}
                else{$('#buy_in_'+aid).html('<p font-family:Verdana>1</p>');}
	}else{
	//alert(total_tm+" is < 0"+">>>>>>>>>"+aid+" user name::::::"+result.winner.winner_username);
		if ($('#user_bid_section_'+aid).css('display') == 'block'){
			
			$('#user_bid_section_'+aid).css("display","none");
		}
		//alert(result.winner.winner_username+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('NO BIDS YET');
		}
	}
        if(total_tm == 0){
            $('.countDays,.countDiv0,.countHours,.countDiv1,.countMinutes,.countDiv2,.countSeconds,.timerhead').hide();
            $('.ENDED').show();
        }
   
	   $('#no_of_bids_'+aid).html(result.bid);
	   $('#auction_price_'+aid).html((result.bid*0.01).toFixed(2));
	   $('#auction_price_2nd_'+aid).html((result.bid*0.01).toFixed(2));
	   $("#you_have_spend_"+aid).text(parseFloat(result.your_contribution*0.75).toFixed(2));
	   $("#you_have_spend_2_"+aid).text(parseFloat(result.your_contribution*0.75).toFixed(2));
	   $("#your_contribution_bid_"+aid).text(result.your_contribution);
	   //var you_have_spend = parseFloat($("#you_have_spend_"+aid).text());
	   var item_value = parseFloat($("#item_value_"+aid).text());
	   
		var you_save = (item_value - (parseFloat(result.your_contribution*0.75).toFixed(2)));
		$("#you_save_"+aid).text(you_save);
		$("#save_persentage_"+aid).text(((you_save/item_value)*100).toFixed(2));
		
		//$('#current_winner_'+aid).html(result.winner.winner_username);
		$('#winner_contribution_bid_'+aid).html(result.winner.winner_credit);
		$('#winner_id_'+aid).html(result.winner.winner_user_id);
		$.post("<?php echo base_url();?>users/item/bidding_history",{ auction_id: aid}, function(data){
			$('.biddingHistoryDetails').html(data);
		});

}
function bid_action(aid){
	user_id = '<?php echo $this->session->userdata('user_id');?>';
	if(user_id){
		var credit_val = $("#credit_val_"+aid).val();
		if(!isNaN(credit_val)){
			var your_contribution_bid = parseInt($("#your_contribution_bid_"+aid).text());
			var your_total_contribution_bid = parseInt(credit_val)+parseInt(your_contribution_bid);
			var winner_contribution_bid = parseInt($('#winner_contribution_bid_'+aid).text());
			var winner_id = parseInt($('#winner_id_'+aid).text());
			
			if(winner_id != user_id){
				if(your_total_contribution_bid > winner_contribution_bid){
				var total_credit_available = parseInt($("#total_credit_available").text());
				if(parseInt(credit_val) <= total_credit_available){
					var hive_credit_available = parseInt($("#hive_credit_available").text());
					var bonus_credit_available = parseInt($("#bonus_credit_available").text());
					$.post("<?php echo base_url();?>users/manage_credits/bid",{ auction_id: aid, credit_val: credit_val, bonus_credit_available: bonus_credit_available, hive_credit_available: hive_credit_available, total_credit_available: total_credit_available }, function(data){
					if(data.logout == 'logout'){
							window.location.href = "<?php echo base_url();?>registration";
						}else{
							$("#credit_val_"+aid).val('');
							$("#total_credit_available").text(data.total_credit_available);
							$("#hive_credit_available").text(data.hive_credit_available);
							$("#bonus_credit_available").text(data.bonus_credit_available);
							var total_credit_value_available = parseFloat(data.total_credit_available*0.75).toFixed(2);
							$("#total_credit_value_available").text(total_credit_value_available);
							$("#you_have_spend_"+aid).text(parseFloat(data.your_contribution_bid*0.75).toFixed(2));
							$("#you_have_spend_2_"+aid).text(parseFloat(data.your_contribution_bid*0.75).toFixed(2));
							var item_value = parseFloat($("#item_value_"+aid).text());
							var you_save = (item_value - (parseFloat(data.your_contribution_bid*0.75).toFixed(2)));
							$("#you_save_"+aid).text(you_save);
							$("#save_persentage_"+aid).text(((you_save/item_value)*100).toFixed(2));
							
							$("#your_contribution_bid_"+aid).text(data.your_contribution_bid);
							$('#current_winner_'+aid).html(data.winner.winner_username);
							$('#winner_contribution_bid_'+aid).html(data.winner.winner_credit);
							$('#winner_id_'+aid).html(data.winner.winner_user_id);
						}
					},'json');
				}else{
					alert("You don't have enough bids");
				}
				}else{
					var need_credit = (winner_contribution_bid-your_contribution_bid);
					alert("Please Enter More Than "+(need_credit)+"Bids");
				}
			}else{
				alert("You're Winning !!!");
				$("#credit_val_"+aid).val('');
			}
		
		}
	}else{
		window.location.href = "<?php echo base_url();?>registration";
	}
}
function check_watchlist(aid){
	$.post("<?php echo base_url();?>users/activity/update_watchlist",{ auction_id: aid}, function(data){
	});
}
function tag_search(tag){
	$("#tag_search_field").val(tag);
	$("#tag_search_form").submit();
}
  </script>
  <style>
.countdownHolder{
	margin:0 auto;
	/*font: 40px/1.5 'Open Sans Condensed',sans-serif;*/
	text-align:center;
	letter-spacing:-3px;
}

.position{
	display: inline-block;
	height: 21px;
	overflow: hidden;
	position: relative;
	width: 15px;
}

.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}

.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}

/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{ /* display:none !important;*/ }
.countDiv0{ /* display:none !important;*/ }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}


.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}

.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}

.countDiv:after{
	top:0.9em;
}
</style>
<div style="display:none">
<form name="tag_search_form" id="tag_search_form" method="post" action="<?php echo base_url();?>home/search">
	<input type="text" name="search_str" id="tag_search_field" />
</form>
</div>
<?php //echo '<pre>';print_r($auction_details);?>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<p class="tags txt13B"><?php echo 'List #'.$auction_details[0]['id']?></p>
<div class="Row flt_lft">
<p class="auctionTitle_b"><?php echo $auction_details[0]['title']?></p>
<?php $tags = explode(" ",$auction_details[0]['tags']); //echo '';print_r($tags);?>
<p class="tags txt13B">Tags <?php foreach($tags as $tags_val){ ?> <a href="javascript:void(0)" class="lnk12NU" onclick='tag_search("<?php echo $tags_val;?>")'><?php echo $tags_val;?></a><?php }?></p>
</div>
<br clear="all" />
<div class="flt_right watchList">
	<p>
	<?php if(is_watchlist($auction_details[0]['id'], $this->session->userdata('user_id')) == true){ ?>
	<span><a class="watchlist_<?php echo $auction_details[0]['id']?>" href="javascript:void(0);" onclick="check_watchlist('<?php echo $auction_details[0]['id']?>');"><img src="<?php echo base_url()?>assetts/images/watchlist_icon_on.png" width="35" height="20" border="0" align="absmiddle" /></a></span>
	<?php }else{ ?>
	<span><a class="watchlist_<?php echo $auction_details[0]['id']?>" href="javascript:void(0);" onclick="check_watchlist('<?php echo $auction_details[0]['id']?>');"><img src="<?php echo base_url()?>assetts/images/watchlist_icon.png" width="35" height="20" border="0" align="absmiddle" /></a></span>
	<?php }?>
        
        &nbsp;<span class="txt13B">Watch List</span>
	</p>
 

</div>
<br clear="all" />
<div class="Row">
<div class="itemLft flt_lft">
<div class="commBorderBox marTop10">
<div id="gallery" class="ad-gallery mar10">
      <div class="ad-image-wrapper">
      </div>
      <div class="ad-controls">
      </div>
      <div class="ad-nav">
		<div class="ad-thumbs">
			<ul class="ad-thumb-list">
				<?php foreach($auction_details[0]['images'] as $auction_images){ ?>
				<li>
					<a href="<?php echo base_url()?>upload/<?php echo $auction_images['image_name']?>">
						<img src="<?php echo base_url()?>upload/<?php echo $auction_images['image_name']?>" height="60" class="image1">
					</a>
				</li>
				<?php }?>
			</ul>
		</div>
      </div>
    </div>
<!--<p class="mar10"><img src="<?php echo base_url()?>assetts/images/item.jpg" width="333" height="327" border="0" /></p>-->
</div>
</div>
<?php
$current_winner_details = get_current_winner($auction_details[0]['id']);
//echo '<pre>';print_r($auction_details);
?>
<div class="flt_right itemRight commBorderBox">
<div class="auctionList flt_lft">
<div id="no_of_bids_<?php echo $auction_details[0]['id']?>" style="display:none"><?php echo $auction_details[0]['no_of_bids']?></div>
<p class=" txtalignCenter marTop10" style="float:left;"></p><span id="buy_in_<?php echo $auction_details[0]['id']?>" style="margin-left:110px;color:#4ABB38;font-weight:bold;float:left;"></span><span style="font-weight:bold;">bid buy-in</span>
<p class="txt11pN txtalignCenter marTop5">Minimum Auction Price $<?php echo $auction_details[0]['min_bids']*$this->config->item("pennies");?></p>
<p class="red22B txtalignCenter marTop10">
										<div id="countdown_<?php echo $auction_details[0]['id']?>"></div>
                                                                                <h7 class="timerhead" style="word-spacing:8px;">days:       hours:       mins:       secs</h7>
                                                                                <h4 class="ENDED" style="word-spacing:8px;">ENDED</h4>
										<input type="hidden" name="auction_last_for" id="auction_last_for_<?php echo $auction_details[0]['id']?>" value="<?php echo $auction_details[0]['schedule_endtime'];?>" />
										<input type="hidden" name="schedule_endtime" id="schedule_endtime_<?php echo $auction_details[0]['id']?>" value="<?php echo get_adjust_time($auction_details[0]['schedule_endtime']);?>" />
</p>
<p class="txtalignCenter marTop10"><?php if($this->session->userdata('photo') == NULL){ ?>
                                <img src="<?php echo base_url()?>assetts/images/User_thumb.png" width="57" height="57" border="0" title="User Name" />    
                                <?php } else{ ?>  <img src="<?php echo $this->session->userdata('photo')?> " width="57" height="57" border="0" title="User Name" style="border-radius:5px;"/><?php } ?>
                                        
    
    
</p>
<p class="txtalignCenter txt11pN"><div style="font-family:verdana;font-size:16px; display:block; text-align:center;" id="current_winner_<?php echo $auction_details[0]['id']?>"><?php if(!empty($current_winner_details)){ echo $current_winner_details['winner_username'];}else{ echo 'User Name';}?></div></p>
<p class="txtalignCenter marTop10">
<div style="margin-left:72px;" id="user_bid_section_<?php echo $auction_details[0]['id']?>">
	<form class="bid_form" action="" id="b_<?php echo $auction_details[0]['id']?>">
		<input type="hidden" name="aid" id="auction_ids" value="<?php echo $auction_details[0]['id']?>"/>
		<?php if($auction_details[0]['user_id'] != $this->session->userdata('user_id')){?>
		<input style ="float:left;font-size:16px;text-align:center;border-radius:5px;"type="text" name="credit" id="credit_val_<?php echo $auction_details[0]['id']?>" class="credit_val" value="" />
		<input type="button" class="bid_button" id="bid_button_<?php echo $auction_details[0]['id']?>" onclick="bid_action('<?php echo $auction_details[0]['id']?>')" />
		<?php }?>
	</form>
</div>
</p>
<div class="tableAuctionPrice flt_lft">
<table width="100%" class="txt11pN"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Auction Price</td>
    <td class="right"><div style="float:left">$</div><div style="float:left" id="auction_price_2nd_<?php echo $auction_details[0]['id']?>"><?php echo $auction_details[0]['no_of_bids']*$this->config->item("pennies");?></div></td>
  </tr>
   <tr>
	<?php $user_bid_details = $this->session->userdata('bid_details');
	if(isset($user_bid_details['bid_hive'][$auction_details[0]['id']])){
		$user_bid_hive = $user_bid_details['bid_hive'][$auction_details[0]['id']];
	}else{
		$user_bid_hive = 0;
	}
	if(isset($user_bid_details['bid_bonus'][$auction_details[0]['id']])){
		$user_bid_bonus = $user_bid_details['bid_bonus'][$auction_details[0]['id']];
	}else{
		$user_bid_bonus =0;
	}
	 ?>
    <td>You've Spent</td>
    <td class="right">
	
	<div id="winner_contribution_bid_<?php echo $auction_details[0]['id']?>" style="display:none"><?php if(!empty($current_winner_details)){ echo $current_winner_details['winner_credit'];}else{ echo '0';}?></div>
	<div id="winner_id_<?php echo $auction_details[0]['id']?>" style="display:none"><?php echo $current_winner_details['winner_user_id'];?></div>
	<div id="your_contribution_bid_<?php echo $auction_details[0]['id']?>" style="display:none;"><?php echo $user_bid_hive+$user_bid_bonus;?></div>
	<div style="float:left">$</div><div style="float:left" id="you_have_spend_<?php echo $auction_details[0]['id']?>"><?php echo $this->config->item("credit_val")*($user_bid_hive+$user_bid_bonus);?></div></td>
  </tr>
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
    <tr>
    <td>QYT</td>
    <td class="right"><div style="float:left"></div><div style="float:left"><?php echo $auction_details[0]['qty']?></div></td>
  </tr>
     <tr>
    <td>Item(s) value</td>
    <td class="right"><div style="float:left">$</div><div style="float:left" id="item_value_<?php echo $auction_details[0]['id']?>"><?php echo $auction_details[0]['item_value'];?></div></td>
  </tr>
    <tr>
    <td colspan="2"><hr /></td>
  </tr>
   <tr>
    <td>If you win you save</td>
    <td class="right"><div style="float:left">$</div><div style="float:left" id="you_save_<?php echo $auction_details[0]['id']?>"><?php echo $you_save = number_format(($auction_details[0]['item_value'] - ($this->config->item("credit_val")*($user_bid_hive+$user_bid_bonus))), 2, '.', '');?></div></td>
  </tr>
   <tr style="font-family:verdana; font-size:14px;">
    <td class="center" colspan="2">Saving &nbsp;&nbsp; <span class="green28B" id="save_persentage_<?php echo $auction_details[0]['id']?>"><?php echo number_format(($you_save/$auction_details[0]['item_value'])*100, 2, '.', '')?></span>%</td>
  </tr>
</table>
</div>
</div>
<div class="biddingHistory flt_lft" style="border-radius:10px;">
<div class="bidHistoryHeader">Bidding History</div>
<div class="flt_lft Row biddingHistoryDetails">
	<table width="100%" border="0" class="txtalignCenter txt11pN" cellspacing="0" cellpadding="0">
	<?php
	$i= 1;
	foreach($bidding_history as $bidding_history_val){
	?>
	  <tr <?php if($i%2 != 0) echo 'class="odd"';?>>
		<td><?php if($this->session->userdata('photo') == NULL){ ?>
                                <img src="<?php echo base_url();?>assetts/images/icon_user.png" width="30" height="30" align="absmiddle"/>    
                                <?php } else{ ?>  <img src="<?php echo $this->session->userdata('photo')?> " width="30" height="30" align="absmiddle" style="border-radius:5px;"/><?php } ?>
                                        
                
                </td>
		<td><?php echo get_user_username($bidding_history_val['user_id'])?></td>
		<td><?php echo $bidding_history_val['credits']?></td>
                <!-- add bids for multiple and bid for single -->
                <td>Bid</td>
	  </tr>
	 <?php $i++; }?>
	</table>
</div>
</div>
</div>
</div>
<div class="cls"><br clear="all" /></div>
<div class="Row98">
<p class="txt13pB"> <span style="color:#0066cc; font-size:14px;"><?php echo $auction_details[0]['remaining_qty'];?></span> are available for purchase after the auction ends at the value of <span class="txt13pB_or"><span style="color:#0066cc; font-size:14px;">$<?php echo $auction_details[0]['item_value'];?></span></span> minus what you've spent <span class="txt13pB_or"><span style="color:#0066cc; font-size:14px;">$<span id="you_have_spend_2_<?php echo $auction_details[0]['id']?>"><?php echo $this->config->item("credit_val")*($user_bid_hive+$user_bid_bonus);?></span></span></span></p>
</div>
<div class="cls"><br clear="all" /></div>
<div class="Row98">
<p class="txt13B">Description:</p>
<p class="txt11pN marTop10 marRlt10" style="background-color:#D6E6FF;border:2px #3A3A3A;border-radius:15px;padding-top:20px;padding-bottom:20px;padding-left:65px;padding-right:15px;text-align:left;font-size:14px;">
    <span>Condition:<?php echo $auction_details[0]['item_condition']?></span><br><br><?php echo nl2br($auction_details[0]['description']);?>
</p>
</div>
<div class="cls"><br clear="all" /></div>
<div class="Row98" >
<div class="Row_borBot">
<p class="txt13B marTop10">Shipping Carrier : <span class="txt11pN"><?php if($auction_details[0]['us_company_name'] ==''){echo 'In-store/In-person pickup only'; }else{ echo $auction_details[0]['us_company_name'];} ?></span></p>   
<p class="txt13B marTop10">Shipping Costs : <span class="txt11pN"><?php if($auction_details[0]['us_company_name'] ==''){echo 'N/A'; }else{ echo '$'. ($auction_details[0]['us_paytype'] == ''? $auction_details[0]['us_price']: 'Free');}?></span></p>
<p class="txt13B marTop10">Estimated Delivery : <span class="txt11pN"><?php if($auction_details[0]['us_company_name'] ==''){echo 'N/A'; }else{ echo $auction_details[0]['us_handling_time'];};?> day(s) </span></p>
</div>
<div class="Row_borBot">

<p class="txt13B marTop10">International Shipping To : <span class="txt11pN"><?php if($auction_details[0]['ww_handling_time'] ==''){echo 'No international shipping';}else{echo $auction_details[0]['ww_handling_time'];}?> </span></p>
</div>
<p class="txt13B marTop10">In-store/In-person Pickup and Payment : <span class="txt11pN"><?php if(!$auction_details[0]['payat_pickup']){ echo 'No';}elseif($auction_details[0]['payat_pickup']){ echo 'Yes';}?></span></p>
<p class="txt13B marTop10">Return Policy : <span class="txt11pN"><?php  echo '<span style="font-style:italics;">All auctions are covered by the HIVE Marketplace return guarantee. Just open a case and allow the seller the opportunity to resolve the issue. If you are not able to meet an agreement with the seller, HIVE Marketplace will make buyers whole for all valid refund requests.</span> '?></span></p>
</div>
<div class="cls"><br clear="all" /></div>
</div>

<script>
$(function(){
	var auction_ids = $("#auction_ids").val();
	var stringArray = auction_ids.split(',');
	for (i=0;i<stringArray.length;i++)
		{
			if(stringArray[i] != ''){
				//var auction_last_for = $("#auction_last_for_"+stringArray[i]).val();
				//var auction_start_time = $("#auction_start_time_"+stringArray[i]).val();
				//var auction_end_time = parseInt(auction_start_time)+ (parseInt(auction_last_for)*60*60);
				var schedule_endtime = $("#schedule_endtime_"+stringArray[i]).val();
				var auction_end_time = parseInt(schedule_endtime);
				var get_date = new Date(parseInt(auction_end_time) * 1000);
				var a_year = get_date.getFullYear();
				var months = get_date.getMonth();
				//months = parseInt(months)+1;
				var months = get_date.getMonth();
				var note = $('#note'),
					ts = new Date(get_date.getFullYear(), months, get_date.getDate(), get_date.getHours(), get_date.getMinutes(), get_date.getSeconds()),
					newYear = true;
				if((new Date()) > ts){
					// The new year is here! Count towards something else.
					// Notice the *1000 at the end - time must be in milliseconds
					ts = (new Date()).getTime();
					newYear = false;
				}
				
				$('#countdown_'+stringArray[i]).countdown({
					timestamp	: ts,
					callback	: function(days, hours, minutes, seconds){
						var message = "";
						message += days + " day" + ( days==1 ? '':'s' ) + ", ";
						message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
						message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
						message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";
						if(newYear){
							message += "left until the new year!";
						}
						else {
							message += "left to 10 days from now!";
						}
						note.html(message);
					}
				});
			}
		} 
});

$(document).ready(function(){
	var aids = $("#auction_ids").val();
	get_fb_complete(aids);
});

</script>

<?php $this->load->view('frontend/footer');?>