<?php $this->load->view('frontend/header');?>
<script src="<?php echo base_url();?>assetts/js/jquery.ad-gallery.js" language="javascript" type="text/javascript"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#slider-range-min" ).slider({
			range: "min",
			value: 32,
			min: 1,
			max: 168,
			slide: function( event, ui ) {
			$( "#amount" ).val(ui.value);
			}
		});
		$( "#amount" ).val( $( "#slider-range-min" ).slider( "value" ) );
		
		var check_hive_combs = $('input:checkbox[name=check_hive_combs]');
		check_hive_combs.click(function(){
			if($('input[name=check_hive_combs]').is(':checked')){
				$('input[name=check_hive_combs]').attr('checked');
				$('input[name=check_bonus_credit]').removeAttr('checked');
			}
		});
		var check_bonus_credit = $('input:checkbox[name=check_bonus_credit]');
		check_bonus_credit.click(function(){
			if($('input[name=check_bonus_credit]').is(':checked')){
				$('input[name=check_bonus_credit]').attr('checked');
				$('input[name=check_hive_combs]').removeAttr('checked');
			}
		});	
	});
	$(document).ready(function(){
	$("#create_auction_credit").keyup(function(){
		var credit_val = $("#credit_val").text();
		if(!isNaN($("#create_auction_credit").val())){
			var create_auction_credit = $("#create_auction_credit").val();
			var total_credit = parseFloat(create_auction_credit*parseFloat(credit_val)).toFixed(2);
			var value = total_credit;
			$("#create_auction_value").text(value);
		}else{
			$("#create_auction_credit").val('0');
			var value = "0.00";
			$("#create_auction_value").text(value);
		}
	});
	
	$("#withdraw_honey_combs").keyup(function(){
		var withdraw_credit_val = 0.71;
		if(!isNaN($("#withdraw_honey_combs").val())){
			var withdraw_honey_combs = $("#withdraw_honey_combs").val();
			var total_credit = parseFloat(withdraw_honey_combs*parseFloat(withdraw_credit_val)).toFixed(2);
			var value = total_credit;
			$("#withdraw_credit_value").text(value);
		}else{
			$("#withdraw_honey_combs").val('0');
			var value = "0.00";
			$("#withdraw_credit_value").text(value);
		}
	});
/*****************************27.8.13****************************************/
	$('input:radio[name=with_draw]').click(function(){
	if($(this).is(':checked')){
		//alert($(this).attr('credit'));
		var available = parseInt($('#available_withdraw_credits').text());		
		if(parseInt($(this).attr('credit')) < available){		
		$('#withdraw_honey_combs').val($(this).attr('credit'));
		$("#withdraw_credit_value").text($(this).val());
		}else{		
		var withdraw_credit_val = 0.71;
		$('#withdraw_honey_combs').val(available);
		var total_credit = parseFloat(available*parseFloat(withdraw_credit_val)).toFixed(2);
		var value = total_credit;
		$("#withdraw_credit_value").text(value);
		}
		
	}else{
		$("#withdraw_honey_combs").val('0');
		var value = "0.00";
		$("#withdraw_credit_value").text(value);
	}
	});
	/*****************************bhaiya****************************************/
});
function checkcreateauction(){
	var credit_val = $("#credit_val").text();
	var create_auction_credit = $("#create_auction_credit").val();
	if(create_auction_credit != ''){
	var auction_endtime = $("#amount").val();
		if($('input[name=check_hive_combs]').is(':checked')){
			var available_hive_credits = parseInt($("#available_hive_credits").text());
			var item_value = parseFloat(create_auction_credit*parseFloat(credit_val)).toFixed(2);
			if(parseInt(create_auction_credit) < parseInt(available_hive_credits)){
				$.post("<?php echo base_url();?>users/manage_credits/create_auction",{ credit_type: "Bids", create_auction_credit: create_auction_credit, auction_endtime: auction_endtime, item_value: item_value }, function(data) {
					var available_credits = $("#available_credits").text();
					available_credits = parseInt(available_credits)-parseInt(create_auction_credit);
					$("#available_credits").text(available_credits);
					var available_price = $("#available_price").text();
					$("#available_price").text(parseFloat(available_price) - parseFloat(item_value));
					$("#available_hive_credits").text(available_hive_credits-create_auction_credit);
					$("#create_auction_credit").val('');
					$("#create_auction_value").text('0.00');
					$("#total_credit_available").text(available_credits);
					$("#total_credit_value_available").text(parseFloat(available_price) - parseFloat(item_value));
					
					var hive_credit_available = parseInt($("#hive_credit_available").text());
					$("#hive_credit_available").text(hive_credit_available-create_auction_credit);
					$("#available_withdraw_credits").text(available_hive_credits-create_auction_credit);
					$('input[name=check_hive_combs]').removeAttr('checked');
					alert("Auction has been successfully created!!! You have "+available_credits+" bid(s) available.");
				});
			}else{
				alert("You do not have enough bids to create this auction");
				return false;
			}
		}else if($('input[name=check_bonus_credit]').is(':checked')){
			var available_bonus_credits = $("#available_bonus_credits").text();
			var item_value = parseFloat(create_auction_credit*parseFloat(credit_val)).toFixed(2);
			if(parseInt(create_auction_credit)<parseInt(available_bonus_credits)){
				$.post("<?php echo base_url();?>users/manage_credits/create_auction",{ credit_type: "Bonus Bids", create_auction_credit: create_auction_credit, auction_endtime: auction_endtime, item_value: item_value }, function(data) {
					var available_credits = $("#available_credits").text();
					available_credits = parseInt(available_credits)-parseInt(create_auction_credit);
					$("#available_credits").text(available_credits);
					var available_price = $("#available_price").text();
					$("#available_price").text(parseFloat(available_price) - parseFloat(item_value));
					$("#available_bonus_credits").text(available_bonus_credits-create_auction_credit);
					$("#create_auction_credit").val('');
					$("#create_auction_value").text('0.00');
					$("#total_credit_available").text(available_credits);
					$("#total_credit_value_available").text(parseFloat(available_price) - parseFloat(item_value));
					var bonus_credit_available = parseInt($("#bonus_credit_available").text());
					$("#bonus_credit_available").text(bonus_credit_available-create_auction_credit);
					$('input[name=check_bonus_credit]').removeAttr('checked');
					alert("Auction has been Successfully Created. Check Activity -> active auctions to see your auction.");
				});
			}else{
				alert("You do not have enough credits");
				return false;
			}
		}else{
			alert("Please choose a type of bid to auctioned");
			return false;
		}
	}else{
		alert("Please enter the amount of bids to be auctioned");
		return false;
	}
}
function check_withdraw_funds(){

	var withdraw_honey_combs = $("#withdraw_honey_combs").val();
	if(withdraw_honey_combs != ''){
		if($('input[name=check_withdraw_hive_combs]').is(':checked')){
			var available_withdraw_credits = parseInt($("#available_withdraw_credits").text());
			if(withdraw_honey_combs<=available_withdraw_credits){
			var id=$('input:radio[name=with_draw]:checked').attr('creditID');	
			var credit=withdraw_honey_combs;
			var price=$('#withdraw_credit_value').text();
			//alert(price);
			var createdDiv='<div id="suss_refund" class="sucess_mail">Your request is being processed.....</div>';
				  $('#sucess_msg').append(createdDiv);
			$.post("<?php echo base_url();?>users/buy_credit/refund",{ id: id, credit:credit, price:price, }, function(response){
	           if(response.error==null){
			     $("#suss_refund").remove();
		         
				  if(response.status == 'success'){
						var credit_val = parseFloat($("#credit_val").text());
						var item_value = parseFloat(credit*parseFloat(credit_val)).toFixed(2);
						var available_hive_credits = parseInt($("#available_hive_credits").text());
						var available_credits = $("#available_credits").text();
						available_credits = parseInt(available_credits)-parseInt(credit);
						$("#available_credits").text(available_credits);
						var available_price = $("#available_price").text();
						$("#available_price").text(parseFloat(available_price) - parseFloat(item_value));
						$("#available_hive_credits").text(available_hive_credits-credit);
						$("#withdraw_honey_combs").val('');
						$("#withdraw_credit_value").text('0.00');
						var total_credit_available = parseInt($("#total_credit_available").text());
						$("#total_credit_available").text(total_credit_available-credit);
						var total_credit_value_available = parseFloat($("#total_credit_value_available").text());
						$("#total_credit_value_available").text(parseFloat(total_credit_value_available) - parseFloat(item_value));
						var hive_credit_available = parseInt($("#hive_credit_available").text());
						$("#hive_credit_available").text(hive_credit_available-credit);
						$("#available_withdraw_credits").text(available_hive_credits-credit);
						$("#creditID_"+id).remove();
					var createdDiv='<div id="suss_refund" class="sucess_mail txt13B">You have successfully withdrawn '+credit+' bids and you have '+(total_credit_available-credit)+' bid(s) remaining. Please check your Paypal account to view the deposit.</div>';
					
					$('#sucess_msg').append(createdDiv);
					setTimeout(function(){
						$("#suss_refund").remove();
					}, 5000);						
				  }else{
					var createdDiv='<div id="suss_refund" class="sucess_mail txt13B">'+decodeURIComponent(response.L_LONGMESSAGE0)+'</div>';
					$('#sucess_msg').append(createdDiv);
					setTimeout(function(){
						$("#suss_refund").remove();
					}, 5000);				  
				  }
			  }
	       },'json');	
				return true;
			}else{
				alert("You do not have enough credits");
				return false;
			}
		}else{
			alert("Please choose bids");
			return false;
		}
	}else{
		alert("Please enter the amount of bids to be Withdraws");
		return false;
	}
}
function check_withdraw_funds1(){
	var withdraw_honey_combs = $("#withdraw_honey_combs").val();
	if(withdraw_honey_combs != ''){
		var available_withdraw_credits = parseInt($("#available_withdraw_credits").text());
		if(withdraw_honey_combs<=available_withdraw_credits){
			var credit=withdraw_honey_combs;
			var price=parseFloat($('#withdraw_credit_value').text());
			var createdDiv='<div id="suss_refund" class="sucess_mail">Processing your request.....</div>';
			$('#sucess_msg').append(createdDiv);		
			
			
			$.post("<?php echo base_url();?>users/buy_credit/refund1",{credit:credit, price:price, }, function(response){
							 $("#suss_refund").remove();
							 
							  if(response.status == 'success'){
									var credit_val = parseFloat($("#credit_val").text());
									var item_value = parseFloat(credit*parseFloat(credit_val)).toFixed(2);
									var available_hive_credits = parseInt($("#available_hive_credits").text());
									var available_credits = $("#available_credits").text();
									available_credits = parseInt(available_credits)-parseInt(credit);
									$("#available_credits").text(available_credits);
									var available_price = $("#available_price").text();
									$("#available_price").text(parseFloat(available_price) - parseFloat(item_value));
									$("#available_hive_credits").text(available_hive_credits-credit);
									$("#withdraw_honey_combs").val('');
									$("#withdraw_credit_value").text('0.00');
									var total_credit_available = parseInt($("#total_credit_available").text());
									$("#total_credit_available").text(total_credit_available-credit);
									var total_credit_value_available = parseFloat($("#total_credit_value_available").text());
									$("#total_credit_value_available").text(parseFloat(total_credit_value_available) - parseFloat(item_value));
									var hive_credit_available = parseInt($("#hive_credit_available").text());
									$("#hive_credit_available").text(hive_credit_available-credit);
									$("#available_withdraw_credits").text(available_hive_credits-credit);
									
								var createdDiv='<div id="suss_refund" class="sucess_mail txt13B">You have successfully withdrawn'+credit+' bids and you have '+(total_credit_available-credit)+' bid(s) remaining. Please check your Paypal account to view the deposit.</div>';
								
								$('#sucess_msg').append(createdDiv);
								setTimeout(function(){
									$("#suss_refund").remove();
								}, 5000);						
							  }else{
								var createdDiv='<div id="suss_refund" class="sucess_mail txt13B">Sorry there was a transaction error. Please try again later.</div>';
								$('#sucess_msg').append(createdDiv);
								setTimeout(function(){
									$("#suss_refund").remove();
								}, 5000);				  
							  }
					   },'json');	
				return true;			
			
			
			
			
			
		}else{
			alert("You do not have enough bids");
			return false;
		}
	}else{
		alert("Please enter the amount of bids to be withdrawn");
		return false;
	}
			
			
}
function pagination(page,limit){
	$.post("<?php echo base_url()?>users/manage_credits/credit_history_pagination", { page : page, limit : limit },function(data) {
		$(".faq_Right_inner").html(data);
	});
}

</script>
<style>
.sucess_mail {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000; 
	border-radius: 8px 8px 8px 8px;  
    margin-bottom: 10px;   
    padding: 15px;
    position: absolute;
	z-index:1000px;
	left:270px;
}
</style>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<div class="Row98 faq_wrap clearfix">
	<div class="faq_left">
		<div class="flt_lft txt11pN mar10">Welcome <a href="#" class="txt12B"><?php echo $this->session->userdata('username')?></a></div>
		<br clear="all" />
		<h2 class="">Manage Bids</h2>
		<div class="Row">
		<div class="faq_info_box">
		<p style="text-align:center;">Use the Manage Bids page to monitor, withdraw or auction your Bids. Also see the bottom of the page to redeem your automatic rewards.</p>
		</div>
		<div class="Row marTop10">
			<p class="txt16pB">Summary</p>
			<?php
			$hive_credit_Auction=0;$hive_credit_Bid=0;$hive_credit_Buy=0;$hive_credit_Withdraw=0;$bonus_credit_Auction=0;$bonus_credit_Bid=0;$bonus_credit_Convert=0;
			$hive_credits_price_Auction=0;$hive_credits_price_Bid=0;$hive_credits_price_Buy=0;$bonus_credits_price_Auction=0;$bonus_credits_price_Bid=0;
			$bonus_credits_price_Convert=0;

			if(array_key_exists('hive_credit',$summary)){
				if(array_key_exists('hive_credit_Auction',$summary['hive_credit'])){
					$hive_credit_Auction = $summary['hive_credit']['hive_credit_Auction'];
					$hive_credits_price_Auction = $summary['hive_credit']['hive_credits_price_Auction'];
				}
				if(array_key_exists('hive_credit_Bid',$summary['hive_credit'])){
					$hive_credit_Bid = $summary['hive_credit']['hive_credit_Bid'];
					$hive_credits_price_Bid = $summary['hive_credit']['hive_credits_price_Bid'];
				}
				if(array_key_exists('hive_credit_Buy',$summary['hive_credit'])){
					$hive_credit_Buy = $summary['hive_credit']['hive_credit_Buy'];
					$hive_credits_price_Buy = $summary['hive_credit']['hive_credits_price_Buy'];
				}
				if(array_key_exists('hive_credit_Withdraw',$summary['hive_credit'])){
					$hive_credit_Withdraw = $summary['hive_credit']['hive_credit_Withdraw'];
					$hive_credits_price_Withdraw = $summary['hive_credit']['hive_credits_price_Withdraw'];
				}

			}
			if(array_key_exists('bonus_credit',$summary)){
				if(array_key_exists('bonus_credit_Auction',$summary['bonus_credit'])){
					$bonus_credit_Auction = $summary['bonus_credit']['bonus_credit_Auction'];
					$bonus_credits_price_Auction = $summary['bonus_credit']['bonus_credits_price_Auction'];
				}
				if(array_key_exists('bonus_credit_Bid',$summary['bonus_credit'])){
					$bonus_credit_Bid = $summary['bonus_credit']['bonus_credit_Bid'];
					$bonus_credits_price_Bid = $summary['bonus_credit']['bonus_credits_price_Bid'];
				}
				if(array_key_exists('bonus_credit_Convert',$summary['bonus_credit'])){
					$bonus_credit_Convert = $summary['bonus_credit']['bonus_credit_Convert'];
					$bonus_credits_price_Convert = $summary['bonus_credit']['bonus_credits_price_Convert'];
				}
			}
			$available_credits = ($hive_credit_Buy - ($hive_credit_Auction+$hive_credit_Bid+$hive_credit_Withdraw))+($bonus_credit_Convert-($bonus_credit_Auction+$bonus_credit_Bid));
			$available_price = $available_credits*0.75;
			?>
			<p class="marLft10 txt12B marTop10">You have <span class="txt11pB_or" id="available_credits"><?php echo $available_credits;?></span> Bids Worth $<span class="txt11pB_or" id="available_price"><?php echo $available_price;?></span> </p>
		</div>
		<div class="Row">
			<div class="faq_result_box_yellow">AUCTION YOUR BIDS</div>
			<div class="comm_rounded_grey w658">
			<p class="txt11pN mar10" style="color:#FFFFFF;text-align:center;">Make money by auctioning your bids. However, there is no minimum that can be set for Bid Auctions. (You can select only one at time for auction)</p>
			</div>
			<form name="create_auction_form" action="<?php echo base_url();?>users/manage_credits/create_auction" method="post">
			<div class="Row98 marTop10">
			<table width="100%" class="comm_table">
			  <tr>
				<td class="vert"><input name="check_hive_combs" type="checkbox" value="" /></td>
				<td class="vert txt13B"><span style="float:left;">Bids (</span><span id="available_hive_credits" style="float:left"><?php echo $hive_credit_Buy - ($hive_credit_Auction+$hive_credit_Bid+$hive_credit_Withdraw);?></span>&nbsp;available)</td>
				<td class="vert txt13B">Auction</td>
				<td class="vert"><input name="create_auction_credit" id="create_auction_credit" type="text" value="" style="width:100px; height:35px; font-size:16px; text-align:center;" /></td>
			  </tr>
			  <tr><td class="vert">&nbsp;</td>
			  <tr>
				<td class="vert"><input name="check_bonus_credit" type="checkbox" value="" /></td>
				<td class="vert txt13B"><span style="float:left;">Bonus Bids (</span><span id="available_bonus_credits" style="float:left"><?php echo $bonus_credit_Convert-($bonus_credit_Auction+$bonus_credit_Bid);?></span>&nbsp;available)</td>
			  </tr>
			</table>
			</div>
			<!--Slider  -->
			<div class="Row98 marTop10">
				<div class="flt_lft txt11pN ">Auction will last for</div>
				<div class="flt_lft marLft10" style="width:350px; padding-left:10px;"><div id="slider-range-min"></div></div>
				<div class="flt_lft txt11pN" style="padding-left:18px;"><input name="auction_endtime" type="text" id="amount" style="border: 0; width:30px; color: #f6931f; font-weight: bold;" />&nbsp;hours
				</div>
				<div class="Row98 marTop20">
				<p align="center" class="txt16pB" style="float:left;margin-left: 271px;">Value $</p><div id="create_auction_value" style="float:left">0.00</div>
				<br clear="both" />
				<p align="center" class="marTop10"><input type="button" value="" class="btn_create_auction" onclick="return checkcreateauction();" /></p>
				</div>
			</div>
			</form>
			<!--Slider  -->
			<div class="cls" style="height:10px;"></div>
			<hr />
			</div>
			<div class="Row">
			
				<div class="faq_result_box_yellow" id="sucess_msg">WITHDRAW BIDS FOR CASH</div>
				<div class="Row98 marTop10">
					<table width="100%" class="comm_table">
					  <tr>
						<td class="vert txt13B"><span style="float:left;">Bids (</span><span id="available_withdraw_credits" style="float:left"><?php echo $hive_credit_Buy - ($hive_credit_Auction+$hive_credit_Bid+$hive_credit_Withdraw);?></span>&nbsp;available)</td>
						<td class="vert txt13B">Withdraw</td>
						<td class="vert"><input name="withdraw_honey_combs" type="text" class="comm_sel" id="withdraw_honey_combs" style="width:100px; height:35px; font-size:16px; text-align:center;" /></td>
					  </tr>
					</table>
				</div>
				<div class="Row98 marTop20">
					<p align="center" class="txt16pB" style="float:left;margin-left:278px;">Value $<div id="withdraw_credit_value" style="float:left">0.00</div></p>
					<br clear="both" />
					<p align="center" class="marTop10"><input type="submit" value="" class="btn_confirm_w_funds"  onclick="return check_withdraw_funds1()"/></p>
				</div>
				<div class="comm_rounded_grey w658 marTop10">
				<p class="txt11pN mar10"style="color:#FFFFFF;text-align:center;">The Total of your withdrawl will be sent via PayPal immediatly</p>
				</div>
				<div class="cls" style="height:10px;"></div>
				<hr />
			
			</div>
			<div class="Row">
				<div class="faq_result_box_yellow">REWARDS DOLLARS</div>
				<div class="comm_rounded_grey w658">
				<p class="txt11pN mar10"style="color:#ffffff;text-align:center;">You earn 5 cents for every bid you spend in an auction, which you can redeem for bonus bids</p>
				</div>
				<div class="Row98 marTop20">
				<p align="center" class="txt16pB">$<?php if(isset($rewards[0]->reward_amount)){ echo $rewards[0]->reward_amount;}else{ echo '0';} ?> reward dollars</p>
				<?php if(isset($rewards[0]->reward_amount) && $rewards[0]->reward_amount >= 5){ ?>
				<p align="center" class="marTop10"><a href="<?php echo base_url();?>users/manage_credits/redeem_rewards"><img src="<?php echo base_url();?>assetts/images/btn_redeem.png" width="193" height="22" border="0" /></a></p>
				<?php }?>
				<p class="marTop10 txt11pN"><em>Look for more reward dollar opportunities in the future</em></p>
				</div>
				<div class="cls" style="height:10px;"></div>
				<hr />
			</div>
		</div>
	</div>
	<div class="faq_Right marTop10">
		<div class="faq_Right_inner clearfix">
			<div class="Row"><h2>Bid History</h2></div>
			<br class="cls" clear="all" />
			<div class="Row marTop10" style="">
			<?php //echo '<pre>';print_r($credit_history);?>
				<?php for($i = $start; $i<$limit; $i++){
				//echo $credit_history[$i]['title']; echo $credit_history[$i]['type'];
				//foreach($credit_history as $credit_history_val){ ?>
						<?php if(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction' && $credit_history[$i]['winner_id'] == $this->session->userdata('user_id')){ ?>
							<p class="odd">Won <?php echo $credit_history[$i]['title'];?></p>
						<?php }elseif(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction'){ ?>
							 <?php if(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction' && $credit_history[$i]['winner_id'] == 0 && $credit_history[$i]['status'] == '1'){ ?>
                                <p class="odd"><?php echo $credit_history[$i]['title'];?> returned from reversed auction</p>
                            <?php }?>
							<p class="odd">Auctioned <?php echo ($credit_history[$i]['item_value']/$this->config->item("credit_val")).' Bids';?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Bid'){ ?>
							<p class="odd">Bid <?php echo $credit_history[$i]['hive_credit'].' on ITEM #'.$credit_history[$i]['auction_id'];?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Withdrew'){ ?>
							<p class="odd">Withdrew <?php echo $credit_history[$i]['hive_credit'].' Bid(s)';?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Buy'){ ?>
							<p class="odd">Bought <?php echo $credit_history[$i]['hive_credit'].' Bids';?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Convert'){ ?>
							<p class="odd">Converted <?php echo $credit_history[$i]['hive_credit'].' Bid(s)';?></p>
						<?php }elseif(isset($credit_history[$i]['winner_id']) && $credit_history[$i]['winner_id'] = $this->session->userdata('user_id')){ ?>
							<p class="odd">Won Auction <?php echo $credit_history[$i]['title'];?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] = 'Reversed'){ ?>
							<p class="odd"><?php echo $credit_history[$i]['hive_credit'];?> bid(s) returned from auction</p>
						<?php }
						}?>
			</div>

		<!--pagination -->
		<div class="flt_lft Row">
		<ul class="pagination">
			<?php
			$adjacents = 1;
			if ($page == 0) $page = 1;					//if no page var is given, default to 1.
			$prev = $page - 1;							//previous page is page - 1
			$next = $page + 1;							//next page is page + 1
			$lastpage = ceil($total_rows/$limit);		//lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;						//last page minus 1
			$pagination = "";
			if($lastpage > 1)
			{	
				//previous button
				if ($page > 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($prev,$limit)\">Previous</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Previous</li>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active_page\">$counter</li>";
						else
							$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($next,$limit)\">Next</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Next</li>";
					
				echo $pagination;
			}
			?>

		</ul> 		
		</div>
		
		<!--pagination -->

		</div>
	</div>
</div>
<div class="cls"><br clear="all" /></div>
</div>
<!--REGISTRATIOM AREA-->
<!--FOOTER SRT-->
<? $this->load->view('frontend/footer');?>