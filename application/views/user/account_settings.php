<?php $this->load->view('frontend/header');?>
    <link href="<?php echo base_url()?>assetts/css/basic.css" media="screen" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url()?>assetts/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assetts/js/jquery.modal.1.4.1.min.js"></script>
<script type="text/javascript">
	function notification_update(field, user_id){
		var field_name = field.name;
		if(field.checked){
			var field_value =1;
			$.post("<?php echo base_url()?>users/account_settings/notification_update", { user_id: user_id, field_name: field_name, field_value: field_value },function(data) {
				//$(".admin_list_"+delete_id).remove();
			});
		}else{
			var field_value =0;
			$.post("<?php echo base_url()?>users/account_settings/notification_update", { user_id: user_id, field_name: field_name, field_value: field_value},function(data) {
				//$(".admin_list_"+delete_id).remove();
			});
		}
	}
function openmodal_avater(url, height,width){
	$.modal('<iframe scrolling="no" src="' + url + '" height="' + height + '" width="' + width +'" style="border:0">', {
		containerCss:{
			backgroundColor:"#fff",
		},
	});
}
	function userinfo_request_code(user_id){
		$.post("<?php echo base_url()?>users/account_settings/userinfo_request_code", { user_id: user_id},function(data) {
		});
	}
	function update_userinfo(){
		var user_id = $("#user_id").val();
		var request_code = $("#request_code").val();
		if(request_code == '' || request_code == 'Enter Code Here'){
			alert('Please enter the code');
			$("#request_code").focus();
			return false;
		}else{
			$('#update_user_info_form').submit();
		}
	}
	function requset_code_onblur(field_val){
		if(field_val == ''){
			$("#request_code").val('Enter Code Here');
		}
	}
	function requset_code_onfocus(field_val){
		if(field_val == 'Enter Code Here'){
			$("#request_code").val('');
		}
	}
	
	
	function paypalemail_request_code(user_id){
		$.post("<?php echo base_url()?>users/account_settings/paypalemail_request_code", { user_id: user_id},function(data) {
		});
	}
	function update_paypalemail(){
		var user_id = $("#user_id").val();
		var paypal_request_code = $("#paypal_request_code").val();
		var paypalemail = $("#paypalemail").val();
		if(paypal_request_code == '' || paypalemail == ''){
		alert('Please enter the credentials');
			$("#paypal_request_code").focus();
			return false;
		}else{
			$.post("<?php echo base_url()?>users/account_settings/update_paypalemail", { user_id: user_id, paypal_request_code: paypal_request_code, paypalemail: paypalemail },function(data) {
				alert(data);
			});
		}
	}
	
</script>
<?php //echo '<pre>';print_r($user_notifications);?>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">

<div class="messageWrap marTop10 clearfix">
<div class="Row98 RowMail_borBot">
<ul class="messagebox_menu">
<li <?php if($this->uri->segment(2) == 'activity'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/activity">Activity</a> </li>
<li<?php if($this->uri->segment(2) == 'messages'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/messages" id="unread_msg">Messages (<?=count($unread);?>)</a></li>
<li <?php if($this->uri->segment(2) == 'account_settings'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/account_settings">Account Settings</a> </li>
</ul>
</div>
<div class="cls"><br clear="all" /></div>

<div class="Row98">
<div class="messagebox_left_wrap">
<div class="messagebox_left">
	<div class="flt_lft inbox_div first">
		<span class="inbox_head"><a href="<?php echo base_url();?>users/manage_credits" class="lnk">Manage Credits</a></span>
	</div>
	<div class="flt_lft inbox_div">
		<span class="inbox_head"><a href="<?php echo base_url();?>feedback/profile" class="lnk">Feedback Profile</a></span>
	</div>
<!--	<div class="flt_lft inbox_div">
		<span class="inbox_head"><a href="<?php echo base_url();?>users/list_auction" class="lnk">List Auction</a></span>
	</div>
--></div>
<div class="flt_lft mar10"><input name="" type="button" class="btn_logout" /></div>
</div><!--messagebox_left_wrap ends -->
<div class="messagebox_right">
<div class="Row98 clearfix">
<div class="myAccountheader"><p>MY ACCOUNT</p></div>
<div class="roundBox_black">
<h1>Want to know how to sell on HIVE MarketPlace? its simple!</h1>
<p>1. add your paypal account email to receive you auction profit and payment from buyer</p>
<p>2. List your item for auction</p>
</div>
<div class="roundBox_grey">
<form name="user_paypalemail" action="<?php echo base_url()?>users/account_settings/update_paypalemail" method="post" id="user_paypalemail">
<input type="hidden" name="user_id" value="<?php echo $user_details[0]->id;?>" />
<table width="100%" class="comm_table">
  <tr>
    <td class="vert">Your PayPal Email</td>
    <td class="vert"><input name="paypalemail" id="paypalemail" type="text" class="inp" value="<?php echo $user_details[0]->paypalemail; ?>" style="width:250px;" /></td>
     <td class="vert"><a href="javascript:void(0)" onclick="paypalemail_request_code('<?php echo $user_details[0]->id;?>');"><img src="<?php echo base_url();?>assetts/images/btn_request_code_to_email.png" width="162" height="30" /></a><br />
     <input name="paypal_request_code" id="paypal_request_code" type="text" class="inp" value="" placeholder="Enter Code Here" style="width:127px; margin-left:20px; padding-left:3px;" />
		</td>
        <td class="vert"><input type="button" class="btn_update_account" onclick="return update_paypalemail()" /></td>
  </tr>
  <tr><td colspan="4" class="vert">Don't have PayPal? Don't worry! Enter your email address and payment will be waiting for you to sign up PayPal with that email address</td></tr>
</table>
</form>
</div>
<div class="roundBox_grey_one">
<form name="user_notification" action="" method="post">
<table width="100%" class="comm_table">
  <tr><td class="vert"><h2>Notifications Options</h2></td></tr>
<tr>
<td class="vert">
<?php if(isset($user_notifications[0])){?>
<table class="comm_table" cellpadding="5">
    <tr>
<td width="4%" class="vert"><input name="email_auction_watchlist_ends" type="checkbox" id="email_auction_watchlist_ends" <?php echo($user_notifications[0]->email_auction_watchlist_ends ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>

<td width="96%" class="vert">Email me when auction on watch list ends</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_bid_end" id="email_auction_bid_end" type="checkbox" <?php echo($user_notifications[0]->email_auction_bid_end ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when auction I've bid on ends</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_win_auction" id="email_win_auction" type="checkbox" <?php echo($user_notifications[0]->email_win_auction ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when I win an auction</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_sudden_death" id="email_auction_sudden_death" type="checkbox" <?php echo($user_notifications[0]->email_auction_sudden_death ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when an auction I've bid goes in sudden death</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_bid_doesnot_mmeets_and_sudden_death" id="email_auction_bid_doesnot_mmeets_and_sudden_death" type="checkbox" <?php echo($user_notifications[0]->email_auction_bid_doesnot_mmeets_and_sudden_death ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when an auction I've bid does not mmeet it's minimum and moves to sudden death</td>
  </tr>
    <tr>
<td width="4%" class="vert"><input name="forward_email_from_member" id="forward_email_from_member" type="checkbox" <?php echo($user_notifications[0]->forward_email_from_member ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Forward my emails from members to my email</td>
  </tr>
    <tr>
<td width="4%" class="vert"><input name="notify_5_min_left_auction" id="notify_5_min_left_auction" type="checkbox" <?php echo($user_notifications[0]->notify_5_min_left_auction ==1 ?'checked="checked"':'')?> value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Notify me when there is 5 minites left on an auction i've bid on</td>
  </tr>
</table>
<?php }else{ ?>
<table class="comm_table" cellpadding="5">
    <tr>
<td width="4%" class="vert"><input name="email_auction_watchlist_ends" type="checkbox" id="email_auction_watchlist_ends" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when auction on watch list ends</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_bid_end" id="email_auction_bid_end" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when auction I've bid on ends</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_win_auction" id="email_win_auction" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when I win an auction</td>
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_sudden_death" id="email_auction_sudden_death" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>

<td width="96%" class="vert">Email me when an auction I've bid goes in sudden death</td>
        
  </tr>
   <tr>
<td width="4%" class="vert"><input name="email_auction_bid_doesnot_mmeets_and_sudden_death" id="email_auction_bid_doesnot_mmeets_and_sudden_death" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Email me when an auction I've bid does not mmeet it's minimum and moves to sudden death</td>
  </tr>
    <tr>
<td width="4%" class="vert"><input name="forward_email_from_member" id="forward_email_from_member" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>
<td width="96%" class="vert">Forward my emails from members to my email</td>
  </tr>
    <tr>
<td width="4%" class="vert"><input name="notify_5_min_left_auction" id="notify_5_min_left_auction" type="checkbox" value="" onclick="notification_update(this,<?php echo $user_details[0]->id;?>)" /></td>

<td width="96%" class="vert">Notify me when there is 5 minites left on an auction i've bid on</td>
  </tr>
</table>
<?php }?>
</td>
</tr>
</table>
</form>

</div>

<div class="roundBox_grey_one">
<form id="update_user_info_form" enctype="multipart/form-data" action="<?php echo base_url();?>users/account_settings/update" method="post">
<table width="100%" class="comm_table">

  <tr><td class="vert"><h2>Update User info</h2></td></tr>

<tr>
<td class="vert">

<table width="100%%" class="comm_table" >
  <tr>
    <td width="33%" class="top">
    <table width="100%">
  <tr>
	<td width="48%" class="top">
		<table width="100%" cellspacing="5">
		  <tr>
		  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_details[0]->id;?>" />
			<td class="vert">Fist Name :<input name="fname" id="user_fname" type="text"  value="<?php echo $user_details[0]->fname;?>" class="inp_A" style="width:150px;" /></td>
		  </tr>
		  <tr>
			<td class="vert">Last Name :<input name="lname" id="user_lname" type="text" value="<?php echo $user_details[0]->lname;?>" class="inp_A" style="width:150px;" /></td>
		  </tr>
		  <tr>
			<td class="vert">Account Email :<input name="email" id="user_email" type="text" value="<?php echo $user_details[0]->email;?>" class="inp_A" style="width:150px;" onblur="update_info_onblur('user_email',this.value)" onfocus="update_info_onfocus('user_email', this.value)" /></td>
		  </tr>
		  <tr>
			<td class="vert">User Name :<input name="username" id="user_username" type="text" value="<?php echo $user_details[0]->username;?>" class="inp_A" style="width:150px;" /></td>
		  </tr>
		   <tr>
			<td class="vert">Password :<input name="password" id="user_password" type="text" value="<?php echo $user_details[0]->password;?>" class="inp_A" style="width:150px;" /></td>
		  </tr>
		</table>
	</td>
   
   <td width="48%" class="top">
    <table width="100%" cellspacing="5">
  <tr>
    <td class="vert">Street Address :<input name="street_address" id="user_street_address" type="text"  value="<?php echo $user_details[0]->street_address;?>" class="inp_A" style="width:150px;" /></td>
  </tr>
  
  <tr>
    <td class="vert">City / Province :<input name="city_province" id="user_city_province" type="text" value="<?php echo $user_details[0]->city_province;?>" class="inp_A" style="width:150px;" /></td>
  </tr>
  
  <tr>
    <td class="vert">Country :<input name="country" id="user_country" type="text" value="<?php echo $user_details[0]->country;?>" class="inp_A" style="width:150px;" /></td>
  </tr>
  
  <tr>
    <td class="vert">Zip Code : <input name="zip" id="user_zip" type="text" value="<?php echo $user_details[0]->zip;?>" class="inp_A" style="width:150px;" /></td>
  </tr>
  <?php
  $timezonesArray = $this->config->item("timezones");
  ?>
  <tr>
  <td>Tmezone : </td>
  </tr>
  <tr>
    <td class="vert">
		<select name="timezone" id="user_country" style="width:156px;">
			<?php  foreach($timezonesArray as $key=>$val){ ?>
					<option value="<?php echo $key;?>" <?php if($key == unserialize($user_details[0]->timezone)){ echo 'selected="selected"'; }else{ echo '';}?>><?php echo $key;?></option>
			  	<?php }
			  ?>
		</select>
	</td>
  </tr>
  <?php
  ?>
</table>

    </td>
   
  </tr>
</table>

    </td>
    <td width="33%" class="top"><a href="javascript:void(0)" onclick="userinfo_request_code('<?php echo $user_details[0]->id;?>');"><img src="<?php echo base_url();?>assetts/images/btn_request_code_to_email.png" width="162" height="30" /></a><br /><br />
     <input name="request_code" type="text" class="inp_A" id="request_code" value="Enter Code Here" style="width:127px; margin-left:20px; padding-left:3px;" onblur="requset_code_onblur(this.value)" onfocus="requset_code_onfocus(this.value);" />
     <br /><br />
<input name="" type="submit" value="" style="margin-left:30px;" class="btn_update_account" onclick="return update_userinfo()" />
<?php if(isset($user_update_result)){ 
		if($user_update_result == 'unsuccess'){?>
			<div style="color:#FF0000;">you have entered wrong code</div>
  <?php }else{ ?>
			<div style="color:#006633;">Update successfully</div>
	<?php }?>
<?php } ?>
	
     </td>
    <td width="33%" align="center" class="top">
	<?php $avter_image = 'assetts/uploads/'.$user_details[0]->photo;// die();?>
	<?php if(file_exists($avter_image)){ ?>
    <img src="<?php echo base_url();?>assetts/uploads/<?php echo $user_details[0]->photo;?>" width="120"  border="0" />
	<?php }else{ ?>
		<img src="<?php echo base_url();?>assetts/uploads/noimage.jpg" width="120"  border="0" />
	<?php }?>
    <br />
<a href="javascript:void(0);" onclick="openmodal_avater('<?php echo base_url();?>users/account_settings/avater_popup',300,500)" class="lnk">Update Avater Image</a>
    </td>

  </tr>
</table>



</td></tr>



  
</table>
</form>

</div>


<div class="roundBox_Orange clearfix">
<h1>Seller Info</h1>

<h1>Your HIVEMP fee on your auction prorceeds is currently (<?php echo user_current_hivemp_fee($this->session->userdata('user_id'));?>%)</h1>

<div class="roundBox_white clearfix">
<p>
Your HMP Fee is based on how many live auctions you have at the time of the auction, or you can become a <strong>Premium Bee for $20/Month</strong> for a steady 5%
</p>

<p align="center">20 or more auction 5%</p>
<p align="center">10-19 auctions 10%</p>
<p align="center">1-9 auctions 15%</p>

</div>


<div class="Row">
<?php if(check_subscribe($this->session->userdata('user_id')) == 1){?>
<div class="flt_lft"><p class="bee">You are a Premium Bee</p></div>
	<div class="flt_right"><a href="<?php echo base_url();?>users/account_settings/unsubscribe" style="margin-top:15px; margin-right:10px;" value="" class="btn_unsubscribe"></a></div>
<?php }else{

	}?>


</div>
<script language="javascript">
$(document).ready(function(){
$('.notechar').text('140 characters');
$('#user_note').keyup(function () {
    var max = 140;
    var len = $(this).val().length;
    if (len >= max) {
        $('.notechar').text('Reached max limit');
    } else {
        var ch = max - len;
        $('.notechar').text(ch+' characters');
    }
	$('#user_note').attr('maxlength','140');
});
});
function update_usernote(){
       var notes = $('#user_note').val();
		$.post("<?php echo base_url()?>users/account_settings/update_user_note", { notes: notes },function(data) {
			 		//alert(response);
					  var createdDiv='<div id="suss_notes" class="sucess_mail">'+data+'</div>';
					  
					  $('.notechar').append(createdDiv);					
					  setTimeout(function(){
					  $("#suss_notes").remove(); 									
					}, 2000);	
		});
	}
</script>
<style>
.sucess_mail {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000; 
	border-radius: 8px 8px 8px 8px;  
    margin-bottom: 15px;   
    padding: 15px !important;
    position: absolute;	
		
	z-index:1000px; 	
}
</style>

<div style="padding-left:35px; padding-bottom:10px;">
<h1>Tell Us the Story of you or your Products:</h1>
<textarea name="user_note" id="user_note" cols="75" class="comm_txtArea" rows="7"><?php echo $user_details[0]->notes;?></textarea>
<p align="right" class="notechar" style="color:#FFFFFF;">140 characters</p>
<br>
<input class="btn_update_account" type="submit" onclick="return update_usernote()" style="margin-left:30px;" value="" name="">
</div>
</div>
<!--recent feedback -->
<div class="recent_feedback Row clearfix">
<h2>Recent Feedback</h2>

<div class="Row  recent_feedback clearfix">
<table width="100%">
<tr class="recent_topRow">
<td width="47%" class="vert">Comments</td>
<td width="16%" class="vert">Form</td>
<td width="14%" class="vert">Date / Time</td>
<td width="11%" class="vert">List#</td>
<td width="12%" class="vert">overall</td>
</tr>
<?php foreach($feedall as $feedback){?> 
<tr>
<td width="47%" class="vert">

<?php if($feedback->response=='positive'){
$icon = 'plus';
} else {
$icon = 'minus';
} ?>
<img src="<?php echo base_url();?>assetts/images/icon_<?php echo $icon;?>.png" align="absmiddle" width="18" height="18" border="0" />&nbsp;<?php echo $feedback->feedback;?></td>
<td width="16%" class="vert">Buyer : <a href="#" class="lnk"><?php echo get_user_username($feedback->user_id);?></a></td>
<td width="14%" class="vert"><?php echo date("d/m/Y  g:i a",strtotime($feedback->date));?></td>
<td width="11%" class="vert"><?php echo $feedback->auction_id;?></td>
<td width="12%" class="vert">
<?php
		$rates=$feedback->comm_rate + $feedback->speed_rate + $feedback->desc_rate;
		$avg_rate = floor($rates/3);			
		for($i=1; $i<=$avg_rate; $i++){
		echo '<img src="'.base_url().'assetts/images/rate_full.png" width="12" height="12" align="absmiddle" />'; 
		}
		if($rates%3 > 0){
		echo '<img src="'.base_url().'assetts/images/rate_half.png" width="12" height="12" align="absmiddle" />';
		$avg_rate = $avg_rate+1;
		}
		$blank_avg = 5-$avg_rate;
		for($i=1; $i<=$blank_avg; $i++){
		echo '<img src="'.base_url().'assetts/images/rate_blank.png" width="12" height="12" align="absmiddle" />';
		}
		?>
</td>
</tr>
<?php } ?>
</table>
</div>

</div>

<!--recent feedback -->

</div>

</div>

</div>


</div>

<div class="cls"><br clear="all" /></div>
</div>
<?php $this->load->view('frontend/footer');?>