
<?php//Body of the Daily Deals email
?><div id="messagecontain" style="font-family:Verdana;align:center;padding:30px;">
     <div id="messagehead" style="width:90%;background:#ffcc00; border-top-left-radius:15px;border-top-right-radius:15px;font-size:22px;">HIVE MARKETPLACE Daily Deals</div>
     <br>
     <div id="messagebody" style="background:#EBF5FF;border-radius:10px;box-shadow:-1 1 #cccccc;border:1px solid #cccccc; width:90%;">
       <div class="messagegreet" style="font-size:14px;font-weight:bold;">Congratulations on becoming a member of HIVE MarketPlace, the best place with the best prices and profits!</div>
       <br>
       <br>
       <span style="font-size:13px;">WAIT!! YOU HAVE TO READ THIS! YOUR SUCCESS DEPENDS ON IT!
       <br>
       <br>
       <p>Before you get started changing the way you buy and sell online I just want to give you some quick tips that will help you get started.</p>
       </span>
       <br>
      
       <span style="font-weight:bold; font-style:underline;font-size:13px;">Auctions<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>HOW AUCTIONS WORKS FOR BIDDERS: The winner of an auction is the person who places the highest number of bids by the end of the clock. Those joining an auction late will have to bid more than highest bidder. A high initial bid is a great way to price out the competition. If an auction has a minimum price that is not met at the end of the clock all bids will be returned to their owners as though the auction never happened.</li>
        <li>HOW AUCTIONS WORK FOR SELLERS: Once create your auction you just sit back and watch the bids come in. After all those bids are yours. Each auction has acebook and Twitter share buttons to help you promote your auction to all prospective winners.</li>
       
       </ul></p>
       <span style="font-weight:bold; font-style:underline;font-size:13px;">After Auction Ends<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>IF YOU WON: You are responsible for the accumilated pennies plus any shipping if applicable. The auction will appear in your checkout. You will either have the option to pay and pickup in person, if the seller is in your area, or have the item shipped to you.</li>
        <li>SOMEONE JUST WON YOUR AUCTION: Once the winner pays to have the item shipped or chooses to pickup the item in person, you will see the auction in your activity/paid section. Your auction profit will be disbursed when the buyer confirms they have received the auction or automically 7 days after shipping.</li>
        <li>DID NOT WIN THE AUCTION: No worries some sellers make limited number of the item from the auction available exclusively to their bidders. And the best part the normal price is discounted by the amount you spent in the auction. </li>
       </ul></p>
       <span style="font-weight:bold; font-style:underline;font-size:13px;">Bids/Credits<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>COST: Each bid cost 75cents</li>
        <li>AUCTION YOUR BIDS: At any time you can auction your bids to potentially make more than they are worth. Profits from these type of auctions are release immediatley into your PayPal account.</li>
        <li>REDEEM BIDS FOR CASH: At any time you can reedem your bids for money directly into your PayPal account.</li>
       </ul></p>
       <br>
       <span style="font-size:13px;">
       <p>Now that you know all the basics, you are ready to get started! Click the link below to verify your account.</p>
       <br>
       
     <a href="'.base_url().'registration/activate_account/'.$id.'">Click to Activate Account</a>
     <br>
     <br>
     <p>If you have any questions checkout the LIVE HELP/FAQ to get your question answered and find great tips from the entire HIVE MarketPlace community.
Good Luck!
</p>
<br>
<br>
<p style="font-weight:bold;">Thanks for bee-ing a part of the HIVE!</p>
<br>
<p style="font-style:italic;">Ricardo and HIVE MarketPlace Staff</p>

     
     
     </span></div></div>' 
<?php ?>
