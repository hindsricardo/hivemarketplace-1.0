<?php $this->load->view('frontend/header');?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link href="<?php echo base_url()?>assetts/css/site.css" rel="stylesheet" type="text/css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script src="<?php echo base_url()?>assetts/js/jquery.price_format.1.8.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assetts/js/jquery.timeentry.js"></script>
  <script>
 $(function() {
    $( "#slider-range-min" ).slider({
      range: "min",
      value: 32,
      min: 1,
      max: 168,
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.value);
      }
    });
    //$( "#amount" ).val( "(" + $( "#slider-range-min" ).slider( "value" ) + ")" );
	$( "#amount" ).val( $( "#slider-range-min" ).slider( "value" ) );
  });
  </script>
  
<script language="javascript">
$(document).ready(function(){
		//$("#us_company_name").prop("disabled", true);
		//$("#ww_company_name").prop("disabled", true);
		$("#us_handling_time").prop("disabled", true);
		$("#ww_handling_time").prop("disabled", true);
		$("#us_price").prop("disabled", true);
		$('input:checkbox[name=us_paytype]').prop("disabled", true);
		$("#ww_price").prop("disabled", true);
		$('input:checkbox[name=ww_paytype]').prop("disabled", true);
                $('input:checkbox[name=ww_handling_time]').prop("disabled", true);
		
		
$('.count_num').text('(Your title has 40 characters remaining)');
$('input[name=title]').keyup(function () {
    var max = 40;
    var len = $(this).val().length;
    if (len >= max) {
        $('.count_num').text(' you have reached the limit');
    } else {
        var ch = max - len;
        $('.count_num').text('(Your title has '+ ch + ' characters remaining)');
    }
	$('input[name=title]').attr('maxlength','40');
});

	
	$('input[name=tags]').keyup(function () {
	var a = $('#group_tag #single_tag').size() + 1;
	if(a < 5){
		myString = $(this).val();
		if(myString.indexOf(' ') === -1){
				
		}else{
			var tag = $.trim(myString);
			var group_tag = $('#group_tag');
			var a = $('#group_tag #single_tag').size() + 1;
			var tag_html = $('<div id="single_tag" class="mar10" style="background:#CCCCCC; float:left;"><input type="hidden" name="tag_hidden[]" value="'+tag+'" /><span class="single_tag_val_'+a+'" style="font-size: 14px;">'+tag+'</span><span id="remove_tag_'+a+'" onclick="remove_tag('+a+')" style="font-size: 14px;cursor:pointer; color:red; border:1px solid;margin-left: 5px;" title="Remove">x</span></div>').appendTo(group_tag);
			$("#tags").val('');
			var tags_hidden = $("#tags_hidden").val();
			tags_hidden += ' '+tag;
			$("#tags_hidden").val(tags_hidden);
		}
	}else{
		$("#tags").attr("disabled", true);
		alert('Sorry no more tags will be avilable');
	}
	});
	
	var us_paytype = $('input:checkbox[name=us_paytype]');
	us_paytype.removeAttr('checked');
	us_paytype.click(function(){
		if($(this).is(':checked')){
			$('input[name=us_price]').val('');
			$('input[name=us_price]').attr('readonly', 'readonly');
		}else{
			$('input[name=us_price]').removeAttr('readonly');
		}
	});
	var ww_paytype = $('input:checkbox[name=ww_paytype]');
	ww_paytype.click(function(){
		if($(this).is(':checked')){
			$('input[name=ww_price]').val('');
			$('input[name=ww_price]').attr('readonly', 'readonly');
		}else{
			$('input[name=ww_price]').removeAttr('readonly');
		}
	});
	
	var auction_public = $('#auction_public');
	var auction_private = $('#auction_private');
	var passcode= $('input[name=passcode]');
	var pass_val='';
	auction_public.click(function(){
		if($(this).is(':checked')){
		  pass_val=passcode.val();
		  auction_private.attr('disabled','disabled');
		  passcode.attr('readonly', 'readonly');
		  passcode.val('');
		}else{
			auction_private.removeAttr('disabled');
		    passcode.removeAttr('readonly');
			passcode.val(pass_val);
		}
	});
	
	auction_private.click(function(){
		if($(this).is(':checked')){
		  auction_public.attr('disabled','disabled');
		 }else{
			auction_public.removeAttr('disabled');
		    passcode.removeAttr('readonly');
		}
	});
	 var auction_now = $('#auction_now');
	 var auction_later = $('#auction_later');
	 auction_now.click(function(){
		if($(this).is(':checked')){
		  auction_later.attr('disabled','disabled');
		  $('#datepicker_active').hide();
		}else{
			auction_later.removeAttr('disabled');
		     $('#datepicker_active').show();
		}
	});
	auction_later.click(function(){
		if($(this).is(':checked')){
		  auction_now.attr('disabled','disabled');
		 }else{
			auction_now.removeAttr('disabled');
		 }
	});
	$('#auction_list_form_submit').on('click', function() {
		var list_title = $("#list_title").val();
		
		if($("#list_title").val() == ''){
			alert("Please enter title");
			$("#list_title").focus();
			return false;
		}else if($("#item_value").val() =='USD$ 0.00'){
			alert("Please Enter Item Value");
			return false;
		}else if($("#upload_filename1").val() == '' && $("#upload_filename2").val() == '' && $("#upload_filename3").val() == '' && $("#upload_filename4").val() == '' && $("#upload_filename5").val() == ''){
			alert("Please choose images");
			return false;
		}
		if($("#auction_public").is(':checked') || $("#auction_private").is(':checked')){
			if($("#auction_now").is(':checked') || $("#auction_later").is(':checked')){
				$("#auction_list_form").submit();
			}else{
				alert("Decide when you want your auction to start");
				return false;
			}
		}else{
			alert("Decide whether to make you auction public or private");
			return false;
		}
		
	});
});

function remove_tag(a){
	$("#remove_tag_"+a).parents("#single_tag").remove();
	//var tags_hidden = $("#tags_hidden").val();
	//var single_tag_val = $(".single_tag_val_"+a).text();
	//alert(single_tag_val);
	$("#tags").attr("disabled", false);
}
</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo base_url()?>assetts/images/icon_calendar.png",
			buttonImageOnly: true
		});
		$( "#datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		$('.ui-datepicker-trigger').attr('align','absmiddle');
		$('.ui-datepicker-trigger').css('padding-left','4px');
	});
</script>
<script>
$(function(){
	$('#item_value').priceFormat({
		prefix: 'USD$ ',
		centsSeparator: '.',
		thousandsSeparator: ','
	});
	$('#ww_price').priceFormat({
		prefix: '',
		centsSeparator: '.',
		thousandsSeparator: ','
	});
	$('#us_price').priceFormat({
		prefix: '',
		centsSeparator: '.',
		thousandsSeparator: ','
	});
        $('#min_bids').priceFormat({
		prefix: '',
		centsSeparator: '',
		thousandsSeparator: ',',
                centsLimit: 1
	});
        $('#for_bidders').priceFormat({
		prefix: '',
		centsSeparator: '',
		thousandsSeparator: ',',
                centsLimit: 1
	});
        $('.quantity').priceFormat({
		prefix: '',
		centsSeparator: '',
		thousandsSeparator: ',',
                centsLimit: 1
	});
	
//var offer_pickup = $('input:checkbox[name=offer_pickup]');
//offer_pickup.click(function(){
//		if($(this).is(':checked')){
//		}else{
//			$('input[name=payat_pickup]').removeAttr('checked');
//		}
//	});
//var payat_pickup = $('input:checkbox[name=payat_pickup]');
//payat_pickup.click(function(){
//		if($('input[name=offer_pickup]').is(':checked')){
//			$('input[name=payat_pickup]').attr('checked');
//		}else{
//			$('input[name=payat_pickup]').removeAttr('checked');
//			alert('Please Choose Offer pickup as a delivery method');
//		}
//	});
	
	$("#offer_shipping_delivery_method").click(function(){
		if ($("#offer_shipping_delivery_method").is(":checked")) {
			$("#us_company_name").prop("disabled", false);
			
		} else {
			$("#shipping_destination_us").prop("disabled", true);
			$("#us_company_name").prop("disabled", true);
			$("#us_handling_time").prop("disabled", true);
			$("#us_price").prop("disabled", true);
			$('input:checkbox[name=us_paytype]').prop("disabled", true);
			$("#ww_company_name").prop("disabled", true);
			$("#ww_handling_time").prop("disabled", true);
			$("#ww_price").prop("disabled", true);
			$('input:checkbox[name=ww_paytype]').prop("disabled", true);
                        $('input:checkbox[name=ww_handling_time]').prop("disabled", true);
		}
	});
});
function quantity_isnumeric(quantity){
	if($.isNumeric(quantity)){
		return true
	}else{
		alert("Only numeric value is accepted");
		return false;
	}
}
function get_min_bids(bids){
	var credit_value = 0.75;
	if($.isNumeric(bids)){
		var minimun_profit = (bids*credit_value);
		//alert(minimun_profit);
		minimun_profit = parseFloat(minimun_profit).toFixed(2);
		$("#min_profit").val(minimun_profit);
	}else{
		alert("Only numeric value is accepted");
		return false;
	}
}
function change_us_handling_time(us_value){
	if(us_value == ''){
		$("#us_handling_time").prop("disabled", true);
		$("#us_handling_time").prop("disabled", true);
		$("#us_price").prop("disabled", true);
                $("#ww_handling_time").prop("disabled", true);
		$('input:checkbox[name=us_paytype]').prop("disabled", true);
                $('input:checkbox[name=ww_handling_time]').prop("disabled", true);
	}else{
		$("#us_handling_time").prop("disabled", false);
		$("#us_handling_time").prop("disabled", false);
		$("#us_price").prop("disabled", false);
                $("#ww_handling_time").prop("disabled", false);
		$('input:checkbox[name=us_paytype]').prop("disabled", false);
                $('input:checkbox[name=ww_handling_time]').prop("disabled", false);

	}
		
}
//function change_ww_handling_time(ww_value){
//	if(ww_value == ''){
//		$("#ww_handling_time").prop("disabled", true);
//		$("#ww_price").prop("disabled", true);
//		$('input:checkbox[name=ww_paytype]').prop("disabled", true);
//	}else{
//		$("#ww_handling_time").prop("disabled", false);
//		$("#ww_price").prop("disabled", false);
//		$('input:checkbox[name=ww_paytype]').prop("disabled", false);
//	}
//}
$(function () {
	$('#starting_with_time').timeEntry();
});
function get_for_bidders(quantity){
	if(Math.floor(quantity) == quantity && $.isNumeric(quantity)){
		return true
	}else{
		alert("Only numeric value is accepted");
		return false;
	}
}
</script>

<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<div class="comm_yellow_box">Create Your Auction Listing</div><br>
<div class="cls" style="height:10px;"></div>

<form name="auction_list_form" id="auction_list_form" class="auction_list_form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>users/sell/auction_list_add">

<!--Loop -->
<div class="listing_block clearfix">

<div class="Row marBot40 clearfix">
<div class="list_black_row">
<div class="numBox_black"><h1>1</h1></div>
<h1 class="List_header">Create a descriptive title for your auction listing</h1>
</div> 

<div class="Row Listing_greyBox marTop20">

<div class="flt_lft mar20"><input name="title" type="text" id="list_title" style="width:575px; height:25px;" value="" />
<p class="txt11pN marTop5 marleftIE15 count_num">(Your title has 40 characters remaining)</p>
<div class="under"></div>
</div> 

</div>

</div>
<div class="Row marBot40 clearfix">
	<div class="list_black_row">
	<div class="numBox_black"><h1>2</h1></div>
	<h1 class="List_header">Bring your auction to life with pictures</h1>
	</div> 
	
	<div class="Row Listing_greyBox marTop20">
	
	<p class="txt11pN mar10">Click "Add a photo" and select the photo you want to upload</p>
	
	<div class="Row95 marLft10 marTop10">
	
	<div class="listingPicswrap flt_lft">
	<div class="listingPicInner" id="preview-area1">
	<img src="<?php echo base_url()?>assetts/images/item.jpg" width="160" height='200' border="0" />
	</div>
	<div class="btn_add_photo_wrap">
		<input type="button" id="button_1" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:75px;height:25px;font-size:10px;margin-left: 11px;" />
		<input type="file" id="upload_filename1" name="filename[]" multiple style="width:75px; margin-left: -75px; position:absolute;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" />
	</div>
	<div class="btn_add_photo_wrap"></div>
	</div>
	
	<div class="listingPicswrap flt_lft">
	<div class="listingPicInner" id="preview-area2">
	<img src="<?php echo base_url()?>assetts/images/item.jpg" width="160" height='200' border="0"  />
	</div>
	<div class="btn_add_photo_wrap"><input type="button" id="button_2" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:80px;height:25px;font-size:10px;margin-left: 11px;" /><input type="file" id="upload_filename2" name="filename[]" multiple style="width:80px; margin-left: -80px; position:absolute;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" /></div>
	</div>
	
	<div class="listingPicswrap flt_lft">
	<div class="listingPicInner" id="preview-area3">
	<img src="<?php echo base_url()?>assetts/images/item.jpg" width="160" height='200' border="0"  />
	</div>
	<div class="btn_add_photo_wrap"><input type="button" id="button_3" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:80px;height:25px;font-size:10px;margin-left: 11px;" /><input type="file" id="upload_filename3" name="filename[]" multiple style="width:80px; margin-left: -80px; position:absolute;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" /></div>
	</div>
	
	<div class="listingPicswrap flt_lft">
	<div class="listingPicInner" id="preview-area4">
	<img src="<?php echo base_url()?>assetts/images/item.jpg" width="160" height='200' border="0"  />
	</div>
	<div class="btn_add_photo_wrap"><input type="button" id="button_4" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:80px;height:25px;font-size:10px;margin-left: 11px;" /><input type="file" id="upload_filename4" name="filename[]" multiple style="width:80px; margin-left: -80px; position:absolute;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" /></div>
	</div>
	
	<div class="listingPicswrap flt_lft">
	<div class="listingPicInner" id="preview-area5">
	<img src="<?php echo base_url()?>assetts/images/item.jpg" width="160" height='200' border="0"  />
	</div>
	<div class="btn_add_photo_wrap"><input type="button" id="button_5" value="Add a photo" style="background-color:#FF8000; font-weight:bold; color:#ffffff;z-index:1;width:80px;height:25px;font-size:10px;margin-left: 11px;" /><input type="file" id="upload_filename5" name="filename[]" multiple style="width:80px; margin-left: -80px; position:absolute;-moz-opacity:0;filter:alpha(opacity: 0);opacity: 0;z-index: 2;" /></div>
	</div>
	</div>
	<div class="under"></div>
	</div> 
</div>
<!--Describe the item(s) you are selling -->

<div class="Row marBot40 clearfix">
<div class="list_orange_row">
<div class="numBox_orange"><h1>3</h1></div>
<h1 class="List_header">Describe the item(s) you are selling.</h1>
</div> 

<div class="Row Listing_greyBox marTop20">

<p class="txt11pN mar10">Give your listing more details that will help it stand out to buyers</p>

<div class="Row98 marLft10 marTop10">
<div class="flt_lft Row">
<p class="txt11pN marBot8" style='font-weight:bold;'>Add Tags</p>
<div id="group_tag">

</div>
<input name="tags" type="text" id="tags" style="width:450px; height:25px;"/>
<p class="txt11pN marTop10">To Create a tag type a word and then press space (Limit 4 tag per listing)</p>

<hr />
</div>
<div class="marTop10 Row">
<p class="txt11pN marBot8" style='font-weight:bold;'>Item(s) Condition</p>
<select name="item_condition">
<option value="New">New</option>  
<option value="Like New">Like New</option>  
<option value="Used">Used</option>  
<option value="Gently Used">Gently Used</option>  
<option value="Some Damage">Some Damage</option>
<option value="Spare Parts">Spare Parts</option>
</select><hr />
</div>
<div class="marTop10 Row">
<p class="txt11pN marBot8" style='font-weight:bold;'>Quantity</p>
<div class="flt_lft">
<input name="qty" type="text" class="quantity" onkeyup="quantity_isnumeric(this.value)" style="width:80px; height:20px;" /></div>
<div class="flt_lft marLft10 tips_horizontal">
<div class="tips_head">Tips</div>
<div class="flt_lft txt11pN marLft10" style="width:737px;">
<p>
Someone selling exactly what you're selling? Differentiate your auction by offering more.  After all, two is better than one.
</p>
</div>

</div>
</div>
<div class="Row marTop10">

<div class="flt_lft">
<div class="open_txt_filed">
<div class="topRow" style='font-weight:bold;'>Description</div>
<div class="flt_lft Row white_bg" style="height:200px;">
<textarea name="description" style="width:644px; height:194px; border:none; max-height:194px;max-width:644px;min-height: 194px;min-width: 644px; " ></textarea>
</div>
</div>

<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft txt11pN">
<p class="mar10">
Help buyers find what they're looking for by providing detaild, easy-to-read information such as the following :
<br />
<br />

Color,size,quantity and measurements, Item conditoon details. Notable features or a good story associated with the item. 
<br />
<br />

Whether you allow returns. <br />

Be sure to check your spelling and grammer. 
</p>
</div>

</div>
</div>
</div>

</div>
<div class="under"></div>
</div> 
</div>
<!--Describe the item(s) you are selling -->
<!--Set item(s) value price and shipping / pickup details -->
<div class="Row marBot40 clearfix">
<div class="list_orange_row">
<div class="numBox_orange"><h1>4</h1></div>
<h1 class="List_header">Set Price Per Quantity and delivery details</h1>
</div> 
<div class="Row Listing_greyBox marTop20">
<div class="Row marTop10">
<div class="flt_lft w650">

<div class="Row95">
<div class="flt_lft"><p class="txt11pN" style='font-weight:bold;'>Price Per Quantity: </p></div>

<div class="flt_lft marLft10"><input type="text" name="item_value" id="item_value" style="width:120px;" value="USD$ 0.00" /></div>
</div>

<div class="Row95 marTop10">

<div class="flt_lft txt11pN ">Auction will be last for</div>
<div class="flt_lft marLft10" style="width:350px; padding-left:10px;"><div id="slider-range-min"></div></div>
<div class="flt_lft txt11pN" style="padding-left:18px;"><input name="auction_endtime" type="text" id="amount" style="border: 0; width:30px; background:#eeeeee; color: #f6931f; font-weight: bold;" />&nbsp;hours
</div>

</div>
<div class="Row95  marTop10">
<div class="flt_lft"><p class="txt11pN">Minimum Number of Bids: </p></div>

<div class="flt_lft marLft10" ><input type="text" name="min_bids" id='min_bids' onkeyup="get_min_bids(this.value)" style="width:70px;"/></div>

<div class="flt_lft marLft10"><p class="txt11pN">Your Minimum Profit will be $</p></div>

<div class="flt_lft marLft10"><input type="text" readonly="readonly" name="min_profit"  style="width:70px;" value="0.00" id="min_profit"/></div>

</div>
<div class="Row95 marTop10">
<div class="white_bg_a txt11pN">
<p>
Let losers of your auction purchase additional inventory of this item at price per quantity minus their amount spent in the auction. This is something we like to call ADP(Auction Discount Purchase).
</p>
</div>
</div>

<div class="Row98  marTop10">

<div class="flt_lft marTop5" style="width:70px"><input name="for_bidders" id='for_bidders'  style="width:70px;" type="text" onkeyup="get_for_bidders(this.value)" /></div>

<div class="marLft10 flt_right" style="width:556px"><p class="txt11pN">is your ADP Inventory.</p></div>

</div>

</div>
<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft mar10 txt11pN">
The item value will be multiplied by the quantity to determine the auction value.<br /><br />
If the minimum number of bids is not met all bids will be reverse as though the auction never happened. Its practically risk free!<br />
<br />
If you have additional inventory of this item, make it available to those that lose after the auction. Its a good way to increase participation in your auction and make some additional sales. 
</div>

</div>

</div>
<div class="under"></div>

</div> 
<div class="Row Listing_greyBox marTop20">

<div class="greyBox_inner clearfix">

<div class="faq_left w600">

<div class="Row">
<div class="flt_lft"><input name="offer_shipping_delivery_method" type="checkbox" id="offer_shipping_delivery_method" value="1" /></div>
<div class="flt_lft txt11pN marLft10" style='font-weight:bold;'>Offer Shipping as delivery method</div>
</div>
<div class="Row marTop10">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="txt11pN">
		<tr>
			<td valign="middle" align="left"style="font-weight:bold;">Carrier:</td>
			<td valign="middle" align="left">
				<select name="us_company_name" id="us_company_name" onchange="change_us_handling_time(this.value)">
					<option value="">No Shipping</option>
					<option value="UPS">UPS</option>
					<option value="USPS">USPS</option>
					<option value="FEDEX">FEDEX</option>
                                        <option value="DHL">DHL</option>
					<option value="Other Service Inside the United States">Other Service Inside the United States</option>
					<option value="Other Service Outside the United States">Other Service Outside the United States</option>
				</select>
			</td>
			<td valign="middle" align="left">Estimated Delivery:</td>
			<td valign="middle" align="left">
				<select name="us_handling_time" id="us_handling_time">
					<option value="1">1day</option>
					<option value="2">2days</option>
					<option value="3">3days</option>
					<option value="4">4days</option>
					<option value="5">5days</option>
					<option value="6">6days</option>
					<option value="7">7days</option>
					<option value="8">8days</option>
					<option value="9">9days</option>
					<option value="10">10days</option>
                                        <option value="11">More than 10days</option>
				</select>
			</td>
			<td valign="middle" align="left">$</td>
			<td valign="middle" align="left">
				<input name="us_price" type="text" id="us_price" style="width:50px;" value="0.00" />
			</td>
			<td valign="middle" align="left"><input name="us_paytype" type="checkbox" checked="checked" value="" /></td>
			<td valign="middle" class="txt11pN" align="left">FREE</td>
		</tr>
		<tr>
			<td valign="middle" colspan="10" style="height:10px;" align="left"></td>
		</tr>
		<tr>
			<td valign="middle" align="left" style="font-weight:bold;">Other countries you will ship to are:</td>
			<td valign="middle" align="left">
				<input name="ww_handling_time" id="ww_handling_time" type="text" style='font-size:13px; height:20px; width:250px;'>example: Japan, Thailand, UK, Germany, Canada</input>
				
				
			</td>
			
		</tr>
		<tr>
			<td valign="middle" colspan="10" style="height:10px;" align="left"></td>
		</tr>
		<tr>
			<td valign="middle" align="left" colspan="6">
				<table class="comm_table" >
					<tr>
                                                <!-- Offer shipping as an option input before pay at pickup input no longer needed.-->
						<!--<td width="5%" align="left" valign="middle">
						<input name="offer_pickup" type="checkbox" checked="checked" id="offer_pickup" value="1" />
						</td>
						<td width="40%" align="left" valign="middle">Offer pickup as a delivery method</td>-->
						<td width="5%" align="left" valign="middle">
							<input name="payat_pickup" type="checkbox" id="payat_pickup1" value="1" />
						</td>
						<td width="50%" align="left" valign="middle" style='font-weight:bold; font-size:13px;'>Offer In-store/In-person Pickup and Payment</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>


<div class="Row98 marTop10">
<div class="white_bg_a txt11pN">
<p>
Don't want to deal with shipping? Have the winner pickup their item(s) in person. 
</p>
</div>
</div>

</div>

<div class="marLft10 tips_vertical clearfix">
<div class="tips_vertical_head">Tips</div>

<div class="flt_lft mar10 txt11pN">
<p>
Free shipping can set your auction apart from similar listing.
</p>
</div>

</div>

</div>

<div class="Row95 marLft10 marTop20">
<div class="flt_lft marLft10">
<p class="txt11pN" style="width: 825px;font-style:italic;font-size:13px;">All auctions are covered by the HIVE Marketplace return guarantee. You are required to resolve valid requests for refunds with partial or full repayments or exchanges. Funds are to be sent via PayPal. The buyer would pay for return shipping. Failure to adhere to agreed upon return policies will result in your account being terminated.</p>
</div>
</div>
<div class="under"></div>

</div>

</div>
<!--Set item(s) value price and shipping / pickup details -->
<!--Decide how you would like to get paid -->
<div class="Row marBot40 clearfix">
<div class="list_yellow_row">
<div class="numBox_yellow"><h1>5</h1></div>
<h1 class="List_header">Where should we send your money!? </h1>
</div> 

<div class="Row Listing_greyBox marTop20">


<div class="Row marTop10">

<div class="flt_lft">

<div class="Row98">
<div class="flt_lft"><p class="txt11pN">Accept Payment with PayPal Account </p></div>
<?php echo get_user_paypalemail($this->session->userdata('user_id'));?>
<div class="flt_lft marLft10"><input name="paypal_account" style="width:200px;" type="text" value="<?php echo (get_user_paypalemail($this->session->userdata('user_id')) != ''?get_user_paypalemail($this->session->userdata('user_id')):get_user_email($this->session->userdata('user_id')));?>" /></div>
</div>


<div class="Row98 marTop10">
<div class="white_bg_a txt11pN">
<p>
Please your PayPal email address. Don't have a PayPal account? You can wait until your auction ends to create one. Just enter the email address where you would like us to send your payment. You'll have to sign up for PayPal with this email to collect your money, so an accurate email is vital to receiving your auction payment.</p>
</div>
</div>

</div>
</div>
<div class="under"></div>

</div> 
</div>

<!--Decide how you would like to get paid -->


<!--Decide whether to make you auction public or private-->
<div class="Row marBot40 clearfix">
<div class="list_red_row">
<div class="numBox_red"><h1>6</h1></div>
<h1 class="List_header">Decide whether to make you auction public or private</h1>
</div> 

<div class="Row Listing_greyBox marTop20">
<div class="Row marTop10">

<div class="flt_lft">

<div class="Row98">
<div class="flt_lft"><input name="auction_type[]" type="checkbox" value="Public" id="auction_public"/></div>
<div class="flt_lft marLft10"><p class="txt11pN">Public : make this auction open and visible for everyone</p></div>
</div>

<div class="Row98 marTop10">
<div class="flt_lft"><input name="auction_type[]" type="checkbox" value="Private" id="auction_private"/></div>
<div class="flt_lft marLft10"><p class="txt11pN">Private : your auction can only be found by searching for the list number. To see the auction's page your password below must be entered.</p></div>
<br clear="all" />

<div class="flt_lft"><p class="txt11pN">Enter the passcode you would like to secure the auction with</p></div>

<div class="flt_lft marLft10"><input name="passcode" type="text"  style="width:200px;" /></div>

</div>
</div>
</div>
<div class="under"></div>
</div> 
</div>
<!--Decide whether to make you auction public or private-->
<!--Decide when you want your auction to start-->
<div class="Row marBot20 clearfix">
<div class="list_white_row">
<div class="numBox_white"><h1>7</h1></div>
<h1 class="List_header">Decide when you want your auction to start</h1>
</div> 

<div class="Row Listing_greyBox marTop20">


	<div class="Row marTop10">
	
		<div class="flt_lft w600">
			<div class="Row98">
				<div class="flt_lft"><input name="starting_with[]" type="checkbox" value="now" id="auction_now" /></div>
				<div class="flt_lft marLft10"><p class="txt11pN">Start it right now</p></div>
			</div>
			<div class="Row98 marTop10">
				<div class="flt_lft"><input name="starting_with[]" type="checkbox" value="later" id="auction_later" /></div>
				<div class="flt_lft marLft10"><p class="txt11pN">Schedule your auction to start at:</p></div>
				<br clear="all" />
				<div class="Row98 marTop10" id="datepicker_active">
				<div class="flt_lft txt11pN marTop5">Date</div>
				<div class="flt_lft marLft10"><input name="starting_with_date" type="text" id="datepicker" /></div>
				<div class="flt_lft txt11pN marTop5 marLft10">Time</div>
				<div class="flt_lft marLft10"><input name="starting_with_time" type="text" id="starting_with_time" style="100px;" /></div>
				</div>
			</div>
		</div>
		<!--<div class="marLft10 tips_vertical clearfix">
			<div class="tips_vertical_head">Tips</div>
			<div class="flt_lft txt11pN">
			<p class="mar10">Private auctions are a good way to host your own penny auction to a niche consumer base without unwanted attention. You will have an opportunity to edit start time later if need be.</p>
			</div>
		</div>-->
		<div class="under"></div>
	</div> 
</div>
<!--Decide when you want your auction to start-->
</div>
<div class="Row">
<p align="center" class="marBot20"><input name="" type="button" class="btn_list_auction" id="auction_list_form_submit" value=""/></p>
</div>
</div>
</form>
<!--Loop -->

</div>
<!--REGISTRATIOM AREA-->

<!--FOOTER SRT-->
<script>
  function handleFileSelect1(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area1').innerHTML = ['<img height="200" width="160" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_1").val('Remove');
		$("#upload_filename1").css("height","0px");
		$("#upload_filename1").css("display","none");
		$("#button_1").attr('onclick','remove_photo(1)');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect2(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area2').innerHTML = ['<img height="200" width="160" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_2").val('Remove');
		$("#upload_filename2").css("height","0px");
		$("#upload_filename2").css("display","none");
		$("#button_2").attr('onclick','remove_photo(2)');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect3(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area3').innerHTML = ['<img height="200" width="160" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_3").val('Remove');
		$("#upload_filename3").css("height","0px");
		$("#upload_filename3").css("display","none");
		$("#button_3").attr('onclick','remove_photo(3)');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect4(evt) {
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();
      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area4').innerHTML = ['<img height="200" width="160" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_4").val('Remove');
		$("#upload_filename4").css("height","0px");
		$("#upload_filename4").css("display","none");
		$("#button_4").attr('onclick','remove_photo(4)');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  function handleFileSelect5(evt) {
  var button_5 = $("#button_5").val();
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();
      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('preview-area5').innerHTML = ['<img height="200" width="160" class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		$("#button_5").val('Remove');
		$("#upload_filename5").css("height","0px");
		$("#upload_filename5").css("display","none");
		$("#button_5").attr('onclick','remove_photo(5)');
		
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  document.getElementById('upload_filename1').addEventListener('change', handleFileSelect1, false);
  document.getElementById('upload_filename2').addEventListener('change', handleFileSelect2, false);
  document.getElementById('upload_filename3').addEventListener('change', handleFileSelect3, false);
  document.getElementById('upload_filename4').addEventListener('change', handleFileSelect4, false);
  document.getElementById('upload_filename5').addEventListener('change', handleFileSelect5, false);
  function remove_photo(id){
  	$("#upload_filename"+id).val('');
	$("#button_"+id).val('Add a photo');
	$("#upload_filename"+id).css("height","25px");
	$("#upload_filename"+id).css("display","inline");
	$("#button_"+id).attr('onclick','').unbind('click');
	$("#preview-area"+id).html('<img src="<?php echo base_url()?>assetts/images/item.jpg" width="100" border="0"  />');
  }
</script>
<?php $this->load->view('frontend/footer');?>