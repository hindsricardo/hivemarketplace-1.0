			<div class="Row"><h2>Credit History</h2></div>
			<br class="cls" clear="all" />
			<div class="Row marTop10" style="">
				<?php for($i = $start; $i<($start + $limit); $i++){
				//foreach($credit_history as $credit_history_val){ ?>
				<?php
						if(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction' && $credit_history[$i]['winner_id'] == $this->session->userdata('user_id')){ ?>
							<p class="odd">Won <?php echo $credit_history[$i]['title'];?></p>
						<?php }elseif(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction'){ ?>
								<?php if(isset($credit_history[$i]['type']) && $credit_history[$i]['type'] == 'credit_auction' && $credit_history[$i]['winner_id'] == 0 && $credit_history[$i]['status'] == '1'){ ?>
                                    <p class="odd">Reversed <?php echo $credit_history[$i]['title'];?></p>
                                <?php }?>
							<p class="odd">Auctioned <?php echo ($credit_history[$i]['item_value']/$this->config->item("credit_val")).' '.$credit_history[$i]['title'];?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Bid'){ ?>
							<p class="odd">Bid <?php echo $credit_history[$i]['hive_credit'].' Honey Comb ITEM #'.$credit_history[$i]['auction_id'];?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Withdraw'){ ?>
							<p class="odd">Withdraw <?php echo $credit_history[$i]['hive_credit'].' Honey Combs';?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Buy'){ ?>
							<p class="odd">Bought <?php echo $credit_history[$i]['hive_credit'].' Honey Combs';?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] == 'Convert'){ ?>
							<p class="odd">Convert <?php echo $credit_history[$i]['hive_credit'].' Honey Combs';?></p>
						<?php }elseif(isset($credit_history[$i]['winner_id']) && $credit_history[$i]['winner_id'] = $this->session->userdata('user_id')){ ?>
							<p class="odd">Won Auction <?php echo $credit_history[$i]['title'];?></p>
						<?php }elseif(isset($credit_history[$i]['hive_action']) && $credit_history[$i]['hive_action'] = 'Reversed'){ ?>
							<p class="odd"><?php echo $credit_history[$i]['hive_credit'];?> bids returned from auction</p>
						<?php }?>
				<?php }?>
			</div>
		<!--pagination -->
		<div class="flt_lft Row">
		<ul class="pagination">
			<?php
			$adjacents = 1;
			if ($page == 0) $page = 1;					//if no page var is given, default to 1.
			$prev = $page - 1;							//previous page is page - 1
			$next = $page + 1;							//next page is page + 1
			$lastpage = ceil($total_rows/$limit);		//lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;						//last page minus 1
			$pagination = "";
			if($lastpage > 1)
			{	
				//previous button
				if ($page > 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($prev,$limit)\">Previous</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Previous</li>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active_page\">$counter</li>";
						else
							$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
						$pagination.= "<li>...</li>";
						$pagination.= "<li><a href=\"javascript:pagination($lpm1,$limit)\">$lpm1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination($lastpage,$limit)\">$lastpage</a></li>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<li><a href=\"javascript:pagination(1,$limit)\">1</a></li>";
						$pagination.= "<li><a href=\"javascript:pagination(2,$limit)\">2</a></li>";
						$pagination.= "<li>...</li>";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active_page\">$counter</li>";
							else
								$pagination.= "<li><a href=\"javascript:pagination($counter,$limit)\">$counter</a></li>";					
						}
					}
				}
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li class=\"next_prev\"><a href=\"javascript:pagination($next,$limit)\">Next</a></li>";
				else
					$pagination.= "<li class=\"next_prev\">Next</li>";
					
				echo $pagination;
			}
			?>
		</ul> 		
		</div>
		<!--pagination -->