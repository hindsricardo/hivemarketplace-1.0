<?php $this->load->view('frontend/header');?>
<?php //echo '<pre>';print_r($user_notifications);?>
<script src="<?php echo base_url()?>assetts/js/jquery.countdown.js"></script>
<script>
window.get_fb_complete = function(aid){
      var data = {aid:aid,abc:2}
      var feedback = $.ajax({
       type: "POST",
	   data:data,
       url: "<?php echo base_url(); ?>users/manage_credits/check_bids",
       async: false,
	   dataType: JSON,
   }).complete(function(){
       setTimeout(function(){get_fb_complete(aid);}, 10000);
   }).responseText;
   
   	var result = JSON.parse(feedback);
	
	
	var time_day1 = $( "#countdown_"+aid+" .countDays" ).find('.position').first();
	var time_day2 = $( "#countdown_"+aid+" .countDays" ).find('.position').last();
	var time_hr1 = $( "#countdown_"+aid+" .countHours" ).find('.position').first();
	var time_hr2 = $( "#countdown_"+aid+" .countHours" ).find('.position').last();
	var time_min1 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').first();
	var time_min2 = $( "#countdown_"+aid+" .countMinutes" ).find('.position').last();
	var time_sec1 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').first();
	var time_sec2 = $( "#countdown_"+aid+" .countSeconds" ).find('.position').last();
	var total_tm = parseInt(time_day1.text())+parseInt(time_day2.text())+parseInt(time_hr1.text())+parseInt(time_hr2.text())+parseInt(time_min1.text())+parseInt(time_min2.text())+parseInt(time_sec1.text())+parseInt(time_sec2.text());
	if(total_tm > 0){
		//alert(total_tm+" is > 0"+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('NO WINNER');
		}
	}else{
	//alert(total_tm+" is < 0"+">>>>>>>>>"+aid+" user name::::::"+result.winner.winner_username);
		if ($('#user_bid_section_'+aid).css('display') == 'block'){
			
			$('#user_bid_section_'+aid).css("display","none");
		}
		//alert(result.winner.winner_username+">>>>>>>>>"+aid);
		if(typeof result.winner.winner_username !== "undefined"){
			$('#current_winner_'+aid).html(result.winner.winner_username);
		}else{
			$('#current_winner_'+aid).html('NO WINNER');
		}
	}
	
	
	
	
	$('#no_of_bids_'+aid).html(result.bid);
	$('#auction_price_'+aid).html((result.bid*0.01).toFixed(2));
		//$('#current_winner_'+aid).html(result.winner.winner_username);
		$('#winner_contribution_bid_'+aid).html(result.winner.winner_credit);
		$('#winner_id_'+aid).html(result.winner.winner_user_id);
		
		$("#your_contribution_bid_"+aid).text(result.your_contribution);
		$("#your_contribution_"+aid).text(result.your_contribution*.01);
	   	$("#you_have_spend_"+aid).text(parseFloat(result.your_contribution*0.75).toFixed(2));
		
}
function bid_action(aid){
	
	var user_id = '<?php echo $this->session->userdata('user_id');?>';
	if(user_id){
		var credit_val = $("#credit_val_"+aid).val();
		if(!isNaN(credit_val)){
			var your_contribution_bid = parseInt($("#your_contribution_bid_"+aid).text());
			var your_total_contribution_bid = parseInt(credit_val)+parseInt(your_contribution_bid);
			var winner_contribution_bid = parseInt($('#winner_contribution_bid_'+aid).text());
			var winner_id = parseInt($('#winner_id_'+aid).text());
			
			if(winner_id != user_id){
				if(your_total_contribution_bid > winner_contribution_bid){
				var total_credit_available = parseInt($("#total_credit_available").text());
			if(parseInt(credit_val) <= total_credit_available){
				var hive_credit_available = parseInt($("#hive_credit_available").text());
				var bonus_credit_available = parseInt($("#bonus_credit_available").text());
				$.post("<?php echo base_url();?>users/manage_credits/bid",{ auction_id: aid, credit_val: credit_val, bonus_credit_available: bonus_credit_available, hive_credit_available: hive_credit_available, total_credit_available: total_credit_available }, function(data){
					if(data.logout == 'logout'){
						window.location.href = "<?php echo base_url();?>registration";
					}else if(data.not_enough_credit == 'not_enough_credit'){
						alert("Please Enter More Than "+(data.need_credit)+"Credit");
					}else{
						$("#credit_val_"+aid).val('');
						$("#total_credit_available").text(data.total_credit_available);
						var total_credit_value_available = parseFloat(data.total_credit_available*0.75).toFixed(2);
						$("#total_credit_value_available").text(total_credit_value_available);
						$("#hive_credit_available").text(data.hive_credit_available);
						$("#bonus_credit_available").text(data.bonus_credit_available);
						$("#no_of_bids_"+aid).text(data.auction_bid_no);
						$("#auction_price_"+aid).text(parseFloat(data.auction_bid_no*0.01).toFixed(2));
						$("#your_contribution_"+aid).text(parseFloat(data.your_contribution_bid*0.01).toFixed(2));
						$("#you_have_spend_"+aid).text(parseFloat(data.your_contribution_bid*0.75).toFixed(2));
						
							$("#your_contribution_bid_"+aid).text(data.your_contribution_bid);
							$('#current_winner_'+aid).html(data.winner.winner_username);
							$('#winner_contribution_bid_'+aid).html(data.winner.winner_credit);
							$('#winner_id_'+aid).html(data.winner.winner_user_id);
					}
				},'json');
			}else{
				alert("You have not enough credit");
			}
				}else{
					var need_credit = (winner_contribution_bid-your_contribution_bid);
					alert("Please Enter More Than "+(need_credit)+"Credit");
				}
			}else{
				alert("You current Winner !!!");
				$("#credit_val_"+aid).val('');
			}
		}
	}else{
		window.location.href = "<?php echo base_url();?>registration";
	}
}
function check_watchlist(aid){
	$.post("<?php echo base_url();?>users/activity/update_watchlist",{ auction_id: aid}, function(data){
		$("#auctionActivityRow_"+aid).css( "display", "none");
	});
}
function tag_search(tag){
	$("#tag_search_field").val(tag);
	$("#tag_search_form").submit();
}
document.userpop = function(id){ 
  $('.sucess_mail').remove();
  var userid = $('#user_id'+id).text();
  $.post("<?php echo base_url();?>feedback/rating",{ userid: userid, id:id }, function(response){
  //alert(response);  
  var substr = response.split('*');
  var avg_rating = substr[0];
  var total_rating = substr[1];
  var notes = substr[2];
  var createdDiv='<div id="senduser_mail" class="sucess_mail" style="max-width:300px;" align="left">';
  createdDiv+='<img src="<?php echo base_url();?>assetts/images/icon_user.png" width="16" height="16" align="absmiddle"/>';
  avg_rate = parseInt(avg_rating);
  
  var avg =Math.floor(avg_rate/3); 
  if(avg_rating!=''){
  for(var i=1; i<=avg; i++){
  createdDiv+='<img src="<?php echo base_url();?>assetts/images/rate_full.png" width="16" height="16" align="absmiddle"/>';
  }
  if(avg_rating%3 > 0){
  createdDiv+='<img src="<?php echo base_url();?>assetts/images/rate_half.png" width="16" height="16" align="absmiddle"/>';
  avg=avg+1;
  }
  var blank_avg = 5-avg; 
  for(var i=1; i<=blank_avg; i++){
  createdDiv+='<img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/>';
  }
  }else{
  for(var i=1; i<=5; i++){
  createdDiv+='<img src="<?php echo base_url();?>assetts/images/rate_blank.png" width="16" height="16" align="absmiddle"/>';
  total_rating=0;
  }
  }
  createdDiv+='&nbsp;<a href="javascript:;">('+total_rating+') Ratings</a>&nbsp;&nbsp;&nbsp;&nbsp; <a onclick="mailbox_open('+id+')" style="cursor:pointer;"><img src="<?php echo base_url();?>assetts/images/icon_mail_sm.png" width="25" height="25" border="0" align="absmiddle" /></a> <br/>'+notes+'<a class="close" style="cursor:pointer;" onclick="closed_userpop()">Close</a></div>';
$('.user_pop'+id).append(createdDiv);
  
  });
}
document.closed_userpop = function(){
   $('#senduser_mail').remove();
}
document.mailbox_open = function(id){
$('#senduser_mail').remove();
var title=$('#title'+id+' h3').text();
var createdDiv='<div id="senduser_mail" class="sucess_mail"><input name="subject" type="text" value="'+title+' # '+id+'" style="width:400px; height:25px;" readonly="readonly"/><br/><textarea name="message" id="messagess" style="width:400px; height:55px;" placeholder="Message" ></textarea><br/><a href="javascript:void(0);" class="btn_send_msg" onclick="send_msg1('+id+');"></a><a class="close" style="cursor:pointer;" onclick="closed_userpop()">Close</a></div>';
$('.user_pop'+id).append(createdDiv);
}
document.send_msg1 = function(id){
var usar_id = $('#user_id'+id).text();
var auction_id = id;
var subject = $('input[name=subject]').val();
	if($('#messagess').val()==''){
	alert('your message box is empty!!')
	}else{
	var msg = $('#messagess').val();
	
	$.post("<?php echo base_url();?>users/messages/sendMail",{ auction_usar_id: usar_id, auction_id:auction_id, subject: subject, msg: msg, type:'auction' }, function(response){
				   $('#senduser_mail').remove();
					  var createdDiv='<div id="suss_mail" class="sucess_mail">'+response+'</div>';
					  $('.user_pop'+id).append(createdDiv);					
					  setTimeout(function(){
					  $("#suss_mail").remove();
					}, 2000);				   
				 	
					});
	}
}

</script>
<style>
.countdownHolder{
	margin:0 auto;
	/*font: 40px/1.5 'Open Sans Condensed',sans-serif;*/
	text-align:center;
	letter-spacing:-3px;
}

.position{
	display: inline-block;
	height: 21px;
	overflow: hidden;
	position: relative;
	width: 15px;
}

.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}

.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}

/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{ /* display:none !important;*/ }
.countDiv0{ /* display:none !important;*/ }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}


.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}

.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}

.countDiv:after{
	top:0.9em;
}
.sucess_mail {
    background-color: #FFFFFF;
    border: 3px solid #CCCCCC;
    color: #000000;
	border-radius: 8px 8px 8px 8px;
    margin-bottom: 15px;
    padding: 15px !important;
    position: absolute;
	z-index:1000px;
}
a.close {
    background: url("<?php echo base_url();?>assetts/images/fancy_closebox.png") no-repeat scroll 0 0 transparent;
    height: 30px;
    overflow: hidden;
    position: absolute;
    right: -10px;
    text-indent: -1000px;
    top: -10px;
    width: 30px;
}

</style>
<div style="display:none">
<form name="tag_search_form" id="tag_search_form" method="post" action="<?php echo base_url();?>home/search">
	<input type="text" name="search_str" id="tag_search_field" />
</form>
</div>
<!--REGISTRATIOM AREA-->
<div class="Row borderbox">
<div class="messageWrap marTop10 clearfix">
<div class="Row98 RowMail_borBot">
	<ul class="messagebox_menu">
		<li <?php if($this->uri->segment(2) == 'activity'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/activity">Activity</a> </li>
		<li<?php if($this->uri->segment(2) == 'messages'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/messages" id="unread_msg">Messages (<?=count($unread);?>)</a></li>
		<li <?php if($this->uri->segment(2) == 'account_settings'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>users/account_settings">Account Settings</a> </li>
	</ul>
</div>
<div class="cls"><br clear="all" /></div>
<div class="Row98">
	<div class="messagebox_left_wrap">
		<div class="messagebox_left">
			<div class="flt_lft inbox_div first">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity" class="lnk">Summary</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/bids_and_offers" class="lnk">Bids/Offers</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/watchlist" class="lnk">Watchlist</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/paid" class="lnk">Paid</a></span>
			</div>
			<div class="flt_lft inbox_div" style="height:86px">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/sold" class="lnk">sold</a></span>
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/active_auction" class="lnk">Active Auction</a></span>
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/schedule_auction" class="lnk">Schedule Auction</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/recent_search" class="lnk">Recent Search</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/cases" class="lnk">Cases</a></span>
			</div>
			<div class="flt_lft inbox_div">
				<span class="inbox_head"><a href="<?php echo base_url();?>users/activity/direct_purchase" class="lnk">Direct Purchase</a></span>
			</div>
			
			
			
		</div>
		<!--<div class="flt_lft mar10"><input name="" type="button" class="btn_logout" /></div>-->
	</div><!--messagebox_left_wrap ends -->
	<div class="messagebox_right">
		<div class="Row98 clearfix">
			<div class="myAccountheader"><p>Watchlist</p></div>
			<div class="roundBox_black">
				<h1></h1>
				<p></p>
				<p></p>
			</div>
			<?php //echo '<pre>';print_r($credit_details);?>
			<textarea name="auction_ids" id="auction_ids" style="display:none;"><?php echo $auction_ids?></textarea>
			<?php //echo '<pre>';print_r($this->session->all_userdata());?>
			<?php foreach($list_summary as $list_summary_val){ ?>
				<?php
				$current_winner_details = get_current_winner($list_summary_val['id']);
				//echo '<pre>';print_r($current_winner_details);
				?>
				<div class="auctionActivityRow" id="auctionActivityRow_<?php echo $list_summary_val['id']?>" style="display:block;">
				<table width="100%" border="0" class="admin_comm_table">
					<tr>
						<td class="top" width="10%">
							<?php
							if(!empty($list_summary_val['images'])){
								$auction_image = 'upload/'.$list_summary_val['images'][0]['image_name'];
								if(file_exists($auction_image)){ ?>
									<img src="<?php echo base_url();?>upload/<?php echo $list_summary_val['images'][0]['image_name']?>" width="120"  border="0" />
									
									<?php 
									$item_image = base_url().'upload/'.$list_summary_val['images'][0]['image_name'];
									//$item_image = '<img src="'.base_url().'upload/'.$list_summary_val['images'][0]['image_name'].'" width="120"  border="0" />'?>
								<?php }else{ ?>
									<img src="<?php echo base_url();?>assetts/images/t1.jpg" width="90" height="90" border="0" />
									<?php $item_image = base_url().'assetts/images/t1.jpg';?>
								<?php }
							}else{ ?>
								<img src="<?php echo base_url();?>assetts/images/HIVEcomb.png" width="90" height="90" border="0" />
								<?php $item_image = base_url().'assetts/images/HIVEcomb.png';?>
							<?php }
							
							//echo $item_image;
							?>
						</td>
						<td width="2%">&nbsp;</td>
						<td  width="63%" class="marLft10 top">
							<table>
								<tr>
									<td class="top" width="152">
										<p class="txt11pN">List#: <?php echo $list_summary_val['id']?></p>
										<p class="txt12B marTop5"><a href="<?php echo base_url();?>users/item/details/<?php echo $list_summary_val['id']?>"><?php echo $list_summary_val['title']?></a></p>
										<!--<p class="txt11pN marTop5">Seller : <a href="#" class="txt11pN"><?php echo get_user_username($list_summary_val['user_id']);?></a></p>-->
										<div id="user_id<?php echo $list_summary_val['id']?>" style="display:none;"><?php echo $list_summary_val['user_id'];?></div>
										<p class="user_pop<?php echo $list_summary_val['id']?>" style="font-size: 12px;">Seller : <a id="user_pop<?php echo $list_summary_val['id']?>" onclick="userpop(<?php echo $list_summary_val['id'];?>)" style="cursor:pointer;"><?php echo get_user_username($list_summary_val['user_id']);?></a></p>
										<?php $tags = explode(" ",$list_summary_val['tags']); //echo '';print_r($tags);?>
										<p class="txt11pN marTop5">Tags : <?php foreach($tags as $tags_val){ ?> <a href="javascript:void(0)" class="lnk12NU" onclick='tag_search("<?php echo $tags_val;?>")'><?php echo $tags_val;?></a><?php }?></p>
										<p class="txt11pN marTop5 marRlt10"><?php echo $list_summary_val['description']?></p>
									</td>
									<td class="top" width="61">
										<p class="txt12B grey_txt">BIDS</p>
										<p class="txt13B marTop5"><div id="no_of_bids_<?php echo $list_summary_val['id']?>"><?php echo $list_summary_val['no_of_bids'];?></div></p>
										<p class="txt11pN marTop5">QTY : <?php echo $list_summary_val['qty']?></p>
										<P><a id="watchlist_<?php echo $list_summary_val['id']?>" href="javascript:void(0);" onclick="check_watchlist('<?php echo $list_summary_val['id']?>');"><img src="<?php echo base_url();?>assetts/images/icon_plus_c.png" width="18" height="18" border="0" align="absmiddle" /></a><span>Watchlist</span></P>
									</td>
									<td class="top" width="150">
										<p class="txt12B grey_txt">Summery</p>
										<p class="txt13B marTop5">Item value : $<?php echo $list_summary_val['item_value']?></p>
										<p class="txt11pN marTop5"><div class="txt11pN" style="float:left;">Auction price : $</div><div class="txt11pN" id="auction_price_<?php echo $list_summary_val['id']?>"><?php echo $list_summary_val['no_of_bids']*$this->config->item("pennies");?></div></p>
										<?php $user_bid_details = $this->session->userdata('bid_details');
										if(isset($user_bid_details['bid_hive'][$list_summary_val['id']])){
											$user_bid_hive = $user_bid_details['bid_hive'][$list_summary_val['id']];
										}else{
											$user_bid_hive = 0;
										}
										if(isset($user_bid_details['bid_bonus'][$list_summary_val['id']])){
											$user_bid_bonus = $user_bid_details['bid_bonus'][$list_summary_val['id']];
										}else{
											$user_bid_bonus =0;
										}
										 ?>
										 	<div id="winner_contribution_bid_<?php echo $list_summary_val['id'];?>" style="display:none"><?php if(!empty($current_winner_details)){ echo $current_winner_details['winner_credit'];}else{ echo '0';}?></div>
	<div id="winner_id_<?php echo $list_summary_val['id'];?>" style="display:none"><?php echo $current_winner_details['winner_user_id'];?></div>
	<div id="your_contribution_bid_<?php echo $list_summary_val['id'];?>" style="display:none;"><?php echo $user_bid_hive+$user_bid_bonus;?></div>

										<p class="txt11pN marTop5"><div class="txt11pN" style="float:left;">Your Contribution:$</div><div class="txt11pN" id="your_contribution_<?php echo $list_summary_val['id'];?>"><?php echo $this->config->item("pennies")*($user_bid_hive+$user_bid_bonus);?></div></p>
										<!--<p class="txt11pN marTop5">Credit Avg : 0</p>-->
										<p class="txt11pN marTop5"><div class="txt11pN" style="float:left;">You've spent : $</div><div class="txt11pN" id="you_have_spend_<?php echo $list_summary_val['id'];?>"><?php echo $this->config->item("credit_val")*($user_bid_hive+$user_bid_bonus);?></div></p>
										<!--<p class="txt11pN marTop5">Total if you win : $0.03</p>-->
									</td>
								</tr>
							</table>
						</td>
						<td class="top" width="25%">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="txtalignCenter" style="white-space:nowrap;">
										<div id="countdown_<?php echo $list_summary_val['id']?>"></div>
										<input type="hidden" name="auction_last_for" id="auction_last_for_<?php echo $list_summary_val['id']?>" value="<?php echo $list_summary_val['auction_endtime'];?>" />
										<!--<input type="hidden" name="auction_start_time" id="auction_start_time_<?php echo $list_summary_val['id']?>" value="<?php echo strtotime($list_summary_val['schedule_time']);?>" />-->
										<input type="hidden" name="schedule_endtime" id="schedule_endtime_<?php echo $list_summary_val['id']?>" value="<?php echo get_adjust_time($list_summary_val['schedule_endtime']);?>" />
										<div class="marTop5" id="user_bid_section_<?php echo $list_summary_val['id']?>" style="display:block;">
											<?php if($list_summary_val['user_id'] != $this->session->userdata('user_id')){?>
											<form class="bid_form" action="" id="b_<?php echo $list_summary_val['id']?>">
												<input type="hidden" name="aid" value="<?php echo $list_summary_val['id']?>"/>
												<input type="text" name="credit" id="credit_val_<?php echo $list_summary_val['id']?>" class="credit_val" value="" />
												<input type="button" class="bid_button" id="bid_button_<?php echo $list_summary_val['id']?>" onclick="bid_action('<?php echo $list_summary_val['id']?>')" />
											</form>
											<?php }?>
										</div>
										<p class="txt11pN marTop5"><div class="txt11pN" style="float:left;">Wiiner: </div><div class="txt11pN" id="current_winner_<?php echo $list_summary_val['id']?>"></div></p>
										<p class="marTop5 txt11pN">Minimun Auction Price :$<?php echo $list_summary_val['min_bids']*$this->config->item("pennies");?></p>
									</td>
									<td class="txtalignCenter mid">
										<!--<p class="txtalignCenter"><img src="<?php echo base_url();?>assetts/images/icon_fb.png" width="25" height="25" border="0" /></p>-->
										<p class="txtalignCenter"><a href="javascript:fbShare('<?php echo base_url();?>users/item/details/<?php echo $list_summary_val['id']?>', '<?php echo $list_summary_val['title']?>', '<?php echo $list_summary_val['description']?>', '<?php echo $item_image;?>', 520, 350)"><img src="<?php echo base_url();?>assetts/images/icon_fb.png" width="25" height="25" border="0" /></a></p>
										<p class="txtalignCenter marTop10"><a id="shareOnTwitter" title="Tweet example.com!" href="http://twitter.com/share?url=<?php echo base_url();?>users/item/details/<?php echo $list_summary_val['id']?>&counturl=http://example.com&text=Check outthis awesome dynamic page&via=<?php echo $this->session->userdata('fname').' '.$this->session->userdata('lname') ?>" onclick="shareOnTwitter(this.href);return false;"><img src="<?php echo base_url();?>assetts/images/icon_twitter.png" width="25" height="25" border="0" /></a></p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="mid" colspan="8">
							<div class="flt_lft">
							</div>
						</td>
					</tr>
				</table>
			</div>
			<?php }?>
		</div>
	</div>
</div>
</div>
<div class="cls"><br clear="all" /></div>
</div>
<script type="text/javascript">
function shareOnTwitter(url) {
popupWindow = window.open(url,'popUpWindow','height=300,width=500,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
}
</script>
<script>
  function fbShare(url, title, descr, image, winWidth, winHeight) {
       var winTop = (screen.height / 2) - (winHeight / 2);
       var winLeft = (screen.width / 2) - (winWidth / 2);
       window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width='+winWidth+',height='+winHeight);
   }
</script>
<script>
$(function(){
	var auction_ids = $("#auction_ids").val();
	var stringArray = auction_ids.split(',');
	for (i=0;i<stringArray.length;i++)
		{
			if(stringArray[i] != ''){
				//var auction_last_for = $("#auction_last_for_"+stringArray[i]).val();
				//var auction_start_time = $("#auction_start_time_"+stringArray[i]).val();
				//var auction_end_time = parseInt(auction_start_time)+ (parseInt(auction_last_for)*60*60);
				var schedule_endtime = $("#schedule_endtime_"+stringArray[i]).val();
				var auction_end_time = parseInt(schedule_endtime);
				var get_date = new Date(parseInt(auction_end_time) * 1000);
				var a_year = get_date.getFullYear();
				var months = get_date.getMonth();
				//months = parseInt(months)+1;
				months = parseInt(months);
				var note = $('#note'),
					ts = new Date(get_date.getFullYear(), months, get_date.getDate(), get_date.getHours(), get_date.getMinutes(), get_date.getSeconds()),
					newYear = true;
				if((new Date()) > ts){
					// The new year is here! Count towards something else.
					// Notice the *1000 at the end - time must be in milliseconds
					ts = (new Date()).getTime();
					newYear = false;
				}
				
				$('#countdown_'+stringArray[i]).countdown({
					timestamp	: ts,
					callback	: function(days, hours, minutes, seconds){
						var message = "";
						message += days + " day" + ( days==1 ? '':'s' ) + ", ";
						message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
						message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
						message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";
						if(newYear){
							message += "left until the new year!";
						}
						else {
							message += "left to 10 days from now!";
						}
						note.html(message);
					}
				});
			}
		}
});
$(document).ready(function(){
	var credit_val = $("#credit_val").text();
	var aids = $("#auction_ids").val();
	var aids_array = aids.split(',');
	for(i=0;i<aids_array.length;i++){
		if(aids_array[i] != ''){
			get_fb_complete(aids_array[i]);
		}
	}
});

</script>
<?php $this->load->view('frontend/footer');?>