<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config["credit_val"] = '0.75';
$config["withdraw_credits_val"] = '0.71';
$config["pennies"] = '0.01';
$config["support_email"] = 'support@gmail.com';
$config["reward_amount"] = '0.0375';
$config["reward_purchase_amount"] = '0.75';
$config["pickup_scheduled_date"] = '7';
$config["timezones"] = array (
  '(GMT- 12:00) International Date Line West' => 
  array (
	'gmtOffset' => -720,
  ),
   '(GMT- 11:00) Midway Island Samoa' => 
  array (
	'gmtOffset' => -660,
  ),
   '(GMT- 10:00) Hawaii' => 
  array (
	'gmtOffset' => -600,
  ),
  '(GMT- 09:00) Alaska' => 
  array (
	'gmtOffset' => -540,
  ),
   '(GMT- 08:00) Pacific Time (US & Canada)' => 
  array (
	'gmtOffset' => -480,
	'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => 7,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => 0,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT- 08:00) Tijuna, Baja California' => 
  array (
	'gmtOffset' => -480,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
	 '(GMT- 07:00) Arizona' => 
  array (
	'gmtOffset' => -420,
  ),
   '(GMT- 07:00)  Chihuahua, La Paz, Mazatlan - New' => 
  array (
	'gmtOffset' => -420,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT- 07:00)  Chihuahua, La Paz, Mazatlan - Old' => 
  array (
	'gmtOffset' => -420,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
    '(GMT- 07:00) Mountain Time (US & Canada)' => 
  array (
	'gmtOffset' => -420,
  ),
   '(GMT- 06:00) Central America' => 
  array (
	'gmtOffset' => -360,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => 7,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => 0,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
 ),
	 '(GMT- 06:00) Central Time (US & Canda)' => 
  array (
	'gmtOffset' => -360,
  ),
   '(GMT- 06:00) Guadalajara, Mexico City, Monterrey - New' => 
  array (
	'gmtOffset' => -360,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT- 06:00) Guadalajara, Mexico City, Monterrey - Old' => 
  array (
	'gmtOffset' => -360,
  ),
    '(GMT- 06:00) Saskatchewan' => 
  array (
	'gmtOffset' => -360,
  ),
  '(GMT- 05:00) Bogota, Lima, Quito, Rio Branco' => 
  array (
	'gmtOffset' => -300,
  ),
   '(GMT- 05:00) Eastern Time (US & Canada)' => 
  array (
	'gmtOffset' => -300,
  ),
   '(GMT- 05:00) Indiana (East)' => 
  array (
	'gmtOffset' => -300,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => 7,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => 0,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
  '(GMT- 04:00) Atlantic Time (Canda)' => 
  array (
	'gmtOffset' => -240,
    'dstOffset' => 60,
    'dstMonth' => 9,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 4,
    'stdStartday' => 15,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT- 04:00) Caracas, La Paz' => 
  array (
	'gmtOffset' => -240,
  ),
   '(GMT- 04:00) Manaus' => 
  array (
	'gmtOffset' => -240,
  ),
  '(GMT- 04:00) Santiago' => 
  array (
	'gmtOffset' => -240,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 9,
    'dstWeekday' => 0,
    'stdMonth' => 3,
    'stdStartday' => 9,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 28800,
    'stdStartTimeSec' => 25200,
  ),
   '(GMT- 03:30) Newfoundland' => 
  array (
	'gmtOffset' => -210,
  ),
    '(GMT- 03:00) Brazilia' => 
  array (
	'gmtOffset' => -180,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 15,
    'dstWeekday' => 0,
    'stdMonth' => 2,
    'stdStartday' => 15,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => -3600,
  ),
  '(GMT- 03:00) Buenos Aires, Georgtown' => 
  array (
	'gmtOffset' => -180,
  ),
   '(GMT- 03:00) Greenland' => 
  array (
	'gmtOffset' => -180,
  ),
   '(GMT- 03:00) Montevideo' => 
  array (
	'gmtOffset' => -180,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 9,
    'dstWeekday' => -1,
    'stdMonth' => 3,
    'stdStartday' => 27,
    'stdWeekday' => -1,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
  '(GMT- 02:00) Mid-Atlantic' => 
  array (
	'gmtOffset' => -120,
  ),
   '(GMT- 01:00) Azores' => 
  array (
	'gmtOffset' => -60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
    '(GMT- 01:00) Cape Verde Is.' => 
  array (
	'gmtOffset' => -60,
  ),
  '(GMT) Casablanka, Monrovia, Reykjavik' => 
  array (
	'gmtOffset' => 0,
  ),
   '(GMT) Greenwhich Mean Time : Dublin, Edinburgh, Lisbon, London' => 
  array (
	'gmtOffset' => 0,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 3600,
    'stdStartTimeSec' => 3600,
  ),
  '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
  '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+01:00) Sarajeva,Skopje, Warsaw, Zagreb' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+01:00) West Central Africa' => 
  array (
	'gmtOffset' => 60,
  ),
  '(GMT+02:00) Amman' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 4,
    'stdMonth' => 9,
    'stdStartday' => -1,
    'stdWeekday' => 4,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+02:00) Athens, Bucharest, Istanbul' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => -3600,
    'stdStartTimeSec' => -3600,
  ),
   '(GMT+02:00) Beriut' => 
  array (
	'gmtOffset' => 120,
  ),
   '(GMT+02:00) Cario' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => -1,
    'dstWeekday' => 5,
    'stdMonth' => 9,
    'stdStartday' => -1,
    'stdWeekday' => 4,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => 82800,
  ),
     '(GMT+02:00) Harare, Pretoria' => 
  array (
	'gmtOffset' => 120,
  ),
   '(GMT+02:00) Helsinki, Kyiv, Rig, Sofia, Tallin, Vilnius' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => -3600,
    'stdStartTimeSec' => -3600,
  ),
  '(GMT+02:00) Jerusalem' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => 24,
    'dstWeekday' => 5,
    'stdMonth' => 9,
    'stdStartday' => 12,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT+02:00) Minsk' => 
  array (
	'gmtOffset' => 120,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+02:00) Windhoek' => 
  array (
	'gmtOffset' => 60,
    'dstOffset' => 60,
    'dstMonth' => 9,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 4,
    'stdStartday' => 1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 3600,
  ),
   '(GMT+03:00) Baghdad' => 
  array (
	'gmtOffset' => 180,
    'dstOffset' => 60,
    'dstMonth' => 4,
    'dstStartday' => 1,
    'dstWeekday' => -1,
    'stdMonth' => 10,
    'stdStartday' => 1,
    'stdWeekday' => -1,
    'dstStartTimeSec' => 10800,
    'stdStartTimeSec' => 10800,
  ),
   '(GMT+03:00) Kuwait, Riyadh' => 
  array (
	'gmtOffset' => 180,
  ),
  '(GMT+03:00) Moscow, St. Petersburg, Volgograd' => 
  array (
	'gmtOffset' => 180,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+03:00) Nairobi' => 
  array (
	'gmtOffset' => 180,
  ),
   '(GMT+03:00) Tbilisi' => 
  array (
	'gmtOffset' => 180,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+03:00) Tehran' => 
  array (
	'gmtOffset' => 210,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => 22,
    'dstWeekday' => -1,
    'stdMonth' => 9,
    'stdStartday' => 22,
    'stdWeekday' => -1,
    'dstStartTimeSec' => 0,
    'stdStartTimeSec' => -3600,
  ),
     '(GMT+04:00) Abu Dhabi, Muscat' => 
  array (
	'gmtOffset' => 240,
  ),
  '(GMT+04:00) Baku' => 
  array (
	'gmtOffset' => 240,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 3600,
    'stdStartTimeSec' => 0,
  ),
   '(GMT+04:00) Yerevan' => 
  array (
	'gmtOffset' => 240,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+04:00) Kabul' => 
  array (
	'gmtOffset' => 240,
  ),
   '(GMT+05:00) Ekaterinburg' => 
  array (
	'gmtOffset' => 300,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
    '(GMT+05:00) Islamabad, Karachi, Tashkent' => 
  array (
	'gmtOffset' => 300,
  ),
  '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi' => 
  array (
	'gmtOffset' => 330,
  ),
   '(GMT+05:30) Sri Jayawardenepura' => 
  array (
	'gmtOffset' => 330,
  ),
   '(GMT+05:45) Kathmandu' => 
  array (
	'gmtOffset' => 345,
  ),
   '(GMT+06:00) Almaty, Novosibirsk' => 
  array (
	'gmtOffset' => 360,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+06:00) Astana, Dhaka' => 
  array (
	'gmtOffset' => 360,
  ),
   '(GMT+06:30) Yangon (Rangoon)' => 
  array (
	'gmtOffset' => 390,
  ),
   '(GMT+07:00) Bangkok, Hanoi, Jakarta' => 
  array (
	'gmtOffset' => 420,
  ),
   '(GMT+07:00) Krasnoyarsk' => 
  array (
	 'gmtOffset' => 420,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
    '(GMT+08:00) Beijing, Chongping, Hong Kong, Urmaqi' => 
  array (
	'gmtOffset' => 480,
  ),
   '(GMT+08:00) Irkutsk, Ulaan Bataar' => 
  array (
	'gmtOffset' => 480,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+08:00) Kuala Lumpur, Singapore' => 
  array (
	'gmtOffset' => 480,
  ),
   '(GMT+08:00) Perth' => 
  array (
	'gmtOffset' => 480,
  ),
   '(GMT+08:00) Taipei' => 
  array (
	'gmtOffset' => 480,
  ),
  '(GMT+09:00) Osaka, Sapporo, Tokyo' => 
  array (
	'gmtOffset' => 540,
  ),
    '(GMT+09:00) Seoul' => 
  array (
	'gmtOffset' => 540,
  ),
   '(GMT+09:00) Yakutsk' => 
  array (
	'gmtOffset' => 540,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+09:30) Adelaide' => 
  array (
	 'gmtOffset' => 570,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 4,
    'stdStartday' => 1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+09:30) Darwin' => 
  array (
	'gmtOffset' => 570,
  ),
   '(GMT+10:00) Brisbane' => 
  array (
	'gmtOffset' => 600,
  ),
    '(GMT+10:00) Canberra, Melbourne, Sydney' => 
  array (
	'gmtOffset' => 600,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 4,
    'stdStartday' => 1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+10:00) Guam, Port Moresby' => 
  array (
	'gmtOffset' => 600,
  ),
  '(GMT+10:00) Hobart' => 
  array (
	'gmtOffset' => 600,
  ),
    '(GMT+10:00) Vladivostok' => 
  array (
	 'gmtOffset' => 600,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+11:00) Magadan, Solomon Is., New Caledonia' => 
  array (
	 'gmtOffset' => 660,
    'dstOffset' => 60,
    'dstMonth' => 3,
    'dstStartday' => -1,
    'dstWeekday' => 0,
    'stdMonth' => 10,
    'stdStartday' => -1,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+12:00) Auckland, wellington' => 
  array (
	 'gmtOffset' => 720,
    'dstOffset' => 60,
    'dstMonth' => 10,
    'dstStartday' => 1,
    'dstWeekday' => 0,
    'stdMonth' => 3,
    'stdStartday' => 15,
    'stdWeekday' => 0,
    'dstStartTimeSec' => 7200,
    'stdStartTimeSec' => 7200,
  ),
   '(GMT+12:00) Fiji, Kamchatka, Marshall Is.' => 
  array (
	'gmtOffset' => 720,
  ),
   '(GMT+13:00) Nuku alofa' => 
  array (
	'gmtOffset' => 780,
  ),
);
?>