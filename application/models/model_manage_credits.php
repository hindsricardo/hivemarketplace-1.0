<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_manage_credits extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	public function summary(){
		$data = array();
		$data['hive_credit'] = $this->hive_credit_sum();
		$data['bonus_credit'] = $this->bonus_credit_sum();
		return $data;
	}
	public function hive_credit_sum($user_id = ''){
		if($user_id == ''){
			$user_id = $this->session->userdata('user_id');
		}
		$select_hive_sql = "SELECT sum( `credits` ) as hive_credit, sum( `credits_price` ) as hive_credits_price ,
							action FROM `user_buycredits`
							WHERE `user_id` = '".$user_id."'
							GROUP BY `action` ORDER BY action";
		$hive_query = $this->db->query($select_hive_sql);
		//$hive_result = $hive_query->result();
		//echo '<pre>';print_r($hive_result);die();
		$data = array();
		if($hive_query->num_rows() > 0){
			$hive_result = $hive_query->result();
			foreach($hive_result as $hive_result_val){
				$data['hive_credit_'.$hive_result_val->action] = $hive_result_val->hive_credit;
				$data['hive_credits_price_'.$hive_result_val->action] = $hive_result_val->hive_credits_price;
			}
			//$data['hive_credit'] = $hive_result[0]->hive_credit;
			//$data['hive_credits_price'] = $hive_result[0]->hive_credits_price;
		}
		//echo '<pre>';print_r($data);die();
		return $data;
	}
	public function bonus_credit_sum($user_id = ''){
		if($user_id == ''){
			$user_id = $this->session->userdata('user_id');
		}
		$select_bonus_sql = "SELECT sum(credits) as bonus_credit, sum(credits_price) as bonus_credits_price, action
							 FROM user_bonus_credits
							 WHERE user_id = '".$user_id."'
							 GROUP BY `action` ORDER BY action";
		$bonus_query = $this->db->query($select_bonus_sql);
		$data = array();
		if($bonus_query->num_rows() > 0){
			$bonus_result = $bonus_query->result();
			
			foreach($bonus_result as $bonus_result_val){
				$data['bonus_credit_'.$bonus_result_val->action] = $bonus_result_val->bonus_credit;
				$data['bonus_credits_price_'.$bonus_result_val->action] = $bonus_result_val->bonus_credits_price;
			}
			//$data['bonus_credit'] = $bonus_result[0]->bonus_credit;
			//$data['bonus_credits_price'] = $bonus_result[0]->bonus_credits_price;
		}
		return $data;
	}
	public function create_auction($auction_record){
		$insert_query = "INSERT INTO auction SET
							user_id = '".$auction_record['user_id']."',
							title = '".$auction_record['title']."',
							qty = '".$auction_record['qty']."',
							remaining_qty = '".$auction_record['remaining_qty']."',
							item_value = '".$auction_record['item_value']."',
							auction_endtime = '".$auction_record['auction_endtime']."',
							paypal_account = '".$auction_record['paypal_account']."',
							auction_type = '".$auction_record['auction_type']."',
							schedule_time = '".$auction_record['schedule_time']."',
							schedule_endtime = ADDDATE('".$auction_record['schedule_time']."', INTERVAL ".$auction_record['auction_endtime']." HOUR),
							create_date = '".$auction_record['create_date']."',
							modify_date = '".$auction_record['modify_date']."',
							modify_by = '".$auction_record['modify_by']."',
							type = '".$auction_record['type']."',
							HMP_fee = '".$auction_record['HMP_fee']."',
							status = '".$auction_record['status']."'
							";
		$query = $this->db->query($insert_query);
		return $this->db->insert_id();
	}
	public function insert_credits($credit_val, $credit_type){
		if($credit_type == 'HIVECombs'){
			$this->db->insert('user_buycredits',$credit_val);
		}else{
			$this->db->insert('user_bonus_credits',$credit_val);
		}
		$insert_id = $this->db->insert_id();
		if($credit_val['action'] == 'Bid'){
			$rewards = "UPDATE rewards SET
						reward_amount = reward_amount + '".$credit_val['credits']*$this->config->item('reward_amount')."',
						date = '".to_db_date()."'
						WHERE user_id = '".$this->session->userdata('user_id')."'";
			
			$this->db->query($rewards);
		}
		return $insert_id;
	}
	public function user_available_credit($user_id){
		$data['hive_credit'] = $this->hive_credit_sum($user_id);
		$data['bonus_credit'] = $this->bonus_credit_sum($user_id);
		
		if(!isset($data['hive_credit']['hive_credit_Buy'])) $data['hive_credit']['hive_credit_Buy']= 0;
		if(!isset($data['hive_credit']['hive_credit_Auction'])) $data['hive_credit']['hive_credit_Auction']= 0;
		if(!isset($data['hive_credit']['hive_credit_Bid'])) $data['hive_credit']['hive_credit_Bid']= 0;
		if(!isset($data['hive_credit']['hive_credit_Withdraw'])) $data['hive_credit']['hive_credit_Withdraw']= 0;
		if(!isset($data['bonus_credit']['bonus_credit_Convert'])) $data['bonus_credit']['bonus_credit_Convert']= 0;
		if(!isset($data['bonus_credit']['bonus_credit_Auction'])) $data['bonus_credit']['bonus_credit_Auction']= 0;
		if(!isset($data['bonus_credit']['bonus_credit_Bid'])) $data['bonus_credit']['bonus_credit_Bid']= 0;
		
		$data['hive_credit_available'] = $data['hive_credit']['hive_credit_Buy']-($data['hive_credit']['hive_credit_Auction']+$data['hive_credit']['hive_credit_Bid']+$data['hive_credit']['hive_credit_Withdraw']);
		$data['bonus_credit_available'] = $data['bonus_credit']['bonus_credit_Convert']-($data['bonus_credit']['bonus_credit_Auction']+$data['bonus_credit']['bonus_credit_Bid']);
		$data['total_credit_available'] = $data['hive_credit_available']+$data['bonus_credit_available'];
		return $data;
		//echo '<pre>';print_r($data);
	}
	public function auction_total_credits($auction_id){
		$select_credit = "SELECT sum( credits ) total_credit
							FROM (
								SELECT credits
								FROM user_buycredits
								WHERE ACTION = 'Bid'
								AND auction_id = '".$auction_id."'
								UNION ALL
								SELECT credits
								FROM user_bonus_credits
								WHERE ACTION = 'Bid'
								AND auction_id = '".$auction_id."'
							)a";
		$query = $this->db->query($select_credit);
		if($query->num_rows() > 0){
			$total_credit = $query->result();
			return $total_credit[0]->total_credit;
		}else{
			return '';
		}
	}
	function reverse_credits($aid){
		$select_reversed_credits = "SELECT user_id, SUM( credit ) as total_credit, SUM( `credits_price` ) AS total_credits_price
			FROM (
				SELECT user_id, SUM( `credits` ) AS credit, SUM( `credits_price` ) AS credits_price
				FROM `user_buycredits`
				WHERE `action` = 'Bid'
				AND `auction_id` = '".$aid."'
				GROUP BY `user_id`
				UNION ALL
				SELECT user_id, SUM( `credits` ) AS credits, SUM( `credits_price` ) AS credits_price
				FROM `user_bonus_credits`
				WHERE `action` = 'Bid'
				AND `auction_id` = '".$aid."'
				GROUP BY `user_id`
			)a
			GROUP BY a.user_id ";
		$reversed_query = $this->db->query($select_reversed_credits);
		$result_reversed = $reversed_query->result('array');
		
		if(!empty($result_reversed)){
			foreach($result_reversed as $result_reversed_val){
				$insert_reversed_query = "INSERT INTO auction_reversed SET
								user_id = '".$result_reversed_val['user_id']."',
								auction_id = '".$aid."',
								credits = '".$result_reversed_val['total_credit']."',
								credits_price = '".$result_reversed_val['total_credits_price']."',
								date_entered = '".to_db_date()."',
								action = 'Reversed',
								status = '0'";
				$this->db->query($insert_reversed_query);
			}
		}
	
	
		$delete_buycredits_sql = "DELETE FROM user_buycredits WHERE auction_id = '".$aid."' AND action = 'Bid'";
		$buycredits_query = $this->db->query($delete_buycredits_sql);
		$delete_bonus_credits_sql = "DELETE FROM user_bonus_credits WHERE auction_id = '".$aid."' AND action = 'Bid'";
		$buycredits_query = $this->db->query($delete_bonus_credits_sql);
		
		$update_sql = "UPDATE auction SET status = '1' WHERE ID = '".$aid."'";
		$this->db->query($update_sql);
		return $aid;
	}
	public function get_rewards($user_id){
		$select_rewards = "SELECT id, reward_amount, date
							FROM `rewards`
							WHERE user_id ='".$user_id."'";
		$rewards_query = $this->db->query($select_rewards);
		if($rewards_query->num_rows() > 0){
			$rewards_result = $rewards_query->result();
			return $rewards_result;
		}else{
			return array();
		}
	}
	public function redeem_rewards($value){
		$rewards = "UPDATE rewards SET
					reward_amount = '".$value['reward_amount']."',
					date = '".$value['date']."'
					WHERE user_id = '".$this->session->userdata('user_id')."'";
		$query = $this->db->query($rewards);
	}
	
	public function insert_rewards_bonus_credit($value){
		//echo '<pre>';print_r($value);die();
		$rewards = "INSERT INTO user_bonus_credits SET
						user_id = '".$value['user_id']."',
						credits = '".$value['credits']."',
						credits_price = '".$value['credits_price']."',
						create_date = '".$value['create_date']."',
						action = '".$value['action']."'";
		$rewards_query = $this->db->query($rewards);
	}
	
	
	function withdraw_credits(){
		$withdraw_credits = "SELECT * FROM user_buycredits WHERE ACTION = 'Buy' AND user_id='".$this->session->userdata('user_id')."' AND status!=1";
		$query = $this->db->query($withdraw_credits);
		 if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	function refund_credits($id){
		$withdraw_credits = "SELECT * FROM user_buycredits WHERE id='".$id."'";
		$query = $this->db->query($withdraw_credits);
		 if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}

	public function update_shipped_scheduled_pickup($value){
		$update_sql = "UPDATE orders SET payment_scheduled_date = '".$value['payment_scheduled_date']."' WHERE order_id = '".$value['order_id']."'";
		$query = $this->db->query($update_sql);
	}	
	public function seller_payment_products(){
		$select_sql = "SELECT * FROM orders as o LEFT JOIN order_details as od
						ON o.order_id = od.order_id
						WHERE od.product_name != 'buy credit'
						AND o.payment_status = 'Pending'
						AND payment_scheduled_date <= UTC_TIMESTAMP()";
		$query = $this->db->query($select_sql);
		 if($query->num_rows() > 0){
			$result = $query->result();
			
			$i=0;
			foreach($result as $result_val){
				$data[$i]['order_id'] = $result_val->order_id;
				$data[$i]['buyer_id'] = $result_val->user_id;
				$data[$i]['payer_id'] = $result_val->payer_id;
				$data[$i]['txn'] = $result_val->txn;
				$data[$i]['payment_status'] = $result_val->payment_status;
				$data[$i]['order_status'] = $result_val->order_status;
				$data[$i]['date'] = $result_val->date;
				$data[$i]['payment_scheduled_date'] = $result_val->payment_scheduled_date;
				$data[$i]['product_id'] = $result_val->product_id;
				$data[$i]['product_name'] = $result_val->product_name;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['price'] = $result_val->price;
				$data[$i]['shipping'] = $result_val->shipping;
				
				$auction_details = $this->model_list_auction->get_auction_details($result_val->product_id);
				//echo '<pre>';print_r($auction_details);die();
				$data[$i]['HMP_fee'] = $auction_details[0]['HMP_fee'];
				$data[$i]['seller_id'] = $auction_details[0]['user_id'];
				$data[$i]['seller_paypal_email'] = get_user_paypalemail($auction_details[0]['user_id']);
				$i++;
			}			
			return $data;
		}else{
			return array();
		}
	}
	public function user_credit_history($user_id){
	$result_data = array();
		$select_auction_sql = "SELECT a.ID, a.title, a.item_value, a.type, a.winner_id, a.modify_date, a.status
								FROM auction AS a
								WHERE (
								a.user_id = '".$user_id."'
								AND a.type = 'credit_auction'
								)
								OR a.winner_id = '".$user_id."'
								ORDER BY modify_date DESC";
		$query_auction = $this->db->query($select_auction_sql);
		$result = $query_auction->result('array');
		$i = 0;
		foreach($result as $result_val){
			$result_data[$i]['id'] = $result_val['ID'];
			$result_data[$i]['title'] = $result_val['title'];
			$result_data[$i]['item_value'] = $result_val['item_value'];
			$result_data[$i]['type'] = $result_val['type'];
			$result_data[$i]['winner_id'] = $result_val['winner_id'];
			$result_data[$i]['status'] = $result_val['status'];
			$result_data[$i]['date_mod'] = strtotime($result_val['modify_date']);
			$i++;
		}
		//echo "<pre>";print_r($result_data);die();
		$select_hive_sql = "SELECT ub.auction_id, ub.credits AS hive_credit, ub.action AS hive_action, ub.create_date
						FROM user_buycredits AS ub
						WHERE ub.user_id = '".$user_id."' and status != '1'
						ORDER BY create_date DESC";
		$query_hive = $this->db->query($select_hive_sql);
		$result = $query_hive->result('array');
		
		foreach($result as $result_val){
			$result_data[$i]['auction_id'] = $result_val['auction_id'];
			$result_data[$i]['hive_credit'] = $result_val['hive_credit'];
			$result_data[$i]['hive_action'] = $result_val['hive_action'];
			$result_data[$i]['date_mod'] = strtotime($result_val['create_date']);
			$i++;
		}
		$select_bonus_sql = "SELECT ubc.auction_id, ubc.credits AS bonus_credit, ubc.action AS bonus_action, ubc.create_date
						FROM user_bonus_credits AS ubc
						WHERE ubc.user_id = '".$user_id."'
						ORDER BY create_date DESC";
		$query_bonus = $this->db->query($select_bonus_sql);
		$result = $query_bonus->result('array');
		
		foreach($result as $result_val){
			$result_data[$i]['auction_id'] = $result_val['auction_id'];
			$result_data[$i]['hive_credit'] = $result_val['bonus_credit'];
			$result_data[$i]['hive_action'] = $result_val['bonus_action'];
			$result_data[$i]['date_mod'] = strtotime($result_val['create_date']);
			$i++;
		}
		
		$select_reversed_sql = "SELECT ar.auction_id, ar.credits, ar.action AS reversed_action, ar.date_entered
						FROM auction_reversed AS ar
						WHERE ar.user_id = '".$user_id."'
						ORDER BY date_entered DESC";
		$query_reversed = $this->db->query($select_reversed_sql);
		$result = $query_reversed->result('array');
		
		foreach($result as $result_val){
			$result_data[$i]['auction_id'] = $result_val['auction_id'];
			$result_data[$i]['hive_credit'] = $result_val['credits'];
			$result_data[$i]['hive_action'] = $result_val['reversed_action'];
			$result_data[$i]['date_mod'] = strtotime($result_val['date_entered']);
			$i++;
		}
		
		
		//array sort
		foreach($result_data as $c=>$key) {
			$sort_date_mod[] = $key['date_mod'];
		}
		array_multisort($sort_date_mod, SORT_DESC, $result_data);
		return $result_data;
	}
	public function send_shipping_or_pickup($order_data){
		$update_sql = "UPDATE order_details SET payment_scheduled_date = '".$order_data['payment_scheduled_date']."' WHERE id = '".$order_data['id']."' AND seller_payment ='0'";
		$this->db->query($update_sql);
		return $order_data['id'];
	}
	public function order_shipped_scheduled_pickup_check($order_details_id){
		$select_sql = "SELECT shipping_or_pickup, payment_scheduled_date FROM order_details WHERE id = '".$order_details_id."'";
		$query = $this->db->query($select_sql);
		 if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function is_item_received($order_details_id){
			$select_sql = "SELECT order_status FROM order_details WHERE id = '".$order_details_id."'";
		$query = $this->db->query($select_sql);
		 if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function credit_auction_payment($record){
			$this->config->load('paypal');
			$config = array(
				'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
				'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
				'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
				'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
				'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
				'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			);
			if($config['Sandbox'])
			{
				error_reporting(E_ALL);
				ini_set('display_errors', '1');
			}
			
			$this->load->library('paypal/Paypal_pro', $config);	
				$product_details = $this->model_paypal->get_seller_item_details($record['auction_id']);
				$product_price = $record['total_bid']*$this->config->item('credit_val');
				$price = $product_price*(1-$product_details[0]['HMP_fee']/100);
				$hmp_fee_amount = ($product_price*$product_details[0]['HMP_fee'])/100;
				$MPFields = array(
									'emailsubject' => 'Buyer has been received the item '.$product_details[0]['title'].' from HIVEMarketPlace', 						// The subject line of the email that PayPal sends when the transaction is completed.  Same for all recipients.  255 char max.
									'currencycode' => 'USD', 						// Three-letter currency code.
									'receivertype' => 'EmailAddress' 						// Indicates how you identify the recipients of payments in this call to MassPay.  Must be EmailAddress or UserID
								);
				
				// MassPay accepts multiple payments in a single call.  
				// Therefore, we must create an array of payments to pass into the class.
				// In this sample we're simply passing in 2 separate payments with static amounts.
				// In most cases you'll be looping through records in a data source to generate the $MPItems array below.
				
				$Item1 = array(
							'l_email' => $product_details[0]['paypal_account'], 							// Required.  Email address of recipient.  You must specify either L_EMAIL or L_RECEIVERID but you must not mix the two.
							'l_receiverid' => '', 						// Required.  ReceiverID of recipient.  Must specify this or email address, but not both.
							'l_amt' => number_format($price,2), 								// Required.  Payment amount.
							'l_uniqueid' => strtotime(to_db_date()), 						// Transaction-specific ID number for tracking in an accounting system.
							'l_note' => '' 								// Custom note for each recipient.
						);
				//$MPItems = array($Item1, $Item2);
				$MPItems = array($Item1);
				
				$PayPalRequestData = array(
								'MPFields' => $MPFields, 
								'MPItems' => $MPItems
							);
							
				$PayPalResult = $this->paypal_pro->MassPay($PayPalRequestData);
				//echo '<pre>';print_r($PayPalResult);die();
				if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
				{
					//echo '<pre>';print_r($errors);echo $errors;
					//$this->load->view('paypal_error',$errors);
				}
				else
				{
					$insert_buycredits = "INSERT INTO user_buycredits SET user_id = '".$record['buyer_id']."', auction_id = '".$record['auction_id']."',credits = '".($product_details[0]['item_value']/$this->config->item('credit_val'))."',credits_price = '".$product_details[0]['item_value']."',txn = '',create_date = '".to_db_date()."',action = 'Buy',status = '1',left_amt = '0',left_credit = '0'";
					$query = $this->db->query($insert_buycredits);
					
					$insert_orders = "INSERT INTO orders SET user_id = '".$record['buyer_id']."', payer_id = '',txn = '',amount = '".$price."',payment_type = 'MassPay', payment_status = 'Received',order_status = 'Received',date = '".to_db_date()."'";
					$query = $this->db->query($insert_orders);
					$orders_id = $this->db->insert_id();
					$insert_order_details = "INSERT INTO order_details SET order_id = '".$orders_id."', product_id = '".$record['auction_id']."',product_name = '".$product_details[0]['title']."',qty = '1',price = '".$price."', shipping = '0.00',shipping_or_pickup = '',payment_scheduled_date = '',order_status = 'Received',seller_payment_amount = '".$price."',hmp_fee_amount = '".$hmp_fee_amount."',CorrelationId = '".$PayPalResult['CORRELATIONID']."',seller_payment_time = '".$PayPalResult['TIMESTAMP']."',seller_payment = '1'";
					$query = $this->db->query($insert_order_details);
				}
	}
}