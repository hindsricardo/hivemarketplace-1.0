<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_list_auction extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	public function auction_list_add($auction_record){
		$insert_query = "INSERT INTO auction SET
							user_id = '".$auction_record['user_id']."',
							title = '".$auction_record['title']."',
							tags = '".$auction_record['tags']."',
							item_condition = '".$auction_record['item_condition']."',
							qty = '".$auction_record['qty']."',
							remaining_qty = '".$auction_record['remaining_qty']."',
							description = '".$auction_record['description']."',
							item_value = '".$auction_record['item_value']."',
							auction_endtime = '".$auction_record['auction_endtime']."',
							min_bids = '".$auction_record['min_bids']."',
							min_profit = '".$auction_record['min_profit']."',
							for_bidders = '".$auction_record['for_bidders']."',
							paypal_account = '".$auction_record['paypal_account']."',
							auction_type = '".$auction_record['auction_type']."',
							passcode = '".$auction_record['passcode']."',
							starting_with = '".$auction_record['starting_with']."',
							schedule_time = '".$auction_record['schedule_time']."',
							schedule_endtime = ADDDATE('".$auction_record['schedule_time']."', INTERVAL ".$auction_record['auction_endtime']." HOUR),
							create_date = '".$auction_record['create_date']."',
							modify_date = '".$auction_record['modify_date']."',
							modify_by = '".$auction_record['modify_by']."',
							HMP_fee = '".$auction_record['HMP_fee']."'
							";
		$query = $this->db->query($insert_query);
		return $this->db->insert_id();
	}
	public function auction_list_edit($auction_record){
		$update_query = "UPDATE auction SET
							user_id = '".$auction_record['user_id']."',
							title = '".$auction_record['title']."',
							tags = '".$auction_record['tags']."',
							item_condition = '".$auction_record['item_condition']."',
							qty = '".$auction_record['qty']."',
							remaining_qty = '".$auction_record['remaining_qty']."',
							description = '".$auction_record['description']."',
							item_value = '".$auction_record['item_value']."',
							auction_endtime = '".$auction_record['auction_endtime']."',
							min_bids = '".$auction_record['min_bids']."',
							min_profit = '".$auction_record['min_profit']."',
							for_bidders = '".$auction_record['for_bidders']."',							
							paypal_account = '".$auction_record['paypal_account']."',
							auction_type = '".$auction_record['auction_type']."',
							passcode = '".$auction_record['passcode']."',
							starting_with = '".$auction_record['starting_with']."',
							schedule_time = '".$auction_record['schedule_time']."',
							schedule_endtime = ADDDATE('".$auction_record['schedule_time']."', INTERVAL ".$auction_record['auction_endtime']." HOUR),
							modify_date = '".$auction_record['modify_date']."',
							modify_by = '".$auction_record['modify_by']."',
							HMP_fee = '".$auction_record['HMP_fee']."'
							WHERE ID = '".$auction_record['id']."'
							";
		$query = $this->db->query($update_query);
		return $auction_record['id'];
	}

	public function auction_delivery_list_add($list_aucton_id, $delivery_data){
		$insert_query = "INSERT INTO shipping_method SET 
							auction_id = '".$list_aucton_id."',
							offer_shipping = '".$delivery_data['offer_shipping']."',
							us_company_name = '".$delivery_data['us_company_name']."',
							us_handling_time = '".$delivery_data['us_handling_time']."',
							us_price = '".$delivery_data['us_price']."',
							us_paytype = '".$delivery_data['us_paytype']."',
							ww_company_name = '".$delivery_data['ww_company_name']."',
							ww_handling_time = '".$delivery_data['ww_handling_time']."',
							ww_price = '".$delivery_data['ww_price']."',
							ww_paytype = '".$delivery_data['ww_paytype']."',
							offer_pickup = '".$delivery_data['offer_pickup']."',
							payat_pickup = '".$delivery_data['payat_pickup']."'
							";
		$query = $this->db->query($insert_query);
		return $this->db->insert_id();
	}
	public function auction_delivery_list_edit($list_aucton_id, $delivery_data){
		$update_query = "UPDATE shipping_method SET
							offer_shipping = '".$delivery_data['offer_shipping']."',
							us_company_name = '".$delivery_data['us_company_name']."',
							us_handling_time = '".$delivery_data['us_handling_time']."',
							us_price = '".$delivery_data['us_price']."',
							us_paytype = '".$delivery_data['us_paytype']."',
							ww_company_name = '".$delivery_data['ww_company_name']."',
							ww_handling_time = '".$delivery_data['ww_handling_time']."',
							ww_price = '".$delivery_data['ww_price']."',
							ww_paytype = '".$delivery_data['ww_paytype']."',
							offer_pickup = '".$delivery_data['offer_pickup']."',
							payat_pickup = '".$delivery_data['payat_pickup']."'
							WHERE auction_id = '".$list_aucton_id."'
							";
		$query = $this->db->query($update_query);
		return $this->db->insert_id();
	}

	public function auction_image_add($list_aucton_id, $uploadedFiles){
		foreach($uploadedFiles as $uploadedFiles_val){
			$query = $this->db->query("INSERT INTO auction_image SET auction_id = '".$list_aucton_id."', image_name = '".$uploadedFiles_val['file_name']."' ");
		}
	}
	public function auction_image_edit($list_aucton_id, $uploadedFiles){
		//$delete_sql = $this->db->query("DELETE FROM auction_image WHERE auction_id = '".$list_aucton_id."'");
		foreach($uploadedFiles as $uploadedFiles_val){
			$query = $this->db->query("INSERT INTO auction_image SET auction_id = '".$list_aucton_id."', image_name = '".$uploadedFiles_val['file_name']."' ");
		}
	}

	public function get_auction_details($auction_id){
		$select_sql = "SELECT * FROM auction LEFT JOIN shipping_method
						ON auction.ID = shipping_method.auction_id
						WHERE auction.status = '0' AND
						auction.ID = '".$auction_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['remaining_qty'] = $result_val->remaining_qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['paypal_account'] = $result_val->paypal_account;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['type'] = $result_val->type;
				$data[$i]['HMP_fee'] = $result_val->HMP_fee;
				
				$data[$i]['shipping_id'] = $result_val->shipping_id;
				$data[$i]['offer_shipping'] = $result_val->offer_shipping;
				$data[$i]['us_company_name'] = $result_val->us_company_name;
				$data[$i]['us_handling_time'] = $result_val->us_handling_time;
				$data[$i]['us_price'] = $result_val->us_price;
				$data[$i]['us_paytype'] = $result_val->us_paytype;
				$data[$i]['ww_company_name'] = $result_val->ww_company_name;
				$data[$i]['ww_handling_time'] = $result_val->ww_handling_time;
				$data[$i]['ww_price'] = $result_val->ww_price;
				$data[$i]['ww_paytype'] = $result_val->ww_paytype;
				
				$data[$i]['offer_pickup'] = $result_val->offer_pickup;
				$data[$i]['payat_pickup'] = $result_val->payat_pickup;
				$data[$i]['return_policy'] = $result_val->return_policy;
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	
	public function get_auction_images($auction_id){
		$select_sql = "SELECT *
						FROM `auction_image`
						WHERE `auction_id` ='".$auction_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function user_auth_private_auction(){
		$private_auction_auth = $this->session->userdata('private_auction');
		$private_auction_auth = str_replace(',', "','", $private_auction_auth);
		return $private_auction_auth;
	}
	public function get_auction_list($pageination = array()){
		$private_auctions = $this->user_auth_private_auction();
		$select_sql = "SELECT * FROM auction LEFT JOIN shipping_method
						ON auction.ID = shipping_method.auction_id
						WHERE auction.status = '0'
						AND auction.stage != 'win'
						AND auction.schedule_time < UTC_TIMESTAMP()
						AND auction.schedule_endtime > UTC_TIMESTAMP()";
						
		$select_sql .= " AND (auction.auction_type = 'Public' OR auction.ID IN('".$private_auctions."'))";
						
		$select_sql .= " ORDER BY create_date DESC";
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['remaining_qty'] = $result_val->remaining_qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['paypal_account'] = $result_val->paypal_account;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				
				$data[$i]['shipping_id'] = $result_val->shipping_id;
				$data[$i]['offer_shipping'] = $result_val->offer_shipping;
				$data[$i]['us_company_name'] = $result_val->us_company_name;
				$data[$i]['us_handling_time'] = $result_val->us_handling_time;
				$data[$i]['us_price'] = $result_val->us_price;
				$data[$i]['us_paytype'] = $result_val->us_paytype;
				$data[$i]['ww_company_name'] = $result_val->ww_company_name;
				$data[$i]['ww_handling_time'] = $result_val->ww_handling_time;
				$data[$i]['ww_price'] = $result_val->ww_price;
				$data[$i]['ww_paytype'] = $result_val->ww_paytype;
				$data[$i]['offer_pickup'] = $result_val->offer_pickup;
				$data[$i]['payat_pickup'] = $result_val->payat_pickup;
				$data[$i]['return_policy'] = $result_val->return_policy;
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function get_auction_search($search_value, $pageination = array()){
		$private_auctions = $this->user_auth_private_auction();
		$select_sql = "SELECT * FROM auction as a
						WHERE a.status = '0' AND a.stage != 'win'
						AND
						(title LIKE '%".$search_value."%' OR tags LIKE '%".$search_value."%' OR ID = '".$search_value."' )";
		$select_sql .=" AND a.schedule_time < UTC_TIMESTAMP()";
		$select_sql .=" AND a.schedule_endtime > UTC_TIMESTAMP()";
		$select_sql .= " AND (a.auction_type = 'Public' OR a.ID IN('".$private_auctions."','".$search_value."'))";
		$select_sql .=" ORDER BY create_date DESC";
		//echo $select_sql;//die();
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		//echo $select_sql;die();
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function auction_filter($filter, $pageination = array()){
		$private_auctions = $this->user_auth_private_auction();
		$select_sql ="SELECT * FROM auction as a";
		$select_sql .=" WHERE a.status = '0'
						AND a.stage != 'win'
						AND a.schedule_time < UTC_TIMESTAMP()
						AND a.schedule_endtime > UTC_TIMESTAMP()";
		$select_sql .= " AND (a.auction_type = 'Public' OR a.ID IN('".$private_auctions."'))";
		if($filter['search_str']){
			$select_sql .=" AND (title LIKE '%".$filter['search_str']."%' OR tags LIKE '%".$filter['search_str']."%' OR ID = '".$filter['search_str']."')";
		}
		if($filter['price_0_50'] || $filter['price_50_100'] || $filter['price_100_500'] || $filter['price_500']){
			$select_sql .=" AND (";
		}
		if($filter['price_0_50']){
			$auction_id = $this->get_auctions_price_range('0','50');
			$select_sql .=" a.ID IN('".$auction_id."')";
		}
		if($filter['price_50_100']){
			$auction_id = $this->get_auctions_price_range('50','100');
			if($filter['price_0_50']){
				$select_sql .=" OR";
			}else{
				$select_sql .="";
			}
			$select_sql .=" a.ID IN('".$auction_id."')";
		}
		if($filter['price_100_500']){
			$auction_id = $this->get_auctions_price_range('100','500');
			
			if($filter['price_0_50'] || $filter['price_50_100']){
				$select_sql .=" OR";
			}else{
				$select_sql .="";
			}
			$select_sql .=" a.ID IN('".$auction_id."')";
		}
		if($filter['price_500']){
			$auction_id = $this->get_auctions_price_range('500','');
			if($filter['price_0_50'] || $filter['price_50_100'] || $filter['price_100_500']){
				$select_sql .=" OR";
			}else{
				$select_sql .="";
			}
			if($filter['price_0_50'] || $filter['price_50_100'] || $filter['price_100_500'] || $filter['price_500']){
				$select_sql .=" a.ID IN('".$auction_id."')";
			}
		}
		if($filter['price_0_50'] || $filter['price_50_100'] || $filter['price_100_500'] || $filter['price_500']){
			$select_sql .=" )";
		}
		if($filter['features_no_min_bid']){
			$select_sql .=" AND a.min_bids ='0'";
		}
		if($filter['features_credit_auction']){
			$select_sql .=" AND a.type ='credit_auction'";
		}
		if($filter['display_auction_within'] && $filter['miles_of_zipcode']){
			$zip_user_ids = $this->distance_from_zip($filter['display_auction_within'], $filter['miles_of_zipcode']);
			$zip_user_ids = str_replace(',', "','", $zip_user_ids);
			$select_sql .=" AND a.user_id IN('".$zip_user_ids."')";
		}
		
		if($filter['auction_sort'] == 'ending_soon'){
			$select_sql .=" ORDER BY a.schedule_endtime ASC";
		}elseif($filter['auction_sort'] == 'hightest_value'){
			$select_sql .=" ORDER BY a.item_value DESC ";
		}elseif($filter['auction_sort'] == 'lowest_value'){
			$select_sql .=" ORDER BY a.item_value ASC";
		}else{
			$select_sql .=" ORDER BY create_date DESC";
		}
		
		//echo $select_sql;
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			//echo $select_sql;
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function bid_details($auction_id){
		$select_bid =  "SELECT SUM(t.count_bid) total_bid FROM(SELECT sum(user_bonus_credits.credits) as count_bid FROM `user_bonus_credits`
							WHERE user_bonus_credits.auction_id = '".$auction_id."'
						AND user_bonus_credits.action = 'Bid'
						UNION ALL
						SELECT sum(user_buycredits.credits) as count_bid
							FROM user_buycredits
							WHERE user_buycredits.auction_id = '".$auction_id."'
						AND user_buycredits.action = 'Bid') t";
		$bid_query = $this->db->query($select_bid);
		if($bid_query->num_rows() > 0){
			$bid_result = $bid_query->result();
			$total_bid = $bid_result[0]->total_bid;
		}else{
			$total_bid = 0;
		}
		return $total_bid;
		}
	public function user_bid_details($user_id){
		$bid_bonus_select = "SELECT sum(user_bonus_credits.credits) as count_bid, auction_id
							FROM user_bonus_credits 
							WHERE user_bonus_credits.action = 'Bid'
							AND user_id = '".$user_id."'
							GROUP BY auction_id";
		$bid_bonus_query = $this->db->query($bid_bonus_select);
		//echo $this->db->last_query();
		$bid_hive_select ="SELECT  sum(user_buycredits.credits) as count_bid, auction_id
							FROM user_buycredits
							WHERE user_buycredits.action = 'Bid'
							AND user_id = '".$user_id."'
							GROUP BY auction_id";
		$bid_hive_query = $this->db->query($bid_hive_select);
		$user_bid =array();
		$user_bid['bid_bonus'] = array();
		$user_bid['bid_hive'] = array();
		if($bid_bonus_query->num_rows() > 0){
			$bid_bonus_result = $bid_bonus_query->result('array');
			
			foreach($bid_bonus_result as $bid_bonus_result_key=>$bid_bonus_result_val){
				$user_bid['bid_bonus'][$bid_bonus_result_val['auction_id']] = $bid_bonus_result_val['count_bid'];
			}
		}
		if($bid_hive_query->num_rows() > 0){
			$bid_hive_result = $bid_hive_query->result('array');
			
			foreach($bid_hive_result as $bid_hive_result_key=>$bid_hive_result_val){
				$user_bid['bid_hive'][$bid_hive_result_val['auction_id']] = $bid_hive_result_val['count_bid'];
			}
		}
		return $user_bid;
	}
	
	public function get_auctions_price_range($lowlrange = '', $highrange = ''){
		$select = "select a.`auction_id`, sum(a.PRICE) a_price from (
					SELECT `auction_id`, SUM(`credits_price`) AS PRICE FROM `user_buycredits` WHERE `action` = 'Bid' group by `auction_id`
					union all
					SELECT `auction_id`, SUM(`credits_price`) AS PRICE FROM `user_bonus_credits` WHERE `action` = 'Bid' group by `auction_id`) a group by a.auction_id";
		$query = $this->db->query($select);
		$result = $query->result('array');
		if($query->num_rows() > 0){
		$aid = array();
			foreach($result as $result_key=>$result_val){
				if($highrange == ''){
					if($lowlrange < $result_val['a_price']){
						$aid[] = $result_val['auction_id'];
					}
				}else{
					if($lowlrange < $result_val['a_price'] && $result_val['a_price'] < $highrange){
						$aid[] = $result_val['auction_id'];
					}
				}
			}
			$get_aid = implode("','",$aid);
			return $get_aid;
		}else{
			return '';
		}
	}
	public function bidding_history($auction_id){
		$select_sql = " SELECT user_id, credits, credits_price, create_date
						FROM (
							SELECT user_id, credits, credits_price, create_date
							FROM user_buycredits
							WHERE ACTION = 'Bid'
							AND auction_id = '".$auction_id."'
							UNION ALL
							SELECT user_id, credits, credits_price, create_date
							FROM user_bonus_credits
							WHERE ACTION = 'Bid'
							AND auction_id = '".$auction_id."'
						)a
						ORDER BY create_date DESC LIMIT 0 , 10";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function user_bidding_history($user_id){
		$select_sql = " SELECT auction_id, credits, credits_price, create_date
						FROM (
							SELECT auction_id, credits, credits_price, create_date
							FROM user_buycredits
							WHERE action = 'Bid'
							AND user_id = '".$user_id."'
							UNION ALL
							SELECT auction_id, credits, credits_price, create_date
							FROM user_bonus_credits
							WHERE action = 'Bid'
							AND user_id = '".$user_id."'
						)a
						ORDER BY create_date DESC LIMIT 0 , 10";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}

	public function check_close_auction($auction_id){
		$select_auction = "SELECT ID, type FROM auction WHERE status = '0' AND schedule_endtime < UTC_TIMESTAMP() AND stage != 'win' and ID = '".$auction_id."'";
		$query = $this->db->query($select_auction);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			$auction_winner = $this->auction_winner_update($auction_id, $result[0]['type']);
		}
	}
	public function auction_winner_update($auction_id, $type){
			$select_user_id = "select a.user_id from (SELECT user_id, SUM(`credits`) AS credit FROM `user_buycredits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`
						union all
						SELECT user_id, SUM(`credits`) AS credits FROM `user_bonus_credits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`) a 
						where a.credit = (select max(b.credit) from (SELECT user_id, SUM(`credits`) AS credit FROM `user_buycredits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`
						union all
						SELECT user_id, SUM(`credits`) AS credits FROM `user_bonus_credits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`) b ) group by a.user_id";		
						
			$query = $this->db->query($select_user_id);
			if($query->num_rows() > 0){
				$result = $query->result('array');
				$total_bid = $this->model_manage_credits->auction_total_credits($auction_id);
				if($type == 'credit_auction'){
					$update_sql = "UPDATE auction SET stage ='win', winner_id = '".$result[0]['user_id']."'
								WHERE ID = '".$auction_id."'";
					$query = $this->db->query($update_sql);
					
					$record['total_bid'] = $total_bid;
					$record['auction_id'] = $auction_id;
					$record['buyer_id'] = $result[0]['user_id'];
					
					$this->load->model('model_paypal');
					
					$paypal_payment = $this->model_manage_credits->credit_auction_payment($record);
					return $result[0]['user_id'];
				}else{
					$select_min_bid = "SELECT ID, min_bids FROM auction WHERE ID = '".$auction_id."'";
					$query_min_bid = $this->db->query($select_min_bid);
					$result_min_bid = $query_min_bid->result('array');
					if($result_min_bid[0]['min_bids'] <= $total_bid){
						$update_sql = "UPDATE auction SET stage ='win', winner_id = '".$result[0]['user_id']."'
									WHERE ID = '".$auction_id."'
									AND min_bids <= '".$total_bid."'";
					}else{
						$select_reversed_credits = "SELECT user_id, SUM( credit ) as total_credit, SUM( `credits_price` ) AS total_credits_price
							FROM (
								SELECT user_id, SUM( `credits` ) AS credit, SUM( `credits_price` ) AS credits_price
								FROM `user_buycredits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
								UNION ALL
								SELECT user_id, SUM( `credits` ) AS credits, SUM( `credits_price` ) AS credits_price
								FROM `user_bonus_credits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
							)a
							GROUP BY a.user_id ";
						$reversed_query = $this->db->query($select_reversed_credits);
						$result_reversed = $reversed_query->result('array');
						if(!empty($result_reversed)){
							foreach($result_reversed as $result_reversed_val){
								$insert_reversed_query = "INSERT INTO auction_reversed SET
												user_id = '".$result_reversed_val['user_id']."',
												auction_id = '".$auction_id."',
												credits = '".$result_reversed_val['total_credit']."',
												credits_price = '".$result_reversed_val['total_credits_price']."',
												date_entered = '".to_db_date()."',
												action = 'Reversed',
												status = '0'";
								$this->db->query($insert_reversed_query);
							}
						}
						
						$delete_buycredits_sql = "DELETE FROM user_buycredits WHERE auction_id = '".$auction_id."' AND action = 'Bid'";
						$buycredits_query = $this->db->query($delete_buycredits_sql);
						
						$delete_bonus_credits_sql = "DELETE FROM user_bonus_credits WHERE auction_id = '".$auction_id."' AND action = 'Bid'";
						$bonus_credits_query = $this->db->query($delete_bonus_credits_sql);
						
						$update_sql = "UPDATE auction SET status = '1'
									WHERE ID = '".$auction_id."'";
					}
					
					$query = $this->db->query($update_sql);
					return $result[0]['user_id'];
				}
			}else{
				if($type == 'credit_auction'){
						$select_reversed_credits = "SELECT user_id, SUM( credit ) as total_credit, SUM( `credits_price` ) AS total_credits_price
							FROM (
								SELECT user_id, SUM( `credits` ) AS credit, SUM( `credits_price` ) AS credits_price
								FROM `user_buycredits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
								UNION ALL
								SELECT user_id, SUM( `credits` ) AS credits, SUM( `credits_price` ) AS credits_price
								FROM `user_bonus_credits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
							)a
							GROUP BY a.user_id ";
						$reversed_query = $this->db->query($select_reversed_credits);
						$result_reversed = $reversed_query->result('array');
						if(!empty($result_reversed)){
							foreach($result_reversed as $result_reversed_val){
								$insert_reversed_query = "INSERT INTO auction_reversed SET
												user_id = '".$result_reversed_val['user_id']."',
												auction_id = '".$auction_id."',
												credits = '".$result_reversed_val['total_credit']."',
												credits_price = '".$result_reversed_val['total_credits_price']."',
												date_entered = '".to_db_date()."',
												action = 'Reversed',
												status = '0'";
								$this->db->query($insert_reversed_query);
							}
						}
					$delete_buycredits_sql = "DELETE FROM user_buycredits WHERE auction_id = '".$auction_id."' AND action = 'Auction'";
					$buycredits_query = $this->db->query($delete_buycredits_sql);
					$delete_bonus_credits_sql = "DELETE FROM user_bonus_credits WHERE auction_id = '".$auction_id."' AND action = 'Auction'";
					$bonus_credits_query = $this->db->query($delete_bonus_credits_sql);
				}
				$update_auction_sql = "UPDATE auction SET status = '1'
										WHERE ID = '".$auction_id."'";
				$query = $this->db->query($update_auction_sql);

				return '';
			}
	}
	public function get_auction_direct_purchase_list($pageination = array()){
		$select_sql = "SELECT *, a.user_id as seller FROM auction as a
						LEFT JOIN user_buycredits as ub
						ON a.ID = ub.auction_id
						LEFT JOIN user_bonus_credits as ubc
						ON a.ID = ubc.auction_id
						WHERE a.status = '0'
						AND a.stage = 'win'
						AND a.remaining_qty > 0
						AND (ub.user_id = '".$this->session->userdata('user_id')."' OR ubc.user_id = '".$this->session->userdata('user_id')."')
						GROUP BY a.ID
						ORDER BY a.create_date DESC";
		//$query = $this->db->query($select_sql);
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['remaining_qty'] = $result_val->remaining_qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->seller;
				$data[$i]['winner_id'] = $result_val->winner_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$data[$i]['shipping_details'] = $this->auction_shipping_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function get_user_bid_auction($user_id){
		$select_sql = "SELECT DISTINCT `auction_id` FROM `user_buycredits` WHERE `action` = 'Bid' AND user_id = '".$user_id."'
					union all
					SELECT DISTINCT `auction_id` FROM `user_bonus_credits` WHERE `action` = 'Bid' AND user_id = '".$user_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result('array');
		if($query->num_rows() > 0){
		$aid = array();
			foreach($result as $result_key=>$result_val){
				$aid[] = $result_val['auction_id'];
			}
			$get_aid = implode("','",$aid);
			return $get_aid;
		}else{
			return '';
		}
	}
	public function get_bids_and_offers_list($filter_data =array(), $pageination = array()){
		
		$bid_auction_ids = $this->get_user_bid_auction($this->session->userdata('user_id'));
		$select_sql = "SELECT * FROM auction
						WHERE auction.status = '0'";
		if($filter_data['sort_by'] == 'endig_soon'){
			$select_sql .= " AND stage != 'win'
							 AND ID IN('".$bid_auction_ids."')
							 AND schedule_time < UTC_TIMESTAMP()
							 AND schedule_endtime > UTC_TIMESTAMP()";
							 
			$select_sql .= " ORDER BY schedule_endtime DESC";
		}elseif($filter_data['sort_by'] == 'ended'){
			$select_sql .= " AND ID IN('".$bid_auction_ids."')";
			$select_sql .= " AND schedule_endtime < UTC_TIMESTAMP()";
			$select_sql .= " ORDER BY schedule_endtime DESC";
			$pageination["start"] = 0;
			$pageination["limit"] = 10;
		}
		//echo $select_sql;
		//$query = $this->db->query($select_sql);
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['remaining_qty'] = $result_val->remaining_qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	
	public function get_watchlist_list($pageination = array()){
		$aid = $this->get_user_watchlist();
		$select_sql = "SELECT * FROM auction
						WHERE auction.status = '0'
						AND stage != 'win'
						AND schedule_time < UTC_TIMESTAMP()
						AND ID IN('".$aid."') ORDER BY create_date DESC";
		//$query = $this->db->query($select_sql);
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['remaining_qty'] = $result_val->remaining_qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function get_user_watchlist(){
		$select_sql = "SELECT auction_id FROM watchlist where user_id = '".$this->session->userdata('user_id')."' AND status = '1'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			$aid = array();
			foreach($result as $result_key=>$result_val){
				$aid[] = $result_val['auction_id'];
			}
			$get_aid = implode("','",$aid);
			return $get_aid;
		}else{
			return '';
		}
	}
	public function update_checkout_direct_payment($auction_id, $remaining_qty){
		$update_query = "UPDATE auction SET remaining_qty = (remaining_qty - '".$remaining_qty."') WHERE ID = '".$auction_id."' AND status = 0 AND stage = 'win'";
		$query = $this->db->query($update_query);
		return $auction_id;
	}
	public function get_sold_list($filter_data =array(), $pageination = array()){
		$select_sql = "SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id,a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.qty, od.price, o.payment_status, o.order_status, od.id as order_details_id, od.order_title as order_title, od.buy_type as buy_type, o.date as payment_date, od.seller_payment_time as seller_payment_time, od.seller_payment
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						WHERE a.user_id = '".$this->session->userdata('user_id')."'
						AND a.stage = 'win'
						AND a.type != 'credit_auction'
						AND od.qty IS NOT NULL
						AND a.user_id <> o.user_id
						AND od.product_name != 'buy credit'";
				if(isset($filter_data['sort_by_date'])){
					$select_sql .= " AND DATE_SUB(UTC_TIMESTAMP(),INTERVAL '".$filter_data['sort_by_date']."' DAY) < o.date";
				} 				
				//echo $select_sql;die();
			if(!empty($pageination)){
				$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
				$query = $this->db->query($select_sql);
			}else{
				$query = $this->db->query($select_sql);
				return $query->num_rows();
			}
			if($query->num_rows() > 0){
				$result = $query->result();
				//echo '<pre>';print_r($result);
				$i=0;
				foreach($result as $result_val){
					$data[$i]['id'] = $result_val->ID;
					$data[$i]['title'] = $result_val->title;
					$data[$i]['description'] = $result_val->description;
					$data[$i]['item_value'] = $result_val->item_value;
					$data[$i]['seller_id'] = $result_val->seller;
					$data[$i]['buyer_id'] = $result_val->buyer_id;
					$data[$i]['winner_id'] = $result_val->winner_id;
					$data[$i]['min_bids'] = $result_val->min_bids;
					$data[$i]['min_profit'] = $result_val->min_bids;
					$data[$i]['HMP_fee'] = $result_val->HMP_fee;
					$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
					
					$data[$i]['order_id'] = $result_val->order_id;
					$data[$i]['qty'] = $result_val->qty;
					$data[$i]['price'] = $result_val->price;
					$data[$i]['payment_status'] = $result_val->payment_status;
					$data[$i]['payment_date'] = $result_val->payment_date;
					$data[$i]['seller_payment_time'] = $result_val->seller_payment_time;
					$data[$i]['seller_payment'] = $result_val->seller_payment;
					
					$data[$i]['order_status'] = $result_val->order_status;
					$data[$i]['order_details_id'] = $result_val->order_details_id;
					$data[$i]['order_title'] = $result_val->order_title;
					$data[$i]['buy_type'] = $result_val->buy_type;
					$data[$i]['images'] = $this->get_auction_images($result_val->ID);
					$data[$i]['case_details'] = $this->cases_model->get_auction_case($result_val->ID);
					
					$i++;
				}
				return $data;
			}else{
				return array();
			}
	}
	public function get_paid_list($filter_data =array(), $pageination = array()){
					
		$select_sql = "SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id, a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.qty, od.price, o.payment_status, o.order_status, o.date as payment_date, od.id as order_details_id, od.order_title as order_title, od.buy_type as buy_type, o.date as payment_date, od.seller_payment_time as seller_payment_time, od.seller_payment
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						WHERE o.user_id = '".$this->session->userdata('user_id')."'
						AND a.stage = 'win'
						AND a.type != 'credit_auction'
						AND od.qty IS NOT NULL
						AND od.product_name != 'buy credit'";
			if(isset($filter_data['sort_by_date'])){
				$select_sql .= " AND DATE_SUB(UTC_TIMESTAMP(),INTERVAL '".$filter_data['sort_by_date']."' DAY) < o.date";
			}
			//echo $select_sql;
			if(!empty($pageination)){
				$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
				$query = $this->db->query($select_sql);
			}else{
				$query = $this->db->query($select_sql);
				return $query->num_rows();
			}
			if($query->num_rows() > 0){
				$result = $query->result();
				//echo '<pre>';print_r($result);
				$i=0;
				foreach($result as $result_val){
					$data[$i]['id'] = $result_val->ID;
					$data[$i]['title'] = $result_val->title;
					$data[$i]['description'] = $result_val->description;
					$data[$i]['item_value'] = $result_val->item_value;
					$data[$i]['seller_id'] = $result_val->seller;
					$data[$i]['buyer_id'] = $result_val->buyer_id;
					$data[$i]['winner_id'] = $result_val->winner_id;
					$data[$i]['min_bids'] = $result_val->min_bids;
					$data[$i]['min_profit'] = $result_val->min_bids;
					$data[$i]['HMP_fee'] = $result_val->HMP_fee;
					$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
					
					$data[$i]['order_id'] = $result_val->order_id;
					$data[$i]['qty'] = $result_val->qty;
					$data[$i]['price'] = $result_val->price;
					$data[$i]['payment_status'] = $result_val->payment_status;
					$data[$i]['payment_date'] = $result_val->payment_date;
					
					$data[$i]['seller_payment_time'] = $result_val->seller_payment_time;
					$data[$i]['seller_payment'] = $result_val->seller_payment;
					$data[$i]['order_status'] = $result_val->order_status;
					$data[$i]['order_details_id'] = $result_val->order_details_id;
					$data[$i]['order_title'] = $result_val->order_title;
					$data[$i]['buy_type'] = $result_val->buy_type;
					$data[$i]['images'] = $this->get_auction_images($result_val->ID);
					$data[$i]['case_details'] = $this->cases_model->get_auction_case($result_val->ID);
					
					$i++;
				}
				return $data;
			}else{
				return array();
			}
	}
	public function count_user_live_auction($user_id = ''){
		if($user_id == ''){
			$user_id = $this->session->userdata('user_id');
		}else{
			$user_id = $user_id;
		}
		$select_query = "SELECT id FROM auction WHERE status = '0' AND stage != 'win' AND schedule_time < UTC_TIMESTAMP() AND schedule_endtime > UTC_TIMESTAMP() AND user_id = '".$user_id."'";
		$query = $this->db->query($select_query);
		
		
		return $query->num_rows();
	}
	public function save_search($search){
		$insert_search = "INSERT INTO recent_search SET user_id = '".$search['user_id']."',title = '".$search['title']."', params = '".$search['params']."', created_date = '".$search['created_date']."'";
		$query = $this->db->query($insert_search);
		return $this->db->insert_id();
	}
	public function get_recent_search(){
		$select_query = "SELECT * FROM recent_search WHERE status = '0' AND user_id = '".$this->session->userdata('user_id')."' ORDER BY created_date DESC";
		$query = $this->db->query($select_query);
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	public function get_recent_search_details($id){
		$select_query = "SELECT * FROM recent_search WHERE status = '0' AND user_id = '".$this->session->userdata('user_id')."' AND id = '".$id."'";
		$query = $this->db->query($select_query);
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	
	public function get_cases_list($pageination = array()){
		$select_sql = "SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id,a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.qty, od.price, o.payment_status, o.order_status, ac.id as case_id, ac.purpose as case_purpose, od.id as order_details_id, od.order_title as order_title, od.buy_type as buy_type, o.date as payment_date
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						LEFT JOIN auction_case as ac ON a.ID = ac.auction_id
						WHERE a.user_id = '".$this->session->userdata('user_id')."'
						AND a.stage = 'win'
						AND od.qty IS NOT NULL
						AND ac.id IS NOT NULL
						AND ac.status != 'close'
						AND a.user_id <> o.user_id
						GROUP BY ac.id
						UNION ALL
						SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id, a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.qty, od.price, o.payment_status, o.order_status,ac.id as case_id, ac.purpose as case_purpose, od.id as order_details_id, od.order_title as order_title, od.buy_type as buy_type, o.date as payment_date
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						LEFT JOIN auction_case as ac ON a.ID = ac.auction_id
						WHERE o.user_id = '".$this->session->userdata('user_id')."'
						AND ac.user_id = '".$this->session->userdata('user_id')."'
						AND a.stage = 'win'
						AND od.qty IS NOT NULL
						AND ac.id IS NOT NULL
						AND ac.status != 'close'
						AND a.user_id <> o.user_id
						GROUP BY ac.id";
						//echo $select_sql;
			if(!empty($pageination)){
				$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
				$query = $this->db->query($select_sql);
			}else{
				$query = $this->db->query($select_sql);
				return $query->num_rows();
			}
			if($query->num_rows() > 0){
				$result = $query->result();
				//echo '<pre>';print_r($result);
				$i=0;
				foreach($result as $result_val){
					$data[$i]['id'] = $result_val->ID;
					$data[$i]['title'] = $result_val->title;
					$data[$i]['description'] = $result_val->description;
					$data[$i]['item_value'] = $result_val->item_value;
					$data[$i]['seller_id'] = $result_val->seller;
					$data[$i]['buyer_id'] = $result_val->buyer_id;
					$data[$i]['winner_id'] = $result_val->winner_id;
					$data[$i]['min_bids'] = $result_val->min_bids;
					$data[$i]['min_profit'] = $result_val->min_bids;
					$data[$i]['HMP_fee'] = $result_val->HMP_fee;
					$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
					
					$data[$i]['order_id'] = $result_val->order_id;
					$data[$i]['qty'] = $result_val->qty;
					$data[$i]['price'] = $result_val->price;
					$data[$i]['payment_status'] = $result_val->payment_status;
					$data[$i]['payment_date'] = $result_val->payment_date;
					$data[$i]['order_status'] = $result_val->order_status;
					$data[$i]['order_details_id'] = $result_val->order_details_id;
					$data[$i]['order_title'] = $result_val->order_title;
					$data[$i]['buy_type'] = $result_val->buy_type;
					$data[$i]['images'] = $this->get_auction_images($result_val->ID);
					$data[$i]['case_id'] = $result_val->case_id;
					$data[$i]['case_purpose'] = $result_val->case_purpose;
					
					$i++;
				}
				return $data;
			}else{
				return array();
			}
	}
	###############################################
		function get_admin_auction_list($auction_value, $pageination = array()){
		//echo print_r($auction_value);
		$select_sql = "SELECT * FROM auction WHERE status != 1";
		if(isset($auction_value['type']) &&  $auction_value['type'] == 'active' ){
			$select_sql .= " AND schedule_time < UTC_TIMESTAMP() AND schedule_endtime > UTC_TIMESTAMP() AND stage != 'win'";
		}
		if(isset($auction_value['type']) &&  $auction_value['type'] == 'ended' ){
			$select_sql .= " AND schedule_endtime < UTC_TIMESTAMP() AND stage = 'win'";
		}
		if(isset($auction_value['search_str']) &&  $auction_value['search_str'] != '' ){
			$user_ids = $this->get_user_id_by_name($auction_value['search_str']);
			$select_sql .= " AND (ID = '".$auction_value['search_str']."' OR user_id IN('".$user_ids."'))";
		}
		if(isset($auction_value['seller_id']) &&  $auction_value['seller_id'] != ''){
			$select_sql .= " AND user_id= '".$auction_value['seller_id']."'";
		}
		$select_sql .= " ORDER BY create_date DESC";				
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['stage'] = $result_val->stage;
				$data[$i]['winner_id'] = $result_val->winner_id;
				
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	function get_user_id_by_name($name){
		$select = "SELECT id from users WHERE status = '1' AND (fname = '".$name."' OR lname = '".$name."' OR username = '".$name."')";
		$query = $this->db->query($select);
		
		if($query->num_rows() > 0){
			$result = $query->result('array');
			$uid = array();
			foreach($result as $result_val){
				$uid[] = $result_val['id'];
			}
			$get_aid = implode("','",$uid);
			return $get_aid;
		}else{
			return '';
		}
	}
	function reverse_auction_update($aid){
		$update_sql = "UPDATE auction SET schedule_endtime = ADDDATE(schedule_endtime, INTERVAL 24 HOUR) WHERE ID = '".$aid."'";
		$query = $this->db->query($update_sql);
		return $aid;
	}
public function get_admin_cases_list($auction_value, $pageination = array()){
		//echo '<pre>';print_r($auction_value);die();
		$select_sql = "SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id,a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.qty, od.price, o.payment_status, o.order_status, ac.id as case_id, ac.purpose as case_purpose,ac.status as case_status
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						LEFT JOIN auction_case as ac
						ON a.ID = ac.auction_id
						WHERE a.stage = 'win'
						AND od.qty IS NOT NULL
						AND ac.id IS NOT NULL";
		if($auction_value['type'] != ''){
			$select_sql .= " AND ac.status = '".$auction_value['type']."'";
		}
		if($auction_value['search_str'] != ''){
			$select_sql .= " AND (a.ID = '".$auction_value['search_str']."' OR ac.id = '".$auction_value['search_str']."')";
		}
			$select_sql .= " GROUP BY ac.id";
			//echo $select_sql;die();
			if(!empty($pageination)){
				$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
				$query = $this->db->query($select_sql);
			}else{
				$query = $this->db->query($select_sql);
				return $query->num_rows();
			}
			//echo $select_sql;die();
			if($query->num_rows() > 0){
				$result = $query->result();
				//echo '<pre>';print_r($result);
				$i=0;
				foreach($result as $result_val){
					$data[$i]['id'] = $result_val->ID;
					$data[$i]['title'] = $result_val->title;
					$data[$i]['description'] = $result_val->description;
					$data[$i]['item_value'] = $result_val->item_value;
					$data[$i]['seller_id'] = $result_val->seller;
					$data[$i]['buyer_id'] = $result_val->buyer_id;
					$data[$i]['winner_id'] = $result_val->winner_id;
					$data[$i]['min_bids'] = $result_val->min_bids;
					$data[$i]['min_profit'] = $result_val->min_bids;
					$data[$i]['HMP_fee'] = $result_val->HMP_fee;
					$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
					
					$data[$i]['order_id'] = $result_val->order_id;
					$data[$i]['qty'] = $result_val->qty;
					$data[$i]['price'] = $result_val->price;
					$data[$i]['payment_status'] = $result_val->payment_status;
					$data[$i]['order_status'] = $result_val->order_status;
					$data[$i]['images'] = $this->get_auction_images($result_val->ID);
					$data[$i]['case_id'] = $result_val->case_id;
					$data[$i]['case_purpose'] = $result_val->case_purpose;
					$data[$i]['case_status'] = $result_val->case_status;
					$data[$i]['case_note'] = $this->cases_model->case_note($result_val->case_id);
					$i++;
				}
				return $data;
			}else{
				return array();
			}
	}

	public function get_current_winner($auction_id){
		$select_sql = "SELECT user_id, w.credits FROM (SELECT user_id, SUM( `credits` ) AS credits
						FROM `user_buycredits`
						WHERE `action` = 'Bid'
						AND `auction_id` = '".$auction_id."'
						GROUP BY `user_id`
						UNION ALL
						SELECT user_id, SUM( `credits` ) AS credits
						FROM `user_bonus_credits`
						WHERE `action` = 'Bid'
						AND `auction_id` = '".$auction_id."'
						GROUP BY `user_id`) w ORDER BY credits DESC limit 0,1";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function get_paid_active_auction_list($filter_data =array(), $pageination = array()){
		$select_sql = "SELECT * FROM auction
						WHERE auction.status = '0'
						AND auction.stage != 'win'
						AND auction.schedule_time < UTC_TIMESTAMP() AND auction.schedule_endtime > UTC_TIMESTAMP()
						AND auction.user_id = '".$this->session->userdata('user_id')."'";
		if(isset($filter_data['sort_by_date'])){
			$select_sql .= " AND DATE_SUB(UTC_TIMESTAMP(),INTERVAL '".$filter_data['sort_by_date']."' DAY) < auction.schedule_time";
		} 				
		$select_sql .= " ORDER BY auction.create_date DESC";
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['stage'] = $result_val->stage;
				$data[$i]['winner_id'] = $result_val->winner_id;
				
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$data[$i]['HMP_fee'] = $result_val->HMP_fee;
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function get_paid_schedule_auction_list($filter_data =array(), $pageination = array()){
		$select_sql = "SELECT * FROM auction
						WHERE auction.status = '0'
						AND auction.stage != 'win'
						AND schedule_time > UTC_TIMESTAMP()
						AND user_id = '".$this->session->userdata('user_id')."'";
		if(isset($filter_data['sort_by_date'])){
			$select_sql .= " AND DATE_SUB(UTC_TIMESTAMP(),INTERVAL '".$filter_data['sort_by_date']."' DAY) < schedule_time";
		} 				
			$select_sql .=" ORDER BY create_date DESC";
						//echo $select_sql;
		//$query = $this->db->query($select_sql);
		if(!empty($pageination)){
			$select_sql .=" LIMIT " . $pageination["start"] . ", " . $pageination["limit"];
			$query = $this->db->query($select_sql);
		}else{
			$query = $this->db->query($select_sql);
			return $query->num_rows();
		}
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->ID;
				$data[$i]['title'] = $result_val->title;
				$data[$i]['tags'] = $result_val->tags;
				$data[$i]['item_condition'] = $result_val->item_condition;
				$data[$i]['qty'] = $result_val->qty;
				$data[$i]['description'] = $result_val->description;
				$data[$i]['item_value'] = $result_val->item_value;
				$data[$i]['auction_endtime'] = $result_val->auction_endtime;
				$data[$i]['min_bids'] = $result_val->min_bids;
				$data[$i]['min_profit'] = $result_val->min_profit;
				$data[$i]['for_bidders'] = $result_val->for_bidders;
				$data[$i]['auction_type'] = $result_val->auction_type;
				$data[$i]['passcode'] = $result_val->passcode;
				$data[$i]['starting_with'] = $result_val->starting_with;
				$data[$i]['schedule_time'] = $result_val->schedule_time;
				$data[$i]['schedule_endtime'] = $result_val->schedule_endtime;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['create_date'] = $result_val->create_date;
				$data[$i]['modify_date'] = $result_val->modify_date;
				$data[$i]['modify_by'] = $result_val->modify_by;
				$data[$i]['stage'] = $result_val->stage;
				$data[$i]['winner_id'] = $result_val->winner_id;
				
				$data[$i]['images'] = $this->get_auction_images($result_val->ID);
				$data[$i]['no_of_bids'] = $this->bid_details($result_val->ID);
				$data[$i]['HMP_fee'] = $result_val->HMP_fee;
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function user_win_add_cart_aid(){
		$get_aid = $this->user_buy_win_auction();
		$select_sql = "SELECT ID FROM auction WHERE ID NOT IN('".$get_aid."') AND winner_id = '".$this->session->userdata('user_id')."'";
		//die();
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return '';
		}
	}
	public function user_buy_win_auction(){
		$select_sql = "SELECT product_id
						FROM order_details AS od
						LEFT JOIN orders AS o ON od.order_id = o.order_id
						WHERE o.user_id = '".$this->session->userdata('user_id')."'";
		//die();
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			$aid = array();
			foreach($result as $result_val){
				$aid[] = $result_val['product_id'];
			}
			$get_aid = implode("','",$aid);
			return $get_aid;
		}else{
			return '';
		}
	}
	public function auction_shipping_details($id){
		$select_sql = "SELECT * FROM shipping_method WHERE auction_id = '".$id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result('array');
		return $result;
	}
	public function update_list_image($aid, $image_name){
		$delete = "DELETE FROM `auction_image` WHERE `auction_id` = '".$aid."' and image_name = '".$image_name."'";
		//echo $delete;die();
		$query = $this->db->query($delete);
		return $aid;
	}
	public function is_watchlist($aid, $uid){
		$select = $query = $this->db->query("SELECT id FROM watchlist WHERE user_id= '".$uid."' AND auction_id = '".$aid."' AND status = '1'");
		if($query->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function distance_from_zip($display_auction_within, $miles_of_zipcode){
		$val = $this->registration_model->getLnt($miles_of_zipcode);
		$lat = $val['lat'];
		$lng = $val['lng'];
		$select_sql = "SELECT id, zip, lat, lon FROM users WHERE status = '1'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		$user_ids = '';
		//echo '<pre>';print_r($result);die();
		foreach($result as $result_val){
			$get_distance = $this->distance($lat, $lng, $result_val->lat, $result_val->lon);
			if($get_distance <= $display_auction_within){
				$user_ids .= $result_val->id.",";
}
		}
		return $user_ids;
	}
	public function distance($lat1, $lon1, $lat2, $lon2) { 
		$theta = $lon1 - $lon2; 
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
		$dist = acos($dist); 
		$dist = rad2deg($dist); 
		$miles = $dist * 60 * 1.1515;
		//echo $miles."<br>";
		return $miles;
	}
	public function get_user_bid_on_a_auction($user_id, $auction_id){
		$select_bid =  "SELECT SUM(t.count_bid) total_bid FROM(SELECT sum(user_bonus_credits.credits) as count_bid FROM `user_bonus_credits`
						WHERE user_bonus_credits.auction_id = '".$auction_id."'
						AND user_bonus_credits.user_id = '".$user_id."'
						AND user_bonus_credits.action = 'Bid'
						UNION ALL
						SELECT sum(user_buycredits.credits) as count_bid
						FROM user_buycredits
						WHERE user_buycredits.auction_id = '".$auction_id."'
						AND user_buycredits.user_id = '".$user_id."'
						AND user_buycredits.action = 'Bid') t";
		$bid_query = $this->db->query($select_bid);
		if($bid_query->num_rows() > 0){
			$bid_result = $bid_query->result();
			$total_bid = $bid_result[0]->total_bid;
		}else{
			$total_bid = 0;
		}
		return $total_bid;
	}
	public function get_user_direct_buy_item_count($auction_id, $user_id){
		$sql_select = "SELECT od.id FROM orders as o LEFT JOIN order_details as od
						ON o.order_id = od.order_id
						WHERE o.user_id = '".$user_id."'
						AND od.product_id = '".$auction_id."'
						AND od.buy_type = 'direct_buy'";
		$query = $this->db->query($sql_select);
		return $query->num_rows();
	}
}