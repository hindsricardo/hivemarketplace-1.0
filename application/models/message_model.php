<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class message_model extends CI_Model {

function __construct()
	{
		parent::__construct();
	}	
	public function insert_mail($mail_val){	
		 $this->db->insert('auction_mail',$mail_val);
		 return $this->db->insert_id();
	}
			
	public function message_all(){	  
	  $query=$this->db->query('select * from auction_mail where recever_id='.$this->session->userdata('user_id').' AND status!=2 ORDER BY id DESC LIMIT 0, 100');
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	
	public function message_unread(){	  
	  $query =$this->db->query('select * from auction_mail where recever_id='.$this->session->userdata('user_id').' AND status=0 ORDER BY id DESC LIMIT 0, 100');
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	
	public function message_userstatus($ids,$status){	  
	  $query =$this->db->query('UPDATE `auction_mail` SET `status` ='.$status.' WHERE `id` IN ('.$ids.')');
	    
	}
	public function message_senderstatus($ids,$status){	  
	  $query =$this->db->query('UPDATE `auction_mail` SET `sender_status` ='.$status.' WHERE `id` IN ('.$ids.')');
	    
	}
	
	public function message_send(){	  
	  $query = $this->db->query('select * from auction_mail where sender_id='.$this->session->userdata('user_id').' AND sender_status!=2 AND recever_id!=0 ORDER BY id DESC LIMIT 0, 100');
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	public function message_trash(){
	  
	  $query = $this->db->query('select * from auction_mail where (sender_id='.$this->session->userdata('user_id').' AND sender_status=2) OR (recever_id='.$this->session->userdata('user_id').' AND status=2) ORDER BY id DESC LIMIT 0, 100');
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}	
	public function trash_restore($ids){	  
	  $query =$this->db->query('UPDATE `auction_mail` SET `sender_status`= CASE WHEN `sender_id`='.$this->session->userdata('user_id').' AND `sender_status`=2 THEN 0 ELSE `sender_status` END WHERE `id` IN ('.$ids.')');
	  $query =$this->db->query('UPDATE `auction_mail` SET `status`= CASE WHEN `recever_id`='.$this->session->userdata('user_id').' AND `status`=2 THEN 0 ELSE `status` END WHERE `id` IN ('.$ids.')');
	    
	}	
	
	
	public function forword_msg($id){	  
	  $query = $this->db->query('select * from auction_mail where id='.$id);
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}	    
	}
	public function message_reply($msg_details){
		$insert_query = "INSERT INTO auction_mail SET sender_id= '".$msg_details['sender_id']."',recever_id= '".$msg_details['recever_id']."',recever_admin= '".$msg_details['recever_admin']."',sender_admin= '".$msg_details['sender_admin']."',parent_id= '".$msg_details['parent_id']."',msg_to= '".$msg_details['msg_to']."',msg_from= '".$msg_details['msg_from']."',subject= '".$msg_details['subject']."',msg= '".$msg_details['msg']."',date= '".$msg_details['date']."',type= '".$msg_details['type']."',status= '".$msg_details['status']."',sender_status= '".$msg_details['sender_status']."',auction_mail_id= '".$msg_details['auction_mail_id']."'";
	$query = $this->db->query($insert_query);
	}
	public function is_reply($auction_mail_id){
		$query = $this->db->query("SELECT date FROM auction_mail WHERE auction_mail_id = '".$auction_mail_id."' and sender_id = '".$this->session->userdata('user_id')."' order by date DESC");
		
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result[0]->date;
		}else{
			return array();
		}
	}
}