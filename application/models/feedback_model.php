<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends CI_Model {

function __construct()
	{
		parent::__construct();
	}	
	
	public function insert_feedback($feedback_val){	
		 $this->db->insert('auction_feedback',$feedback_val);
	}
	
	public function chk_feedback($auction_id){
	  $sql="select * from auction_feedback where auction_id=".$auction_id." AND user_id=".$this->session->userdata('user_id');
	  $res=mysql_query($sql);
	  return mysql_num_rows($res);
	}
			
	public function feedback_details(){	  
	  $query = $this->db->query('select * from auction_feedback where seller_id='.$this->session->userdata('user_id'));
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}	
	
	public function avg_rating($user_id){
	
	$query = $this->db->query('select (AVG(desc_rate)+AVG(speed_rate)+AVG(comm_rate)) as avg_rate from auction_feedback where seller_id='.$user_id);
	  if($query->num_rows() > 0){
			$result = $query->row()->avg_rate; 
			return $result;
		}else{
			return 0;
		}
	}	
	
	public function total_rating($user_id){
	$query = $this->db->query('select id as total from auction_feedback where seller_id='.$user_id);
	  if($query->num_rows() > 0){
			//$result = $query->row()->total; 
			return $query->num_rows();
		}else{
			return 0;
		}
	}		
	
	public function user_notes($user_id){
	
	$query = $this->db->query('select notes from users where id='.$user_id);
	  if($query->num_rows() > 0){
			$result = $query->row()->notes; 
			return $result;
		}else{
			return 0;
		}
	}	
	public function is_feedback($aid, $uid){
	$query = $this->db->query('SELECT id FROM auction_feedback WHERE auction_id="'.$aid.'" AND user_id = "'.$uid.'"');
	  if($query->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
}