<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_users extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_user_list(){
		$select_sql = "SELECT * FROM users WHERE status = '1'";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function get_user_details($id){
		$select_sql = "SELECT * FROM users WHERE status = '1' AND id = '".$id."'";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function user_search_list($user_search_list){
		if($user_search_list){
			$select_sql = "SELECT * FROM users WHERE status = '1' AND username LIKE '%".$user_search_list."%'";
		}else{
			$select_sql = "SELECT * FROM users WHERE status = '1'";
		}
		
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function passwordreset($password_reset){
		$query = $this->db->query("UPDATE users SET password  = '".$password_reset['password']."' WHERE id = '".$password_reset['id']."'");
		return $password_reset['id'];
	}
	public function check_valid_user($user_value){
		$select_sql = "SELECT * FROM users WHERE status = '1' AND username = '".$user_value['username']."' AND password = '".$user_value['password']."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		return $result;
	}
	public function update($user_val){
		$query = $this->db->query("UPDATE `users` SET 
									`username` = '".$user_val['username']."',
									`password` = '".$user_val['password']."',
									`fname` = '".$user_val['fname']."',
									`lname` = '".$user_val['lname']."',
									`email` = '".$user_val['email']."',
									`street_address` = '".$user_val['street_address']."',
									`city_province` = '".$user_val['city_province']."',
									`country` = '".$user_val['country']."',
									`zip` = '".$user_val['zip']."',
									`lat` = '".$user_val['lat']."',
									`lon` = '".$user_val['lon']."',
									`timezone` = '".$user_val['timezone']."'
									WHERE id = '".$user_val['id']."'
									");
		if ($this->db->affected_rows() > 0){
			  return TRUE;
		}else{
			  return FALSE;
		}
	}
	public function get_user_notifications($user_id){
		$select_sql = "SELECT * FROM user_notifications WHERE user_id  = '".$user_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		return $result;
	}
	public function notification_update($notification){
		$query = $this->db->query("UPDATE user_notifications SET ".$notification['field_name']."  = '".$notification['field_value']."' WHERE user_id  = '".$notification['user_id']."'");
	}
	public function avater_upload($data){
		$query = $this->db->query("UPDATE users SET photo = '".$data['photo']."' WHERE id = '".$data['id']."'");
	}
	public function updatecode($code, $user_id){
		$update_query= $this->db->query("UPDATE users SET update_userinfo_code = '".$code."' WHERE id = '".$user_id."'");
		return $user_id;
	}
	public function get_user_username($user_id){
		$select_sql = "SELECT username FROM users WHERE id ='".$user_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		if(isset($result[0])){
			return $result[0]->username;
		}else{
			return '';
		}
		
	}
	public function get_user_email($user_id){
		$select_sql = "SELECT email FROM users WHERE id ='".$user_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		return $result[0]->email;
	}
	public function get_user_paypalemail($user_id){
		$select_sql = "SELECT paypalemail FROM users WHERE id ='".$user_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		return $result[0]->paypalemail;
	}
	public function update_watchlist($auction_id){
		$select_sql = "SELECT * FROM watchlist WHERE user_id= '".$this->session->userdata('user_id')."' AND auction_id= '".$auction_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
		$result = $query->result('array');
			if($result[0]['status'] == 1){
				$update_sql = "UPDATE watchlist SET status = '0' WHERE user_id= '".$this->session->userdata('user_id')."' AND auction_id= '".$auction_id."'";
				$query = $this->db->query($update_sql);
				$status = 0;
			}else{
				$update_sql = "UPDATE watchlist SET status = '1' WHERE user_id= '".$this->session->userdata('user_id')."' AND auction_id= '".$auction_id."'";
				$query = $this->db->query($update_sql);
				$status = 1;
			}
			
		}else{
			$insert_sql = "INSERT INTO watchlist SET user_id= '".$this->session->userdata('user_id')."', auction_id= '".$auction_id."', create_date = '".to_db_date()."',status = '1'";
			$query = $this->db->query($insert_sql);
			$status = 1;
		}
		//return $this->db->last_query();
		return $status;
	}
	public function get_admin_username($admin_id){
		$select_sql = "SELECT username FROM admin WHERE id ='".$admin_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		if(isset($result[0])){
			return $result[0]->username;
		}else{
			return '';
		}
	}
	public function send_forgot_password($email){
		$select_sql = "SELECT fname, lname, email, password FROM users WHERE email = '".$email."' AND status = '1'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function private_auction_pass($pass){
		$select_sql = "SELECT ID FROM auction WHERE ID = '".$pass['auction_id']."' AND passcode = '".$pass['password']."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return '';
		}
	}
	public function private_auction_update($pass){
		$select_sql = "SELECT private_auction FROM users WHERE id = '".$pass['user_id']."'";
		$query = $this->db->query($select_sql);
		$result = $query->result('array');
		if($result[0]['private_auction'] != ''){
			$update_pass = $result[0]['private_auction'].','.$pass['auction_id'];
		}else{
			$update_pass = $pass['auction_id'];
		}
		$update_sql = "UPDATE users SET private_auction = '".$update_pass."' WHERE id = '".$pass['user_id']."'";
		$query = $this->db->query($update_sql);
		return $update_pass;
	}
	public function updatepaypalemailcode($code, $user_id){
		$update_query= $this->db->query("UPDATE users SET update_paypalemail_code = '".$code."' WHERE id = '".$user_id."'");
		return $user_id;
	}
	public function update_paypalemail($value){
		$update_query= $this->db->query("UPDATE users SET update_paypalemail_code = '', paypalemail = '".$value['paypalemail']."' WHERE id = '".$value['user_id']."' AND update_paypalemail_code = '".$value['paypal_request_code']."'");
		
		if ($this->db->affected_rows() > 0) {
			return "Successfully Updated";
		} else {
			return "Please enter correct code";
		}
	}
	public function get_user_timezone($user_id){
		$select_sql = "SELECT timezone FROM users WHERE id = '".$user_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return unserialize($result[0]['timezone']);
		}else{
			return '';
		}
	}
	public function check_subscribe($user_id){
		$select_sql = "SELECT hivemp_subscribe FROM users WHERE id = '".$user_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result[0]['hivemp_subscribe'];
		}else{
			return 0;
		}
	}
	public function unsubscribe($user_id){
		$update_user = "UPDATE users SET hivemp_subscribe = '0' WHERE id = '".$user_id."'";
		$query = $this->db->query($update_user);
	}
	public function subscribe($user_id){
		$update_user = "UPDATE users SET hivemp_subscribe = '1' WHERE id = '".$user_id."'";
		$query = $this->db->query($update_user);
	}
	
}