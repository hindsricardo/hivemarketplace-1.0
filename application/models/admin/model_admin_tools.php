<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_admin_tools extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_admin_user(){
		$select_sql = "SELECT * FROM admin WHERE status = 'Active'";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function insert($admin_data){
		$this->db->insert('admin', $admin_data);
	}
	public function delete($id){
		//$query = $this->db->query("DELETE FROM admin WHERE id = '".$id."'");
		$query = $this->db->query("UPDATE admin SET status = 'Inactive' WHERE id = '".$id."'");
		return $query;
	}
	public function get_edit_details($id){
		$select_sql = "SELECT * FROM admin WHERE id = '".$id."'";
		$query = $this->db->query($select_sql);
		return $query->result('array');

	}
	function edit($data)
	{
		$query = $this->db->query("UPDATE admin SET 
												`username` = '".$data['username']."',
												`password` = '".$data['password']."',
												`name` = '".$data['name']."',
												`email` = '".$data['email']."',
												`status` = '".$data['status']."',
												`level` = '".$data['level']."'
												WHERE id = '".$data['id']."'
								");
		return $data['id'];
		}
}
