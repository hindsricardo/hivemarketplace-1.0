<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_about extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_about(){
		$select_sql = "SELECT * FROM about";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	/*function update_about($data)
	{
									
		$query = $this->db->query("UPDATE `about` SET `title` = '".$data['title']."',
									`heading` ='".$data['heading']."',
									`meta_tag` = '".$data['meta_tag']."',
									`meta_description` = '".$data['meta_description']."',
									`description` = '".$data['description']."' WHERE `id` ='".$data['id']."'");
		return $query;
	}*/
	
	function update_about()
	{
		$data['title'] = $this->input->post('title');
		$data['heading'] = $this->input->post('heading');
		$data['meta_tag'] = $this->input->post('meta_tag');
		$data['meta_description'] = $this->input->post('meta_description');
		$data['description'] = $this->input->post('description');
		
		if($data['title']!='' && $data['heading']!='' && $data['description']!=''){
									
		$query = $this->db->query("UPDATE `about` SET `title` = '".$data['title']."',
									`heading` ='".$data['heading']."',
									`meta_tag` = '".$data['meta_tag']."',
									`meta_description` = '".$data['meta_description']."',
									`description` = '".$data['description']."'");
		}
		//return $query;
	}
}
