<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_report extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function report_data($type){
		$current_date = to_db_date();
		if($type == 'year'){
			$year = "SELECT YEAR('".$current_date."') as year";
			$query = $this->db->query($year);
			$result = $query->result('array');
			$select_sql = "SELECT sum(hmp_fee_amount) as total_hmp_fee FROM `order_details` WHERE  YEAR(seller_payment_time) = '".$result[0]['year']."'";
		}elseif($type == 'month'){
		
			$month = "SELECT MONTH('".$current_date."') as month";
			$query = $this->db->query($month);
			$result = $query->result('array');

		
			$select_sql = "SELECT sum( hmp_fee_amount ) as total_hmp_fee FROM `order_details` WHERE MONTH( seller_payment_time ) = '".$result[0]['month']."'";
		}elseif($type == 'week'){
		
			$week = "SELECT WEEK('".$current_date."') as week";
			$query = $this->db->query($week);
			$result = $query->result('array');
		
			$select_sql ="SELECT sum( hmp_fee_amount ) as total_hmp_fee FROM `order_details` WHERE WEEK( seller_payment_time ) = '".$result[0]['week']."' ";
		}elseif($type == 'today'){
		
			$today = "SELECT TO_DAYS('".$current_date."') as today";
			$query = $this->db->query($today);
			$result = $query->result('array');
		
			$select_sql = " SELECT sum( hmp_fee_amount ) as total_hmp_fee FROM `order_details` WHERE TO_DAYS( seller_payment_time ) = '".$result[0]['today']."' ";
		}
		
		$query = $this->db->query($select_sql);
		return $query->result('array');
	}	
}