<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cases_model extends CI_Model {

function __construct()
	{
		parent::__construct();
	}	
	
	public function insert_case($case_val){	
		 $this->db->insert('auction_case',$case_val);
		 return $this->db->insert_id();
	}
	public function chk_case($auction_id){
		$sql="select * from auction_case where auction_id=".$auction_id." AND user_id = '".$this->session->userdata('user_id')."' AND status!='close'";
		$res=mysql_query($sql);
		return mysql_num_rows($res);
	}
	public function case_details($case_id){
	$select_sql = "SELECT a.ID, a.title,a.item_value,a.description, a.user_id as seller, a.winner_id,a.min_bids, a.min_profit, a.HMP_fee, o.user_id AS buyer_id, o.order_id, od.id as order_details_id, od.qty, od.price, od.order_title as order_title, od.buy_type as buy_type, o.payment_status, o.order_status, o.date as payment_date, ac.id as case_id, ac.purpose, ac.desc, ac.status
						FROM auction AS a
						LEFT JOIN order_details AS od ON a.ID = od.product_id
						LEFT JOIN orders AS o ON o.order_id = od.order_id
						LEFT JOIN auction_case as ac
						ON a.ID = ac.auction_id
						WHERE a.stage = 'win'
						AND ac.id = '".$case_id."'
						AND od.product_name != 'buy credit'";
	
	//echo $select_sql;
	  //$query = $this->db->query('select * from auction_case where id='.$case_id);
			$query = $this->db->query($select_sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				$i=0;
				foreach($result as $result_val){
					$data[$i]['id'] = $result_val->ID;
					$data[$i]['title'] = $result_val->title;
					$data[$i]['description'] = $result_val->description;
					$data[$i]['item_value'] = $result_val->item_value;
					$data[$i]['seller_id'] = $result_val->seller;
					$data[$i]['buyer_id'] = $result_val->buyer_id;
					$data[$i]['winner_id'] = $result_val->winner_id;
					$data[$i]['min_bids'] = $result_val->min_bids;
					$data[$i]['min_profit'] = $result_val->min_bids;
					$data[$i]['HMP_fee'] = $result_val->HMP_fee;
					$data[$i]['no_of_bids'] = $this->model_list_auction->bid_details($result_val->ID);
					
					$data[$i]['order_id'] = $result_val->order_id;
					$data[$i]['qty'] = $result_val->qty;
					$data[$i]['price'] = $result_val->price;
					$data[$i]['payment_status'] = $result_val->payment_status;
					$data[$i]['payment_date'] = $result_val->payment_date;
					$data[$i]['order_status'] = $result_val->order_status;
					$data[$i]['images'] = $this->model_list_auction->get_auction_images($result_val->ID);
					$data[$i]['case_id'] = $result_val->case_id;
					$data[$i]['order_details_id'] = $result_val->order_details_id;
					$data[$i]['order_title'] = $result_val->order_title;
					$data[$i]['buy_type'] = $result_val->buy_type;
					$data[$i]['purpose'] = $result_val->purpose;
					$data[$i]['status'] = $result_val->status;
					
					$i++;
				}
				return $data;
			}else{
				return array();
			}
	}	
	public function case_msg($case_id){	  
	  $query = $this->db->query('select * from auction_mail where parent_id='.$case_id);
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	public function case_note($case_id){	  
	  $query = $this->db->query('select * from case_note where case_id='.$case_id);
	  if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}	    
	}		
	public function add_case_note($note_value){
		$query ="INSERT INTO case_note SET admin_id = '".$note_value['admin_id']."', case_id = '".$note_value['case_id']."', description = '".$note_value['description']."', date = '".$note_value['date']."'";
		$this->db->query($query);
		return $this->db->insert_id();
	}
	public function case_UandB($case_id){	  
	  $query = $this->db->query('select * from auction_case where id='.$case_id);
	  if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return array();
		}
	}
	public function get_auction_case($auction_id){
		$sql = "select * from auction_case where auction_id = ".$auction_id." AND status != 'close'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
}