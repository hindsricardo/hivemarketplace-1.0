<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Registration_model extends CI_Model {
 public function __construct()
 {
  parent::__construct();
 }
 function login($email,$password)
 {
  $this->db->where("email",$email);
  $this->db->where("password",$password);

  $query=$this->db->get("user");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      'user_id'  => $rows->id,
      'user_name'  => $rows->username,
      'user_email'    => $rows->email,
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   return true;
  }
  return false;
 }
	public function add_user(){
		if($this->input->post('zipcode')){
			$val = $this->getLnt($this->input->post('zipcode'));
			$lat = $val['lat'];
			$lng = $val['lng'];
		}else{
			$lat = '';
			$lng = '';
		}
		if($this->upload->file_name){
			$photo = $this->upload->file_name;
		}else{
			$photo = '';
		}
  $data=array(
    'username'=>fSecureInput($this->input->post('username')),
    'password'=>fSecureInput($this->input->post('confirmpass')),
    'fname'=>fSecureInput($this->input->post('firstname')),
	'lname'=>fSecureInput($this->input->post('lastname')),
	'email'=>$this->input->post('email'),
	'paypalemail'=>$this->input->post('paypal'),
	'zip'=>fSecureInput($this->input->post('zipcode')),
			'lat'=>$lat,
			'lon'=>$lng,
			'photo'=>$photo,
	'registeron'=>gmdate('Y-m-d'),
			'timezone'=>serialize($this->input->post('timezone')),
	'status'=>'0'
  );
  //echo '<pre>';print_r($data);die();
  $this->db->insert('users',$data);
  return $this->db->insert_id();
 }
	public function add_user_notification($user_id){
	$data =array(
				'user_id' =>$user_id,
				'date_modified' =>gmdate('Y-m-d')
	);
		$this->db->insert('user_notifications',$data);
	}
	public function activate_account($id){
		$update = "UPDATE users SET status = '1' WHERE id = '".$id."'";
		$query = $this->db->query($update);
		return $id;
	}
	public function add_rewards($id){
		$insert_sql = "INSERT INTO rewards SET user_id = '".$id."'";
		$query = $this->db->query($insert_sql);
		return $id;
	}
	public function getLnt($zip){
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&sensor=false";
		$result_string = file_get_contents($url);
		$result = json_decode($result_string, true);
		$result1[]=$result['results'][0];
		$result2[]=$result1[0]['geometry'];
		$result3[]=$result2[0]['location'];
		return $result3[0];
}
}
?>