<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_paypal extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function insert_buycredits($price){		
		    $auction_val = $this->config->item("credit_val");
			$num_credit=$price/$auction_val;
			
		    $tx = trim($_POST['txn_id']);
			$data=array(
			'user_id'=>$this->session->userdata('user_id'),
			'credits'=>$num_credit,
			'credits_price'=>$price,
			'txn'=>$tx,	
			'create_date'=>to_db_date(),
			'action' => 'Buy'
			 );
		 $this->db->insert('user_buycredits',$data);
	}
	
function insert_orders(){
	$creprice = trim($_POST['payment_gross']);
	$tx = trim($_POST['txn_id']);
	$data=array(
		'user_id'=>$this->session->userdata('user_id'),
		'payer_id' => trim($_POST['payer_id']),
		'txn' => $tx,
		'amount' => $creprice,
		'payment_type' => $_POST['payment_type'],
		'payment_status' => $_POST['payment_status'],
		'order_status' => 'Pending',
		'date' => to_db_date()
	    );
      $this->db->insert('orders',$data);
	  return $this->db->insert_id();
	}
	 
 function order_details($order_id,$product_id,$product_name,$qty,$price,$shipping, $shipping_or_pickup = '', $buy_type = '', $payment_scheduled_date = ''){
	if($buy_type == 'direct_buy'){
		$direct_buy_count = $this->check_direct_buy($product_id);
		$order_title = $product_id."-".($direct_buy_count+1);
	}else{
		$order_title = $product_id;
	}
	if($payment_scheduled_date != ''){
		$payment_scheduled_date = $payment_scheduled_date;
	}else{
		$payment_scheduled_date = '0000-00-00 00:00:00';
	}
	$data=array(
		'order_id'=> $order_id,
		'product_id' => $product_id,
		'order_title' => $order_title,
		'buy_type' => $buy_type,
		'product_name' => $product_name,
		'qty' => $qty,
		'price' => $price,
		'shipping' => $shipping,
		'shipping_or_pickup' => $shipping_or_pickup,
		'payment_scheduled_date' => $payment_scheduled_date
	    );
		//echo '<pre>';print_r($data);die();
      $this->db->insert('order_details',$data);
	  return $this->db->insert_id();
	}
	public function insert_orders_pickup($order_details){
		$this->db->insert('orders',$order_details);
		return $this->db->insert_id();
	}
	public function check_direct_buy($product_id){
		$select_sql = "SELECT id FROM order_details WHERE product_id = '".$product_id."' AND buy_type ='direct_buy'";
		$query = $this->db->query($select_sql);
		return $query->num_rows();
	}
	public function update_payment_received($order_id){
		$update_sql = "UPDATE orders SET payment_status = 'Received' WHERE order_id = '".$order_id."'";
		$query = $this->db->query($update_sql);
		return $order_id;
	}
	public function update_order_received($order_details_id, $order_details){
		$update_sql = "UPDATE order_details SET order_status = '".$order_details['order_status']."', seller_payment_amount = '".$order_details['seller_payment_amount']."', hmp_fee_amount = '".$order_details['hmp_fee_amount']."', CorrelationId = '".$order_details['CorrelationId']."', seller_payment_time = '".$order_details['seller_payment_time']."', seller_payment = '".$order_details['seller_payment']."' WHERE id = '".$order_details_id."' AND product_name !='buy credit'";
		$query = $this->db->query($update_sql);
		return $order_details_id;
	}
	function get_seller_item_details($aid){
		$select_sql = "SELECT ID, user_id, title,item_value, paypal_account, HMP_fee FROM auction WHERE ID = '".$aid."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			return $result;
		}else{
			return array();
		}
	}
	public function get_order_received($order_details_id){
		$select_sql =  "SELECT *
						FROM `order_details`
						WHERE `order_status` = 'Pending'
						AND seller_payment = '0'
						AND id = '".$order_details_id."'";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			//echo '<pre>';print_r($result);die();
			return $result;
		}else{
			$result = array();
		}
	}

}

