<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_faqs extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_faq_list(){
		$select_sql = "SELECT * FROM faq_questions WHERE deleted = '0'";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function question_submit($faq_val){
		$sql_insert = "INSERT INTO faq_questions SET
						date_entered = '".$faq_val['date_entered']."',
						questions = '".$faq_val['questions']."',
						user_id = '".$faq_val['user_id']."',
						deleted = '".$faq_val['deleted']."'";
		$query = $this->db->query($sql_insert);
		return $this->db->insert_id();
	}
	public function search($faq_val){
		$select_sql = "SELECT * FROM faq_questions 
						WHERE deleted = '0' AND
						questions like '%".$faq_val['questions']."%'
						ORDER BY date_entered DESC";
		$query = $this->db->query($select_sql);
		return $query->result();
	}
	public function details($id){
		$select_sql = "SELECT * FROM faq_questions
						WHERE deleted = '0' AND
						id = '".$id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		foreach($result as $result_val){
			$data['id'] = $result_val->id;
			$data['date_entered'] = $result_val->date_entered;
			$data['questions'] = $result_val->questions;
			$data['user_id'] = $result_val->user_id;
			$data['deleted'] = $result_val->deleted;
			$data['answer_details'] = $this->get_answers($result_val->id);
		}
		return $data;
		//echo '<pre>';print_r($data);
	}
	public function get_answers($questions_id){
		$select_sql = "SELECT * FROM faq_answers
						WHERE deleted = '0' AND
						questions_id = '".$questions_id."' ORDER BY date_entered DESC";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->id;
				$data[$i]['questions_id'] = $result_val->questions_id;
				$data[$i]['date_entered'] = $result_val->date_entered;
				$data[$i]['answers'] = $result_val->answers;
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['deleted'] = $result_val->deleted;
				$data[$i]['comment_details'] = $this->get_comments($result_val->id);
				$data[$i]['rate_details'] = $this->get_rates($result_val->id);
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function get_comments($answers_id){
				$select_sql = "SELECT * FROM faq_comments
						WHERE deleted = '0' AND
						answers_id = '".$answers_id."' ORDER BY date_entered DESC";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			$i=0;
			foreach($result as $result_val){
				$data[$i]['id'] = $result_val->id;
				$data[$i]['answers_id'] = $result_val->answers_id;
				$data[$i]['date_entered'] = $result_val->date_entered;
				$data[$i]['comments'] = ($result_val->comments);
				$data[$i]['user_id'] = $result_val->user_id;
				$data[$i]['deleted'] = $result_val->deleted;
				$i++;
			}
			return $data;
		}else{
			return array();
		}
	}
	public function submit_comment($comment_val){
		$insert_sql = "INSERT INTO faq_comments SET
						answers_id = '".$comment_val['answers_id']."',
						comments = '".$comment_val['comments']."',
						date_entered = '".$comment_val['date_entered']."',
						user_id = '".$comment_val['user_id']."',
						deleted = '".$comment_val['deleted']."'";
		$query = $this->db->query($insert_sql);
		return $this->db->insert_id();
	}
	public function answer_submit($comment_val){
		$insert_sql = "INSERT INTO faq_answers SET
						questions_id = '".$comment_val['questions_id']."',
						answers = '".$comment_val['answers']."',
						date_entered = '".$comment_val['date_entered']."',
						user_id = '".$comment_val['user_id']."',
						deleted = '".$comment_val['deleted']."'";
		$query = $this->db->query($insert_sql);
		return $this->db->insert_id();
	}
	public function get_questions_userid($questions_id){
		$select_sql = "SELECT user_id FROM faq_questions WHERE id ='".$questions_id."'";
		$query = $this->db->query($select_sql);
		$result = $query->result();
		return $result[0]->user_id;
	}
	public function get_unanswered_questions($params){
		$select_sql = "SELECT distinct(questions_id) FROM faq_answers WHERE deleted = '0' ";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
		$result = $query->result('array');
		foreach($result as $result_val){
			$question_ids[] = $result_val['questions_id'];
		}
		$question_id_str = implode("','",$question_ids);
		$select_questions = "SELECT * FROM faq_questions
							 WHERE deleted = 0 AND
							 id NOT IN('".$question_id_str."')
							 ORDER BY date_entered DESC
							 LIMIT ".$params['start']." , ".$params['limit']."";
		$questions_query = $this->db->query($select_questions);
		$questions_result = $questions_query->result();
		//die();
		return $questions_result;
		}else{
			return array();
	}
		
	}
	public function count_unanswered_questions(){
		$select_sql = "SELECT distinct(questions_id) FROM faq_answers WHERE deleted = '0' ";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
		$result = $query->result('array');
		foreach($result as $result_val){
			$question_ids[] = $result_val['questions_id'];
		}
		$question_id_str = implode("','",$question_ids);
		$select_questions = "SELECT * FROM faq_questions
							 WHERE deleted = 0 AND
							 id NOT IN('".$question_id_str."')
							 ORDER BY date_entered DESC";
		$questions_query = $this->db->query($select_questions);
		//$questions_result = $questions_query->result();
		return $questions_query->num_rows();
		}else{
			return 0;	
	}
	}
	public function get_rates($answers_id){
		$select_query = "SELECT * FROM answers_rating WHERE answers_id = '".$answers_id."'";
		$query = $this->db->query($select_query);
		$result = $query->result('array');
		return $result;
	}
	public function rate_it($rate_val){
		$insert_sql = "INSERT INTO answers_rating SET
						answers_id = '".$rate_val['answers_id']."',
						rating_num = '".$rate_val['rating_num']."',
						user_id = '".$rate_val['user_id']."'";
		$query = $this->db->query($insert_sql);
		return $this->db->insert_id();
	}
}
