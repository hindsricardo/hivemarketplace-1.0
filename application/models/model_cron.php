<?php
class Model_cron extends CI_Model {  
  
    function __construct(){
        parent::__construct();
    }
    function get_order_received(){
		$current_date=gmdate('Y-m-d H:i:s');
		$select_sql =  "SELECT *
						FROM `order_details`
						WHERE `order_status` = 'Pending'
						AND `seller_payment` =0
						AND payment_scheduled_date !='0000-00-00 00:00:00'
						AND payment_scheduled_date < UTC_TIMESTAMP()";
		$query = $this->db->query($select_sql);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			//echo '<pre>';print_r($result);die();
			return $result;
		}else{
			$result = array();
		}
	}
	
	public function winnauction(){
		$select_auction = "SELECT ID, type FROM auction WHERE status = '0' AND schedule_endtime < UTC_TIMESTAMP() AND stage != 'win'";
		$query = $this->db->query($select_auction);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			foreach($result as $result_val){
				$auction_winner = $this->check_auction($result_val['ID']);
}
			
		}
	}
	public function check_auction($auction_id){
		$select_auction = "SELECT ID, type FROM auction WHERE status = '0' AND schedule_endtime < UTC_TIMESTAMP() AND stage != 'win' and ID = '".$auction_id."'";
		$query = $this->db->query($select_auction);
		if($query->num_rows() > 0){
			$result = $query->result('array');
			$auction_winner = $this->update_auction_winner($auction_id, $result[0]['type']);
		}
	}
	public function update_auction_winner($auction_id, $type){
			$select_user_id = "select a.user_id from (SELECT user_id, SUM(`credits`) AS credit FROM `user_buycredits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`
						union all
						SELECT user_id, SUM(`credits`) AS credits FROM `user_bonus_credits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`) a 
						where a.credit = (select max(b.credit) from (SELECT user_id, SUM(`credits`) AS credit FROM `user_buycredits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`
						union all
						SELECT user_id, SUM(`credits`) AS credits FROM `user_bonus_credits` WHERE `action` = 'Bid' and `auction_id`= '".$auction_id."' group by `user_id`) b ) group by a.user_id";		
						
			$query = $this->db->query($select_user_id);
			if($query->num_rows() > 0){
				$result = $query->result('array');
				$total_bid = $this->model_manage_credits->auction_total_credits($auction_id);
				if($type == 'credit_auction'){
					$update_sql = "UPDATE auction SET stage ='win', winner_id = '".$result[0]['user_id']."'
								WHERE ID = '".$auction_id."'";
					$query = $this->db->query($update_sql);
					
					$record['total_bid'] = $total_bid;
					$record['auction_id'] = $auction_id;
					$record['buyer_id'] = $result[0]['user_id'];
					
					$this->load->model('model_paypal');
					
					$paypal_payment = $this->model_manage_credits->credit_auction_payment($record);
					return $result[0]['user_id'];
				}else{
					$select_min_bid = "SELECT ID, min_bids FROM auction WHERE ID = '".$auction_id."'";
					$query_min_bid = $this->db->query($select_min_bid);
					$result_min_bid = $query_min_bid->result('array');
					if($result_min_bid[0]['min_bids'] <= $total_bid){
						$update_sql = "UPDATE auction SET stage ='win', winner_id = '".$result[0]['user_id']."'
									WHERE ID = '".$auction_id."'
									AND min_bids <= '".$total_bid."'";
					}else{
						$select_reversed_credits = "SELECT user_id, SUM( credit ) as total_credit, SUM( `credits_price` ) AS total_credits_price
							FROM (
								SELECT user_id, SUM( `credits` ) AS credit, SUM( `credits_price` ) AS credits_price
								FROM `user_buycredits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
								UNION ALL
								SELECT user_id, SUM( `credits` ) AS credits, SUM( `credits_price` ) AS credits_price
								FROM `user_bonus_credits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
							)a
							GROUP BY a.user_id ";
						$reversed_query = $this->db->query($select_reversed_credits);
						$result_reversed = $reversed_query->result('array');
						if(!empty($result_reversed)){
							foreach($result_reversed as $result_reversed_val){
								$insert_reversed_query = "INSERT INTO auction_reversed SET
												user_id = '".$result_reversed_val['user_id']."',
												auction_id = '".$auction_id."',
												credits = '".$result_reversed_val['total_credit']."',
												credits_price = '".$result_reversed_val['total_credits_price']."',
												date_entered = '".to_db_date()."',
												action = 'Reversed',
												status = '0'";
								$this->db->query($insert_reversed_query);
							}
						}
						
						$delete_buycredits_sql = "DELETE FROM user_buycredits WHERE auction_id = '".$auction_id."' AND action = 'Bid'";
						$buycredits_query = $this->db->query($delete_buycredits_sql);
						
						$delete_bonus_credits_sql = "DELETE FROM user_bonus_credits WHERE auction_id = '".$auction_id."' AND action = 'Bid'";
						$bonus_credits_query = $this->db->query($delete_bonus_credits_sql);
						
						$update_sql = "UPDATE auction SET status = '1'
									WHERE ID = '".$auction_id."'";
					}
					
					$query = $this->db->query($update_sql);
					return $result[0]['user_id'];
				}
			}else{
				if($type == 'credit_auction'){
						$select_reversed_credits = "SELECT user_id, SUM( credit ) as total_credit, SUM( `credits_price` ) AS total_credits_price
							FROM (
								SELECT user_id, SUM( `credits` ) AS credit, SUM( `credits_price` ) AS credits_price
								FROM `user_buycredits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
								UNION ALL
								SELECT user_id, SUM( `credits` ) AS credits, SUM( `credits_price` ) AS credits_price
								FROM `user_bonus_credits`
								WHERE `action` = 'Bid'
								AND `auction_id` = '".$auction_id."'
								GROUP BY `user_id`
							)a
							GROUP BY a.user_id ";
						$reversed_query = $this->db->query($select_reversed_credits);
						$result_reversed = $reversed_query->result('array');
						if(!empty($result_reversed)){
							foreach($result_reversed as $result_reversed_val){
								$insert_reversed_query = "INSERT INTO auction_reversed SET
												user_id = '".$result_reversed_val['user_id']."',
												auction_id = '".$auction_id."',
												credits = '".$result_reversed_val['total_credit']."',
												credits_price = '".$result_reversed_val['total_credits_price']."',
												date_entered = '".to_db_date()."',
												action = 'Reversed',
												status = '0'";
								$this->db->query($insert_reversed_query);
							}
						}
				
					$delete_buycredits_sql = "DELETE FROM user_buycredits WHERE auction_id = '".$auction_id."' AND action = 'Auction'";
					$buycredits_query = $this->db->query($delete_buycredits_sql);
					$delete_bonus_credits_sql = "DELETE FROM user_bonus_credits WHERE auction_id = '".$auction_id."' AND action = 'Auction'";
					$bonus_credits_query = $this->db->query($delete_bonus_credits_sql);
				}
				$update_auction_sql = "UPDATE auction SET status = '1'
										WHERE ID = '".$auction_id."'";
				$query = $this->db->query($update_auction_sql);

				return '';
			}
	}
}
?>