<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_login');
	}

	function index()
	{
		$data['error'] = "Please enter common password";
		$this->load->view('login_page', $data);
	}
	
	function authentication()
	{
		$username = fSecureInput($this->input->post('username'));
		$password = fSecureInput($this->input->post('password'));
		
		if($username == "" || $password == "")
		{
			$data['error'] = "Invalid Username/Password";
			$data['area'] = "loginarea";
			$this->load->view('login_step2', $data);
		}
		else
		{
			//echo 'hello';die();
			$result = $this->model_login->login($username, $password);

			if($result) 
			{
				$get_login_user_details = $this->model_login->get_login_user_details($username, $password);
				$session_data = array(
									"logged_in" => TRUE,
									"id"  => $get_login_user_details[0]->id,
									"username"  => $get_login_user_details[0]->username,
									"fname"  => $get_login_user_details[0]->fname,
									"lname"  => $get_login_user_details[0]->lname,
									"name"  => $get_login_user_details[0]->name,
									"email"  => $get_login_user_details[0]->email,
									"status"  => $get_login_user_details[0]->status,
									"level"  => $get_login_user_details[0]->level,
								);

				$this->session->set_userdata($session_data);
				//echo '<pre>';print_r($this->session->all_userdata());die();

				$rand=random_string('alnum', 10);
				$data['email']=$this->model_login->updatecode($rand,$username);
				//die();
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('info@hivemarketplace.com', 'System Administrator');
				$this->email->to($data['email']);
							
				$this->email->subject('Penny Auction Admin Passcode');
				$this->email->message('Dear '.$username.',<br><br> Your passcode for Hive Marketplace admin panel is '.$rand.'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				$this->email->send();
				$data['error'] = "Please Check Your Email for the Passcode";
				$data['area'] = "passcodearea";
				$this->load->view('login_step2', $data);
				//redirect('main');
			}
			else
			{
				$data['error'] = "Invalid Username/Password";
				$data['area'] = "loginarea";
				$this->load->view('login_step2', $data);
			}
		}
	}
	
	
	function pass_authentication()
	{
		$password = md5($this->input->post('password'));
				
		if($password == "")
		{
			$data['error'] = "Invalid Password";
			$this->load->view('login_page', $data);
		}
		else
		{
			$result = $this->model_login->passauthenticate($password);

			if($result) 
			{
				//redirect('main');
				$data['area'] = "loginarea";
				$this->load->view('login_step2',$data);
			}
			else
			{
				$data['error'] = "Invalid Password";
				$this->load->view('login_page', $data);
			}
		}
	}
	
	
	function passcode()
	{
	  $passcode = $this->input->post('passcode');
	  $result = $this->model_login->passcodeauthenticate($this->session->userdata('username'), $passcode);
	  if($result) 
			{
				redirect('main');				
			}
			else
			{
				$data['error'] = "Wrong Passcode! Please enter Username/Password Again";
				$data['area'] = "loginarea";
				$this->load->view('login_step2', $data);
			}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */