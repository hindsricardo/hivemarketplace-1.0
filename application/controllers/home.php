<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
		$this->load->model('registration_model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
			$page = 1;
			$start = 0;
			$limit = 10;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
		$pageination['start'] = $start;
		$pageination['limit'] = $limit;
		$data['list'] = $this->model_list_auction->get_auction_list($pageination);
		//echo '<pre>';print_r($data['list']);die();
		$data['total_rows'] = $this->model_list_auction->get_auction_list();
		$data['page'] = $page;
		$data['limit'] = $limit;
		$a_id = '';
			foreach($data['list'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
		$data['auction_ids'] = $a_id;
		
		$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
		$this->load->view('user/home',$data);
	}
	public function authentication(){
	//echo 'hello';die();
		$user_data['username'] = fSecureInput($this->input->post('username'));
		$user_data['password'] = fSecureInput($this->input->post('password'));
		
		$result = $this->model_users->check_valid_user($user_data);
		//echo '<pre>';print_r($result);die();
		if(!empty($result)){
			$session_data = array(
								"user_logged_in" => TRUE,
								"user_id"  => $result[0]->id,
								"username"  => $result[0]->username,
								"fname"  => $result[0]->fname,
								"lname"  => $result[0]->lname,
								"email"  => $result[0]->email,
								"status"  => $result[0]->status,
								"bid_details" => $this->model_list_auction->user_bid_details($result[0]->id),
								"private_auction" => $result[0]->private_auction
							);
			$this->session->set_userdata($session_data);
			$data['user_details'] = $this->model_users->get_user_details($this->session->userdata('user_id'));
			$data['user_notifications'] = $this->model_users->get_user_notifications($this->session->userdata('user_id'));
			
			
			 $id= $this->session->userdata('user_id');
			  $data_login=array(
			   'lost_login' =>  to_db_date()
			  );
			 $this->db->where('id', $id);
			 $this->db->update('users', $data_login);
			
			redirect('');
		}else{
			redirect('');
			//$this->load->view('user/home');
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('home');
		//$this->load->view('user/home');
	}
	public function search($recent_search_id = ''){
	//echo '<pre>';print_r($_POST);die();
		$page = 1;
		$start = 0;
		$limit = 10;
		if($this->input->post("limit")){
			$limit = $this->input->post("limit");
		}
		if($this->input->post("page")){
			$page = $this->input->post("page");
		}else{
			$page = 1;
		}
		if($page)
			$start = ($page - 1) * $limit; 
		else
			$start = 0;	

		
		$pageination['start'] = $start;
		$pageination['limit'] = $limit;
		if($recent_search_id != ''){
			$search_val = $this->model_list_auction->get_recent_search_details($recent_search_id);
			$data['search_str'] = unserialize($search_val[0]->params);
		}else{
			$data['search_str'] = fSecureInput($this->input->post('search_str'));
		}
		$data['list'] = $this->model_list_auction->get_auction_search($data['search_str'], $pageination);
		$data['total_rows'] = $this->model_list_auction->get_auction_search($data['search_str']);
		$data['page'] = $page;
		$data['limit'] = $limit;
		$a_id = '';
		foreach($data['list'] as $list_ids){
			$a_id .= $list_ids['id'].',';
		}
		$data['auction_ids'] = $a_id;
		$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
		if($this->input->post('search_str')){
			$search['title'] = '';
			$search['user_id'] = $this->session->userdata('user_id');
			$search['created_date'] = to_db_date();
			$search['params'] = serialize($data['search_str']);
			//echo "<pre>";print_r($search);die();
			$data['save_search'] = $this->model_list_auction->save_search($search);
		}
		$this->load->view('user/home',$data);
	}
	public function auction_filter(){
		$page = 1;
		$start = 0;
		$limit = 10;
		if($this->input->post("limit")){
			$limit = $this->input->post("limit");
		}
		if($this->input->post("page")){
			$page = $this->input->post("page");
		}else{
			$page = 1;
		}
		if($page)
			$start = ($page - 1) * $limit; 
		else
			$start = 0;
		$filter['search_str'] = fSecureInput($this->input->post('search_str'));
		$filter['auction_sort'] = $this->input->post('auction_sort');
		$filter['price_0_50'] = $this->input->post('price_0_50');
		$filter['price_50_100'] = $this->input->post('price_50_100');
		$filter['price_100_500'] = $this->input->post('price_100_500');
		$filter['price_500'] = $this->input->post('price_500');
		$filter['features_no_min_bid'] = $this->input->post('features_no_min_bid');
		$filter['features_credit_auction'] = $this->input->post('features_credit_auction');
		$filter['display_auction_within'] = $this->input->post('display_auction_within');
		$filter['miles_of_zipcode'] = $this->input->post('miles_of_zipcode');
		$pageination['start'] = $start;
		$pageination['limit'] = $limit;
		
		$data['list'] = $this->model_list_auction->auction_filter($filter, $pageination);
		$data['total_rows'] = $this->model_list_auction->auction_filter($filter);
		$data['page'] = $page;
		$data['limit'] = $limit;
		//echo $this->db->last_query();
		$a_id = '';
		foreach($data['list'] as $list_ids){
			$a_id .= $list_ids['id'].',';
		}
		$data['auction_ids'] = $a_id;
		$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
		$this->load->view('user/home_bk',$data);
	}
	public function private_auction_pass(){
		$pass['user_id'] = $this->session->userdata('user_id');
		$pass['auction_id'] = $this->input->post('auction_id');
		$pass['password'] = $this->input->post('password');
		$result = $this->model_users->private_auction_pass($pass);
		if($result != ''){
			$update_private_auction = $this->model_users->private_auction_update($pass);
				$session_bid_data = $this->session->userdata('private_auction');
				$this->session->set_userdata('private_auction',$update_private_auction);	
			echo 'Sucess!';
		}else{
			echo 'Sorry, Passcode Incorrect';
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */