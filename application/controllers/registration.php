<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Registration extends CI_Controller{
 public function __construct()
 {
  parent::__construct();
  $this->load->model('registration_model');
 }
 public function index()
 {
  	$this->load->view('user/registration');
 }
 public function registeremail($id){
	 $this->load->library('email');
	 $config['mailtype'] = 'html';
	 $this->email->initialize($config);
	 $this->email->from('info@hivemarketplace.com', 'HIVE Marketplace Info');
	 $this->email->to(get_user_email($id));
	
	 $this->email->subject('HIVE MARKETPLACE Account Activation');
	 $this->email->message('<div id="messagecontain" style="font-family:Verdana;align:center;padding:30px;">
     <div id="messagehead" style="width:90%;background:#ffcc00; border-top-left-radius:15px;border-top-right-radius:15px;font-size:22px;">HIVE MARKETPLACE Account Activation</div>
     <br>
     <div id="messagebody" style="background:#EBF5FF;border-radius:10px;box-shadow:-1 1 #cccccc;border:1px solid #cccccc; width:90%;">
       <div class="messagegreet" style="font-size:14px;font-weight:bold;">Congratulations on becoming a member of HIVE MarketPlace, the best place with the best prices and profits!</div>
       <br>
       <br>
       <span style="font-size:13px;">WAIT!! YOU HAVE TO READ THIS! YOUR SUCCESS DEPENDS ON IT!
       <br>
       <br>
       <p>Before you get started changing the way you buy and sell online I just want to give you some quick tips that will help you get started.</p>
       </span>
       <br>
      
       <span style="font-weight:bold; font-style:underline;font-size:13px;">Auctions<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>HOW AUCTIONS WORKS FOR BIDDERS: The winner of an auction is the person who places the highest number of bids by the end of the clock. Those joining an auction late will have to bid more than highest bidder. A high initial bid is a great way to price out the competition. If an auction has a minimum price that is not met at the end of the clock all bids will be returned to their owners as though the auction never happened.</li>
        <li>HOW AUCTIONS WORK FOR SELLERS: Once create your auction you just sit back and watch the bids come in. After all those bids are yours. Each auction has acebook and Twitter share buttons to help you promote your auction to all prospective winners.</li>
       
       </ul></p>
       <span style="font-weight:bold; font-style:underline;font-size:13px;">After Auction Ends<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>IF YOU WON: You are responsible for the accumilated pennies plus any shipping if applicable. The auction will appear in your checkout. You will either have the option to pay and pickup in person, if the seller is in your area, or have the item shipped to you.</li>
        <li>SOMEONE JUST WON YOUR AUCTION: Once the winner pays to have the item shipped or chooses to pickup the item in person, you will see the auction in your activity/paid section. Your auction profit will be disbursed when the buyer confirms they have received the auction or automically 7 days after shipping.</li>
        <li>DID NOT WIN THE AUCTION: No worries some sellers make limited number of the item from the auction available exclusively to their bidders. And the best part the normal price is discounted by the amount you spent in the auction. </li>
       </ul></p>
       <span style="font-weight:bold; font-style:underline;font-size:13px;">Bids/Credits<span>
       <br>
       <p style="font-size:13px;"><ul>
        <li>COST: Each bid cost 75cents</li>
        <li>AUCTION YOUR BIDS: At any time you can auction your bids to potentially make more than they are worth. Profits from these type of auctions are release immediatley into your PayPal account.</li>
        <li>REDEEM BIDS FOR CASH: At any time you can reedem your bids for money directly into your PayPal account.</li>
       </ul></p>
       <br>
       <span style="font-size:13px;">
       <p>Now that you know all the basics, you are ready to get started! Click the link below to verify your account.</p>
       <br>
       
     <a href="'.base_url().'registration/activate_account/'.$id.'">Click to Activate Account</a>
     <br>
     <br>
     <p>If you have any questions checkout the LIVE HELP/FAQ to get your question answered and find great tips from the entire HIVE MarketPlace community.
Good Luck!
</p>
<br>
<br>
<p style="font-weight:bold;">Thanks for bee-ing a part of the HIVE!</p>
<br>
<p style="font-style:italic;">Ricardo and HIVE MarketPlace Staff</p>

     
     
     </span></div></div>');
	
	 $this->email->send();
	 //$this->email->print_debugger();
 }
 
 public function thank()
 {
  $data['title']= 'Thank';
  //$this->load->view('header_view',$data);
  $this->load->view('user/thank', $data);
  //$this->load->view('footer_view',$data);
 }
 public function edit_profilepic(){
    //show($_FILES);
        if($_FILES['profilepic']['error'] == 0){
            //upload and update the file
            $config['upload_path'] = './assetts/uploads/'; // Location to save the image
            $config['allowed_types'] = 'gif|jpg|png';
            $config['overwrite'] = false;
            $config['remove_spaces'] = true;
            //$config['max_size']   = '100';// in KB // if required, remove the comment and give the size
 
            $this->load->library('upload', $config); //codeigniter default function
 
            if ( ! $this->upload->do_upload('profilepic')){
              //  redirect("user/profile/".$username); 
			  $this->index();
            }
            else{
                //Image Resizing
                $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 200; // image re-size  properties
                $config['height'] = 150; // image re-size  properties
 
                $this->load->library('image_lib', $config); //codeigniter default function
 
                if ( ! $this->image_lib->resize()){
                  //  redirect("user/profile/".$username); // redirect  page if the resize fails.
				  $this->index();
                }
 
              //  $this->obj_model->update_profile_pic($this->tank_auth->get_user_id()); 
				
				//redirect("user/profile/".$username);
            }
        }
        /*else{
        redirect("user/profile/".$username);
        }*/
    }
 public function save()
 {
  $this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('username', 'User Name', 'trim|required|xss_clean');  
  $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[4]|max_length[32]');
  $this->form_validation->set_rules('confirmpass', 'Password Confirmation', 'trim|required|matches[pass]');
  $this->form_validation->set_rules('firstname', 'First Name', 'trim|required|xss_clean'); 
  $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|xss_clean'); 
  $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]'); 
  $this->form_validation->set_rules('zipcode', 'Zip', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('accept_terms','TOS','trim|required|xss_clean');
  //$this->form_validation->set_rules('profilepic', 'Upload Image', 'trim|required|xss_clean');  
  
	if($this->form_validation->run() == FALSE){
		$this->index();
	}else{
    //echo "<pre>";print_r($_POST);die();
		$this->edit_profilepic();
		$id = $this->registration_model->add_user();
		$this->registration_model->add_user_notification($id);
		$this->registration_model->add_rewards($id);
		$this->registeremail($id);
		$this->thank();
	}
 }
	public function activate_account($id){
		$id = $this->registration_model->activate_account($id);
		redirect('');
	}
 
}
?>