<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faqs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_faqs');
	}
	public function index(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 10;
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
			if($this->session->userdata('faq_search')){
				$faq_val['questions'] = fSecureInput($this->session->userdata('faq_search'));
				$data['faq_list'] = $this->model_faqs->search($faq_val);
			}
			//echo '<pre>';print_r($data['unanswered_questions_list']);die();
			$this->load->view('faq_answer_center',$data);
		}else{
			$page = 1;
			$start = 0;
			$limit = 10;
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
			if($this->session->userdata('faq_search')){
				$faq_val['questions'] = fSecureInput($this->session->userdata('faq_search'));
				$data['faq_list'] = $this->model_faqs->search($faq_val);
			}
			
			$this->load->view('faq_answer_center',$data);
		}
	}
	public function question_submit(){
		$faq_val['questions'] = fSecureInput($this->input->post('faq_question'));
		$faq_val['date_entered'] = to_db_date();
		$faq_val['user_id'] = $this->session->userdata('user_id');
		$faq_val['deleted'] = 0;
		$id = $this->model_faqs->question_submit($faq_val);
		
		$questions_user_id = $this->model_faqs->get_questions_userid($id);
		$questions_user_name = get_user_username($questions_user_id);
		
			$page = 1;
			$start = 0;
			$limit = 10;			
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
		
		echo $this->load->view('unanswered_questions',$data);
	}
	public function search(){
		if($this->input->post('faq_search')){
			$faq_val['questions'] = fSecureInput($this->input->post('faq_search'));
		}else{
			$faq_val['questions'] = fSecureInput($this->session->userdata('faq_search'));
		}
		$data['faq_list'] = $this->model_faqs->search($faq_val);
		//echo '<pre>';print_r($data['faq_list']);die();
		$page = 1;
		$start = 0;
		$limit = 10;			
		if($page) $start = ($page - 1) * $limit; 
		$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
		$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
		$data['page'] = $page;
		$data['limit'] = $limit;
		$this->session->set_userdata('faq_search', $faq_val['questions']);
		$this->load->view('faq_answer_center',$data);
	}
	public function details($id){
		$data['faq_details'] = $this->model_faqs->details($id);
		//echo '<pre>';print_r($data['faq_details']);die();
		$page = 1;
		$start = 0;
		$limit = 10;			
		if($page) $start = ($page - 1) * $limit; 
		$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
		$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
		$data['page'] = $page;
		$data['limit'] = $limit;	
		$this->load->view('faq_details',$data);
	}
	public function submit_comment(){
		//echo '<pre>';print_r($_POST);die();
		$questions_id = $this->input->post('questions_id');
		$comment_val['answers_id'] = $this->input->post('answers_id');
		$comment_val['comments'] = fSecureInput($this->input->post('comments'));
		$comment_val['date_entered'] = to_db_date();
		$comment_val['user_id'] = $this->session->userdata('user_id');
		$comment_val['deleted'] = 0;
		//echo '<pre>';print_r($comment_val);die();
		$comment_id = $this->model_faqs->submit_comment($comment_val);
		$data['faq_details'] = $this->model_faqs->details($questions_id);
			$page = 1;
			$start = 0;
			$limit = 10;			
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
		$this->load->view('faq_details',$data);

		//echo '<pre>';print_r($comment_val);
	}
	public function answer_submit(){
		$answer_val['questions_id'] = $this->input->post('questions_id');
		$answer_val['answers'] = fSecureInput($this->input->post('answers'));
		$answer_val['date_entered'] = to_db_date();
		$answer_val['user_id'] = $this->session->userdata('user_id');
		$answer_val['deleted'] = 0;
		$answer_id = $this->model_faqs->answer_submit($answer_val);
		
		$questions_user_id = $this->model_faqs->get_questions_userid($answer_val['questions_id']);
		$questions_user_name = get_user_username($questions_user_id);
		
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('info@hivemarketplace.com', 'System Administrator');
				$this->email->to(get_user_email($questions_user_id));
				
				$this->email->subject('Penny Auction FAQ');
				$this->email->message('Dear '.$questions_user_name.',<br><br> Your answer for Hive Marketplace is <b>'.$answer_val['answers'].'</b><br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				//echo '<pre>';print_r($this->email);die();
				$this->email->send();
		
		$data['faq_details'] = $this->model_faqs->details($answer_val['questions_id']);
			$page = 1;
			$start = 0;
			$limit = 10;			
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
		$this->load->view('faq_details',$data);
	}
	public function unanswered_answered_submit(){
		$answer_val['questions_id'] = $this->input->post('questions_id');
		$answer_val['answers'] = fSecureInput($this->input->post('unanswered_answer'));
		$answer_val['date_entered'] = to_db_date();
		$answer_val['user_id'] = $this->session->userdata('user_id');
		$answer_val['deleted'] = 0;
		$answer_id = $this->model_faqs->answer_submit($answer_val);
		
		$questions_user_id = $this->model_faqs->get_questions_userid($answer_val['questions_id']);
		$questions_user_name = get_user_username($questions_user_id);
		
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('info@hivemarketplace.com', 'System Administrator');
				$this->email->to(get_user_email($questions_user_id));
				
				$this->email->subject('Penny Auction FAQ');
				$this->email->message('Dear '.$questions_user_name.',<br><br> Your answer for Hive Marketplace is '.$answer_val['answers'].'</b><br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				//echo '<pre>';print_r($this->email);die();
				$this->email->send();
		
			$page = 1;
			$start = 0;
			$limit = 10;			
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
		echo $this->load->view('unanswered_questions',$data);
	}
	public function unanswred_pagination(){
			$page = 1;
			$start = 0;
			$limit = 10;
			
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;	
			
			if($page) $start = ($page - 1) * $limit; 
			$data['unanswered_questions_list'] = $this->model_faqs->get_unanswered_questions(array("start" => $start, "limit" => $limit));
			$data['total_rows'] =  $this->model_faqs->count_unanswered_questions();
			$data['page'] = $page;
			$data['limit'] = $limit;
			
			echo $this->load->view('unanswered_questions',$data);
	}
	public function rate_it(){
		$record['rating_num'] = $this->input->post('rate_val');
		$record['answers_id'] = $this->input->post('answer_id');
		$record['user_id'] = $this->session->userdata('user_id');
		$rate_id = $this->model_faqs->rate_it($record);
		
		$result['rate_details'] = $this->model_faqs->get_rates($record['answers_id']);
		//echo '<pre>';print_r($result['rate_details']);die();
		echo $this->load->view('faq_rate',$result);
		
	}
}