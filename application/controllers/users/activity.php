<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
		$this->load->model('model_paypal');
		$this->load->model('message_model');
		$this->load->model('cases_model');
	}
	public function index(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 10;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$filter_data['sort_by'] = '';
			$data['sort_by'] = '';
			if($this->input->post("sort_by")){
				$filter_data['sort_by'] = $this->input->post("sort_by");
				$data['sort_by'] = $filter_data['sort_by'];
			}else{
				$filter_data['sort_by'] = 'endig_soon';
				$data['sort_by'] = $filter_data['sort_by'];
			}
			//echo $filter_data['sort_by'];die();
			$data['bids_and_offers'] = $this->model_list_auction->get_bids_and_offers_list($filter_data, $pageination);
						$a_id = '';
						foreach($data['bids_and_offers'] as $list_ids){
							$a_id .= $list_ids['id'].',';
						}
						$data['bids_and_offers_auction_ids'] = $a_id;
			$data['watchlist_list'] = $this->model_list_auction->get_watchlist_list($pageination);
						$a_id = '';
						foreach($data['watchlist_list'] as $list_ids){
							$a_id .= $list_ids['id'].',';
						}
						$data['watchlist_list_auction_ids'] = $a_id;
			
			$data['sold_list'] = $this->model_list_auction->get_sold_list($filter_data, $pageination);
			$data['active_auction_list'] = $this->model_list_auction->get_paid_active_auction_list($filter_data, $pageination);
						$a_id = '';
						foreach($data['active_auction_list'] as $list_ids){
							$a_id .= $list_ids['id'].',';
						}
						$data['active_auction_list_auction_ids'] = $a_id;			
			
			$data['schedule_auction_list'] = $this->model_list_auction->get_paid_schedule_auction_list($filter_data, $pageination);
				$a_id = '';
				foreach($data['schedule_auction_list'] as $list_ids){
					$a_id .= $list_ids['id'].',';
				}
				$data['schedule_auction_list_auction_ids'] = $a_id;			

			$data['search_list'] = $this->model_list_auction->get_recent_search();
			$data['cases_list'] = $this->model_list_auction->get_cases_list($pageination);
			$data['paid_list'] = $this->model_list_auction->get_paid_list($filter_data, $pageination);
			$data['list_direct_purchase'] = $this->model_list_auction->get_auction_direct_purchase_list($pageination);
			//echo '<pre>';print_r($data['list_summary']);die();
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_summary',$data);
		}else{
			redirect('');
		}
	}
	public function summary(){
		$data['list_summary'] = $this->model_list_auction->get_auction_list();
		//echo '<pre>';print_r($data['list_summary']);
		
	}
	public function direct_purchase(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_direct_purchase'] = $this->model_list_auction->get_auction_direct_purchase_list($pageination);
			$data['page'] = $page;
			$data['limit'] = $limit;
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_direct_purchase',$data);
			}else{
			redirect('');
		}
	}
	public function bids_and_offers(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
			$filter_data['sort_by'] = '';
			$data['sort_by'] = '';
			if($this->input->post("sort_by")){
				$filter_data['sort_by'] = $this->input->post("sort_by");
				$data['sort_by'] = $filter_data['sort_by'];
			}else{
				$filter_data['sort_by'] = 'endig_soon';
				$data['sort_by'] = $filter_data['sort_by'];
			}
			//echo $filter_data['sort_by'];die();
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_bids_and_offers_list($filter_data, $pageination);
			//$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_bids_and_offers',$data);
		}else{
			redirect('');
		}
	}
	
	
	public function watchlist(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_watchlist_list($pageination);
			//$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_watchlist',$data);
		}else{
			redirect('');
		}
	}
	public function update_watchlist(){
		$auction_id = $this->input->post("auction_id");
		$status = $this->model_users->update_watchlist($auction_id);
		echo $status;
	}
	public function sold(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$filter_data = array();
			$data['sort_by_date'] = '';
			if($this->input->post("sort_by_date")){
				$filter_data['sort_by_date'] = $this->input->post("sort_by_date");
				$data['sort_by_date'] = $filter_data['sort_by_date'];
			}
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_sold_list($filter_data, $pageination);  
			//echo '<pre>';print_r($data['list_summary']);die();
			//$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_sold',$data);
		}else{
			redirect('');
		}
	}
	
	
	public function paid(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$filter_data = array();
			$data['sort_by_date'] = '';
			if($this->input->post("sort_by_date")){
				$filter_data['sort_by_date'] = $this->input->post("sort_by_date");
				$data['sort_by_date'] = $filter_data['sort_by_date'];
			}
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_paid_list($filter_data, $pageination);  
			//echo '<pre>';print_r($data['list_summary']);die();
			//$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_paid',$data);
		}else{
			redirect('');
		}
	}	
	
	
	
	public function update_payment_received(){
		$order_id = $this->input->post("order_id");
		$result = $this->model_paypal->update_payment_received($order_id);
		echo 'sucess';
	}
	public function recent_search(){
		$data['search_list'] = $this->model_list_auction->get_recent_search();
		$data['unread'] =$this->message_model->message_unread();
		$this->load->view('user/activity_recent_search',$data);
	}
	public function cases(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_cases_list($pageination);
			//$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_case',$data);
		}else{
			redirect('');
		}
	}
	public function active_auction(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
			$filter_data = array();
			//echo $this->input->post("sort_by_date");die();
			if($this->input->post("sort_by_date")){
				$filter_data['sort_by_date'] = $this->input->post("sort_by_date");
				$data['sort_by_date'] = $filter_data['sort_by_date'];
			}else{
				$filter_data['sort_by_date'] = 31;
				$data['sort_by_date'] = 31;
			}
			 
			//echo $sort_by_date;die();
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_paid_active_auction_list($filter_data, $pageination);
			//$data['total_rows'] = $this->model_list_auction->get_paid_active_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_paid_active_auction',$data);
		}else{
			redirect('');
		}
	}
	public function schedule_auction(){
		if($this->session->userdata('user_logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
			$filter_data = array();
			if($this->input->post("sort_by_date")){
				$filter_data['sort_by_date'] = $this->input->post("sort_by_date");
				$data['sort_by_date'] = $filter_data['sort_by_date'];
			}else{
				$filter_data['sort_by_date'] = 31;
				$data['sort_by_date'] = 31;
			}
				
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_summary'] = $this->model_list_auction->get_paid_schedule_auction_list($filter_data, $pageination);
			//$data['total_rows'] = $this->model_list_auction->get_paid_schedule_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_summary'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/activity_paid_schedule_auction',$data);
		}else{
			redirect('');
		}
	}
	
	
	
	
	public function update_shipped_scheduled_pickup(){
		$order_data['order_id'] = $this->input->post("order_id");
		$order_data['payment_scheduled_date'] = date('Y-m-d h:i:s', strtotime('+10 day', strtotime(to_db_date())));
		$order_id = $this->model_manage_credits->update_shipped_scheduled_pickup($order_data);
	}
	public function send_shipping_or_pickup(){
		$order_data['id'] = $this->input->post("id");
		$get_date = $this->input->post("get_date");
		
		
		$order_data['payment_scheduled_date'] = date('Y-m-d h:i:s', strtotime($get_date));
		if($order_data['payment_scheduled_date'] > to_db_date()){
			$order_details_id = $this->model_manage_credits->send_shipping_or_pickup($order_data);
			echo 'Successfully update Scheduled date'.$order_data['payment_scheduled_date'];	
		}else{
			echo 'You have choosen past date!!!';
		}
		//echo '<pre>';print_r($order_data);die();
		
	}
	public function popup_shipping($order_details_id){
		$order_data['order_details_id'] = $order_details_id;
		$this->load->view('user/shipping_popup',$order_data);
	}
	public function update_order_received1(){
		$order_details_id = $this->input->post("order_details_id");
		$result = $this->model_paypal->update_order_received($order_details_id);
		echo $result;
	}
	public function update_order_received(){
		$order_details_id = $this->input->post("order_details_id");
		//$order_details_id = '13';
		$this->config->load('paypal');
		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
		);
		// Show Errors
		if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}
		
		$this->load->library('paypal/Paypal_pro', $config);	
		//echo '<pre>';print_r($config);die();
				$order_id_val = $this->model_paypal->get_order_received($order_details_id);
				$product_details = $this->model_paypal->get_seller_item_details($order_id_val[0]['product_id']);
				
				$product_price = $order_id_val[0]['price'];
				$price = $product_price*(1-$product_details[0]['HMP_fee']/100);
				$hmp_fee_amount = ($product_price*$product_details[0]['HMP_fee'])/100;
				
				// Show Errors
				
				//echo '<pre>';print_r($config);die();
				
				$MPFields = array(
									'emailsubject' => 'Buyer has been received the item '.$order_id_val[0]['product_name'].' from HIVEMarketPlace', 						// The subject line of the email that PayPal sends when the transaction is completed.  Same for all recipients.  255 char max.
									'currencycode' => 'USD', 						// Three-letter currency code.
									'receivertype' => 'EmailAddress' 						// Indicates how you identify the recipients of payments in this call to MassPay.  Must be EmailAddress or UserID
								);
				
				// MassPay accepts multiple payments in a single call.  
				// Therefore, we must create an array of payments to pass into the class.
				// In this sample we're simply passing in 2 separate payments with static amounts.
				// In most cases you'll be looping through records in a data source to generate the $MPItems array below.
				
				$Item1 = array(
							'l_email' => $product_details[0]['paypal_account'], 							// Required.  Email address of recipient.  You must specify either L_EMAIL or L_RECEIVERID but you must not mix the two.
							'l_receiverid' => '', 						// Required.  ReceiverID of recipient.  Must specify this or email address, but not both.
							'l_amt' => number_format($price,2), 								// Required.  Payment amount.
							'l_uniqueid' => '', 						// Transaction-specific ID number for tracking in an accounting system.
							'l_note' => '' 								// Custom note for each recipient.
						);
				//$MPItems = array($Item1, $Item2);
				$MPItems = array($Item1);
				$PayPalRequestData = array(
								'MPFields' => $MPFields, 
								'MPItems' => $MPItems
							);
							
				$PayPalResult = $this->paypal_pro->MassPay($PayPalRequestData);
				//echo '<pre>';print_r($PayPalResult);die();
				if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
				{
					$errors = array('Errors'=>$PayPalResult['ERRORS']);
					echo $PayPalResult['ERRORS'];
					die();
					//echo '<pre>';print_r($errors);
					//$this->load->view('paypal_error',$errors);
				}
				else
				{
					$order_details_data['order_status'] = 'Received';
					$order_details_data['seller_payment_amount'] = $price;
					$order_details_data['hmp_fee_amount'] = $hmp_fee_amount;
					$order_details_data['CorrelationId'] = $PayPalResult['CORRELATIONID'];
					$order_details_data['seller_payment_time'] = $PayPalResult['TIMESTAMP'];
					$order_details_data['seller_payment'] = '1';
					
					$update_order_result = $this->model_paypal->update_order_received($order_details_id, $order_details_data);
					// Successful call.  Load view or whatever you need to do here.	
					echo 'success';
				}
		//echo $update_order_result;
	}
	public function check_edit_auction_endtime(){
	
		$aid = $this->input->post("aid");
		$schedule_time = $this->input->post("schedule_time");
		$current_time = strtotime(to_db_date());
		//echo date("Y-m-d H:i:s",$schedule_time).'>>>>>>>>'.date("Y-m-d H:i:s");die();
		if($schedule_time < $current_time){
			echo 'no';	
		}else{
			echo 'yes';
		}
	}
}
