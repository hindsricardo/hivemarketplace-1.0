<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('message_model');
		
	}
	public function index(){
		if($this->session->userdata('user_logged_in')){
		  $data['title'] = 'Mssages';	
		  $data['msg'] =$this->message_model->message_all();
		  $data['unread'] =$this->message_model->message_unread();
		  //echo '<pre>';print_r($data);die();
			$this->load->view('user/messages',$data);
		}else{
			redirect('');
		}
	}	
	function sendMail(){	
	   if($this->session->userdata('user_id')==''){
	    echo "Please Login and then you can send your mail.";
	   }else{
	 	$this->load->library('email');
		$msg_to = get_user_username($this->input->post('auction_usar_id'));
		$from_name = get_user_username($this->session->userdata('user_id'));
		$insert_msg = array(
			'sender_id' => $this->session->userdata('user_id'),
			'recever_id' => $this->input->post('auction_usar_id'),
			'parent_id' => $this->input->post('auction_id'),
			'msg_to' => $msg_to,
			'msg_from' => $from_name,
			'subject' => $this->input->post('subject'),
			'msg' => fSecureInput($this->input->post('msg')),
			'type' => $this->input->post('type'),
			'status' => 0,
			'date' => to_db_date()
		);	
		$msg_id=$this->message_model->insert_mail($insert_msg);		
		if( $msg_id > 0 ){		
		/*$from_email=get_user_email($this->session->userdata('user_id'));//session user id here			
		$this->email->from($from_email, $from_name);		
		$to_email = get_user_email($this->input->post('auction_usar_id'));  //auction user id here
		$this->email->to($to_email);
		//$this->email->cc('another@another-example.com')====> $this->config->item('support_email');
		//$this->email->bcc('them@their-example.com');		
		$this->email->subject($this->input->post('subject'));
		$this->email->message($this->input->post('msg'));		
		$this->email->send();*/		
	    echo "Message successfully sended.";
	   }else{ echo "Message not send. Try Again"; }
	   }
	}
	
	public function update(){	
	$data = array(
               'status' => $this->input->post('status')
            );
		
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('auction_mail', $data);
        echo count($this->message_model->message_unread());
	
	}
	
	public function msg_multi(){	  
	  $ids=implode(',',$this->input->post('au_id'));
	  $status=$this->input->post('mode');
	  $this->message_model->message_userstatus($ids,$status);
	  redirect('users/messages');	 
	}
	
	public function msg_multi_send(){	  
	  $ids=implode(',',$this->input->post('au_id'));
	  $status=$this->input->post('mode');	   
	  $this->message_model->message_senderstatus($ids,$status);	  
	  redirect('users/messages/send');
	 
	}
	
	public function send(){
		if($this->session->userdata('user_logged_in')){
		  $data['title'] = 'Send Mssages';	
		  $data['msg'] =$this->message_model->message_send();
		  $data['unread'] =$this->message_model->message_unread();
		  
			$this->load->view('user/msg_send',$data);
		}else{
			redirect('');
		}
	}
	
	public function trash(){
		if($this->session->userdata('user_logged_in')){
		  $data['title'] = 'Trash Mssages';	
		  $data['msg'] =$this->message_model->message_trash();
		  $data['unread'] =$this->message_model->message_unread();
		  
			$this->load->view('user/msg_trash',$data);
		}else{
			redirect('');
		}
	}
	public function msg_restore(){	  
	  $ids=implode(',',$this->input->post('au_id'));	    
	  $this->message_model->trash_restore($ids);	  
	  redirect('users/messages/trash');	 
	}	
	
	public function forword_msg(){	
	    
	    $ids=explode(',',$this->input->post('id'));	
		$id_num= count($ids);	
	    $this->load->library('email');	  
	    $from_email=get_user_email($this->session->userdata('user_id'));
	    $msg_to = get_user_email($this->session->userdata('user_id'));
		$from_name = get_user_username($this->session->userdata('user_id'));		
		$this->email->from($from_email, $from_name);
		$this->email->to($msg_to);
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');
		for($i=0;$i<$id_num;$i++){
			$msg = $this->message_model->forword_msg($ids[$i]);	
				if($msg[0]->type=='hivemarket'){
				  $subject= 'General Notification';
				}else{
				   $subject= 'Item# '.$msg[0]->parent_id;
				}
							
			$this->email->subject('Email Forword by Hive Market Place '.$subject);
			$this->email->message($msg[0]->msg);		
			$this->email->send();
	  	} 
	}
	public function mailreply(){
		$message_details = $this->message_model->forword_msg($this->input->post('id'));
		$insert_msg = array(
			'sender_id' => $message_details[0]->recever_id,
			'recever_id' => $message_details[0]->sender_id,
			'recever_admin' => $message_details[0]->sender_admin,
			'sender_admin' => $message_details[0]->recever_admin,
			'parent_id' => $message_details[0]->parent_id,
			'msg_to' => $message_details[0]->msg_from,
			'msg_from' => $message_details[0]->msg_to,
			'subject' => $this->input->post('subject'),
			'msg' => fSecureInput($this->input->post('msg')),
			'type' => $message_details[0]->type,
			'status' => 0,
			'sender_status' => 0,
			'date' => to_db_date(),
			'auction_mail_id' => $this->input->post('id')
		);	
		$result = $this->message_model->message_reply($insert_msg);
		echo 'Success';
	}
		
}