<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_credits extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_manage_credits');
		$this->load->model('model_list_auction');
	}
	public function index(){
		if($this->session->userdata('user_logged_in')){
			$data['summary'] = $this->model_manage_credits->summary();
			$data['rewards'] = $this->model_manage_credits->get_rewards($this->session->userdata('user_id'));
			$data['withdraw'] = $this->model_manage_credits->withdraw_credits();
			
			
			
			
			
			
			
			
			
			
			
			
			
			$data['credit_history'] = $this->model_manage_credits->user_credit_history($this->session->userdata('user_id'));
			$this->load->view('user/manage_credits',$data);
		}else{
			redirect('');
		}
	}
	public function create_auction(){
		$data['title'] = $this->input->post('create_auction_credit').' '.$this->input->post('credit_type');
		$data['auction_endtime'] = $this->input->post('auction_endtime');
		$data['item_value'] = number_format($this->input->post('item_value'), 2, '.', '');
		$data['qty'] = '1';
		$data['remaining_qty'] = '0';
		$data['auction_type'] = 'Public';
		$data['schedule_time'] = date("Y-m-d H:i:s");
		$data['user_id'] = $this->session->userdata('user_id');
		$data['create_date'] = date("Y-m-d H:i:s");
		$data['modify_date'] = date("Y-m-d H:i:s");
		$data['modify_by'] = $this->session->userdata('user_id');
		$data['type'] = 'credit_auction';
		$data['status'] = '0';
		$user_live_auctions = $this->model_list_auction->count_user_live_auction();
		if($user_live_auctions <= 9){
			$data['HMP_fee'] = '15';
		}elseif($user_live_auctions > 9 && $user_live_auctions <= 19){
			$data['HMP_fee'] = '10';
		}else{
			$data['HMP_fee'] = '5';
		}

		//echo '<pre>';print_r($data);die();
		$list_aucton_id = $this->model_manage_credits->create_auction($data);
		$credits=array(
			'user_id'=>$this->session->userdata('user_id'),
			'credits'=>$this->input->post('create_auction_credit'),
			'auction_id' => $list_aucton_id,
			'credits_price'=>number_format($this->input->post('item_value'), 2, '.', ''),
			'create_date'=>date('Y-m-d H:i:s'),
			'action' => 'Auction'
		  );
		$credit_type = $this->input->post('credit_type');
		$credits_id = $this->model_manage_credits->insert_credits($credits,$credit_type);
		return true;
	}
	public function bid(){
		if($this->session->userdata('user_logged_in')){
			$data['credit_val'] = $this->input->post('credit_val');
			$data['bonus_credit_available'] = $this->input->post('bonus_credit_available');
			$data['hive_credit_available'] = $this->input->post('hive_credit_available');
			$data['total_credit_available'] = $this->input->post('total_credit_available');
			if($data['bonus_credit_available'] == '0'){
				$credits=array(
					'user_id'=>$this->session->userdata('user_id'),
					'credits'=>$this->input->post('credit_val'),
					'auction_id' => $this->input->post('auction_id'),
					'credits_price'=>number_format(($this->input->post('credit_val')*0.75), 2, '.', ''),
					'create_date'=>date('Y-m-d H:i:s'),
					'action' => 'Bid'
					);
				$credit_type = 'HIVECombs';
				$credits_id = $this->model_manage_credits->insert_credits($credits,$credit_type);
				$return_data['total_credit_available'] = $data['total_credit_available'] - $data['credit_val'];
				$return_data['bonus_credit_available'] = $data['bonus_credit_available'];
				$return_data['hive_credit_available'] = $data['hive_credit_available'] - $data['credit_val'];
				
				$session_bid_data = $this->session->userdata('bid_details');
				//echo '<pre>';print_r($session_bid_data);
				if(array_key_exists($credits['auction_id'], $session_bid_data['bid_hive'])){
					$session_bid_data['bid_hive'][$credits['auction_id']] = $session_bid_data['bid_hive'][$credits['auction_id']]+$this->input->post('credit_val');
				}else{
					$session_bid_data['bid_hive'][$credits['auction_id']] = $this->input->post('credit_val');
				}
				$this->session->set_userdata('bid_details',$session_bid_data);
			}elseif($data['credit_val'] >= $data['bonus_credit_available']){
				$hive_credits_no = $data['credit_val'] - $data['bonus_credit_available'];
				$credits=array(
					'user_id'=>$this->session->userdata('user_id'),
					'credits'=>$data['bonus_credit_available'],
					'auction_id' => $this->input->post('auction_id'),
					'credits_price'=>number_format(($data['bonus_credit_available']*0.75), 2, '.', ''),
					'create_date'=>date('Y-m-d H:i:s'),
					'action' => 'Bid'
					);
				$credit_type = 'bonus_credits';
				$credits_id = $this->model_manage_credits->insert_credits($credits,$credit_type);
				
				$session_bid_data = $this->session->userdata('bid_details');
				//echo '<pre>';print_r($session_bid_data);
				if(array_key_exists($credits['auction_id'], $session_bid_data['bid_bonus'])){
					$session_bid_data['bid_bonus'][$credits['auction_id']] = $session_bid_data['bid_bonus'][$credits['auction_id']]+$data['bonus_credit_available'];
				}else{
					$session_bid_data['bid_bonus'][$credits['auction_id']] = $data['bonus_credit_available'];
				}
				$this->session->set_userdata('bid_details',$session_bid_data);
				$credits=array(
					'user_id'=>$this->session->userdata('user_id'),
					'credits'=>$hive_credits_no,
					'auction_id' => $this->input->post('auction_id'),
					'credits_price'=>number_format(($hive_credits_no*0.75), 2, '.', ''),
					'create_date'=>date('Y-m-d H:i:s'),
					'action' => 'Bid'
					);
				$credit_type = 'HIVECombs';
				$credits_id = $this->model_manage_credits->insert_credits($credits,$credit_type);
				$return_data['total_credit_available'] = $data['total_credit_available'] - $data['credit_val'];
				$return_data['bonus_credit_available'] = '0';
				$return_data['hive_credit_available'] = $data['hive_credit_available'] - ($data['credit_val'] - $data['bonus_credit_available']);
				$session_bid_data = $this->session->userdata('bid_details');
				//echo '<pre>';print_r($session_bid_data);
				if(array_key_exists($credits['auction_id'], $session_bid_data['bid_hive'])){
					$session_bid_data['bid_hive'][$credits['auction_id']] = $session_bid_data['bid_hive'][$credits['auction_id']]+$hive_credits_no;
				}else{
					$session_bid_data['bid_hive'][$credits['auction_id']] = $hive_credits_no;
				}
				$this->session->set_userdata('bid_details',$session_bid_data);
			}else{
				$credits=array(
					'user_id'=>$this->session->userdata('user_id'),
					'credits'=>$this->input->post('credit_val'),
					'auction_id' => $this->input->post('auction_id'),
					'credits_price'=>number_format(($this->input->post('credit_val')*0.75), 2, '.', ''),
					'create_date'=>date('Y-m-d H:i:s'),
					'action' => 'Bid'
				  );
				$credit_type = 'bonus_credits';
				$credits_id = $this->model_manage_credits->insert_credits($credits,$credit_type);
				$return_data['total_credit_available'] = $data['total_credit_available'] - $data['credit_val'];
				$return_data['bonus_credit_available'] = $data['bonus_credit_available'] - $data['credit_val'];
				$return_data['hive_credit_available'] = $data['hive_credit_available'];
				$session_bid_data = $this->session->userdata('bid_details');
				
				if(array_key_exists($credits['auction_id'], $session_bid_data['bid_bonus'])){
					$session_bid_data['bid_bonus'][$credits['auction_id']] = $session_bid_data['bid_bonus'][$credits['auction_id']]+$this->input->post('credit_val');
				}else{
					$session_bid_data['bid_bonus'][$credits['auction_id']] = $this->input->post('credit_val');
				}
				$this->session->set_userdata('bid_details',$session_bid_data);
			}
			$return_data['auction_bid_no'] = $this->model_list_auction->bid_details($credits['auction_id']);
			
			
			$user_bid_data = $this->session->userdata('bid_details');
			
			if(array_key_exists($credits['auction_id'], $user_bid_data['bid_bonus'])){
				$bid_bonus = $user_bid_data['bid_bonus'][$credits['auction_id']];
			}else{ $bid_bonus = 0;}
			if(array_key_exists($credits['auction_id'], $user_bid_data['bid_hive'])){
				$bid_hive = $user_bid_data['bid_hive'][$credits['auction_id']];
			}else{ $bid_hive = 0;}
				$return_data['your_contribution_bid'] = $bid_bonus + $bid_hive;
				$return_data['winner'] = get_current_winner($credits['auction_id']);
			echo json_encode($return_data);
		}else{
			$return_data['logout'] = 'logout';
			echo json_encode($return_data);
		}
	}
	public function check_bids(){
		$aid = $this->input->post('aid');
		$bid_result = array();
		$bid_result['bid'] = $this->model_list_auction->bid_details($aid);
		
		$user_bid_details = $this->session->userdata('bid_details');
		if(isset($user_bid_details['bid_hive'][$aid])){
			$user_bid_hive = $user_bid_details['bid_hive'][$aid];
		}else{
			$user_bid_hive = 0;
		}
		if(isset($user_bid_details['bid_bonus'][$aid])){
			$user_bid_bonus = $user_bid_details['bid_bonus'][$aid];
		}else{
			$user_bid_bonus =0;
		}
		$bid_result['your_contribution'] = $user_bid_hive+$user_bid_bonus;
		$bid_result['winner'] = get_current_winner($aid);
		//die();
		//$bid_result['winner'] =  get_user_username($this->model_list_auction->current_winner($aid));
		$bid_result['winner111'] = $this->model_list_auction->check_close_auction($aid);
		//echo '<pre>';print_r($bid_result);die();
		echo json_encode($bid_result);
		//echo json_encode(array('success'=>'true','value'=>$this->model_list_auction->bid_details($aid),'value2'=>$this->model_list_auction->current_winner($aid));
	}
	public function redeem_rewards(){
		if($this->session->userdata('user_logged_in')){
			$get_rewards = $this->model_manage_credits->get_rewards($this->session->userdata('user_id'));
			if($get_rewards[0]->reward_amount >= 5){
				$credits_val = floor($get_rewards[0]->reward_amount/$this->config->item('reward_purchase_amount'));
				$mod = $get_rewards[0]->reward_amount-($credits_val*$this->config->item('reward_purchase_amount'));
				$reward['reward_amount'] = $mod;
				$reward['date'] = date('Y-m-d H:i:s');
				$this->model_manage_credits->redeem_rewards($reward);
				$credit['credits'] = $credits_val;
				$credit['credits_price'] = $credits_val*$this->config->item('credit_val');
				$credit['user_id'] = $this->session->userdata('user_id');
				$credit['create_date'] = date('Y-m-d H:i:s');
				$credit['action'] = 'Convert';
				$this->model_manage_credits->insert_rewards_bonus_credit($credit);
				redirect('users/manage_credits');
			}
		}else{
			redirect('');
		}	
	}
}
