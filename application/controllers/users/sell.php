<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sell extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
	}
	public function index(){
		if($this->session->userdata('user_logged_in')){
			$this->load->view('user/list_auction');
		}else{
			redirect('');
		}
	}
	public function auction_list_add(){
	
		//echo '<pre>';print_r($_POST);die();
		$auction_list_data['title'] = fSecureInput($this->input->post('title'));
		$auction_list_data['tags'] = fSecureInput(implode(" ",$this->input->post('tag_hidden')));
		$auction_list_data['item_condition'] = $this->input->post('item_condition');
		$auction_list_data['qty'] = $this->input->post('qty');
		
		$auction_list_data['description'] = fSecureInput($this->input->post('description'));
		$item_value = explode(" ",$this->input->post('item_value'));
		$auction_list_data['item_value'] = $item_value[1];
		$auction_list_data['auction_endtime'] = $this->input->post('auction_endtime');
		$auction_list_data['min_bids'] = $this->input->post('min_bids');
		$auction_list_data['min_profit'] = $this->input->post('min_profit');
		$auction_list_data['for_bidders'] = $this->input->post('for_bidders');
		$auction_list_data['remaining_qty'] = $this->input->post('for_bidders');
		$auction_list_data['ww_paytype'] = $this->input->post('ww_paytype');
		$auction_list_data['offer_pickup'] = $this->input->post('offer_pickup');
		$auction_list_data['paypal_account'] = $this->input->post('paypal_account');
		$auction_type = $this->input->post('auction_type');
		
		if($auction_type[0] == 'Public'){
			$auction_list_data['auction_type'] = 'Public';
			$auction_list_data['passcode'] = '';
		}else{
			$auction_list_data['auction_type'] = 'Private';
			$auction_list_data['passcode'] = fSecureInput($this->input->post('passcode'));
		}
		$starting_with = $this->input->post('starting_with');
		//echo '<pre>';print_r($_POST);die();
		if($starting_with[0] == 'now'){
			$auction_list_data['starting_with'] = 'now';
			$auction_list_data['schedule_time'] = to_db_date();
		}else{
			$auction_list_data['starting_with'] = 'later';
			$starting_with_date = $this->input->post('starting_with_date');
			$starting_with_time = $this->input->post('starting_with_time');
			$with_time = substr($starting_with_time, 0, 5);
			$meridiem = substr($starting_with_time, -2);
			if($meridiem == 'AM'){
				$list = explode(":",$with_time);
				if($list[0] == '12'){
					$h = '00';
					$m = $list[1];
					$s = '00';
				}else{
					$h = $list[0];
					$m = $list[1];
					$s = '00';
				}
			}else{
				$list = explode(":",$with_time);
				if($list[0] == '12'){
					$h = $list[0];
				}else{
					$h = 12+$list[0];
				}
				$m = $list[1];
				$s = '00';
			}
			$time = $h.':'.$m.':'.$s;
			//$starting_with_time = $this->input->post('starting_with_time');			
			//$auction_list_data['schedule_time'] = $starting_with_date.' '.$time;
			//$new_schedule = strtotime($starting_with_date.' '.$time);
			//$new_schedule_server_time = $new_schedule-(60*330);
			$auction_list_data['schedule_time'] = to_db_date(strtotime($starting_with_date.' '.$time));
		}
		$auction_list_data['user_id'] = $this->session->userdata('user_id');
		$auction_list_data['create_date'] = to_db_date();
		$auction_list_data['modify_date'] = to_db_date();
		$auction_list_data['modify_by'] = $this->session->userdata('user_id');
		
		//echo "<pre>";print_r($auction_list_data);die();
		$auction_list_data['HMP_fee'] = user_current_hivemp_fee($this->session->userdata('user_id'));
		
		
		if($this->input->post('offer_shipping_delivery_method')){
			if($this->input->post('us_company_name') || $this->input->post('ww_company_name')){
				$auction_delivery_list_data['offer_shipping'] = '1';
				if($this->input->post('us_company_name')){
					$auction_delivery_list_data['us_company_name'] = $this->input->post('us_company_name');
				}else{
					$auction_delivery_list_data['us_company_name'] = '';
				}
				if($this->input->post('us_handling_time')){
					$auction_delivery_list_data['us_handling_time'] = $this->input->post('us_handling_time');
				}else{
					$auction_delivery_list_data['us_handling_time'] = '';
				}
				if($this->input->post('us_price')){
					$auction_delivery_list_data['us_price'] = $this->input->post('us_price');
				}else{
					$auction_delivery_list_data['us_price'] = '';
				}
				if($this->input->post('us_paytype')){
					$auction_delivery_list_data['us_paytype'] = $this->input->post('us_paytype');
				}else{
					$auction_delivery_list_data['us_paytype'] = '';
				}
				if($this->input->post('ww_company_name')){
					$auction_delivery_list_data['ww_company_name'] = $this->input->post('ww_company_name');
				}else{
					$auction_delivery_list_data['ww_company_name'] = '';
				}
				if($this->input->post('ww_handling_time')){
					$auction_delivery_list_data['ww_handling_time'] = $this->input->post('ww_handling_time');
				}else{
					$auction_delivery_list_data['ww_handling_time'] = '';
				}
				if($this->input->post('ww_price')){
					$auction_delivery_list_data['ww_price'] = $this->input->post('ww_price');
				}else{
					$auction_delivery_list_data['ww_price'] = '';
				}
				if($this->input->post('ww_paytype')){
					$auction_delivery_list_data['ww_paytype'] = $this->input->post('ww_paytype');
				}else{
					$auction_delivery_list_data['ww_paytype'] = '';
				}
			}else{
				$auction_delivery_list_data['offer_shipping'] = '0';
			}
		}else{
				$auction_delivery_list_data['offer_shipping'] = '0';
			}
		//$auction_delivery_list_data['us_company_name'] = $this->input->post('us_company_name');
		//$auction_delivery_list_data['us_handling_time'] = $this->input->post('us_handling_time');
		//$auction_delivery_list_data['us_price'] = $this->input->post('us_price');
		//$auction_delivery_list_data['us_paytype'] = $this->input->post('us_paytype');
		//$auction_delivery_list_data['ww_company_name'] = $this->input->post('ww_company_name');
		//$auction_delivery_list_data['ww_handling_time'] = $this->input->post('ww_handling_time');
		//$auction_delivery_list_data['ww_price'] = $this->input->post('ww_price');
		//$auction_delivery_list_data['ww_paytype'] = $this->input->post('ww_paytype');
		
		if($this->input->post('offer_pickup') == '1'){
			$auction_delivery_list_data['offer_pickup'] = '1';
		}else{
			$auction_delivery_list_data['offer_pickup'] = '0';
		}
		if($this->input->post('payat_pickup') == '1'){
			$auction_delivery_list_data['payat_pickup'] = '1';
		}else{
			$auction_delivery_list_data['payat_pickup'] = '0';
		}
		//echo '<pre>';print_r($auction_delivery_list_data);die();
		$list_aucton_id = $this->model_list_auction->auction_list_add($auction_list_data);
		$auction_delivery_id = $this->model_list_auction->auction_delivery_list_add($list_aucton_id,$auction_delivery_list_data);
		
		//echo '<pre>';print_r($_FILES);die();
		
		//if(isset($_FILES)) {
//		  /* Create the config for upload library */
//		  /* (pretty self-explanatory) */
//		  $config['upload_path'] = './upload/'; /* NB! create this dir! */
//		  $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
//		  $config['max_size']  = '0';
//		  $config['max_width']  = '0';
//		  $config['max_height']  = '0';
//		  /* Load the upload library */
//		  $this->load->library('upload', $config);
//	 
//		  /* Create the config for image library */
//		  /* (pretty self-explanatory) */
//		  $configThumb = array();
//		  $configThumb['image_library'] = 'gd2';
//		  $configThumb['source_image'] = '';
//		  $configThumb['create_thumb'] = TRUE;
//		  $configThumb['maintain_ratio'] = TRUE;
//		  /* Set the height and width or thumbs */
//		  /* Do not worry - CI is pretty smart in resizing */
//		  /* It will create the largest thumb that can fit in those dimensions */
//		  /* Thumbs will be saved in same upload dir but with a _thumb suffix */
//		  /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
//		  $configThumb['width'] = 140;
//		  $configThumb['height'] = 210;
//		  /* Load the image library */
//		  $this->load->library('image_lib');
//	 
//		  /* We have 5 files to upload
//		   * If you want more - change the 6 below as needed
//		   */
//		   //echo '<pre>';print_r($_FILES);die();
//		  for($i = 1; $i < 6; $i++) {
//			/* Handle the file upload */
//			$upload = $this->upload->do_upload('image'.$i);
//			/* File failed to upload - continue */
//			if($upload === FALSE) continue;
//			/* Get the data about the file */
//			$data = $this->upload->data();
//	 
//			$uploadedFiles[$i] = $data;
//			
//			/* If the file is an image - create a thumbnail */
//			if($data['is_image'] == 1) {
//			  $configThumb['source_image'] = $data['full_path'];
//			  $this->image_lib->initialize($configThumb);
//			  $this->image_lib->resize();
//			}
//		  }
//		  $uploadedFiles_id = $this->model_list_auction->auction_image_add($list_aucton_id, $uploadedFiles);
//		}

		$this->load->library('upload');  // NOTE: always load the library outside the loop
		$this->load->library('image_lib');
		$this->total_count_of_files = count($_FILES['filename']['name']);
		/*Because here we are adding the "$_FILES['userfile']['name']" which increases the count, and for next loop it raises an exception, And also If we have different types of fileuploads */
		for($i=0; $i<$this->total_count_of_files; $i++)
		{
			$_FILES['userfile']['name']    = $_FILES['filename']['name'][$i];
			$_FILES['userfile']['type']    = $_FILES['filename']['type'][$i];
			$_FILES['userfile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
			$_FILES['userfile']['error']       = $_FILES['filename']['error'][$i];
			$_FILES['userfile']['size']    = $_FILES['filename']['size'][$i];
			
			$config['file_name']     = $list_aucton_id.'_'.$_FILES['userfile']['name'];
			$config['upload_path']   = './upload/';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			
			$configThumb = array();
			$configThumb['image_library'] = 'gd2';
			$configThumb['source_image'] = '';
			$configThumb['create_thumb'] = TRUE;
			$configThumb['maintain_ratio'] = TRUE;
			/* Set the height and width or thumbs */
			/* Do not worry - CI is pretty smart in resizing */
			/* It will create the largest thumb that can fit in those dimensions */
			/* Thumbs will be saved in same upload dir but with a _thumb suffix */
			/* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
			$configThumb['width'] = 140;
			$configThumb['height'] = 210;
			$this->upload->initialize($config);
			$error = 0;
			if($this->upload->do_upload())
			{
				$error += 0;
				$data= $this->upload->data();
				$uploadedFiles[$i] = $data;
				
				$configThumb['source_image'] = $data['full_path'];
				$this->image_lib->initialize($configThumb);
				$this->image_lib->resize();
			
			}else{
				$error += 1;
			}
		}
		$uploadedFiles_id = $this->model_list_auction->auction_image_add($list_aucton_id, $uploadedFiles);
		redirect('users/activity');
	}
	public function details($list_aucton_id){
		if($this->session->userdata('user_logged_in')){
			$data['auction_details'] = $this->model_list_auction->get_auction_details($list_aucton_id);
			//echo '<pre>';print_r($data['auction_details']);die();
			$this->load->view('user/itempage',$data);
		}else{
			redirect('');
		}
	}
	public function edit($id){
		if($this->session->userdata('user_logged_in')){
			$data['auction_details'] = $this->model_list_auction->get_auction_details($id);
			//echo '<pre>';print_r($data['auction_details']);die();
			$this->load->view('user/list_auction_edit',$data);
		}else{
			redirect('');
		}
	}
	
	
	
	
	public function auction_list_edit(){
		if($this->session->userdata('user_logged_in')){
		//echo '<pre>';print_r($_POST);print_r($_FILES);die();
		$auction_list_data['id'] = $this->input->post('id');
		$auction_list_data['title'] = fSecureInput($this->input->post('title'));
		$auction_list_data['tags'] = fSecureInput(implode(" ",$this->input->post('tag_hidden')));
		$auction_list_data['item_condition'] = $this->input->post('item_condition');
		$auction_list_data['qty'] = $this->input->post('qty');
		
		$auction_list_data['description'] = fSecureInput($this->input->post('description'));
		$item_value = explode(" ",$this->input->post('item_value'));
		$auction_list_data['item_value'] = $item_value[1];
		$auction_list_data['auction_endtime'] = $this->input->post('auction_endtime');
		$auction_list_data['min_bids'] = $this->input->post('min_bids');
		$auction_list_data['min_profit'] = $this->input->post('min_profit');
		$auction_list_data['for_bidders'] = $this->input->post('for_bidders');
		$auction_list_data['remaining_qty'] = $this->input->post('for_bidders');
		$auction_list_data['ww_paytype'] = $this->input->post('ww_paytype');
		$auction_list_data['offer_pickup'] = $this->input->post('offer_pickup');
		$auction_list_data['paypal_account'] = $this->input->post('paypal_account');
		$auction_type = $this->input->post('auction_type');
		
		if($auction_type[0] == 'Public'){
			$auction_list_data['auction_type'] = 'Public';
			$auction_list_data['passcode'] = '';
		}else{
			$auction_list_data['auction_type'] = 'Private';
			$auction_list_data['passcode'] = fSecureInput($this->input->post('passcode'));
		}
		$starting_with = $this->input->post('starting_with');
		if($starting_with[0] == 'now'){
			$auction_list_data['starting_with'] = 'now';
			$auction_list_data['schedule_time'] = to_db_date();
		}else{
			$auction_list_data['starting_with'] = 'later';
			$starting_with_date = $this->input->post('starting_with_date');
			$starting_with_time = $this->input->post('starting_with_time');
			
			$with_time = substr($starting_with_time, 0, 5);
			$meridiem = substr($starting_with_time, -2);
			if($meridiem == 'AM'){
				$list = explode(":",$with_time);
				if($list[0] == '12'){
					$h = '00';
					$m = $list[1];
					$s = '00';
				}else{
					$h = $list[0];
					$m = $list[1];
					$s = '00';
				}
			}else{
				$list = explode(":",$with_time);
				if($list[0] == '12'){
					$h = $list[0];
				}else{
					$h = 12+$list[0];
				}
				$m = $list[1];
				$s = '00';
			}
			$time = $h.':'.$m.':'.$s;
			//$starting_with_time = $this->input->post('starting_with_time');			
			//$auction_list_data['schedule_time'] = $starting_with_date.' '.$time;
			//$new_schedule = strtotime($starting_with_date.' '.$time);
			//$new_schedule_server_time = $new_schedule-(60*330);
			$auction_list_data['schedule_time'] = to_db_date(strtotime($starting_with_date.' '.$time));

		}
		
		$auction_list_data['user_id'] = $this->session->userdata('user_id');
		$auction_list_data['create_date'] = to_db_date();
		$auction_list_data['modify_date'] = to_db_date();
		$auction_list_data['modify_by'] = $this->session->userdata('user_id');
		$auction_list_data['HMP_fee'] = user_current_hivemp_fee($this->session->userdata('user_id'));
		//echo '<pre>';print_r($auction_list_data);die();
		
		
		$auction_delivery_list_data['us_company_name'] = $this->input->post('us_company_name');
		$auction_delivery_list_data['us_handling_time'] = $this->input->post('us_handling_time');
		$auction_delivery_list_data['us_price'] = $this->input->post('us_price');
		$auction_delivery_list_data['us_paytype'] = $this->input->post('us_paytype');
		$auction_delivery_list_data['ww_company_name'] = $this->input->post('ww_company_name');
		$auction_delivery_list_data['ww_handling_time'] = $this->input->post('ww_handling_time');
		$auction_delivery_list_data['ww_price'] = $this->input->post('ww_price');
		$auction_delivery_list_data['ww_paytype'] = $this->input->post('ww_paytype');
		if($this->input->post('offer_pickup') == '1'){
			$auction_delivery_list_data['offer_pickup'] = '1';
		}else{
			$auction_delivery_list_data['offer_pickup'] = '0';
		}
		if($this->input->post('payat_pickup') == '1'){
			$auction_delivery_list_data['payat_pickup'] = '1';
		}else{
			$auction_delivery_list_data['payat_pickup'] = '0';
		}
		
		$list_aucton_id = $this->model_list_auction->auction_list_edit($auction_list_data);
		$auction_delivery_id = $this->model_list_auction->auction_delivery_list_edit($list_aucton_id,$auction_delivery_list_data);
		

		$this->load->library('upload');  // NOTE: always load the library outside the loop
		$this->load->library('image_lib');
		$this->total_count_of_files = count($_FILES['filename']['name']);
		/*Because here we are adding the "$_FILES['userfile']['name']" which increases the count, and for next loop it raises an exception, And also If we have different types of fileuploads */
		for($i=0; $i<$this->total_count_of_files; $i++)
		{
			$_FILES['userfile']['name']    = $_FILES['filename']['name'][$i];
			$_FILES['userfile']['type']    = $_FILES['filename']['type'][$i];
			$_FILES['userfile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
			$_FILES['userfile']['error']       = $_FILES['filename']['error'][$i];
			$_FILES['userfile']['size']    = $_FILES['filename']['size'][$i];
			
			$config['file_name']     = $list_aucton_id.'_'.$_FILES['userfile']['name'];
			$config['upload_path']   = './upload/';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			
			$configThumb = array();
			$configThumb['image_library'] = 'gd2';
			$configThumb['source_image'] = '';
			$configThumb['create_thumb'] = TRUE;
			$configThumb['maintain_ratio'] = TRUE;
			/* Set the height and width or thumbs */
			/* Do not worry - CI is pretty smart in resizing */
			/* It will create the largest thumb that can fit in those dimensions */
			/* Thumbs will be saved in same upload dir but with a _thumb suffix */
			/* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
			$configThumb['width'] = 140;
			$configThumb['height'] = 210;
			$this->upload->initialize($config);
			$error = 0;
			if($this->upload->do_upload())
			{
				$error += 0;
				$data= $this->upload->data();
				$uploadedFiles[$i] = $data;
				
				$configThumb['source_image'] = $data['full_path'];
				$this->image_lib->initialize($configThumb);
				$this->image_lib->resize();
			
			}else{
				$error += 1;
			}
		}
		$uploadedFiles_id = $this->model_list_auction->auction_image_edit($list_aucton_id, $uploadedFiles);
		redirect('users/activity');
		}else{
			redirect('');
		}
	}
	public function seller_payment_products(){
		$order_details = $this->model_manage_credits->seller_payment_products();
		//echo '<pre>';print_r($order_details);
	
	}	
	
}
