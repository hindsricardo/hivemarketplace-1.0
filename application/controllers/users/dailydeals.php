<?php
//Function to send daily deals until unless user unsubscribes. Run as Cron Job on the server. 
class Dailydeals extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model("model_dailydeals");
                $this->load->model("model_list_auction");
    }
	
	public function index(){
	
	}
         public function dealsemail(){
	 $this->load->library('email');
	 $config['mailtype'] = 'html';
         $mesg = $this->load->view('user/dailydeals',true);
	 $this->email->initialize($config);
	 $this->email->from('deals@hivemarketplace.com', 'HIVE Marketplace Deals');
	 $this->email->to($this->model_dailydeals->get_dailydeals_subscribed());
	
	 $this->email->subject('HIVE MARKETPLACE Account Activation');
	 $this->email->message($mesg);
	
	 $this->email->send();
	 //$this->email->print_debugger();
 }
}
?>
