<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buy_credit extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_paypal');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
	}

	function index()
	{
	//print_r($this->session->userdata);die();
		if($this->session->userdata('user_logged_in'))
		{
			//$this->load->view('adminnav/nav_header');
			//$this->load->view('adminnav/nav_left');
			$this->load->view('user/buy_credit');
		}
		else
		{
			redirect('');
		}
	}
	public function paypal() {
	$cart = $this->cart->contents();
	  
	//echo base_url();die();
		$this->load->library('paypal_class');
		$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
		//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';	 // paypal url
		//$this->paypal_class->add_field('cmd', '_xclick');
		$this->paypal_class->multi_items('true');
		//$this->paypal_class->add_field('business', 'duttasujata8@gmail.com');
		$this->paypal_class->add_field('business', 'subhra.beas-facilitator@gmail.com');
		$this->paypal_class->add_field('currency_code', 'USD');
		//$this->paypal_class->add_field('business', $this->config->item('bussinessPayPalAccount'));
		$this->paypal_class->add_field('return', base_url().'users/buy_credit/success'); // return url
		$this->paypal_class->add_field('cancel_return', base_url().'users/buy_credit/cancel'); // cancel url
		$this->paypal_class->add_field('notify_url', base_url().'users/buy_credit/validatePaypal'); // notify url
		if ($cart = $this->cart->contents()):
		//echo '<pre>';print_r($cart);die();
		$i=1; $grand_total=0; $shipping_total=0;
		foreach ($cart as $item):
		
		$item_price=$item['price']/$item['qty'];
		if(isset($item['buy_type']) && $item['buy_type'] == 'direct_buy'){
			$buy_type = 'direct_buy';
		}else{
			$buy_type = '';
		}

			if (array_key_exists('delivery_type', $item) && $item['delivery_type'] == 'pick') {
				$order_details['user_id'] = $this->session->userdata('user_id');
				$order_details['payer_id'] = '';
				$order_details['txn'] = '';
				$order_details['amount'] = $item['price'];
				$order_details['payment_type'] = 'pay_at_pickup';
				$order_details['payment_status'] = 'Pending';
				$order_details['order_status'] = 'Pending';
				$order_details['date'] = to_db_date();
				$order_id=$this->model_paypal->insert_orders_pickup($order_details);
				
				$product_id = $item['id'];
				$product_name = fSecureInput($item['name']);
				$qty = $item['qty'];
				$price = $item['price'];
				$shipping = '0';
				$shipping_or_pickup = 'pickup';
				$scheduled_date = strtotime("+".$this->config->item('pickup_scheduled_date')." day", strtotime(to_db_date()));
				$payment_scheduled_date = date('Y-m-d H:i:s', $scheduled_date);
				if($item['buy_type'] == 'direct_buy'){
					$buy_type = 'direct_buy';
				}else{
					$buy_type = '';
				}
				$this->model_paypal->order_details($order_id,$product_id,$product_name,$qty,$price,$shipping, $shipping_or_pickup, $buy_type, $payment_scheduled_date);
				$this->model_list_auction->update_checkout_direct_payment($product_id, $qty);
				
			}elseif (array_key_exists('delivery_type', $item) && $item['delivery_type'] == 'ship') {
				$this->paypal_class->add_field('item_name_'.$i,$item['name']);
				$this->paypal_class->add_field('item_number_'.$i,$item['id']);
				$this->paypal_class->add_field('amount_'.$i, number_format($item_price,2));
				$this->paypal_class->add_field('quantity_'.$i, $item['qty']);
				$this->paypal_class->add_field('shipping_'.$i, $item['shipping']);
				$this->paypal_class->add_field('custom_'.$i, $buy_type);
				$grand_total = $grand_total + number_format($item['price'],2); 
				$shipping_total = $shipping_total + number_format($item['shipping'],2);
				$i++;
			}elseif(!array_key_exists('delivery_type', $item) && $item['name'] == 'buy credit'){
				$this->paypal_class->add_field('item_name_'.$i,$item['name']);
				$this->paypal_class->add_field('item_number_'.$i,$item['id']);
				$this->paypal_class->add_field('amount_'.$i, $item['price']);
				$this->paypal_class->add_field('quantity_'.$i, 1);
				$this->paypal_class->add_field('shipping_'.$i, '0');
				$this->paypal_class->add_field('custom_'.$i, $buy_type);
				$grand_total = $grand_total + number_format($item['price'],2); 
				$shipping_total = $shipping_total + number_format($item['shipping'],2);
				$i++;
			}
		endforeach;
		endif;
		
		$totalPrice = number_format($grand_total,2)+number_format($shipping_total,2);
		//die();
		//$this->paypal_class->add_field('item_name', 'Buy Credits');
		//$this->paypal_class->add_field('shipping', $totalshiping);
		$this->paypal_class->add_field('amount', $totalPrice);
		//$this->paypal_class->add_field('custom', $this->session->userdata('user_id').'~'.$this->input->get('amount'));
		if($totalPrice > 0){
			$this->paypal_class->submit_paypal_post(); // submit the fields to paypal
		}else{
			$this->load->model('cart_model');
			$this->cart->destroy();
			redirect('checkout');
		}
		
		//$p->dump_fields();	  // for debugging, output a table of all the fields
		//exit;
	}

	public function paypal_single_item($row_id) {
	 $cart = $this->cart->contents();
	//echo '<pre>';print_r($cart);die();
		$this->load->library('paypal_class');
		$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
		//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';	 // paypal url
		//$this->paypal_class->add_field('cmd', '_xclick');
		$this->paypal_class->multi_items('true');
		//$this->paypal_class->add_field('business', 'duttasujata8@gmail.com');
		$this->paypal_class->add_field('business', 'subhra.beas-facilitator@gmail.com');
		$this->paypal_class->add_field('currency_code', 'USD');
		//$this->paypal_class->add_field('business', $this->config->item('bussinessPayPalAccount'));
		$this->paypal_class->add_field('return', base_url().'users/buy_credit/success'); // return url
		$this->paypal_class->add_field('cancel_return', base_url().'users/buy_credit/cancel'); // cancel url
		$this->paypal_class->add_field('notify_url', base_url().'users/buy_credit/validatePaypal'); // notify url
		
		if ($cart = $this->cart->contents()):
			$item = $cart[$row_id];
			
			$i=1; $grand_total=0; $shipping_total=0;
			//foreach ($cart as $item):
			
			$item_price=$item['price']/$item['qty'];
			if(isset($item['buy_type']) && $item['buy_type'] == 'direct_buy'){
				$buy_type = 'direct_buy';
			}else{
				$buy_type = '';
			}
			if (array_key_exists('delivery_type', $item) && $item['delivery_type'] == 'pick') {
				$order_details['user_id'] = $this->session->userdata('user_id');
				$order_details['payer_id'] = '';
				$order_details['txn'] = '';
				$order_details['amount'] = $item['price'];
				$order_details['payment_type'] = 'pay_at_pickup';
				$order_details['payment_status'] = 'Pending';
				$order_details['order_status'] = 'Pending';
				$order_details['date'] = to_db_date();
				//echo '<pre>';print_r($order_details);die();
				$order_id=$this->model_paypal->insert_orders_pickup($order_details);
				
				$product_id = $item['id'];
				$product_name = fSecureInput($item['name']);
				$qty = $item['qty'];
				$price = $item['price'];
				$shipping = '0';
				$shipping_or_pickup = 'pickup';
				
				$scheduled_date = strtotime("+".$this->config->item('pickup_scheduled_date')." day", strtotime(to_db_date()));
				$payment_scheduled_date = date('Y-m-d H:i:s', $scheduled_date);
				
				$this->model_paypal->order_details($order_id,$product_id,$product_name,$qty,$price,$shipping, $shipping_or_pickup, $buy_type, $payment_scheduled_date);
				$this->model_list_auction->update_checkout_direct_payment($product_id, $qty);
				
				$this->load->model('cart_model');
				$this->cart->destroy();
				redirect('checkout');
			}
			
			$this->paypal_class->add_field('item_name_'.$i,$item['name']);
			$this->paypal_class->add_field('item_number_'.$i,$item['id']);
			$this->paypal_class->add_field('amount_'.$i, number_format($item_price,2));
			$this->paypal_class->add_field('quantity_'.$i, $item['qty']);
			$this->paypal_class->add_field('shipping_'.$i, $item['shipping']);
			$this->paypal_class->add_field('custom', $buy_type);
			$grand_total = $grand_total + number_format($item['price'],2); 
			$shipping_total = $shipping_total + number_format($item['shipping'],2);
			$i++;
			//endforeach;
		endif;		
		$totalPrice = number_format($grand_total,2)+number_format($shipping_total,2);
		
		//$this->paypal_class->add_field('item_name', 'Buy Credits');
		//$this->paypal_class->add_field('shipping', $totalshiping);
		$this->paypal_class->add_field('amount', $totalPrice);
		//$this->paypal_class->add_field('custom', $this->session->userdata('user_id').'~'.$this->input->get('amount'));
		$this->paypal_class->submit_paypal_post(); // submit the fields to paypal
		//$p->dump_fields();	  // for debugging, output a table of all the fields
		//exit;
	}

function refund(){

$this->load->model('model_manage_credits');
$refund_details = $this->model_manage_credits->refund_credits($this->input->post('id'));
//print_r($refund_details);
/** RefundTransaction NVP example; last modified 08MAY23.
 *
 *  Issue a refund for a prior transaction. 
*/

$environment = 'sandbox';     // or 'beta-sandbox' or 'live'

/**
 * Send HTTP POST Request
 *
 * @param     string     The API method name
 * @param     string     The POST Message fields in &name=value pair format
 * @return     array     Parsed HTTP Response body
 */


// Set request-specific fields.
$transactionID = urlencode($refund_details[0]->txn);
if($this->input->post('price')==$refund_details[0]->credits_price){
  $ptype='Full';
  $left_credit=0;
  $left_amt=0;
  $ststus=1;
}else{
  $left_credit=$refund_details[0]->credits - $this->input->post('credit');
  $left_amt=$refund_details[0]->credits_price - $this->input->post('price');
  $ptype='Partial';
  $ststus=2;
}
$refundType = urlencode($ptype);  //Full or 'Partial'
$amount=$this->input->post('price');                          // required if Partial.
$memo='refund my store';                            // required if Partial.
$currencyID = urlencode('USD');   // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

// Add request-specific fields to the request string.
$nvpStr = "&TRANSACTIONID=$transactionID&REFUNDTYPE=$refundType&CURRENCYCODE=$currencyID";

if(isset($memo)) {
     $nvpStr .= "&NOTE=$memo";
}

if(strcasecmp($refundType, 'Partial') == 0) {
     if(!isset($amount)) {
          exit('Partial Refund Amount is not specified.');
     } else {
           $nvpStr = $nvpStr."&AMT=$amount";
     }

     if(!isset($memo)) {
          exit('Partial Refund Memo is not specified.');
     }
}

// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = $this->PPHttpPost('RefundTransaction', $nvpStr);

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
//echo "<pre>";
//print_r($httpParsedResponseAr);
     //exit('Refund Completed Successfully: '.print_r($httpParsedResponseAr, true));
		$data_update = array(
					'status'=>$ststus,
					'left_amt'=>$left_amt,
					'left_credit'=>$left_credit,
					);
		  $this->db->where('txn',$refund_details[0]->txn);
		  $this->db->update('user_buycredits',$data_update); 
		  
	    $data_insert = array(
               'user_id' => $this->session->userdata('user_id'),
               'credits' => $this->input->post('credit'),
               'credits_price' => $this->input->post('price'),
			   'txn' => $refund_details[0]->txn,
			   'create_date' =>to_db_date() ,
			   'action' => 'Withdraw'
            );
       $this->db->insert('user_buycredits', $data_insert);  
	 //echo '<pre>';print_r($httpParsedResponseAr);die();
	 $return_data['L_LONGMESSAGE0'] = $httpParsedResponseAr["ACK"];
	 $return_data['status'] = 'success';
	 echo json_encode($return_data);
	} else  {
	//echo '<pre>';print_r($httpParsedResponseAr);die();
		$return_data['L_LONGMESSAGE0'] = $httpParsedResponseAr["L_LONGMESSAGE0"];
		$return_data['status'] = 'failed';
		echo json_encode($return_data);
		 //exit('RefundTransaction failed: ' . print_r($httpParsedResponseAr, true));
	}

}

public function refund1(){

		$credit = $this->input->post("credit");
		$price = $this->input->post("price");
		//$order_details_id = '13';
		$this->config->load('paypal');
		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
		);
		// Show Errors
		if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}
		
		$this->load->library('paypal/Paypal_pro', $config);	
		//echo '<pre>';print_r($config);die();
				
				$MPFields = array(
									'emailsubject' => 'Withdraw HONEY COMBS from HIVEMarketPlace', 						// The subject line of the email that PayPal sends when the transaction is completed.  Same for all recipients.  255 char max.
									'currencycode' => 'USD', 						// Three-letter currency code.
									'receivertype' => 'EmailAddress' 						// Indicates how you identify the recipients of payments in this call to MassPay.  Must be EmailAddress or UserID
								);
				
				// MassPay accepts multiple payments in a single call.  
				// Therefore, we must create an array of payments to pass into the class.
				// In this sample we're simply passing in 2 separate payments with static amounts.
				// In most cases you'll be looping through records in a data source to generate the $MPItems array below.
				
				$Item1 = array(
							'l_email' => get_user_paypalemail($this->session->userdata('user_id')), 							// Required.  Email address of recipient.  You must specify either L_EMAIL or L_RECEIVERID but you must not mix the two.
							'l_receiverid' => '', 						// Required.  ReceiverID of recipient.  Must specify this or email address, but not both.
							'l_amt' => number_format($price,2), 								// Required.  Payment amount.
							'l_uniqueid' => '', 						// Transaction-specific ID number for tracking in an accounting system.
							'l_note' => '' 								// Custom note for each recipient.
						);
				//$MPItems = array($Item1, $Item2);
				$MPItems = array($Item1);
				$PayPalRequestData = array(
								'MPFields' => $MPFields, 
								'MPItems' => $MPItems
							);
							
				$PayPalResult = $this->paypal_pro->MassPay($PayPalRequestData);
				//echo '<pre>';print_r($PayPalResult);die();
				if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
				{
					$errors = array('Errors'=>$PayPalResult['ERRORS']);
					$return_data['status'] = 'failed';
					echo json_encode($return_data);
					//echo '<pre>';print_r($errors);echo $errors;die();
					//$this->load->view('paypal_error',$errors);
				}
				else
				{
					$withdraw_details['user_id'] = $this->session->userdata('user_id');
					$withdraw_details['auction_id'] = 0;
					
					$withdraw_details['credits'] = $credit;
					$withdraw_details['credits_price'] = $credit*$this->config->item('credit_val');
					$withdraw_details['create_date'] = to_db_date();
					$withdraw_details['action'] = 'Withdraw';
					$withdraw_details['status'] = 0;
					
					$id = $this->model_manage_credits->insert_credits($withdraw_details, 'HIVECombs');
					$return_data['status'] = 'success';
					echo json_encode($return_data);
				}



}
function PPHttpPost($methodName_, $nvpStr_) {

$environment = 'sandbox'; 
    // global $environment;
//echo $environment;
     // Set up your API credentials, PayPal end point, and API version.
     $API_UserName = urlencode('subhra.beas-facilitator_api1.gmail.com');
     $API_Password = urlencode('1369898562');
     $API_Signature = urlencode('A4H3Ve.GNRlZYC3lWSlsY5XtantHAwCu47fum.O8MsaV-eiQkIP.jRsU');
     $API_Endpoint = "https://api-3t.paypal.com/nvp";
     if("sandbox" === $environment || "beta-sandbox" === $environment) {
          $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
     }
     $version = urlencode('51.0');
//echo $API_Endpoint;exit; Balance:999998999 USD
 
     // Set the curl parameters.
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
     curl_setopt($ch, CURLOPT_VERBOSE, 1);

     // Turn off the server and peer verification (TrustManager Concept).
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_POST, 1);

     // Set the API operation, version, and API signature in the request.
     $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

     // Set the request as a POST FIELD for curl.
     curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

     // Get response from the server.
     $httpResponse = curl_exec($ch);
//echo "<pre>";print_r($httpResponse);exit;
     if(!$httpResponse) {
          exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
     }
//echo "<pre>";print_r($httpResponse);exit();
     // Extract the response details.
     $httpResponseAr = explode("&", $httpResponse);

     $httpParsedResponseAr = array();
     foreach ($httpResponseAr as $i => $value) {
          $tmpAr = explode("=", $value);
          if(sizeof($tmpAr) > 1) {
               $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
          }
     }

     if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
          exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
     }

     return $httpParsedResponseAr;
}
public function validatePaypal() {
	$this->load->library('paypal_class');
	$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
	//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';	 // paypal url
	if ($this->paypal_class->validate_ipn()) {
	
	/*$orderId = trim($_POST['custom']);
	$val=explode("~",$orderId);
	$creprice = trim($_POST['payment_gross']);
	$tx = trim($_POST['txn_id']);
	$this->model_paypal->insert_buycredits($val[0],$val[1],$creprice,$tx);*/
	//$this->model_paypal->insert_buycredits();
	}
	break;
}


	/*public function ipn() {
        $this->load->library( 'Paypal' );
        if ( $this->paypal->validate_ipn() ) {
            $pdata = $this->paypal->ipn_data;
            if ($pdata['txn_type'] == "web_accept") {
                if($pdata['payment_status'] == "Completed"){
                    if($pdata['business'] == $this->config->item( 'paypal_email' )) {
                        //handle payment...
                    }
                }
            }
        }
    }*/
 public function success() {
	   if($this->session->userdata('user_logged_in')){
	   $cart = $this->cart->contents();
	   //echo '<pre>';print_r($cart);die();
		//print_r($_POST);die();
				$order_id=$this->model_paypal->insert_orders();
				
				$num=trim($_POST['num_cart_items']);
				  for($i=1;$i<=$num; $i++){
				  	$buy_type = '';
				  	foreach($cart as $cart_val){
						if($cart_val['buy_type'] == 'direct_buy' && $_POST['item_name'.$i] == $cart_val['name']){
							$buy_type = 'direct_buy';
							//continue;
						}
					}
					  //$order_id=$_POST['item_number'.$i];
					  $product_id = $_POST['item_number'.$i];
					  $product_name = fSecureInput($_POST['item_name'.$i]);
					  $qty = $_POST['quantity'.$i];
					  $price = $_POST['mc_gross_'.$i];
					  $shipping = $_POST['mc_shipping'.$i];
					  
					  if($_POST['item_name'.$i]=='buy credit'){
						$shipping_or_pickup = '';
					  }else{
						$shipping_or_pickup = 'shipping';
					  }
					 $this->model_paypal->order_details($order_id,$product_id,$product_name,$qty,$price,$shipping, $shipping_or_pickup, $buy_type);
					  if($_POST['item_name'.$i]=='buy credit'){
						$this->model_paypal->insert_buycredits($price);
					  }else{
						$this->model_list_auction->update_checkout_direct_payment($product_id, $qty);
					  }
				   }
					$this->load->model('cart_model');
					$this->cart->destroy();
					$_POST['num_cart_items'] = 0;
				redirect('checkout');
		}
		else{
	        redirect('');
		 }
    }
	
	public function cancel() {
	if($this->session->userdata('user_logged_in'))
		{
				
			$this->load->view('user/buy_credit_failure');
		}
		else
		{
        	 redirect('');
		}
    }
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */