<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_settings extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('message_model');
		$this->load->model('feedback_model');
		$this->load->model('registration_model');
	}
	public function index(){
	    
		if($this->session->userdata('user_logged_in')){
			//echo '<pre>';print_r($this->session->all_userdata());die();
			$data['user_details'] = $this->model_users->get_user_details($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data);die();
			$data['user_notifications'] = $this->model_users->get_user_notifications($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data);die();
			$data['feedall'] = $this->feedback_model->feedback_details();
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/account_settings',$data);
		}else{
			redirect('');
		}
	}
	public function update(){
			if($this->input->post('zip')){
				$val = $this->registration_model->getLnt($this->input->post('zip'));
				$lat = $val['lat'];
				$lng = $val['lng'];
			}else{
				$lat = '';
				$lng = '';
			}
		  $data=array(
		  			'id'=>$this->input->post('user_id'),
					'username'=>fSecureInput($this->input->post('username')),
					'password'=>fSecureInput($this->input->post('password')),
					'fname'=>fSecureInput($this->input->post('fname')),
					'lname'=>fSecureInput($this->input->post('lname')),
					'email'=>$this->input->post('email'),
					'street_address'=>fSecureInput($this->input->post('street_address')),
					'city_province'=>fSecureInput($this->input->post('city_province')),
					'country'=>fSecureInput($this->input->post('country')),
					'zip'=>fSecureInput($this->input->post('zip')),
					'lat'=>$lat,
					'lon'=>$lng,
					'timezone'=>serialize($this->input->post('timezone')),
				  );
			$user_request_code = $this->model_users->get_user_details($this->session->userdata('user_id'));
			if($user_request_code[0]->update_userinfo_code == $this->input->post('request_code')){
				$result = $this->model_users->update($data);
				$data['user_update_result'] = 'success';
			}else{
				$data['user_update_result'] = 'unsuccess';
			}
			$data['user_details'] = $this->model_users->get_user_details($this->session->userdata('user_id'));
			$data['user_notifications'] = $this->model_users->get_user_notifications($this->session->userdata('user_id'));
			$data['feedall'] = $this->feedback_model->feedback_details();
			$data['unread'] =$this->message_model->message_unread();
			$this->load->view('user/account_settings',$data);
	}
	public function notification_update(){
		$notification['user_id'] = $this->input->post('user_id');
		$notification['field_name'] = $this->input->post('field_name');
		$notification['field_value'] = $this->input->post('field_value');
		$this->model_users->notification_update($notification);
	}
		public function avater_popup(){
		$this->load->view('user/avater_popup');
	}
	public function avater_upload(){
		//echo '<pre>';print_r($_FILES);die();
		$this->edit_avater();
		$data['id'] = $this->session->userdata('user_id');
		$data['photo']= $this->upload->file_name;
		$this->model_users->avater_upload($data);
		echo "<script type=\"text/javascript\">parent.$.modal.close();</script>";
		$data['user_details'] = $this->model_users->get_user_details($this->session->userdata('user_id'));
		$data['user_notifications'] = $this->model_users->get_user_notifications($this->session->userdata('user_id'));
		//echo '<pre>';print_r($data);die();
		$this->load->view('user/account_settings',$data);
	}
	public function edit_avater(){
		//show($_FILES);die();
		if($_FILES['avaterimage']['error'] == 0){
			//upload and update the file
			$config['upload_path'] = './assetts/uploads/'; // Location to save the image
			$config['allowed_types'] = 'gif|jpg|png';
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config); //codeigniter default function
 
			if ( ! $this->upload->do_upload('avaterimage')){
			  //  redirect("user/profile/".$username); 
			  $this->index();
			}
			else{
				//Image Resizing
				$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
				$config['maintain_ratio'] = FALSE;
				$config['width'] = 200; // image re-size  properties
				$config['height'] = 150; // image re-size  properties
 
				$this->load->library('image_lib', $config); //codeigniter default function
 
				if ( ! $this->image_lib->resize()){
				  //  redirect("user/profile/".$username); // redirect  page if the resize fails.
				  $this->index();
				}
			}
		}
	}
	public function userinfo_request_code(){
		$user_id = $this->input->post('user_id');
		
		echo $rand=random_string('alnum', 10);
		$user_id = $this->model_users->updatecode($rand,$user_id);
		
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('info@hivemarketplace.com', 'System Administrator');
		$this->email->to($this->session->userdata('email'));
					
		$this->email->subject('Penny Auction Request Code for Update user info');
		$this->email->message('Dear '.$username.',<br><br> Your Request Code for Hive Marketplace Update user info panel is '.$rand.'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
		
		$this->email->send();
	}
	
	public function update_user_note(){
	  $id= $this->session->userdata('user_id');
	  $data=array(
	   'notes' => fSecureInput($this->input->post('notes'))
	  );
	 $this->db->where('id', $id);
	 $this->db->update('users', $data);
     echo "Your Notes successfully updated";
		
	}
	public function paypalemail_request_code(){
		$user_id = $this->input->post('user_id');
		
		echo $rand=random_string('alnum', 10);
		$user_id = $this->model_users->updatepaypalemailcode($rand,$user_id);
		
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('info@hivemarketplace.com', 'System Administrator');
		$this->email->to($this->session->userdata('email'));
					
		$this->email->subject('Penny Auction Request Code for Update Paypal Email');
		$this->email->message('Dear '.$username.',<br><br> Your Request Code for Hive Marketplace Update Paypal Email panel is '.$rand.'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
		
		$this->email->send();
	}
	public function update_paypalemail(){
		$value['user_id'] = $this->input->post('user_id');
		$value['paypalemail'] = $this->input->post('paypalemail');
		$value['paypal_request_code'] = $this->input->post('paypal_request_code');
		$result =  $this->model_users->update_paypalemail($value);
		echo $result;
	}
	public function unsubscribe(){
		$this->model_users->unsubscribe($this->session->userdata('user_id'));
		redirect('users/account_settings');
	}
	public function subscribe(){
		$this->model_users->subscribe($this->session->userdata('user_id'));
		echo 'You have successfully subscribed Premium Bee and have a steady 5% HMP fee for only $20/Month';
	}
}
