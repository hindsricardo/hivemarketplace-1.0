<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
		
	}
	public function details($list_aucton_id){
		//echo '<pre>';print_r($_SESSION['bid_details']['bid_hive'][35]);die();
		if($this->session->userdata('user_logged_in')){
			$data['auction_details'] = $this->model_list_auction->get_auction_details($list_aucton_id);
			$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			$data['bidding_history'] = $this->model_list_auction->bidding_history($list_aucton_id);
			//echo '<pre>';print_r($data['auction_details']);die();
			$this->load->view('user/itempage',$data);
		}else{
			redirect('');
		}
	}
	public function current_winner(){
		$this->model_list_auction->current_winner(1);
	}
	public function bidding_history(){
	//echo 'hello';
		$auction_id = $this->input->post('auction_id');
		$data['bidding_history'] = $this->model_list_auction->bidding_history($auction_id);
		echo $this->load->view('user/item_bidding_history',$data);
		
	}
}
