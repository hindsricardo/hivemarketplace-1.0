<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Feedback extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('feedback_model');
	}

	public function index()
	{	
		$data['title'] = 'Auction Feedback';
		$data['seller_id'] = $this->input->post('seller_id');
		$data['auction_id'] = $this->input->post('auction_id');
		$this->load->view('feedback', $data);
	}

  public function add()
	{	
		$insert_feedback = array(
			'user_id' => $this->session->userdata('user_id'),
			'seller_id' => $this->input->post('seller_id'),
			'auction_id' => $this->input->post('auction_id'),
			'desc_rate' => $this->input->post('desc_rate'),
			'speed_rate' => $this->input->post('speed_rate'),
			'comm_rate' => $this->input->post('comm_rate'),
			'response' => $this->input->post('response'),
			'feedback' => fSecureInput($this->input->post('feedback')),
			'status' => '1',
			'date' => to_db_date()
		);	
		
				
        if($this->feedback_model->chk_feedback($this->input->post('auction_id')) <= 0 ){
		   $this->feedback_model->insert_feedback($insert_feedback);
		}
		redirect('feedback/profile');
	}
	public function profile()
	{	
	    $data['title'] = 'Feedback Profile';
		$data['feedall'] = $this->feedback_model->feedback_details();
		$this->load->view('feedback-profile', $data);
	}
	public function rating()
	{	
		echo $this->feedback_model->avg_rating($this->input->post('userid'))."*".$this->feedback_model->total_rating($this->input->post('userid'))."*".$this->feedback_model->user_notes($this->input->post('userid'));
		
	}
}