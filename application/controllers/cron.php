<?php
class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model("model_cron");
		$this->load->model('model_paypal');
		$this->load->model('model_manage_credits');
    }
	
	public function index(){
	
	}
	
	public function update_order_received(){
	 	$order_id = $this->model_cron->get_order_received();
		
		if(!empty($order_id)){
			$this->config->load('paypal');
			$config = array(
				'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
				'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
				'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
				'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
				'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
				'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			);
			if($config['Sandbox'])
			{
				error_reporting(E_ALL);
				ini_set('display_errors', '1');
			}
			
			$this->load->library('paypal/Paypal_pro', $config);	
			//echo '<pre>';print_r($order_id);die();
			foreach($order_id as $order_id_val){
			
				$product_details = $this->model_paypal->get_seller_item_details($order_id_val['product_id']);
				$product_price = $order_id_val['price'];
				$price = $product_price*(1-$product_details[0]['HMP_fee']/100);
				$hmp_fee_amount = ($product_price*$product_details[0]['HMP_fee'])/100;
				
				// Show Errors
				
				//echo '<pre>';print_r($config);die();
				
				$MPFields = array(
									'emailsubject' => 'Buyer has been received the item '.$order_id_val['product_name'].' from HIVEMarketPlace', 						// The subject line of the email that PayPal sends when the transaction is completed.  Same for all recipients.  255 char max.
									'currencycode' => 'USD', 						// Three-letter currency code.
									'receivertype' => 'EmailAddress' 						// Indicates how you identify the recipients of payments in this call to MassPay.  Must be EmailAddress or UserID
								);
				
				// MassPay accepts multiple payments in a single call.  
				// Therefore, we must create an array of payments to pass into the class.
				// In this sample we're simply passing in 2 separate payments with static amounts.
				// In most cases you'll be looping through records in a data source to generate the $MPItems array below.
				
				$Item1 = array(
							'l_email' => $product_details[0]['paypal_account'], 							// Required.  Email address of recipient.  You must specify either L_EMAIL or L_RECEIVERID but you must not mix the two.
							'l_receiverid' => '', 						// Required.  ReceiverID of recipient.  Must specify this or email address, but not both.
							'l_amt' => number_format($price,2), 								// Required.  Payment amount.
							'l_uniqueid' => '', 						// Transaction-specific ID number for tracking in an accounting system.
							'l_note' => '' 								// Custom note for each recipient.
						);
				//$MPItems = array($Item1, $Item2);
				$MPItems = array($Item1);
				//echo '<pre>';print_r($Item1);die();
				$PayPalRequestData = array(
								'MPFields' => $MPFields, 
								'MPItems' => $MPItems
							);
							
				$PayPalResult = $this->paypal_pro->MassPay($PayPalRequestData);
				//echo '<pre>';print_r($PayPalResult);die();
				if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
				{
					//echo '<pre>';print_r($errors);echo $errors;
					//$this->load->view('paypal_error',$errors);
				}
				else
				{
					$order_details_data['order_status'] = 'Received';
					$order_details_data['seller_payment_amount'] = $price;
					$order_details_data['hmp_fee_amount'] = $hmp_fee_amount;
					$order_details_data['CorrelationId'] = $PayPalResult['CORRELATIONID'];
					$order_details_data['seller_payment_time'] = $PayPalResult['TIMESTAMP'];
					$order_details_data['seller_payment'] = '1';
					
					$update_order_result = $this->model_paypal->update_order_received($order_id_val['id'], $order_details_data);
					// Successful call.  Load view or whatever you need to do here.	
				}
			}
		}
	}	
	
	public function winnauction(){
		$data = $this->model_cron->winnauction();
}
}

?>