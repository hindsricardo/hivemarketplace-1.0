<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cases extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cases_model');
		$this->load->model('model_list_auction');
	}

	public function index()
	{	
		$data['title'] = 'Auction Case';
		$data['seller_id'] = $this->input->post('seller_id');
		$data['auction_id'] = $this->input->post('auction_id');
		$this->load->view('cases', $data);
	}

  public function add()
	{
		$this->load->model('message_model');	
		$insert_case = array(
			'user_id' => $this->session->userdata('user_id'),
			'seller_id' => $this->input->post('seller_id'),
			'auction_id' => $this->input->post('auction_id'),
			'purpose' => $this->input->post('purpose'),
			'desc' => fSecureInput($this->input->post('desc')),
			'status' => 'open',
			'case_date' => to_db_date(),
			'update_date' => ''
		);	
		
				
        if($this->cases_model->chk_case($this->input->post('auction_id')) <= 0 ){

		  $case_id = $this->cases_model->insert_case($insert_case);
		  $subject = 'case: '.$case_id;
		  
		  $msg_to = get_user_username($this->input->post('seller_id'));
		  $from_name = get_user_username($this->session->userdata('user_id'));
		  
		  $insert_msg = array(
			'sender_id' => $this->session->userdata('user_id'),
			'recever_id' => $this->input->post('seller_id'),
			'recever_admin' => '0',
			'parent_id' => $this->input->post('auction_id'),
			'msg_to' => $msg_to,
			'msg_from' => $from_name,
			'subject' => $subject,
			'msg' => fSecureInput($this->input->post('desc')),
			'type' => 'case',
			'status' => 0,
			'date' => to_db_date()		
		  );	
		  
		  $insert_admin = array(
			'sender_id' => $this->session->userdata('user_id'),
			'recever_id' => '0',
			'recever_admin' => '1',
			'parent_id' => $this->input->post('auction_id'),
			'msg_to' => $msg_to,
			'msg_from' => 'Support',
			'subject' => $subject,
			'msg' => fSecureInput($this->input->post('desc')),
			'type' => 'case',
			'status' => 0,
			'date' => to_db_date()		
		  );
		$msg_id=$this->message_model->insert_mail($insert_msg);	
		$msg_id=$this->message_model->insert_mail($insert_admin);
		//$case_id = $this->cases_model->insert_case($insert_case);
		   redirect('cases/details/'.$case_id);
		}else{
			redirect('users/activity/cases');
		}
		
	}
	public function details($case_id)
	{	
	    $data['details'] = $this->cases_model->case_details($case_id);
	    $data['msg'] = $this->cases_model->case_msg($case_id);
		$data['note'] = $this->cases_model->case_note($case_id);
		$data['title'] = 'Case Details';		
		$this->load->view('case_details', $data);
	}
	public function change_status()
	{	
	   $data = array(
               'status' => $this->input->post('status')
            );
		
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('auction_case', $data);
       echo 'Status Successfully updated.';	
	}
	
	public function chkcase_status()
	{
        if($this->cases_model->chk_case($this->input->post('auction_id')) <= 0 ){
		echo '';
		}else{
		echo 'Already one case is opened, You can not give any case.';	
		}
		
	}
}