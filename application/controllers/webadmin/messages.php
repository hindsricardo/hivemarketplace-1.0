<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/message_model');
		$this->load->model('message_model');
		
	}
	public function index(){
	
		if($this->session->userdata('logged_in')){
		  $data['title'] = 'Mssages';	
		  $data['msg'] =$this->message_model->message_all();
		  $data['unread'] =$this->message_model->message_unread();
		  
			$this->load->view('admin/messages',$data);
		}else{
			redirect('');
		}
	}	
	function sendMail(){	
	   if($this->session->userdata('logged_in')==''){
	    echo "Please Login and then you can send your mail.";
	   }else{
	 	$this->load->library('email');
		$msg_to = get_user_username($this->input->post('auction_usar_id'));
		$from_name = get_user_username($this->session->userdata('user_id'));
		$insert_msg = array(
			'sender_id' => $this->session->userdata('user_id'),
			'recever_id' => $this->input->post('auction_usar_id'),
			'parent_id' => $this->input->post('auction_id'),
			'msg_to' => $msg_to,
			'msg_from' => $from_name,
			'subject' => $this->input->post('subject'),
			'msg' => fSecureInput($this->input->post('msg')),
			'type' => $this->input->post('type'),
			'status' => 0,
			'date' => to_db_date()
		);	
		$msg_id=$this->message_model->insert_mail($insert_msg);		
		if( $msg_id > 0 ){		
		/*$from_email=get_user_email($this->session->userdata('user_id'));//session user id here			
		$this->email->from($from_email, $from_name);		
		$to_email = get_user_email($this->input->post('auction_usar_id'));  //auction user id here
		$this->email->to($to_email);
		//$this->email->cc('another@another-example.com')====> $this->config->item('support_email');
		//$this->email->bcc('them@their-example.com');		
		$this->email->subject($this->input->post('subject'));
		$this->email->message($this->input->post('msg'));		
		$this->email->send();*/		
	    echo "Message successfully sended.";
	   }else{ echo "Message not send. Try Again"; }
	   }
	}
	
	public function update(){	
	$data = array(
               'status' => $this->input->post('status')
            );
		
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('auction_mail', $data);
        echo count($this->message_model->message_unread());
	
	}
	
	public function msg_multi(){	  
	  $ids=implode(',',$this->input->post('au_id'));
	  $status=$this->input->post('mode');
	  $this->message_model->message_userstatus($ids,$status);
	  redirect('webadmin/messages');	 
	}
	
	public function msg_multi_send(){	  
	  $ids=implode(',',$this->input->post('au_id'));
	  $status=$this->input->post('mode');	   
	  $this->message_model->message_senderstatus($ids,$status);	  
	  redirect('webadmin/messages/send');
	 
	}
	
	public function send(){
		if($this->session->userdata('logged_in')){
		  $data['title'] = 'Send Mssages';	
		  $data['msg'] =$this->message_model->message_send();
		  $data['unread'] =$this->message_model->message_unread();
		  
			$this->load->view('admin/msg_send',$data);
		}else{
			redirect('');
		}
	}
	
	public function trash(){
		if($this->session->userdata('user_logged_in')){
		  $data['title'] = 'Trash Mssages';	
		  $data['msg'] =$this->message_model->message_trash();
		  $data['unread'] =$this->message_model->message_unread();
		  
			$this->load->view('user/msg_trash',$data);
		}else{
			redirect('');
		}
	}
	public function msg_restore(){	  
	  $ids=implode(',',$this->input->post('au_id'));	    
	  $this->message_model->trash_restore($ids);	  
	  redirect('webadmin/messages/trash');	 
	}	
	
	function mailUandB(){
	
	    $case_id=$this->input->post('case_id');				   
	 	$this->load->model('cases_model');
		$case=$this->cases_model->case_UandB($case_id);	
		
		$recever_id1=$case[0]->user_id;
		$msg_to1 = get_user_username($case[0]->user_id);
		$from_name = 'Support';
		$date = to_db_date();
		
		$buyer_msg = array(
			'sender_admin' => $this->session->userdata('logged_in'),
			'recever_id' => $recever_id1,
			'parent_id' => $case_id,
			'msg_to' => $msg_to1,
			'msg_from' => $from_name,
			'subject' => $this->input->post('subject'),
			'msg' => fSecureInput($this->input->post('msg')),
			'type' => $this->input->post('type'),
			'status' => 0,
			'date' => $date		
		);	
		$recever_id2=$case[0]->seller_id;
		$msg_to2 = get_user_username($case[0]->seller_id);		
		
		$seller_msg = array(
			'sender_admin' => $this->session->userdata('logged_in'),
			'recever_id' => $recever_id2,
			'parent_id' => $case_id,
			'msg_to' => $msg_to2,
			'msg_from' => $from_name,
			'subject' => $this->input->post('subject'),
			'msg' => fSecureInput($this->input->post('msg')),
			'type' => $this->input->post('type'),
			'status' => 0,
			'date' => $date		
		);	
		
		
		$msg_id=$this->message_model->insert_mail($buyer_msg);	
		$msg_id=$this->message_model->insert_mail($seller_msg);	
		if( $msg_id > 0 ){		
			
	    echo "Message successfully sended.";
	   }else{ echo "Message not send. Try Again"; }
	   
	}	
		
}