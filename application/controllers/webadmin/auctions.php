<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auctions extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
		$this->load->model('model_manage_credits');
		$this->load->library('form_validation');
	}
	function index(){
		if($this->session->userdata('logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
				
			if($this->input->post("type")){
				$auction_value['type'] = $this->input->post("type");
			}else{
				$auction_value['type'] = '';
			}
			
			if($this->input->post("search_str")){
				$auction_value['search_str'] = $this->input->post("search_str");
			}else{
				$auction_value['search_str'] = '';
			}
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_auction'] = $this->model_list_auction->get_admin_auction_list($auction_value, $pageination);
			$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_auction'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			//$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$this->load->view('admin/auctions',$data);
		}else{
			redirect('');
		}
	}
	function auction_filter(){
		if($this->session->userdata('logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
				
			if($this->input->post("type")){
				$auction_value['type'] = $this->input->post("type");
			}else{
				$auction_value['type'] = '';
			}
			
			if($this->input->post("search_str")){
				$auction_value['search_str'] = fSecureInput($this->input->post("search_str"));
			}else{
				$auction_value['search_str'] = '';
			}
			if($this->input->post("seller_id")){
				$auction_value['seller_id'] = $this->input->post("seller_id");
			}
			
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_auction'] = $this->model_list_auction->get_admin_auction_list($auction_value, $pageination);
			$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_auction'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			//$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$this->load->view('admin/auctions_bk',$data);
		}else{
			redirect('');
		}
	}
	function reverse($aid){
		if($this->session->userdata('logged_in')){
			$user_credit = $this->model_manage_credits->reverse_credits($aid);
			$update_auction = $this->model_list_auction->reverse_auction_update($aid);
			redirect('webadmin/auctions');
		}else{
			redirect('');
		}
	}
}