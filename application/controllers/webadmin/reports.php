<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->library('form_validation');
		$this->load->model('model_list_auction');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['user_list'] = $this->model_users->get_user_list();
			$this->load->view('admin/reports', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
}