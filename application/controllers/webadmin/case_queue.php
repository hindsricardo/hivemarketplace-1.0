<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Case_queue extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cases_model');
		$this->load->model('admin/model_users');
		$this->load->model('model_list_auction');
	}
	public function index(){
		if($this->session->userdata('logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
			if($this->input->post("type")){
				$auction_value['type'] = $this->input->post("type");
			}else{
				$auction_value['type'] = '';
			}
			
			if($this->input->post("search_str")){
				$auction_value['search_str'] = $this->input->post("search_str");
			}else{
				$auction_value['search_str'] = '';
			}
			if($this->input->post("seller_id")){
				$auction_value['seller_id'] = $this->input->post("seller_id");
			}
			
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_auction'] = $this->model_list_auction->get_admin_cases_list($auction_value, $pageination);
			//$data['total_rows'] = $this->model_list_auction->get_admin_cases_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_auction'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			//$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$this->load->view('admin/case_queue', $data);
		}else{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	public function case_queue_filter(){
		if($this->session->userdata('logged_in')){
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
			if($this->input->post("type")){
				$auction_value['type'] = $this->input->post("type");
			}else{
				$auction_value['type'] = '';
			}
			
			if($this->input->post("search_str")){
				$auction_value['search_str'] = $this->input->post("search_str");
			}else{
				$auction_value['search_str'] = '';
			}
			
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_auction'] = $this->model_list_auction->get_admin_cases_list($auction_value, $pageination);
			//$data['total_rows'] = $this->model_list_auction->get_admin_cases_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_auction'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			//$data['credit_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//$data['auction_details'] = $this->model_manage_credits->user_available_credit($this->session->userdata('user_id'));
			//echo '<pre>';print_r($data['list_summary']);die();
			$this->load->view('admin/case_queue_bk', $data);
		}else{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	public function add_case_note(){
		$auction_value['admin_id'] = $this->input->post("admin_id");
		$auction_value['case_id'] = $this->input->post("case_id");
		$auction_value['description'] = fSecureInput($this->input->post("description"));
		$auction_value['date'] = to_db_date();
		$note_id = $this->cases_model->add_case_note($auction_value);
		redirect('webadmin/case_queue');
	}
}