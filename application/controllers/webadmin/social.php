<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_social');
	}
	
	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			//$this->load->view('nav/nav_header');
			//$this->load->view('nav/nav_left');
			$data['result'] = $this->model_social->get_social();
			$this->load->view('admin/social', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	
	/*function update($id)
	{
		$data['id'] = $id;
		$data['twitter'] = $this->input->post('twitter');
		$data['facebook'] = $this->input->post('facebook');
		$data['rss'] = $this->input->post('rss');
		
		
		$result = $this->model_social->update_privacy_policy($data);
		
		//$this->load->view('nav/nav_header');
		//$this->load->view('nav/nav_left');
		$data['result'] = $this->model_social->get_social();
		$this->load->view('admin/social', $data);
	}*/
	
	function update()
	{
		//$data['id'] = $id;		
		if($this->input->post('save')){
		//echo "hello";
		$this->model_social->update_social();
		}
		$data['result'] = $this->model_social->get_social();
		//print_r($data['result']);
		$this->load->view('admin/social', $data);
	}
	
}