<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Market_place extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_market_place');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['result'] = $this->model_market_place->get_market_place();
			$this->load->view('admin/market_place', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	
	/*function update($id)
	{
		$data['id'] = $id;
		$data['title'] = $this->input->post('title');
		$data['heading'] = $this->input->post('heading');
		$data['meta_tag'] = $this->input->post('meta_tag');
		$data['meta_description'] = $this->input->post('meta_description');
		$data['description'] = $this->input->post('description');
		
		$result = $this->model_market_place->update_market_place($data);
		
		//$this->load->view('nav/nav_header');
		//$this->load->view('nav/nav_left');
		$data['result'] = $this->model_market_place->get_market_place();
		$this->load->view('admin/market_place', $data);
	}*/
	
	function update()
	{
		//$data['id'] = $id;		
		if($this->input->post('save')){
		//echo "hello";
		$this->model_market_place->update_market_place();
		}
		$data['result'] = $this->model_market_place->get_market_place();
		//print_r($data['result']);
		$this->load->view('admin/market_place', $data);
	}
	
}