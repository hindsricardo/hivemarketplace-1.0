<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
		$this->load->library('form_validation');
		$this->load->model('model_list_auction');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['user_list'] = $this->model_users->get_user_list();
			$this->load->view('admin/users', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	public function details($id){
		if($this->session->userdata('logged_in'))
		{
			$data['user_details'] = $this->model_users->get_user_details($id);			
			$page = 1;
			$start = 0;
			$limit = 1000;
			if($this->input->post("limit")){
				$limit = $this->input->post("limit");
			}
			if($this->input->post("page")){
				$page = $this->input->post("page");
			}else{
				$page = 1;
			}
			if($page)
				$start = ($page - 1) * $limit; 
			else
				$start = 0;
				
				
			if($this->input->post("type")){
				$auction_value['type'] = $this->input->post("type");
			}else{
				$auction_value['type'] = '';
			}
			
			if($this->input->post("search_str")){
				$auction_value['search_str'] = fSecureInput($this->input->post("search_str"));
			}else{
				$auction_value['search_str'] = '';
			}
			$auction_value['seller_id'] = $id;
			$pageination['start'] = $start;
			$pageination['limit'] = $limit;
			$data['list_auction'] = $this->model_list_auction->get_admin_auction_list($auction_value, $pageination);
			$data['total_rows'] = $this->model_list_auction->get_auction_list();
			$data['page'] = $page;
			$data['limit'] = $limit;

			//echo '<pre>';print_r($data['list_summary']);die();
			$a_id = '';
			foreach($data['list_auction'] as $list_ids){
				$a_id .= $list_ids['id'].',';
			}
			$data['auction_ids'] = $a_id;
			$data['bidding_history'] = $this->model_list_auction->user_bidding_history($id);
			$this->load->view('admin/user_details', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	public function search(){
		$user_search_value = fSecureInput($this->input->post('user_search_value'));
		$data['user_list'] = $this->model_users->user_search_list($user_search_value);
		$data['user_search_value'] = $user_search_value;
		$this->load->view('admin/users', $data);
	}
	public function passwordreset(){
		$password_reset['id'] = $this->input->post('id');
		$password_reset['password'] = fSecureInput($this->input->post('password'));
		$id = $this->model_users->passwordreset($password_reset);
		$data['user_details'] = $this->model_users->get_user_details($id);
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('info@hivemarketplace.com', 'System Administrator');
				$this->email->to($data['user_details'][0]->email);
							
				$this->email->subject('Penny Auction Password Reset');
				$this->email->message('Dear '.$data['user_details'][0]->fname.' '.$data['user_details'][0]->lname.',<br><br> Your password for Hive Marketplace  is reset. The password is '.$data['user_details'][0]->password.'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				
				$this->email->send();
		
		
		$data['user_details'] = $this->model_users->get_user_details($id);
		$this->load->view('admin/user_details', $data);
	}
	public function send_forgot_password(){
		$forgot_email = $this->input->post('forgot_email');
		$result = $this->model_users->send_forgot_password($forgot_email);
		$user_pass = $result[0]['password'];
		
		
		if(!empty($result)){
			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			
			$this->email->from('info@hivemarketplace.com', 'System Administrator');
			$this->email->to($result[0]['email']);
			$this->email->subject('Penny Auction User Password');
			$this->email->message('Dear '.fSecureInput($result[0]['fname']).' '.$result[0]['lname'].',<br><br> Your password for Hive Marketplace is '.$result[0]['password'].'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
			
			$this->email->send();
			echo 'Password has been send to your mail';
		}else{
			echo 'Sorry!!! does not match.';
		}
	}
	public function check_subscribe(){
		$user_id = $this->input->post('user_id');
		//echo $user_id;
		$result = $this->model_users->check_subscribe($user_id);
		if($result == 1){
			echo 'subscribe';	
		}else{
			echo 'unsubscribe';
}
	}
}