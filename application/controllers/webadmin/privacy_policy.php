<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Privacy_policy extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_privacy_policy');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['result'] = $this->model_privacy_policy->get_privacy_policy();
			$this->load->view('admin/privacy', $data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	
	function update()
	{
		//$data['id'] = $id;		
		if($this->input->post('save')){
		//echo "hello";
		$this->model_privacy_policy->update_privacy_policy();
		}
		$data['result'] = $this->model_privacy_policy->get_privacy_policy();
		//print_r($data['result']);
		$this->load->view('admin/privacy', $data);
	}
	
}