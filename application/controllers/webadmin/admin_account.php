<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_account extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_admin_account');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['result'] = $this->model_admin_account->get_account();
			$this->load->view('admin/my_account',$data);
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	
	
	
	function update()
	{
	//$this->session->set_flashdata('adminacc_message', '');
		//$data['id'] = $id;		
		if($this->input->post('save')){
		$this->load->library('form_validation');  
  		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');    
 		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean'); 
 		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean'); 
		
  
	  if($this->form_validation->run() != FALSE)
	  {
		$this->model_admin_account->update_admin_account();
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('info@hivemarketplace.com', 'System Administrator');
		$this->email->to(mysql_real_escape_string($this->input->post('email')));
							
		$this->email->subject('Penny Auction Admin Account Change');
		$this->email->message('Dear '.mysql_real_escape_string($this->input->post('name')).',<br><br> Your email id and password for Hive Marketplace admin panel is '.mysql_real_escape_string($this->input->post('email')).' and '.mysql_real_escape_string($this->input->post('password')).'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				
		$this->email->send();
	//	$this->session->set_flashdata('adminacc_message', 'Updated Successfully.');
	  }
	  
	  }
		$data['result'] = $this->model_admin_account->get_account();
		//print_r($data['result']);
		$this->load->view('admin/my_account', $data);
	}
	
}