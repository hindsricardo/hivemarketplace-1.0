<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_tools extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_admin_tools');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$data['admin_list'] = $this->model_admin_tools->get_admin_user();
			$this->load->view('admin/admin_tools',$data );
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
	public function add(){
		$this->form_validation->set_rules('add_username', 'Username', 'trim|required|xss_clean');    
 		$this->form_validation->set_rules('add_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('add_name', 'Name', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('add_email', 'Email', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE){
			$admin_data['username'] = fSecureInput($this->input->post('add_username'));
			$admin_data['password'] = fSecureInput($this->input->post('add_password'));
			$admin_data['name'] = fSecureInput($this->input->post('add_name'));
			$admin_data['email'] = $this->input->post('add_email');
			//$admin_data['name'] = $this->input->post('add_fname').' '.$this->input->post('add_lname');
			$admin_data['status'] = 'Active';
			$admin_data['level'] = $this->input->post('add_level');
			$id = $this->model_admin_tools->insert($admin_data);
			
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('info@hivemarketplace.com', 'System Administrator');
				$this->email->to($admin_data['email']);
							
				$this->email->subject('Penny Auction Create Account');
				$this->email->message('Dear '.$admin_data['name'].',<br><br> Your Account has been created for Hive Marketplace . Your Username is '.$admin_data['username'].' and Password is '.$admin_data['password'].'<br><br>Regards,<br>System Administrator<br>Hive Marketplace');
				
				$this->email->send();
			
			
			$data['admin_list'] = $this->model_admin_tools->get_admin_user();
			$this->load->view('admin/admin_tools',$data );
		}else{
			$data['admin_list'] = $this->model_admin_tools->get_admin_user();
			$this->load->view('admin/admin_tools',$data );
		}
	}
	
	public function edit(){
		//echo '<pre>';print_r($_POST);die();
			$admin_data['id'] = $this->input->post('id');
			$admin_data['username'] = fSecureInput($this->input->post('username'));
			$admin_data['password'] = fSecureInput($this->input->post('password'));
			$admin_data['name'] = fSecureInput($this->input->post('name'));
			$admin_data['email'] = $this->input->post('email');
			//$admin_data['name'] = $this->input->post('fname').' '.$this->input->post('lname');
			$admin_data['status'] = 'Active';
			$admin_data['level'] = $this->input->post('level');
			$id = $this->model_admin_tools->edit($admin_data);
			echo $id;
	}
	public function delete(){
		$id = $this->input->post('id');
		$this->model_admin_tools->delete($id);
		echo $id;
	}
	public function get_edit_details(){
		$id = $this->input->post('id');
		$result = $this->model_admin_tools->get_edit_details($id);
		echo json_encode($result);
	}
	
}