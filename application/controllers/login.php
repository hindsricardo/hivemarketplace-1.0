<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/model_users');
	}

	
	public function authentication(){
		$user_data['username'] = $this->input->post('username');
		$user_data['password'] = $this->input->post('password');
		$result = $this->model_users->check_valid_user($user_data);
		//echo '<pre>';print_r($result);
		if(!empty($result)){
			
			$session_data = array(
								"user_logged_in" => TRUE,
								"user_id"  => $result[0]->id,
								"username"  => $result[0]->username,
								"fname"  => $result[0]->fname,
								"lname"  => $result[0]->lname,
								"email"  => $result[0]->email,
								"status"  => $result[0]->status,
							);
			$this->session->set_userdata($session_data);
			$this->load->view('user/account_settings');
		}else{
			$this->load->view('user/home');
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('home');
		//$this->load->view('user/home');
	}
}
