<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cart_model');
	}

	public function index()
	{	
		$this->data['title'] = 'Shopping Carts';

		if (!$this->cart->contents()){
			$this->data['message'] = '<p>Your cart is empty!</p>';
			$this->data['msg_head'] = 0;
		}else{
			$this->data['message'] = $this->session->flashdata('message');
			$this->data['msg_head'] = count($this->cart->contents());
			//$this->data['message'] = count($this->cart->contents());			
		}
		$this->load->view('checkout', $this->data);
	}

	public function add()
	{
		$this->load->model('cart_model');
		if($this->input->post('offer_shipping') == '1'){
			$delivery_type = 'ship';
		}elseif($this->input->post('payat_pickup') == '1'){
			$delivery_type = 'pick';
		}else{
			$delivery_type = '';
		}
		if($this->input->post('buy_type') == 'direct_buy'){
			$buy_type = 'direct_buy';
		}else{
			$buy_type = '';
		}
		$insert_room = array(
			'id' => $this->input->post('id'),
			'user_id' => $this->input->post('user_id'),
			'picture' => $this->input->post('picture'),
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'qty' => $this->input->post('qty'),
			'tags' => $this->input->post('tags'),
			'desc' => $this->input->post('desc'),
			'credit' => $this->input->post('credit'),
			'offer_shipping' => $this->input->post('offer_shipping'),
			'shipping' => $this->input->post('shipping'),
			'payat_pickup' => $this->input->post('payat_pickup'),
			'delivery_type' => $delivery_type,
			'buy_type' => $buy_type
		);		
//echo '<pre>';print_r($_POST);die();
		$this->cart->insert($insert_room);
			
		redirect('checkout');
	}
	
	function remove($rowid) {
		if ($rowid=="all"){
			$this->cart->destroy();
		}else{
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);

			$this->cart->update($data);
		}
		
		redirect('checkout');
	}	

	function update_cart(){
 		foreach($_POST['cart'] as $id => $cart)
		{			
			$price = $cart['price'];
			$amount = $price * $cart['qty'];
			
			$this->Cart_model->update_cart($cart['rowid'], $cart['qty'], $price, $amount,$cart['shipping']);
		}
		
		redirect('checkout');
	}	
	
	function sendMail(){	
	 
	 	$this->load->library('email');
		$msg_to = get_user_username($this->input->post('auction_usar_id')).', Support';
		$from_name = get_user_username($this->session->userdata('user_id'));
		$insert_msg = array(
			'sender_id' => $this->session->userdata('user_id'),
			'recever_id' => $this->input->post('auction_usar_id'),
			'parent_id' => $this->input->post('auction_id'),
			'msg_to' => $msg_to,
			'msg_from' => $from_name,
			'subject' => $this->input->post('subject'),
			'msg' => $this->input->post('msg'),
			'type' => $this->input->post('type'),
			'status' => 0,
			'date' => to_db_date()		
		);	
		$msg_id=$this->cart_model->insert_mail($insert_msg);
		
		if( $msg_id > 0 ){
		
		$from_email=get_user_email($this->session->userdata('user_id'));//session user id here	
		
		$this->email->from($from_email, $from_name);
		
		$to_email = get_user_email($this->input->post('auction_usar_id')).", ". $this->config->item('support_email');  //auction user id here
		$this->email->to($to_email);
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');
		
		$this->email->subject($this->input->post('subject'));
		$this->email->message($this->input->post('msg'));		
		$this->email->send();
			
	  //redirect('checkout');
	 echo "Message successfully sended.";
	 }else{ echo "Message not send. Try Again"; }
	}
	public function update_shipping(){
	//echo '<pre>';print_r($_POST);die();
		$rowid = $this->input->post('rowid');
		$amount = $this->input->post('amount');
		$delivery_type = $this->input->post('delivery_type');
		$cart_update = $this->cart->contents();
		$cart_details = $cart_update[$rowid];
		$data = array(
			'rowid'   => $rowid,
			'qty'     => 0
		);
		$this->cart->update($data);		
		$this->load->model('cart_model');
		$insert_room = array(
			'id' => $cart_details['id'],
			'user_id' =>$cart_details['user_id'],
			'picture' => $cart_details['picture'],
			'name' => $cart_details['name'],
			'price' => $cart_details['price'],
			'qty' => $cart_details['qty'],
			'tags' => $cart_details['tags'],
			'desc' => $cart_details['desc'],
			'credit' => $cart_details['credit'],
			'offer_shipping' => $cart_details['offer_shipping'],
			'shipping' => $amount,
			'payat_pickup' =>$cart_details['payat_pickup'],
			'delivery_type' =>$delivery_type,
			'buy_type' =>$cart_details['buy_type']
			
		);		
		$this->cart->insert($insert_room);
		
	}
}