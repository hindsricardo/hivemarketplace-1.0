<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			//$this->load->view('adminnav/nav_header');
			//$this->load->view('adminnav/nav_left');
			$this->load->view('main_page');
		}
		else
		{
			$data['error'] = "Please login to access this page";
			$this->load->view('login_page', $data);
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */