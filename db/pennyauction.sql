-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2013 at 09:11 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pennyauction`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(5) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `heading` varchar(255) default NULL,
  `meta_tag` varchar(255) default NULL,
  `meta_description` varchar(255) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `heading`, `meta_tag`, `meta_description`, `description`) VALUES
(1, 'About Us', 'About Us', 'About Us', 'About Us', '<p><strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(150) NOT NULL,
  `passcode` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `permission` text NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `fname`, `lname`, `name`, `phone`, `email`, `passcode`, `status`, `permission`, `level`) VALUES
(1, 'administrator', 'administrator', 'aa', 'bb', 'aa bb', '9830098300', 'hindsricardo@gmail.com', '778be2400cb186845b77750a6c97a49b', 'Active', '', 1),
(2, 'atest2', 'atest2', '', '', 'atest2 atest2', '', 'duttasujata8@gmail.com', 'fc1b2cadf9c00d4d3e2ac926a532e3f7', 'Inactive', '', 2),
(5, 'atest5', 'atest5', 'atest5', 'atest5', 'testf1 testl232', '', '', 'fb3ae89e2cebb6b120604dbb6983e5f3', 'Active', '', 1),
(6, 'test3', 'test3', 'test3', 'test3', 'test3', '', '', 'c14fbd657977850e04b5de64672547c6', 'Inactive', '', 3),
(7, 'test33', 'test33', '', '', 'test31', '', 'test3@test.com', 'eaed9a0187697c39d259ea2ec9436c5f', 'Inactive', '', 2),
(8, 'atest8', 'atest8', '', '', 'atest8', '', 'fdfdf@ggg.com', 'a5684e3582c79191ac7b4f75a78802b3', 'Inactive', '', 1),
(9, 'ddd&#039;ff', 'bbb&#039;dd', '', '', 'kkk&#039;kkk', '', 'subhra@gmail.com', '', 'Active', '', 1),
(10, 'rrr&#039;hhh', 'ttt&#039;jjjkk', '', '', 't&#039;ppppkk', '', 'subhra@gmail.com', '3b8b9b1adc2b47ed43a84b3a9e4b3133', 'Active', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_level`
--

CREATE TABLE `admin_level` (
  `id` int(11) NOT NULL auto_increment,
  `adminlevel` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin_level`
--

INSERT INTO `admin_level` (`id`, `adminlevel`) VALUES
(1, 'Admin'),
(2, 'Manager'),
(3, 'Support Admin');

-- --------------------------------------------------------

--
-- Table structure for table `answers_rating`
--

CREATE TABLE `answers_rating` (
  `id` int(11) NOT NULL auto_increment,
  `answers_id` int(11) NOT NULL,
  `rating_num` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `answers_rating`
--

INSERT INTO `answers_rating` (`id`, `answers_id`, `rating_num`, `user_id`) VALUES
(1, 2, 2, 8),
(2, 1, 4, 8),
(3, 2, 4, 7),
(4, 7, 3, 6),
(5, 6, 4, 6),
(6, 3, 2, 6),
(7, 1, 3, 6),
(8, 20, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auction`
--

CREATE TABLE `auction` (
  `ID` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tags` varchar(200) NOT NULL,
  `item_condition` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `remaining_qty` int(11) NOT NULL default '0',
  `description` text NOT NULL,
  `item_value` double(10,2) NOT NULL,
  `auction_endtime` int(11) NOT NULL,
  `min_bids` int(11) NOT NULL,
  `min_profit` double(10,2) NOT NULL,
  `for_bidders` varchar(10) NOT NULL default '0',
  `paypal_account` varchar(50) NOT NULL,
  `auction_type` varchar(50) NOT NULL,
  `passcode` varchar(200) NOT NULL,
  `starting_with` enum('now','later') NOT NULL,
  `schedule_time` datetime NOT NULL,
  `schedule_endtime` datetime NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_by` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `stage` varchar(50) NOT NULL,
  `winner_id` int(11) NOT NULL,
  `HMP_fee` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `auction`
--

INSERT INTO `auction` (`ID`, `user_id`, `title`, `tags`, `item_condition`, `qty`, `remaining_qty`, `description`, `item_value`, `auction_endtime`, `min_bids`, `min_profit`, `for_bidders`, `paypal_account`, `auction_type`, `passcode`, `starting_with`, `schedule_time`, `schedule_endtime`, `create_date`, `modify_date`, `modify_by`, `type`, `stage`, `winner_id`, `HMP_fee`, `status`) VALUES
(1, 1, 'Mobile1', 'mobile', 'New', 3, -1, 'Description Description Description Description Description Description Description', 50.00, 56, 11, 8.25, '5', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-09-14 13:16:00', '2013-09-16 21:16:00', '2013-09-13 10:46:11', '2013-09-13 10:46:11', 1, '', 'win', 2, '15', 0),
(2, 2, 'Mouce', 'mouce', 'New', 4, 2, 'Mouce Mouce Mouce Mouce Mouce ', 100.00, 69, 15, 11.25, '5', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-09-13 11:21:51', '2013-09-16 08:21:51', '2013-09-13 11:21:51', '2013-09-13 11:21:51', 2, '', 'win', 3, '15', 0),
(3, 3, 'keyboard', '', 'New', 3, 0, 'Help buyers find what they', 75.00, 40, 0, 0.00, '8', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-09-13 11:27:42', '2013-09-15 03:27:42', '2013-09-13 11:27:42', '2013-09-13 11:27:42', 3, '', 'win', 2, '15', 0),
(4, 2, '5 HIVECombs', '', '', 1, 0, '', 3.75, 40, 0, 0.00, '', '', 'Public', '', 'now', '2013-09-13 12:19:40', '2013-09-15 04:19:40', '2013-09-13 12:19:40', '2013-09-13 12:19:40', 2, 'credit_auction', '', 0, '', 1),
(5, 1, '6 HIVECombs', '', '', 1, 0, '', 4.50, 32, 0, 0.00, '', '', 'Public', '', 'now', '2013-09-17 06:17:42', '2013-09-18 14:17:42', '2013-09-17 06:17:42', '2013-09-17 06:17:42', 1, 'credit_auction', '', 0, '15', 1),
(6, 2, 'Private laptop', '', 'New', 3, 0, 'Offering multi of an item in an auction can differentiate your auction even', 75.00, 64, 23, 17.25, '2', 'test.skm1@gmail.com', 'Private', 'laptop', 'now', '2013-09-18 06:17:41', '2013-09-20 22:17:41', '2013-09-18 06:17:41', '2013-09-18 06:17:41', 2, '', 'win', 3, '15', 0),
(7, 1, 'monitor', 'monitor', 'New', 3, 3, 'Whether you allow returns.Be sure to check your spelling and grammer. ', 50.00, 57, 0, 0.00, '2', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-09-19 12:22:00', '2013-09-21 21:22:00', '2013-09-18 09:52:57', '2013-09-18 09:52:57', 1, '', 'win', 2, '15', 0),
(8, 1, 'bottle1', '', 'New', 3, 2, 'After all, two is better than one', 35.00, 53, 10, 7.50, '2', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-09-24 13:20:00', '2013-09-26 18:20:00', '2013-09-20 09:51:09', '2013-09-20 09:51:09', 1, '', 'win', 2, '15', 0),
(9, 1, 'sunglass', 'sunglass', 'New', 4, 2, 'your auction even if someone has the same item listed.', 15.00, 29, 12, 9.00, '2', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-09-20 05:23:00', '2013-09-24 10:23:00', '2013-09-20 09:54:51', '2013-09-20 09:54:51', 1, '', 'win', 2, '15', 0),
(10, 2, '5 HIVECombs', '', '', 1, 0, '', 3.75, 41, 0, 0.00, '', '', 'Public', '', 'now', '2013-09-20 12:59:33', '2013-09-22 05:59:33', '2013-09-20 12:59:33', '2013-09-20 12:59:33', 2, 'credit_auction', 'win', 1, '15', 0),
(11, 2, 'sunglass1', 'sunglass', 'New', 3, 3, 'Offering multi of an item in an auction can differentiate your auction', 25.00, 53, 15, 11.25, '2', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-09-23 12:37:51', '2013-10-05 17:37:51', '2013-09-23 12:37:51', '2013-09-23 12:37:51', 2, '', '', 0, '15', 0),
(12, 3, '5 HIVECombs', '', '', 1, 1, '', 3.75, 1, 0, 0.00, '', '', 'Public', '', 'now', '2013-10-07 10:43:13', '2013-10-07 11:43:13', '2013-10-07 10:43:13', '2013-10-07 10:43:13', 3, 'credit_auction', '', 0, '15', 1),
(13, 2, '3 HIVECombs', '', '', 1, 1, '', 2.25, 1, 0, 0.00, '', '', 'Public', '', 'now', '2013-10-07 11:03:09', '2013-10-07 12:03:09', '2013-10-07 11:03:09', '2013-10-07 11:03:09', 2, 'credit_auction', '', 0, '15', 1),
(14, 2, 'test14', '', 'New', 3, 3, 'Description Description Description', 20.00, 8, 20, 15.00, '3', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-10-07 12:24:50', '2013-10-07 20:24:50', '2013-10-07 12:24:50', '2013-10-07 12:24:50', 2, '', '', 0, '15', 0),
(15, 1, '4 HIVECombs', '', '', 1, 0, '', 3.00, 1, 0, 0.00, '', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-10-09 06:31:53', '2013-10-09 13:05:53', '2013-10-09 06:31:53', '2013-10-09 06:31:53', 1, 'credit_auction', 'win', 3, '15', 0),
(16, 2, 'active auction1', 'active', 'New', 2, 2, 'Description Description Description Description Description', 30.00, 4, 10, 7.50, '2', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-10-15 06:37:06', '2013-10-15 09:00:06', '2013-10-15 06:37:06', '2013-10-15 06:37:06', 2, '', 'win', 1, '15', 0),
(17, 2, 'Schedule auction1', 'Schedule', 'New', 2, 2, 'Description Description Description Description', 20.00, 28, 0, 0.00, '2', 'test.skm1@gmail.com', 'Public', '', 'later', '2013-10-21 15:50:00', '2013-10-22 15:50:00', '2013-10-16 06:22:38', '2013-10-16 06:22:38', 2, '', '', 0, '15', 0),
(18, 2, 'Mobile1', 'Mobile1', 'New', 3, 2, 'Description Description Description Description', 20.00, 32, 5, 3.75, '2', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-10-16 08:28:40', '2013-10-21 16:28:40', '2013-10-16 08:28:40', '2013-10-16 08:28:40', 2, '', '', 0, '15', 1),
(19, 1, 'test1', '', 'New', 4, 0, 'Description Description Description', 20.00, 14, 5, 3.75, '2', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-10-30 06:09:19', '2013-10-31 05:09:19', '2013-10-30 06:09:19', '2013-10-30 06:09:19', 1, '', 'win', 2, '15', 0),
(20, 1, '5 HIVECombs', '', '', 1, 0, '', 3.75, 1, 0, 0.00, '0', '', 'Public', '', 'now', '2013-10-30 08:59:40', '2013-10-30 09:02:40', '2013-10-30 08:59:40', '2013-10-30 08:59:40', 1, 'credit_auction', '', 0, '15', 1),
(21, 1, '4 HIVECombs', '', '', 1, 0, '', 3.00, 1, 0, 0.00, '0', '', 'Public', '', 'now', '2013-10-30 09:04:47', '2013-10-30 09:04:47', '2013-10-30 09:04:47', '2013-10-30 09:04:47', 1, 'credit_auction', '', 0, '15', 1),
(22, 1, '3 HIVECombs', '', '', 1, 0, '', 2.25, 1, 0, 0.00, '0', '', 'Public', '', 'now', '2013-10-30 09:07:03', '2013-10-30 09:13:03', '2013-10-30 09:07:03', '2013-10-30 09:07:03', 1, 'credit_auction', 'win', 2, '15', 0),
(23, 1, '6 HIVECombs', '', '', 1, 0, '', 4.50, 1, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-10-30 11:16:08', '2013-10-30 11:20:08', '2013-10-30 11:16:08', '2013-10-30 11:16:08', 1, 'credit_auction', 'win', 2, '15', 0),
(24, 2, '2 HIVECombs', '', '', 1, 0, '', 1.50, 1, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-10-30 12:04:22', '2013-10-30 12:32:22', '2013-10-30 12:04:22', '2013-10-30 12:04:22', 2, 'credit_auction', 'win', 1, '15', 0),
(25, 1, '1 HIVECombs', '', '', 1, 0, '', 0.75, 1, 0, 0.00, '0', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-10-30 12:44:10', '2013-10-30 12:49:10', '2013-10-30 12:44:10', '2013-10-30 12:44:10', 1, 'credit_auction', 'win', 2, '15', 0),
(37, 2, 'test1', '', 'New', 0, 5, '', 5.55, 32, 5, 3.75, '5', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-15 13:14:17', '2013-11-16 21:14:17', '2013-11-15 13:14:17', '2013-11-15 13:14:17', 2, '', '', 0, '15', 0),
(26, 2, 'private_cup', 'cup', 'New', 2, 2, 'Description Description Description Description', 10.00, 1, 0, 0.00, '2', 'test.skm1@gmail.com', 'Private', 'cup', 'now', '2013-10-31 06:25:08', '2013-10-31 10:25:08', '2013-10-31 06:25:08', '2013-10-31 06:25:08', 2, '', '', 0, '15', 0),
(27, 3, 'private_globe', 'globe', 'New', 1, 1, 'Description Description Description Description', 10.00, 3, 5, 3.75, '1', 'test3@gmail.com', 'Private', '', 'now', '2013-10-31 08:31:11', '2013-10-31 11:31:11', '2013-10-31 08:31:11', '2013-10-31 08:31:11', 3, '', '', 0, '15', 0),
(28, 1, 'schedule_cup', '', 'New', 2, 2, 'Description Description Description Description', 20.00, 10, 5, 5.00, '2', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-11-03 13:55:00', '2013-11-06 21:12:00', '2013-10-31 08:49:06', '2013-10-31 11:09:11', 1, '', '', 0, '15', 1),
(29, 1, 'dfdfdf', '', 'New', 0, 0, '', 15.00, 32, 0, 0.00, '', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-11-01 05:48:00', '2013-11-03 18:00:00', '2013-10-31 10:00:49', '2013-10-31 10:00:49', 1, '', '', 0, '15', 0),
(30, 2, '3 HIVECombs', '', '', 1, 0, '', 2.25, 2, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-01 08:32:21', '2013-11-01 08:28:21', '2013-11-01 08:32:21', '2013-11-01 08:32:21', 2, 'credit_auction', '', 0, '15', 1),
(31, 2, '3 HIVECombs', '', '', 1, 0, '', 2.25, 1, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-01 08:35:35', '2013-11-01 08:30:35', '2013-11-01 08:35:35', '2013-11-01 08:35:35', 2, 'credit_auction', '', 0, '15', 1),
(32, 2, '2 HIVECombs', '', '', 1, 0, '', 1.50, 1, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-01 08:37:20', '2013-11-01 08:40:20', '2013-11-01 08:37:20', '2013-11-01 08:37:20', 2, 'credit_auction', 'win', 1, '15', 0),
(33, 2, 'test_33', '', 'New', 2, 3, 'Description Description Description Description', 20.00, 21, 10, 7.50, '3', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-11 10:12:49', '2013-11-14 12:11:49', '2013-11-11 10:12:49', '2013-11-11 10:12:49', 2, '', '', 0, '15', 1),
(34, 1, 'test_34', '', 'New', 2, 3, 'Description Description Description Description Description', 25.00, 32, 4, 7.50, '3', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-11 10:17:37', '2013-11-14 09:38:00', '2013-11-11 10:17:37', '2013-11-11 10:17:37', 1, '', 'win', 3, '15', 0),
(35, 2, 'test_35', '', 'New', 3, 1, 'Description Description Description Description', 20.00, 10, 5, 3.75, '2', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-12 05:31:26', '2013-11-14 07:18:00', '2013-11-12 05:31:26', '2013-11-12 05:31:26', 2, '', 'win', 3, '15', 0),
(36, 3, 'test_36', '', 'New', 3, 1, 'Description Description Description Description', 25.00, 45, 5, 3.75, '1', 'test3@gmail.com', 'Public', '', 'later', '2013-11-14 08:55:00', '2013-11-15 05:55:00', '2013-11-12 09:02:23', '2013-11-12 09:02:23', 3, '', '', 0, '15', 0),
(38, 2, 'hhhhhh', '', 'New', 2, 2, '', 22.22, 32, 2, 1.50, '2', 'test.skm1@gmail.com', 'Public', '', 'later', '2013-11-15 09:16:00', '2013-11-15 13:28:00', '2013-11-15 13:16:18', '2013-11-15 13:16:18', 2, '', 'win', 1, '15', 0),
(39, 2, 'ggggg', '', 'New', 3, 0, '', 30.03, 32, 2, 1.50, '', 'test.skm1@gmail.com', 'Public', '', 'later', '2013-11-16 10:17:00', '2013-11-17 18:17:00', '2013-11-15 13:17:50', '2013-11-15 13:17:50', 2, '', '', 0, '15', 0),
(40, 3, 'test_40', '', 'New', 2, 1, 'Description Description Description', 10.00, 1, 2, 1.50, '1', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-18 04:57:47', '2013-11-18 05:57:47', '2013-11-18 04:57:47', '2013-11-18 04:57:47', 3, '', '', 0, '15', 1),
(41, 1, 'test11', '', 'New', 2, 0, '', 2.22, 1, 2, 1.50, '', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-18 11:48:59', '2013-11-18 12:48:59', '2013-11-18 11:48:59', '2013-11-18 11:48:59', 1, '', '', 0, '15', 0),
(42, 1, 'test66', '', 'New', 0, 0, '', 3.33, 1, 2, 1.50, '', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-18 12:03:31', '2013-11-18 13:03:31', '2013-11-18 12:03:31', '2013-11-18 12:03:31', 1, '', '', 0, '15', 0),
(43, 2, 'test_32', '', 'New', 0, 0, '', 2.22, 1, 2, 1.50, '', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-19 12:24:28', '2013-11-19 13:24:28', '2013-11-19 12:24:28', '2013-11-19 12:24:28', 2, '', '', 0, '15', 0),
(44, 2, 'test_40', '', 'New', 2, 0, '', 3.33, 1, 3, 2.25, '', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-20 05:36:43', '2013-11-20 06:36:43', '2013-11-20 05:36:43', '2013-11-20 05:36:43', 2, '', '', 0, '15', 1),
(45, 2, '5 HIVECombs', '', '', 1, 0, '', 3.75, 1, 0, 0.00, '0', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-20 08:31:48', '2013-11-20 08:25:48', '2013-11-20 08:31:48', '2013-11-20 08:31:48', 2, 'credit_auction', 'win', 1, '15', 0),
(46, 1, '5 HIVECombs', '', '', 1, 0, '', 3.75, 1, 0, 0.00, '0', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-20 08:55:56', '2013-11-20 11:50:56', '2013-11-20 08:55:56', '2013-11-20 08:55:56', 1, 'credit_auction', 'win', 3, '15', 0),
(47, 3, '8 HIVECombs', '', '', 1, 0, '', 6.00, 1, 0, 0.00, '0', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-20 12:33:48', '2013-11-20 13:33:48', '2013-11-20 12:33:48', '2013-11-20 12:33:48', 3, 'credit_auction', '', 0, '15', 1),
(48, 1, 'test''1', '', 'New', 0, 1, 'don''t', 30.33, 1, 3, 3.00, '1', 'subhra.beas.buyer@gmail.com', 'Public', '', 'later', '2013-11-22 13:02:00', '2013-11-22 14:02:00', '2013-11-21 07:32:13', '2013-11-21 07:44:10', 1, '', '', 0, '15', 0),
(49, 1, 'test''12', '', 'New', 2, 1, 'test''12.........', 3.30, 32, 5, 3.75, '1', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-21 11:19:50', '2013-11-22 19:19:50', '2013-11-21 11:19:50', '2013-11-21 11:19:50', 1, '', '', 0, '15', 0),
(50, 1, 'test''34', 'gor''r gh''ll', 'New', 3, 1, 'Description''jk', 4.00, 32, 2, 1.50, '1', 'subhra.beas.buyer@gmail.com', 'Public', '', 'now', '2013-11-21 11:26:08', '2013-11-22 19:26:08', '2013-11-21 11:26:08', '2013-11-21 11:26:08', 1, '', '', 0, '15', 0),
(51, 6, 'test''56_auction', 'test''56_auction', 'New', 1, 1, 'test''56_auction test''56_auction vc', 3.00, 25, 5, 3.75, '1', 'user1@gmail.com', 'Public', '', 'now', '2013-11-22 11:14:38', '2013-11-23 12:14:38', '2013-11-22 11:14:38', '2013-11-22 11:14:38', 6, '', '', 0, '15', 0),
(52, 6, 'test&#039;56_html', 'test&#039;56_html test&#039;56_html2', 'New', 2, 1, 'test&#039;56_html test&#039;56_html test&#039;56_html', 3.00, 28, 1, 0.75, '1', 'user1@gmail.com', 'Public', '', 'now', '2013-11-22 12:04:32', '2013-11-23 16:04:32', '2013-11-22 12:04:32', '2013-11-22 12:04:32', 6, '', '', 0, '15', 0),
(53, 2, 'test&#039;123', 'test&#039;123', 'New', 2, 1, 'test&#039;123 test&#039;123 test&#039;123 test&#039;123 test&#039;123', 3.30, 15, 2, 1.50, '1', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-25 05:36:43', '2013-11-25 20:36:43', '2013-11-25 05:36:43', '2013-11-25 05:36:43', 2, '', '', 0, '15', 0),
(54, 2, 'qu&#039;t', '', 'New', 2, 1, 'qu&#039;t qu&#039;t qu&#039;t', 3.00, 27, 2, 1.50, '1', 'test.skm1@gmail.com', 'Public', '', 'now', '2013-11-25 06:08:31', '2013-11-25 07:00:31', '2013-11-25 06:08:31', '2013-11-25 06:08:31', 2, '', 'win', 1, '15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `auction_case`
--

CREATE TABLE `auction_case` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `case_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `auction_case`
--

INSERT INTO `auction_case` (`id`, `user_id`, `seller_id`, `auction_id`, `purpose`, `desc`, `case_date`, `update_date`, `updated_by`, `status`) VALUES
(3, 2, 1, 1, 'Item does nor match sellers description', 'that is a true fault...', '2013-11-11 06:26:59', '0000-00-00 00:00:00', 0, 'open'),
(2, 2, 1, 1, 'Item does nor match sellers description', 'fcxcxcxc', '2013-09-28 12:50:00', '0000-00-00 00:00:00', 0, 'close'),
(4, 1, 1, 9, 'Item not recieved', 'jjjj\\''lll', '2013-11-21 09:20:46', '0000-00-00 00:00:00', 0, 'open');

-- --------------------------------------------------------

--
-- Table structure for table `auction_feedback`
--

CREATE TABLE `auction_feedback` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `desc_rate` int(11) NOT NULL,
  `speed_rate` int(11) NOT NULL,
  `comm_rate` int(11) NOT NULL,
  `response` varchar(255) NOT NULL,
  `feedback` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `auction_feedback`
--

INSERT INTO `auction_feedback` (`id`, `user_id`, `seller_id`, `auction_id`, `desc_rate`, `speed_rate`, `comm_rate`, `response`, `feedback`, `date`, `status`) VALUES
(1, 2, 3, 3, 2, 3, 2, 'positive', 'Greate item.....', '2013-09-25 06:17:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auction_image`
--

CREATE TABLE `auction_image` (
  `image_id` int(11) NOT NULL auto_increment,
  `auction_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  PRIMARY KEY  (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `auction_image`
--

INSERT INTO `auction_image` (`image_id`, `auction_id`, `image_name`) VALUES
(1, 1, '1_mobile3.jpg'),
(2, 1, '1_index1.jpg'),
(3, 1, '1_mobile5.jpg'),
(4, 1, '1_mobile6.jpg'),
(5, 2, '2_imaaages.jpg'),
(6, 2, '2_imageaas.jpg'),
(7, 2, '2_insdex.jpg'),
(8, 3, '3_keyboard.jpg'),
(9, 3, '3_keyboard1.jpg'),
(10, 3, '3_keyboard2index.jpg'),
(11, 6, '6_laptop.jpg'),
(12, 7, '7_monitor1.jpg'),
(13, 7, '7_monitor2.jpg'),
(14, 7, '7_monitor3.jpg'),
(15, 8, '8_bottle1.jpg'),
(16, 8, '8_bottle4.jpg'),
(17, 8, '8_bottle3.jpg'),
(18, 9, '9_sunglass.jpg'),
(19, 9, '9_sunglass3.jpg'),
(20, 9, '9_sunglass2.jpg'),
(21, 9, '9_sunglass5.jpg'),
(22, 11, '11_sunglass5.jpg'),
(23, 14, '14_bottle2.jpg'),
(24, 16, '16_bottle4.jpg'),
(25, 17, '17_index1.jpg'),
(26, 18, '18_mobile2.jpg'),
(27, 19, '19_imaaaages.jpg'),
(28, 26, '26_imagfffes.jpg'),
(29, 27, '27_imeeeages.jpg'),
(30, 28, '28_imagppes.jpg'),
(31, 29, '29_imayyyges.jpg'),
(32, 33, '33_imsssages.jpg'),
(33, 34, '34_imbbages.jpg'),
(34, 35, '35_imagttes.jpg'),
(35, 36, '36_imageds.jpg'),
(36, 37, '37_imaoooges.jpg'),
(37, 38, '38_imassges.jpg'),
(38, 39, '39_imayyyges.jpg'),
(39, 40, '40_imagttes.jpg'),
(40, 41, '41_imaoooges.jpg'),
(41, 42, '42_imageds.jpg'),
(42, 43, '43_imagppes.jpg'),
(43, 44, '44_imagppes.jpg'),
(44, 48, '48_imaaaages.jpg'),
(45, 49, '49_imagttes.jpg'),
(46, 50, '50_imaoooges.jpg'),
(47, 51, '51_imassges.jpg'),
(48, 52, '52_imiiiages.jpg'),
(49, 53, '53_imassges.jpg'),
(50, 54, '54_imagttes.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `auction_mail`
--

CREATE TABLE `auction_mail` (
  `id` int(11) NOT NULL auto_increment,
  `sender_id` int(11) NOT NULL,
  `recever_id` int(11) NOT NULL,
  `recever_admin` int(11) NOT NULL,
  `sender_admin` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `msg_to` varchar(255) NOT NULL,
  `msg_from` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `msg` text NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `sender_status` int(11) NOT NULL,
  `auction_mail_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `auction_mail`
--

INSERT INTO `auction_mail` (`id`, `sender_id`, `recever_id`, `recever_admin`, `sender_admin`, `parent_id`, `msg_to`, `msg_from`, `subject`, `msg`, `date`, `type`, `status`, `sender_status`, `auction_mail_id`) VALUES
(1, 2, 3, 0, 0, 3, 'test3', 'test2', 'case: 1', 'For this case item is not received........', '2013-09-24 06:40:11', 'case', 1, 0, 0),
(2, 2, 0, 1, 0, 3, 'test3', 'Support', 'case: 1', 'For this case item is not received........', '2013-09-24 06:40:11', 'case', 1, 0, 0),
(3, 2, 1, 0, 0, 1, 'test1', 'test2', 'case: 2', 'fcxcxcxc', '2013-09-28 12:50:00', 'case', 1, 0, 0),
(4, 2, 0, 1, 0, 1, 'test1', 'Support', 'case: 2', 'fcxcxcxc', '2013-09-28 12:50:00', 'case', 1, 0, 0),
(17, 0, 2, 0, 1, 1, 'test2', 'Support', 'Case # 1', 'rrrrrrrrrrrrrrrr', '2013-09-30 01:20:14', 'hivemarket', 1, 0, 0),
(18, 0, 3, 0, 1, 1, 'test3', 'Support', 'Case # 1', 'rrrrrrrrrrrrrrrr', '2013-09-30 01:20:14', 'hivemarket', 1, 0, 0),
(19, 3, 0, 1, 0, 1, 'Support', 'test3', 'Case # 1', 'rrrrrrr reply', '2013-09-30 01:22:04', 'hivemarket', 1, 0, 18),
(20, 0, 3, 0, 1, 1, 'test3', 'Support', 'Case # 1', 'yyyyyyyyyyyy reply', '2013-09-30 01:22:55', 'hivemarket', 0, 0, 19),
(21, 2, 1, 0, 0, 1, 'test1', 'test2', 'case: 3', 'that is a true fault...', '2013-11-11 06:26:59', 'case', 0, 0, 0),
(22, 2, 0, 1, 0, 1, 'test1', 'Support', 'case: 3', 'that is a true fault...', '2013-11-11 06:26:59', 'case', 1, 0, 0),
(23, 3, 3, 0, 0, 3, 'test3', 'test3', ' # 3', 'test111111111', '2013-11-12 06:22:47', 'auction', 0, 0, 0),
(24, 3, 1, 0, 0, 19, 'test1', 'test3', ' # 19', 'test...opopopo', '2013-11-12 06:56:27', 'auction', 1, 0, 0),
(25, 3, 2, 0, 0, 35, 'test2', 'test3', ' # 35', 'test 3 messge......', '2013-11-12 06:59:07', 'auction', 0, 0, 0),
(26, 3, 2, 0, 0, 35, 'test2', 'test3', ' # 35', 'test........35', '2013-11-12 09:20:27', 'auction', 0, 0, 0),
(27, 1, 1, 0, 0, 9, 'test1', 'test1', 'case: 4', 'jjjj\\''lll', '2013-11-21 09:20:46', 'case', 0, 0, 0),
(28, 1, 0, 1, 0, 9, 'test1', 'Support', 'case: 4', 'jjjj\\''lll', '2013-11-21 09:20:46', 'case', 1, 0, 0),
(29, 0, 2, 0, 1, 2, 'test2', 'Support', 'Case # 2', 'hhhhhhh''jjjj', '2013-11-25 05:00:47', 'hivemarket', 0, 0, 0),
(30, 0, 1, 0, 1, 2, 'test1', 'Support', 'Case # 2', 'hhhhhhh''jjjj', '2013-11-25 05:00:47', 'hivemarket', 0, 0, 0),
(31, 0, 2, 0, 1, 2, 'test2', 'Support', 'Case # 2', 'mmmmmmm', '2013-11-25 05:04:54', 'hivemarket', 0, 0, 0),
(32, 0, 1, 0, 1, 2, 'test1', 'Support', 'Case # 2', 'mmmmmmm', '2013-11-25 05:04:54', 'hivemarket', 0, 0, 0),
(33, 0, 2, 0, 1, 2, 'test2', 'Support', 'Case # 2', 'jjjjjj&#039;kkkk', '2013-11-25 05:05:06', 'hivemarket', 0, 0, 0),
(34, 0, 1, 0, 1, 2, 'test1', 'Support', 'Case # 2', 'jjjjjj&#039;kkkk', '2013-11-25 05:05:06', 'hivemarket', 0, 0, 0),
(35, 0, 1, 0, 1, 9, 'Support', 'test1', 'case: 4', 'hhhhhh&#039;ooooooo', '2013-11-25 05:10:09', 'case', 0, 0, 28);

-- --------------------------------------------------------

--
-- Table structure for table `auction_reversed`
--

CREATE TABLE `auction_reversed` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL,
  `credits_price` varchar(20) NOT NULL,
  `date_entered` datetime NOT NULL,
  `action` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `auction_reversed`
--

INSERT INTO `auction_reversed` (`id`, `user_id`, `auction_id`, `credits`, `credits_price`, `date_entered`, `action`, `status`) VALUES
(1, 3, 33, 1, '0.75', '2013-11-14 12:11:50', 'Reversed', 0);

-- --------------------------------------------------------

--
-- Table structure for table `authentication_pass`
--

CREATE TABLE `authentication_pass` (
  `id` int(11) NOT NULL auto_increment,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `authentication_pass`
--

INSERT INTO `authentication_pass` (`id`, `pass`) VALUES
(1, 'fbf7c48eb2711c77ce0f45d88b445acf');

-- --------------------------------------------------------

--
-- Table structure for table `case_note`
--

CREATE TABLE `case_note` (
  `id` int(11) NOT NULL auto_increment,
  `admin_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `case_note`
--

INSERT INTO `case_note` (`id`, `admin_id`, `case_id`, `description`, `date`, `status`) VALUES
(1, 2, 2, 'hhh&#039;hhh', '2013-11-25 05:00:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `faq_answers`
--

CREATE TABLE `faq_answers` (
  `id` int(11) NOT NULL auto_increment,
  `questions_id` int(11) NOT NULL,
  `date_entered` datetime NOT NULL,
  `answers` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `faq_answers`
--

INSERT INTO `faq_answers` (`id`, `questions_id`, `date_entered`, `answers`, `user_id`, `deleted`) VALUES
(1, 1, '2013-05-21 11:30:50', 'Hello answers answers answers', 6, 0),
(2, 1, '2013-05-21 11:15:14', 'This is my 1st post', 6, 0),
(3, 1, '2013-05-21 12:18:32', 'hello this is 2nd answer', 5, 0),
(4, 3, '2013-05-21 13:21:32', 'Answer of question no 2', 6, 0),
(5, 1, '2013-05-31 11:13:18', '', 7, 0),
(6, 1, '2013-06-04 04:57:23', 'test answer..........', 7, 0),
(7, 5, '2013-08-02 04:47:01', 'dsffmkdfjkdf', 6, 0),
(8, 13, '2013-11-22 05:32:44', 'this is reply of question''s 13', 1, 0),
(9, 12, '2013-11-22 05:37:06', 'question''s no 12', 1, 0),
(10, 2, '2013-11-22 05:40:39', 'gdfgdgdg', 1, 0),
(11, 4, '2013-11-22 05:41:37', 'jhjhjhj', 1, 0),
(12, 11, '2013-11-22 05:48:40', 'question''s... 11', 1, 0),
(13, 6, '2013-11-22 05:59:42', 'fxdcvxcv''jj', 1, 0),
(14, 7, '2013-11-22 06:09:42', 'sfdfdf', 1, 0),
(15, 8, '2013-11-22 06:10:04', 'dgdgd''ggggg', 1, 0),
(16, 9, '2013-11-22 06:10:33', 'refsff''hhh', 1, 0),
(17, 10, '2013-11-22 06:31:30', 'jjj''kkk', 1, 0),
(18, 20, '2013-11-22 06:33:42', 'fgfgf''ggg', 1, 0),
(19, 16, '2013-11-22 06:34:23', 'jgjhgjg''gggg', 1, 0),
(20, 15, '2013-11-22 06:35:03', 'ghghg''hhhhh', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `faq_comments`
--

CREATE TABLE `faq_comments` (
  `id` int(11) NOT NULL auto_increment,
  `answers_id` int(11) NOT NULL,
  `date_entered` datetime NOT NULL,
  `comments` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `faq_comments`
--

INSERT INTO `faq_comments` (`id`, `answers_id`, `date_entered`, `comments`, `user_id`, `deleted`) VALUES
(1, 1, '2013-05-21 10:17:13', 'comment1', 6, 0),
(2, 1, '2013-05-21 10:26:48', 'comment2', 6, 0),
(3, 1, '2013-05-21 10:39:33', 'comment3', 6, 0),
(4, 0, '2013-05-21 11:11:30', '', 6, 0),
(5, 2, '2013-05-21 11:19:40', 'This is my 1st post comment1', 6, 0),
(6, 5, '2013-05-31 11:15:18', '', 7, 0),
(7, 20, '2013-11-22 06:35:17', 'hghghg''jjjjjj', 2, 0),
(8, 20, '2013-11-22 06:35:46', 'jhjhj''yyyyy', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `faq_questions`
--

CREATE TABLE `faq_questions` (
  `id` int(11) NOT NULL auto_increment,
  `date_entered` datetime NOT NULL,
  `questions` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `faq_questions`
--

INSERT INTO `faq_questions` (`id`, `date_entered`, `questions`, `user_id`, `deleted`) VALUES
(1, '2013-05-20 12:54:58', 'This is questions no 1', 6, 0),
(2, '2013-05-20 12:57:28', 'hello world', 6, 0),
(3, '2013-05-21 13:19:30', 'question no 2', 6, 0),
(4, '2013-05-21 13:23:15', 'question no 3', 4, 0),
(5, '2013-05-22 12:49:09', 'This is questions no 5', 7, 0),
(6, '2013-05-28 06:06:59', 'question no 6', 7, 0),
(7, '2013-05-28 06:07:07', 'question no 7', 7, 0),
(8, '2013-05-28 06:07:13', 'question no 8', 7, 0),
(9, '2013-05-28 06:07:21', 'question no 9', 7, 0),
(10, '2013-05-28 06:07:27', 'question no 10', 7, 0),
(11, '2013-05-28 06:07:31', 'question no 11', 7, 0),
(12, '2013-05-28 06:07:39', 'question no 12', 7, 0),
(13, '2013-05-28 13:03:48', 'question no 13', 8, 0),
(14, '2013-09-23 05:22:40', 'hhhhhh', 1, 0),
(15, '2013-11-22 05:29:04', 'question''s 123', 1, 0),
(16, '2013-11-22 06:33:11', 'question''s 124', 2, 0),
(17, '2013-11-22 06:33:15', 'question''s 125', 2, 0),
(18, '2013-11-22 06:33:22', 'question''s 126', 2, 0),
(19, '2013-11-22 06:33:25', 'question''s 127', 2, 0),
(20, '2013-11-22 06:33:28', 'question''s 128', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `marketplace`
--

CREATE TABLE `marketplace` (
  `id` int(5) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `heading` varchar(255) default NULL,
  `meta_tag` varchar(255) default NULL,
  `meta_description` varchar(255) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `marketplace`
--

INSERT INTO `marketplace` (`id`, `title`, `heading`, `meta_tag`, `meta_description`, `description`) VALUES
(1, 'Hive Market Place', 'Hive Market Place', 'Hive Market Place', 'Hive Market Place', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `payer_id` varchar(255) NOT NULL,
  `txn` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `order_status` varchar(50) NOT NULL default 'Pending',
  `date` varchar(255) NOT NULL,
  `payment_scheduled_date` datetime NOT NULL,
  PRIMARY KEY  (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `payer_id`, `txn`, `amount`, `payment_type`, `payment_status`, `order_status`, `date`, `payment_scheduled_date`) VALUES
(1, 1, 'YX3YY9X7SCV4S', '1RH395744T639714R', '37.50', 'instant', 'Pending', 'Pending', '2013-09-13 11:31:31', '0000-00-00 00:00:00'),
(2, 2, 'YX3YY9X7SCV4S', '0H165319D43115728', '52.50', 'instant', 'Pending', 'Pending', '2013-09-13 11:36:19', '0000-00-00 00:00:00'),
(3, 3, 'YX3YY9X7SCV4S', '3MT378135M218953P', '75.00', 'instant', 'Pending', 'Pending', '2013-09-16 05:13:54', '0000-00-00 00:00:00'),
(4, 1, 'YX3YY9X7SCV4S', '11N245584R701423F', '150.00', 'instant', 'Pending', 'Pending', '2013-09-16 05:21:29', '0000-00-00 00:00:00'),
(5, 1, 'YX3YY9X7SCV4S', '5J8735010V501193Y', '22.50', 'instant', 'Pending', 'Pending', '2013-09-19 06:08:58', '0000-00-00 00:00:00'),
(6, 1, 'YX3YY9X7SCV4S', '5HU65360J06722911', '45.00', 'instant', 'Pending', 'Pending', '2013-09-19 06:27:26', '0000-00-00 00:00:00'),
(7, 1, 'YX3YY9X7SCV4S', '7KT40511A65489056', '18.75', 'instant', 'Pending', 'Pending', '2013-09-20 06:07:03', '0000-00-00 00:00:00'),
(8, 1, 'YX3YY9X7SCV4S', '7KT40511A65489056', '18.75', 'instant', 'Pending', 'Pending', '2013-09-20 06:10:34', '0000-00-00 00:00:00'),
(9, 1, 'YX3YY9X7SCV4S', '6WE12582N14348416', '15.00', 'instant', 'Pending', 'Pending', '2013-09-20 06:46:01', '0000-00-00 00:00:00'),
(11, 2, 'YX3YY9X7SCV4S', '46810818TN486892X', '26.50', 'instant', 'Pending', 'Pending', '2013-09-24 06:28:47', '2013-10-06 07:25:08'),
(12, 2, 'YX3YY9X7SCV4S', '4A012962UG611960W', '9.75', 'instant', 'Pending', 'Pending', '2013-09-25 08:54:03', '2013-10-06 04:47:57'),
(13, 1, 'YX3YY9X7SCV4S', '9XM04505D8832003B', '26.25', 'instant', 'Pending', 'Pending', '2013-09-27 10:51:44', '0000-00-00 00:00:00'),
(24, 3, 'XXN3LPLD5L77Y', '95888715RU272880W', '15.00', 'instant', 'Pending', 'Pending', '2013-10-09 13:47:07', '0000-00-00 00:00:00'),
(23, 3, '', '', '3.1875', 'MassPay', 'Received', 'Received', '2013-10-09 13:06:01', '0000-00-00 00:00:00'),
(25, 3, 'XXN3LPLD5L77Y', '1G176556Y6409215L', '18.75', 'instant', 'Pending', 'Pending', '2013-10-09 13:53:39', '0000-00-00 00:00:00'),
(26, 3, 'XXN3LPLD5L77Y', '2UM17190CG643240P', '15.00', 'instant', 'Pending', 'Pending', '2013-10-09 14:01:55', '0000-00-00 00:00:00'),
(27, 3, 'XXN3LPLD5L77Y', '9DX73970X7646703N', '18.75', 'instant', 'Pending', 'Pending', '2013-10-09 14:07:43', '0000-00-00 00:00:00'),
(28, 3, 'XXN3LPLD5L77Y', '10G50362XM676471V', '18.75', 'instant', 'Pending', 'Pending', '2013-10-09 14:11:36', '0000-00-00 00:00:00'),
(29, 2, 'XXN3LPLD5L77Y', '21N708804B222390Y', '22.50', 'instant', 'Pending', 'Pending', '2013-10-09 14:15:25', '0000-00-00 00:00:00'),
(30, 2, 'XXN3LPLD5L77Y', '1KG36182267396040', '18.75', 'instant', 'Pending', 'Pending', '2013-10-09 14:16:06', '0000-00-00 00:00:00'),
(31, 2, 'XXN3LPLD5L77Y', '34Y806127L612324N', '15.00', 'instant', 'Pending', 'Pending', '2013-10-09 14:19:22', '0000-00-00 00:00:00'),
(32, 2, 'XXN3LPLD5L77Y', '5GG15788JD543024D', '15.00', 'instant', 'Pending', 'Pending', '2013-10-09 14:21:14', '0000-00-00 00:00:00'),
(33, 1, 'YX3YY9X7SCV4S', '407975183N6261507', '15.00', 'instant', 'Pending', 'Pending', '2013-10-30 09:57:21', '0000-00-00 00:00:00'),
(34, 1, 'YX3YY9X7SCV4S', '7MU84599H9983544J', '16.50', 'instant', 'Pending', 'Pending', '2013-10-30 09:58:01', '0000-00-00 00:00:00'),
(35, 1, 'YX3YY9X7SCV4S', '6VF28789Y2104384E', '18.00', 'instant', 'Pending', 'Pending', '2013-10-30 09:58:42', '0000-00-00 00:00:00'),
(36, 2, 'YX3YY9X7SCV4S', '56D60501XX4191200', '15.00', 'instant', 'Pending', 'Pending', '2013-10-30 10:06:09', '0000-00-00 00:00:00'),
(37, 2, 'YX3YY9X7SCV4S', '9UF82228LJ656080U', '18.00', 'instant', 'Pending', 'Pending', '2013-10-30 10:08:24', '0000-00-00 00:00:00'),
(38, 2, 'YX3YY9X7SCV4S', '70J9147106271754J', '18.75', 'instant', 'Pending', 'Pending', '2013-10-30 10:09:20', '0000-00-00 00:00:00'),
(54, 2, '', '', '1.275', 'MassPay', 'Received', 'Received', '2013-10-30 11:53:19', '0000-00-00 00:00:00'),
(55, 1, '', '', '1.275', 'MassPay', 'Received', 'Received', '2013-10-30 12:32:29', '0000-00-00 00:00:00'),
(56, 2, '', '', '0.6375', 'MassPay', 'Received', 'Received', '2013-10-30 12:49:17', '0000-00-00 00:00:00'),
(57, 3, 'YX3YY9X7SCV4S', '97V01349GA128554Y', '18.55', 'instant', 'Pending', 'Pending', '2013-10-31 05:37:46', '0000-00-00 00:00:00'),
(58, 1, '', '', '1.275', 'MassPay', 'Received', 'Received', '2013-11-01 08:40:38', '0000-00-00 00:00:00'),
(59, 3, 'YX3YY9X7SCV4S', '14G425887D470645M', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 12:17:48', '0000-00-00 00:00:00'),
(63, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:44:41', '0000-00-00 00:00:00'),
(62, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:43:29', '0000-00-00 00:00:00'),
(61, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:43:18', '0000-00-00 00:00:00'),
(60, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:43:06', '0000-00-00 00:00:00'),
(64, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:44:54', '0000-00-00 00:00:00'),
(65, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:44:58', '0000-00-00 00:00:00'),
(66, 1, 'YX3YY9X7SCV4S', '6K500630A72639327', '18.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:46:27', '0000-00-00 00:00:00'),
(67, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:48:17', '0000-00-00 00:00:00'),
(68, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:48:38', '0000-00-00 00:00:00'),
(69, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:48:48', '0000-00-00 00:00:00'),
(70, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:49:49', '0000-00-00 00:00:00'),
(71, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:50:05', '0000-00-00 00:00:00'),
(72, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:53:17', '0000-00-00 00:00:00'),
(73, 1, 'YX3YY9X7SCV4S', '19J862753H169850K', '15.00', 'instant', 'Pending', 'Pending', '2013-11-01 13:53:20', '0000-00-00 00:00:00'),
(74, 1, 'YX3YY9X7SCV4S', '7CD733442U515684U', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 13:54:31', '0000-00-00 00:00:00'),
(75, 1, 'YX3YY9X7SCV4S', '7CD733442U515684U', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 13:54:42', '0000-00-00 00:00:00'),
(76, 1, 'YX3YY9X7SCV4S', '7CD733442U515684U', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 14:00:08', '0000-00-00 00:00:00'),
(77, 1, 'YX3YY9X7SCV4S', '7CD733442U515684U', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 14:00:11', '0000-00-00 00:00:00'),
(78, 1, 'YX3YY9X7SCV4S', '92A24930JK089862E', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 14:01:04', '0000-00-00 00:00:00'),
(79, 1, 'YX3YY9X7SCV4S', '92A24930JK089862E', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 14:01:14', '0000-00-00 00:00:00'),
(80, 1, 'YX3YY9X7SCV4S', '1WD593171D892992L', '16.50', 'instant', 'Pending', 'Pending', '2013-11-01 14:09:05', '0000-00-00 00:00:00'),
(81, 1, 'YX3YY9X7SCV4S', '9T6998906V513220X', '15.75', 'instant', 'Pending', 'Pending', '2013-11-01 14:23:54', '0000-00-00 00:00:00'),
(82, 2, 'XXN3LPLD5L77Y', '2U1736086R2564307', '3.80', 'instant', 'Pending', 'Pending', '2013-11-15 07:16:23', '0000-00-00 00:00:00'),
(83, 3, 'YX3YY9X7SCV4S', '0BA71463R8585442G', '16.25', 'instant', 'Pending', 'Pending', '2013-11-15 07:20:29', '0000-00-00 00:00:00'),
(95, 3, 'XXN3LPLD5L77Y', '7LT454530H085740C', '22.00', 'instant', 'Pending', 'Pending', '2013-11-16 07:34:52', '0000-00-00 00:00:00'),
(109, 3, '', '', '4.5', 'pay_at_pickup', 'Pending', 'Pending', '2013-11-16 09:44:43', '0000-00-00 00:00:00'),
(114, 1, '', '', '1.275', 'MassPay', 'Received', 'Received', '2013-11-20 08:34:16', '0000-00-00 00:00:00'),
(113, 2, '', '', '.75', 'pay_at_pickup', 'Pending', 'Pending', '2013-11-20 06:42:47', '0000-00-00 00:00:00'),
(112, 2, '', '', '8.25', 'pay_at_pickup', 'Pending', 'Pending', '2013-11-20 06:42:47', '0000-00-00 00:00:00'),
(111, 2, '', '', '8.25', 'pay_at_pickup', 'Pending', 'Pending', '2013-11-20 06:42:15', '0000-00-00 00:00:00'),
(110, 3, 'XXN3LPLD5L77Y', '05651500GM789650H', '29.50', 'instant', 'Pending', 'Pending', '2013-11-16 09:45:19', '0000-00-00 00:00:00'),
(115, 3, '', '', '8.2875', 'MassPay', 'Received', 'Received', '2013-11-20 11:42:18', '0000-00-00 00:00:00'),
(116, 3, '', '', '8.2875', 'MassPay', 'Received', 'Received', '2013-11-20 11:42:30', '0000-00-00 00:00:00'),
(117, 3, '', '', '8.2875', 'MassPay', 'Received', 'Received', '2013-11-20 11:50:59', '0000-00-00 00:00:00'),
(119, 3, 'XXN3LPLD5L77Y', '49B61599T6446244R', '15.00', 'instant', 'Pending', 'Pending', '2013-11-27 06:33:23', '0000-00-00 00:00:00'),
(120, 3, 'XXN3LPLD5L77Y', '45182044YX568980Y', '19.50', 'instant', 'Pending', 'Pending', '2013-11-27 06:58:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `shipping` varchar(20) NOT NULL,
  `shipping_or_pickup` varchar(20) NOT NULL,
  `payment_scheduled_date` datetime NOT NULL,
  `order_status` varchar(10) NOT NULL default 'Pending',
  `seller_payment_amount` double(10,4) NOT NULL,
  `hmp_fee_amount` double(10,4) NOT NULL,
  `CorrelationId` varchar(25) NOT NULL,
  `seller_payment_time` varchar(25) NOT NULL,
  `seller_payment` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124 ;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `product_name`, `qty`, `price`, `shipping`, `shipping_or_pickup`, `payment_scheduled_date`, `order_status`, `seller_payment_amount`, `hmp_fee_amount`, `CorrelationId`, `seller_payment_time`, `seller_payment`) VALUES
(1, 1, 1, 'buy credit', 1, '37.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(2, 2, 2, 'buy credit', 1, '52.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(3, 3, 3, 'buy credit', 1, '75.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(4, 4, 1, 'buy credit', 1, '150.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(5, 5, 1, 'buy credit', 1, '22.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(6, 6, 1, 'buy credit', 1, '45.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(7, 7, 1, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(8, 8, 1, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(9, 9, 1, 'buy credit', 1, '15.00', '0.00', '', '2013-09-26 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(13, 11, 3, 'keyboard', 1, '10.75', '4.00', 'pickup', '2013-10-03 11:28:23', 'Received', 9.1375, 1.6125, '88eaedf9afa71', '2013-09-28T10:12:12Z', 1),
(12, 11, 1, 'Mobile1', 1, '15.75', '0.00', '', '2013-10-03 07:27:15', 'Received', 13.3875, 2.3625, '54812f8e6eca9', '2013-10-30T09:23:02Z', 1),
(14, 12, 9, 'sunglass', 1, '9.75', '0.00', 'shipping', '2013-10-04 12:00:00', 'Received', 8.2875, 1.4625, '76bcf3dd5a455', '2013-10-30T09:23:04Z', 1),
(15, 13, 1, 'buy credit', 1, '26.25', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(16, 11, 3, 'keyboard', 1, '10.75', '4.00', 'pickup', '2013-10-03 11:28:23', 'Received', 9.1375, 1.6125, '88eaedf9afa71', '2013-09-21T10:12:12Z', 1),
(26, 23, 15, '4 HIVECombs', 1, '3.1875', '0.00', '', '0000-00-00 00:00:00', 'Received', 3.1875, 0.5625, 'a6ed93a27ff57', '2013-10-09T13:06:00Z', 1),
(27, 24, 3, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(28, 25, 3, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(29, 26, 3, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(30, 27, 3, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(31, 28, 3, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(32, 29, 2, 'buy credit', 1, '22.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(33, 30, 2, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(34, 31, 2, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(35, 32, 2, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(36, 33, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(37, 34, 1, 'buy credit', 1, '16.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(38, 35, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(39, 36, 2, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(40, 37, 2, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(41, 38, 2, 'buy credit', 1, '18.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(76, 73, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(74, 71, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(75, 72, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(73, 70, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(71, 68, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(72, 69, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(70, 67, 1, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(68, 65, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(69, 66, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(67, 64, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(65, 62, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(66, 63, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(64, 61, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(62, 59, 3, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(63, 60, 1, 'buy credit', 1, '18.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(60, 57, 19, 'test1', 1, '18.55', '0.05', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(61, 58, 32, '2 HIVECombs', 1, '1.275', '0.00', '', '0000-00-00 00:00:00', 'Received', 1.2750, 0.2250, '782ee2a1a770d', '2013-11-01T08:40:36Z', 1),
(59, 56, 25, '1 HIVECombs', 1, '0.6375', '0.00', '', '0000-00-00 00:00:00', 'Received', 0.6375, 0.1125, '86c92c114f350', '2013-10-30T12:49:15Z', 1),
(58, 55, 24, '2 HIVECombs', 1, '1.275', '0.00', '', '0000-00-00 00:00:00', 'Received', 1.2750, 0.2250, '1714f2b6e01d1', '2013-10-30T12:32:27Z', 1),
(57, 54, 23, '6 HIVECombs', 1, '1.275', '0.00', '', '0000-00-00 00:00:00', 'Received', 1.2750, 0.2250, '9e24ff0d4280', '2013-10-30T11:53:17Z', 1),
(77, 74, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(78, 75, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(79, 76, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(80, 77, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(81, 78, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(82, 79, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(83, 80, 1, 'buy credit', 1, '16.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(84, 81, 1, 'buy credit', 1, '15.75', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(85, 82, 19, 'test1', 1, '3.80', '0.05', '', '2013-11-22 12:00:00', 'Received', 3.2300, 0.5700, '5c1410ad1314', '2013-11-20T08:47:03Z', 1),
(86, 83, 2, 'Mouce', 1, '16.25', '5.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(115, 112, 8, 'bottle1', 1, '8.25', '0', 'pickup', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(114, 111, 8, 'bottle1', 1, '8.25', '0', 'pickup', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(113, 110, 35, 'test_35', 1, '7.50', '3.00', 'shipping', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(112, 110, 6, 'Private laptop', 1, '22.00', '1.00', 'shipping', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(111, 109, 34, 'test_34', 1, '4.5', '0', 'pickup', '2013-11-15 12:00:00', 'Received', 3.8250, 0.6750, 'de06edd62edfc', '2013-11-16T13:49:18Z', 1),
(116, 113, 7, 'monitor', 1, '.75', '0', 'pickup', '2013-11-29 12:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(117, 114, 45, '5 HIVECombs', 1, '1.275', '0.00', '', '0000-00-00 00:00:00', 'Received', 1.2750, 0.2250, '895a79c2d8744', '2013-11-20T08:34:15Z', 1),
(122, 119, 3, 'buy credit', 1, '15.00', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0),
(120, 117, 46, '5 HIVECombs', 1, '8.2875', '0.00', '', '0000-00-00 00:00:00', 'Received', 8.2875, 1.4625, 'ec95b055ae4fd', '2013-11-20T11:50:58Z', 1),
(123, 120, 3, 'buy credit', 1, '19.50', '0.00', '', '0000-00-00 00:00:00', 'Pending', 0.0000, 0.0000, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(5) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `heading` varchar(255) default NULL,
  `meta_tag` varchar(255) default NULL,
  `meta_description` varchar(255) default NULL,
  `editdescription` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `title`, `heading`, `meta_tag`, `meta_description`, `editdescription`) VALUES
(1, 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `private_auction`
--

CREATE TABLE `private_auction` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `private_auction`
--


-- --------------------------------------------------------

--
-- Table structure for table `recent_search`
--

CREATE TABLE `recent_search` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `params` text NOT NULL,
  `created_date` datetime NOT NULL,
  `status` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `recent_search`
--

INSERT INTO `recent_search` (`id`, `user_id`, `title`, `params`, `created_date`, `status`) VALUES
(1, 6, '', 's:22:"test&amp;#039;56_html2";', '2013-11-22 12:05:19', 0),
(2, 6, '', 's:12:"test&#039;56";', '2013-11-22 12:06:31', 0),
(3, 2, '', 's:13:"test&#039;123";', '2013-11-25 05:45:29', 0),
(4, 2, '', 's:13:"test&#039;123";', '2013-11-25 05:58:50', 0),
(5, 2, '', 's:13:"test&#039;123";', '2013-11-25 05:59:24', 0),
(6, 2, '', 's:17:"test&amp;#039;123";', '2013-11-25 06:04:17', 0),
(7, 2, '', 's:13:"test&#039;123";', '2013-11-25 06:06:00', 0),
(8, 2, '', 's:13:"test&#039;123";', '2013-11-25 06:06:45', 0),
(9, 2, '', 's:9:"qu&#039;t";', '2013-11-25 06:08:52', 0),
(10, 2, '', 's:13:"test&#039;123";', '2013-11-25 06:09:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE `rewards` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `reward_amount` double(10,4) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rewards`
--

INSERT INTO `rewards` (`id`, `user_id`, `reward_amount`, `date`) VALUES
(1, 1, 1.6875, '2013-11-25 06:57:36'),
(2, 2, 1.7250, '2013-11-20 09:07:27'),
(3, 3, 3.1875, '2013-11-25 06:57:13'),
(4, 4, 0.0000, '0000-00-00 00:00:00'),
(5, 5, 0.0000, '0000-00-00 00:00:00'),
(6, 6, 0.0000, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rewards_details`
--

CREATE TABLE `rewards_details` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `credit_spend` int(11) NOT NULL,
  `reward_amount` double(10,4) NOT NULL,
  `reward_from` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `convert_date` datetime NOT NULL,
  `converted` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rewards_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `shipping_method`
--

CREATE TABLE `shipping_method` (
  `shipping_id` int(11) NOT NULL auto_increment,
  `auction_id` int(11) NOT NULL,
  `offer_shipping` tinyint(1) default NULL,
  `us_company_name` varchar(200) NOT NULL,
  `us_handling_time` varchar(200) NOT NULL,
  `us_price` double NOT NULL,
  `us_paytype` enum('free','paid') NOT NULL default 'free',
  `ww_company_name` varchar(255) NOT NULL,
  `ww_handling_time` varchar(255) NOT NULL,
  `ww_price` double NOT NULL,
  `ww_paytype` enum('free','paid') NOT NULL default 'free',
  `offer_pickup` int(11) NOT NULL,
  `payat_pickup` int(11) NOT NULL,
  `return_policy` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`shipping_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `shipping_method`
--

INSERT INTO `shipping_method` (`shipping_id`, `auction_id`, `offer_shipping`, `us_company_name`, `us_handling_time`, `us_price`, `us_paytype`, `ww_company_name`, `ww_handling_time`, `ww_price`, `ww_paytype`, `offer_pickup`, `payat_pickup`, `return_policy`) VALUES
(1, 1, NULL, '', '1', 0, '', 'USPS', '4', 50, '', 1, 1, 0),
(2, 2, NULL, 'USPS', '3', 5, '', '', '', 0, '', 1, 0, 0),
(3, 3, NULL, 'USPS', '2', 4, '', 'FEDEX', '4', 10, '', 1, 0, 0),
(4, 6, 1, 'USPS', '2', 1, 'free', 'FEDEX', '2', 20, 'free', 1, 1, 0),
(5, 7, 1, 'UPS', '2', 20, 'free', 'USPS', '1', 0, 'free', 1, 1, 0),
(6, 8, NULL, 'USPS', '3', 10, '', 'FEDEX', '3', 0, 'free', 1, 1, 0),
(7, 9, NULL, 'USPS', '3', 0, '', 'FEDEX', '3', 0, 'free', 1, 0, 0),
(8, 11, NULL, 'USPS', '1', 0, '', 'FEDEX', '1', 0, 'free', 1, 1, 0),
(9, 14, NULL, 'USPS', '3', 0, '', 'FEDEX', '3', 20, '', 1, 0, 0),
(10, 16, 1, 'USPS', '3', 3, 'free', 'USPS', '2', 10, 'free', 1, 1, 0),
(11, 17, NULL, '', '1', 0, '', '', '1', 0, 'free', 1, 1, 0),
(12, 18, NULL, '', '1', 0, '', '', '1', 0, 'free', 1, 0, 0),
(13, 19, NULL, 'USPS', '2', 0.05, '', 'UPS', '1', 0, 'free', 1, 0, 0),
(14, 26, NULL, 'UPS', '1', 2, '', 'USPS', '1', 1, '', 1, 0, 0),
(15, 27, NULL, 'UPS', '1', 0.05, '', '', '1', 0.05, '', 1, 1, 0),
(16, 28, NULL, '', '1', 0, '', '', '1', 0, '', 1, 0, 0),
(17, 29, NULL, '', '1', 0, '', '', '1', 0, '', 1, 0, 0),
(18, 33, NULL, 'USPS', '2', 10, '', 'UPS', '2', 0, 'free', 1, 0, 0),
(19, 34, 0, 'UPS', '1', 2, 'free', 'USPS', '1', 0, 'free', 1, 1, 0),
(20, 35, 1, 'UPS', '1', 3, 'free', 'USPS', '1', 0, 'free', 1, 0, 0),
(21, 36, NULL, 'UPS', '1', 1, '', 'FEDEX', '1', 0, 'free', 1, 1, 0),
(22, 37, NULL, '', '', 0, '', '', '', 0, '', 1, 1, 0),
(23, 38, 1, 'UPS', '1', 2.22, 'free', '', '', 0, 'free', 1, 0, 0),
(24, 39, 1, 'USPS', '1', 3.03, '', '', '', 0, '', 1, 1, 0),
(25, 40, 1, 'USPS', '2', 2, '', 'UPS', '3', 3, '', 1, 1, 0),
(26, 41, 0, '', '', 0, '', '', '', 0, '', 1, 0, 0),
(27, 42, 0, '', '', 0, '', '', '', 0, '', 1, 0, 0),
(28, 43, 0, '', '', 0, '', '', '', 0, '', 1, 0, 0),
(29, 44, 0, '', '', 0, '', '', '', 0, '', 1, 1, 0),
(30, 48, 0, '', '1', 0, '', '', '1', 0, '', 1, 0, 0),
(31, 49, 1, 'UPS', '', 0, '', '', '', 0, '', 1, 1, 0),
(32, 50, 1, 'USPS', '', 0, '', '', '', 0, '', 1, 1, 0),
(33, 51, 1, 'USPS', '1', 0, '', 'USPS', '1', 0, 'free', 1, 1, 0),
(34, 52, 0, '', '', 0, '', '', '', 0, '', 1, 1, 0),
(35, 53, 0, '', '', 0, '', '', '', 0, '', 1, 1, 0),
(36, 54, 1, '', '', 1, 'free', '', '', 0, 'free', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(5) NOT NULL auto_increment,
  `twitter` varchar(255) NOT NULL,
  `vimeo` varchar(255) default NULL,
  `skype` varchar(255) default NULL,
  `facebook` varchar(255) default NULL,
  `flickr` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `rss` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `twitter`, `vimeo`, `skype`, `facebook`, `flickr`, `linkedin`, `rss`) VALUES
(1, 'twitter.com', '', '', 'https://www.facebook.com/pages/HIVE-Marketplace/477098022355428?fref=ts', '', '', 'rssfeed.comm');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `email` varchar(130) NOT NULL,
  `paypalemail` varchar(130) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `registeron` date NOT NULL,
  `street_address` varchar(255) default NULL,
  `city_province` varchar(100) default NULL,
  `country` varchar(100) default NULL,
  `update_userinfo_code` varchar(20) NOT NULL,
  `update_paypalemail_code` varchar(20) NOT NULL,
  `notes` text NOT NULL,
  `lost_login` datetime NOT NULL,
  `private_auction` text NOT NULL,
  `timezone` text,
  `status` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `paypalemail`, `zip`, `photo`, `registeron`, `street_address`, `city_province`, `country`, `update_userinfo_code`, `update_paypalemail_code`, `notes`, `lost_login`, `private_auction`, `timezone`, `status`) VALUES
(1, 'test1', 'test1', 'test1', 'test1', 'subhra.beas@gmail.com', 'subhra.beas.buyer@gmail.com', '123456', 'images.jpg', '2013-09-13', NULL, NULL, NULL, '', '', 'jjjjjjjjjj''hjhhhh', '2013-11-27 06:30:48', '6,26', NULL, 1),
(2, 'test2&#039;1', 'test2&#039;1', 'test2&#039;1', 'test2&#039;1', 'test.skm1@gmail.com', 'test.skm1@gmail.com', '32145', 'images3.jpg', '2013-09-13', '', '', '', 'wj2gYGFCXN', '', 'Tell Us the Story of you or your Produ&#039;cts', '2013-11-25 05:41:13', '6', 's:47:"(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";', 1),
(3, 'test3', 'test3', 'test3', 'test3', 'subhra.beas.buyer@gmail.com', 'subhra.beas.buyer@gmail.com', '52145', 'images4.jpg', '2013-09-13', NULL, NULL, NULL, '', '', 'Story of you or your Products', '2013-11-27 06:31:46', '6', NULL, 1),
(6, 'test&#039;56', 'test&#039;56', 'test&#039;56', 'test&#039;56', 'test222@test.com', 'user1@gmail.com', 'test&#039;56', 'imaoooges3.jpg', '2013-11-22', '', '', 'test&#039;56 test&#039;56 test&#039;56', 'W2nSuucJVj', '', '', '2013-11-22 11:08:01', '', 's:41:"(GMT- 12:00) International Date Line West";', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_bonus_credits`
--

CREATE TABLE `user_bonus_credits` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL,
  `credits_price` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `action` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_bonus_credits`
--

INSERT INTO `user_bonus_credits` (`id`, `user_id`, `auction_id`, `credits`, `credits_price`, `create_date`, `action`) VALUES
(1, 1, 6, 2, '1.50', '2013-11-13 12:20:02', 'Bid'),
(2, 3, 6, 3, '2.25', '2013-11-13 12:20:43', 'Bid'),
(5, 3, 35, -3, '-2.25', '2013-11-14 07:16:43', 'Bid'),
(6, 1, 38, -2, '-1.50', '2013-11-15 13:26:34', 'Bid');

-- --------------------------------------------------------

--
-- Table structure for table `user_buycredits`
--

CREATE TABLE `user_buycredits` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL,
  `credits_price` varchar(20) NOT NULL,
  `txn` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `action` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `left_amt` varchar(11) default NULL,
  `left_credit` varchar(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=194 ;

--
-- Dumping data for table `user_buycredits`
--

INSERT INTO `user_buycredits` (`id`, `user_id`, `auction_id`, `credits`, `credits_price`, `txn`, `create_date`, `action`, `status`, `left_amt`, `left_credit`) VALUES
(1, 1, 0, 50, '37.50', '1RH395744T639714R', '2013-09-13 11:31:31', 'Buy', 0, NULL, NULL),
(2, 2, 0, 70, '52.50', '0H165319D43115728', '2013-09-13 11:36:19', 'Buy', 0, NULL, NULL),
(3, 1, 3, 1, '0.75', '', '2013-09-13 12:14:23', 'Bid', 0, NULL, NULL),
(4, 2, 3, 2, '1.50', '', '2013-09-13 12:15:19', 'Bid', 0, NULL, NULL),
(5, 1, 3, 3, '2.25', '', '2013-09-13 12:15:57', 'Bid', 0, NULL, NULL),
(6, 2, 3, 3, '2.25', '', '2013-09-13 12:17:09', 'Bid', 0, NULL, NULL),
(8, 3, 0, 100, '75.00', '3MT378135M218953P', '2013-09-16 05:13:54', 'Buy', 0, NULL, NULL),
(9, 1, 0, 200, '150.00', '11N245584R701423F', '2013-09-16 05:21:29', 'Buy', 0, NULL, NULL),
(11, 1, 6, 1, '0.75', '', '2013-09-18 06:55:50', 'Bid', 0, NULL, NULL),
(12, 3, 6, 2, '1.50', '', '2013-09-18 06:58:25', 'Bid', 0, NULL, NULL),
(13, 1, 6, 2, '1.50', '', '2013-09-18 07:06:03', 'Bid', 0, NULL, NULL),
(14, 3, 6, 2, '1.50', '', '2013-09-18 07:18:29', 'Bid', 0, NULL, NULL),
(15, 1, 6, 2, '1.50', '', '2013-09-18 07:37:53', 'Bid', 0, NULL, NULL),
(16, 3, 6, 2, '1.50', '', '2013-09-18 08:30:55', 'Bid', 0, NULL, NULL),
(17, 1, 6, 2, '1.50', '', '2013-09-18 08:43:05', 'Bid', 0, NULL, NULL),
(18, 3, 6, 2, '1.50', '', '2013-09-18 08:57:36', 'Bid', 0, NULL, NULL),
(19, 1, 6, 2, '1.50', '', '2013-09-18 08:59:17', 'Bid', 0, NULL, NULL),
(20, 3, 6, 2, '1.50', '', '2013-09-18 09:01:15', 'Bid', 0, NULL, NULL),
(21, 1, 6, 2, '1.50', '', '2013-09-18 09:02:16', 'Bid', 0, NULL, NULL),
(22, 3, 6, 2, '1.50', '', '2013-09-18 09:05:01', 'Bid', 0, NULL, NULL),
(23, 1, 0, 30, '22.50', '5J8735010V501193Y', '2013-09-19 06:08:58', 'Buy', 1, '0', '0'),
(24, 1, 0, 60, '45.00', '5HU65360J06722911', '2013-09-19 06:27:26', 'Buy', 0, NULL, NULL),
(25, 1, 0, 30, '22.50', '5J8735010V501193Y', '2013-09-19 07:02:11', 'Withdraw', 0, NULL, NULL),
(26, 3, 1, 10, '7.50', '', '2013-09-19 10:08:04', 'Bid', 0, NULL, NULL),
(27, 2, 1, 11, '8.25', '', '2013-09-19 10:13:39', 'Bid', 0, NULL, NULL),
(28, 1, 2, 7, '5.25', '', '2013-09-19 11:12:59', 'Bid', 0, NULL, NULL),
(29, 3, 2, 8, '6.00', '', '2013-09-19 11:13:17', 'Bid', 0, NULL, NULL),
(30, 1, 0, 25, '18.75', '7KT40511A65489056', '2013-09-20 06:07:03', 'Buy', 1, '0', '0'),
(31, 1, 0, 25, '18.75', '7KT40511A65489056', '2013-09-20 06:10:34', 'Buy', 1, '0', '0'),
(32, 1, 0, 20, '15.00', '6WE12582N14348416', '2013-09-20 06:46:01', 'Buy', 1, '0', '0'),
(33, 2, 9, 1, '0.75', '', '2013-09-20 12:03:32', 'Bid', 0, NULL, NULL),
(34, 3, 9, 2, '1.50', '', '2013-09-20 12:58:56', 'Bid', 0, NULL, NULL),
(35, 2, 9, 2, '1.50', '', '2013-09-20 12:59:03', 'Bid', 0, NULL, NULL),
(36, 2, 10, 5, '3.75', '', '2013-09-20 12:59:33', 'Auction', 0, NULL, NULL),
(37, 1, 10, 1, '0.75', '', '2013-09-23 08:32:33', 'Bid', 0, NULL, NULL),
(38, 3, 9, 2, '1.50', '', '2013-09-23 11:41:22', 'Bid', 0, NULL, NULL),
(39, 2, 9, 2, '1.50', '', '2013-09-23 11:51:53', 'Bid', 0, NULL, NULL),
(40, 2, 8, 1, '0.75', '', '2013-09-24 10:16:39', 'Bid', 0, NULL, NULL),
(41, 3, 9, 2, '1.50', '', '2013-09-25 08:49:10', 'Bid', 0, NULL, NULL),
(42, 2, 9, 2, '1.50', '', '2013-09-25 08:49:44', 'Bid', 0, NULL, NULL),
(43, 1, 0, 35, '26.25', '9XM04505D8832003B', '2013-09-27 10:51:44', 'Buy', 1, '0', '0'),
(44, 3, 8, 2, '1.50', '', '2013-10-07 09:54:42', 'Bid', 0, NULL, NULL),
(45, 2, 8, 3, '2.25', '', '2013-10-07 10:08:20', 'Bid', 0, NULL, NULL),
(46, 3, 8, 3, '2.25', '', '2013-10-07 10:11:40', 'Bid', 0, NULL, NULL),
(49, 2, 8, 2, '1.50', '', '2013-10-07 12:52:40', 'Bid', 0, NULL, NULL),
(50, 1, 15, 4, '3.00', '', '2013-10-09 06:31:53', 'Auction', 0, NULL, NULL),
(51, 2, 15, 2, '1.50', '', '2013-10-09 06:50:31', 'Bid', 0, NULL, NULL),
(52, 3, 15, 3, '2.25', '', '2013-10-09 07:01:46', 'Bid', 0, NULL, NULL),
(62, 3, 15, 5, '3.1875', '', '2013-10-09 13:06:00', 'Buy', 1, '0', '0'),
(63, 3, 0, 20, '15.00', '95888715RU272880W', '2013-10-09 13:47:07', 'Buy', 1, '0', '0'),
(64, 3, 0, 20, '15.00', '95888715RU272880W', '2013-10-09 01:47:35', 'Withdraw', 0, NULL, NULL),
(65, 3, 0, 25, '18.75', '1G176556Y6409215L', '2013-10-09 13:53:39', 'Buy', 1, '0', '0'),
(66, 3, 0, 25, '18.75', '1G176556Y6409215L', '2013-10-09 01:56:22', 'Withdraw', 0, NULL, NULL),
(67, 3, 0, 20, '15.00', '2UM17190CG643240P', '2013-10-09 14:01:55', 'Buy', 1, '0', '0'),
(68, 3, 0, 20, '15.00', '2UM17190CG643240P', '2013-10-09 02:02:20', 'Withdraw', 0, NULL, NULL),
(69, 3, 0, 25, '18.75', '9DX73970X7646703N', '2013-10-09 14:07:43', 'Buy', 1, '0', '0'),
(70, 3, 0, 25, '18.75', '9DX73970X7646703N', '2013-10-09 02:08:02', 'Withdraw', 0, NULL, NULL),
(71, 3, 0, 25, '18.75', '10G50362XM676471V', '2013-10-09 14:11:36', 'Buy', 1, '0', '0'),
(72, 3, 0, 25, '18.75', '10G50362XM676471V', '2013-10-09 02:11:55', 'Withdraw', 0, NULL, NULL),
(73, 2, 0, 30, '22.50', '21N708804B222390Y', '2013-10-09 14:15:25', 'Buy', 1, '0', '0'),
(74, 2, 0, 25, '18.75', '1KG36182267396040', '2013-10-09 14:16:06', 'Buy', 1, '0', '0'),
(75, 2, 0, 25, '18.75', '1KG36182267396040', '2013-10-09 02:16:20', 'Withdraw', 0, NULL, NULL),
(76, 2, 0, 30, '22.50', '21N708804B222390Y', '2013-10-09 02:18:35', 'Withdraw', 0, NULL, NULL),
(77, 2, 0, 20, '15.00', '34Y806127L612324N', '2013-10-09 14:19:22', 'Buy', 1, '0', '0'),
(78, 2, 0, 20, '15.00', '34Y806127L612324N', '2013-10-09 02:19:43', 'Withdraw', 0, NULL, NULL),
(79, 2, 0, 20, '15.00', '5GG15788JD543024D', '2013-10-09 14:21:14', 'Buy', 1, '0', '0'),
(80, 2, 0, 20, '15.00', '5GG15788JD543024D', '2013-10-09 02:21:31', 'Withdraw', 0, NULL, NULL),
(81, 1, 16, 1, '0.75', '', '2013-10-15 08:38:29', 'Bid', 0, NULL, NULL),
(82, 3, 16, 2, '1.50', '', '2013-10-15 08:39:07', 'Bid', 0, NULL, NULL),
(83, 1, 16, 2, '1.50', '', '2013-10-15 08:41:04', 'Bid', 0, NULL, NULL),
(84, 3, 16, 2, '1.50', '', '2013-10-15 08:48:41', 'Bid', 0, NULL, NULL),
(85, 1, 16, 4, '3.00', '', '2013-10-15 08:49:05', 'Bid', 0, NULL, NULL),
(87, 1, 0, 20, '15.00', '6WE12582N14348416', '2013-10-17 08:43:58', 'Withdraw', 0, NULL, NULL),
(88, 2, 7, 1, '0.75', '', '2013-10-21 05:42:39', 'Bid', 0, NULL, NULL),
(89, 2, 19, 1, '0.75', '', '2013-10-30 06:30:19', 'Bid', 0, NULL, NULL),
(92, 1, 22, 3, '2.25', '', '2013-10-30 09:07:03', 'Auction', 0, NULL, NULL),
(93, 2, 22, 2, '1.50', '', '2013-10-30 09:09:31', 'Bid', 0, NULL, NULL),
(94, 1, 0, 25, '18.75', '7KT40511A65489056', '2013-10-30 09:46:35', 'Withdraw', 0, NULL, NULL),
(95, 1, 0, 35, '26.25', '9XM04505D8832003B', '2013-10-30 09:47:38', 'Withdraw', 0, NULL, NULL),
(96, 1, 0, 20, '15.00', '407975183N6261507', '2013-10-30 09:57:21', 'Buy', 1, '0', '0'),
(97, 1, 0, 22, '16.50', '7MU84599H9983544J', '2013-10-30 09:58:01', 'Buy', 1, '0', '0'),
(98, 1, 0, 24, '18.00', '6VF28789Y2104384E', '2013-10-30 09:58:42', 'Buy', 1, '0', '0'),
(99, 1, 0, 24, '18.00', '6VF28789Y2104384E', '2013-10-30 10:00:57', 'Withdraw', 0, NULL, NULL),
(100, 1, 0, 22, '16.50', '7MU84599H9983544J', '2013-10-30 10:03:07', 'Withdraw', 0, NULL, NULL),
(101, 1, 0, 20, '15.00', '407975183N6261507', '2013-10-30 10:04:59', 'Withdraw', 0, NULL, NULL),
(102, 2, 0, 20, '15.00', '56D60501XX4191200', '2013-10-30 10:06:09', 'Buy', 0, NULL, NULL),
(103, 2, 0, 24, '18.00', '9UF82228LJ656080U', '2013-10-30 10:08:24', 'Buy', 0, NULL, NULL),
(104, 2, 0, 25, '18.75', '70J9147106271754J', '2013-10-30 10:09:20', 'Buy', 1, '0', '0'),
(105, 2, 0, 25, '18.75', '70J9147106271754J', '2013-10-30 10:18:44', 'Withdraw', 0, NULL, NULL),
(106, 1, 23, 6, '4.50', '', '2013-10-30 11:16:08', 'Auction', 0, NULL, NULL),
(107, 2, 23, 2, '1.50', '', '2013-10-30 11:17:07', 'Bid', 0, NULL, NULL),
(108, 2, 23, 2, '1.275', '', '2013-10-30 11:47:22', 'Buy', 1, '0', '0'),
(123, 2, 23, 2, '1.275', '', '2013-10-30 11:53:19', 'Buy', 1, '0', '0'),
(124, 2, 24, 2, '1.50', '', '2013-10-30 12:04:22', 'Auction', 0, NULL, NULL),
(125, 1, 24, 2, '1.50', '', '2013-10-30 12:28:58', 'Bid', 0, NULL, NULL),
(126, 1, 24, 2, '1.275', '', '2013-10-30 12:32:29', 'Buy', 1, '0', '0'),
(127, 1, 25, 1, '0.75', '', '2013-10-30 12:44:10', 'Auction', 0, NULL, NULL),
(128, 2, 25, 1, '0.75', '', '2013-10-30 12:44:48', 'Bid', 0, NULL, NULL),
(129, 2, 25, 1, '0.6375', '', '2013-10-30 12:49:17', 'Buy', 1, '0', '0'),
(130, 3, 19, 2, '1.50', '', '2013-10-31 05:01:33', 'Bid', 0, NULL, NULL),
(131, 2, 19, 2, '1.50', '', '2013-10-31 05:02:22', 'Bid', 0, NULL, NULL),
(134, 2, 32, 2, '1.50', '', '2013-11-01 08:37:20', 'Auction', 0, NULL, NULL),
(135, 1, 32, 2, '1.50', '', '2013-11-01 08:39:33', 'Bid', 0, NULL, NULL),
(136, 1, 32, 2, '1.275', '', '2013-11-01 08:40:38', 'Buy', 1, '0', '0'),
(137, 3, 0, 20, '15.00', '14G425887D470645M', '2013-11-01 12:17:48', 'Buy', 0, NULL, NULL),
(138, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:43:06', 'Buy', 0, NULL, NULL),
(139, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:43:18', 'Buy', 0, NULL, NULL),
(140, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:43:29', 'Buy', 0, NULL, NULL),
(141, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:44:41', 'Buy', 0, NULL, NULL),
(142, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:44:54', 'Buy', 0, NULL, NULL),
(143, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:44:58', 'Buy', 0, NULL, NULL),
(144, 1, 0, 24, '18.00', '6K500630A72639327', '2013-11-01 13:46:27', 'Buy', 0, NULL, NULL),
(145, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:48:17', 'Buy', 0, NULL, NULL),
(146, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:48:38', 'Buy', 0, NULL, NULL),
(147, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:48:48', 'Buy', 0, NULL, NULL),
(148, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:49:49', 'Buy', 0, NULL, NULL),
(149, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:50:05', 'Buy', 0, NULL, NULL),
(150, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:53:17', 'Buy', 0, NULL, NULL),
(151, 1, 0, 20, '15.00', '19J862753H169850K', '2013-11-01 13:53:20', 'Buy', 0, NULL, NULL),
(152, 1, 0, 21, '15.75', '7CD733442U515684U', '2013-11-01 13:54:31', 'Buy', 0, NULL, NULL),
(153, 1, 0, 21, '15.75', '7CD733442U515684U', '2013-11-01 13:54:42', 'Buy', 0, NULL, NULL),
(154, 1, 0, 21, '15.75', '7CD733442U515684U', '2013-11-01 14:00:08', 'Buy', 0, NULL, NULL),
(155, 1, 0, 21, '15.75', '7CD733442U515684U', '2013-11-01 14:00:11', 'Buy', 0, NULL, NULL),
(156, 1, 0, 21, '15.75', '92A24930JK089862E', '2013-11-01 14:01:04', 'Buy', 0, NULL, NULL),
(157, 1, 0, 21, '15.75', '92A24930JK089862E', '2013-11-01 14:01:14', 'Buy', 0, NULL, NULL),
(158, 1, 0, 22, '16.50', '1WD593171D892992L', '2013-11-01 14:09:05', 'Buy', 0, NULL, NULL),
(159, 1, 0, 21, '15.75', '9T6998906V513220X', '2013-11-01 14:23:54', 'Buy', 0, NULL, NULL),
(166, 3, 35, 9, '6.75', '', '2013-11-14 07:16:43', 'Bid', 0, NULL, NULL),
(168, 3, 34, 6, '4.50', '', '2013-11-14 09:37:32', 'Bid', 0, NULL, NULL),
(176, 3, 0, 3, '2.25', '', '2013-11-14 14:51:18', 'Withdraw', 0, NULL, NULL),
(177, 3, 0, 4, '3', '', '2013-11-15 05:29:13', 'Withdraw', 0, NULL, NULL),
(178, 3, 0, 3, '2.25', '', '2013-11-15 05:32:01', 'Withdraw', 0, NULL, NULL),
(179, 1, 38, 5, '3.75', '', '2013-11-15 13:26:34', 'Bid', 0, NULL, NULL),
(180, 2, 0, 5, '3.75', '', '2013-11-19 14:21:06', 'Withdraw', 0, NULL, NULL),
(181, 2, 45, 5, '3.75', '', '2013-11-20 08:31:48', 'Auction', 0, NULL, NULL),
(182, 1, 45, 2, '1.50', '', '2013-11-20 08:33:06', 'Bid', 0, NULL, NULL),
(183, 1, 45, 2, '1.275', '', '2013-11-20 08:34:16', 'Buy', 1, '0', '0'),
(184, 1, 46, 5, '3.75', '', '2013-11-20 08:55:56', 'Auction', 0, NULL, NULL),
(185, 2, 46, 3, '2.25', '', '2013-11-20 09:07:27', 'Bid', 0, NULL, NULL),
(186, 3, 46, 10, '7.50', '', '2013-11-20 10:02:27', 'Bid', 0, NULL, NULL),
(189, 3, 46, 5, '3.75', '', '2013-11-20 11:50:59', 'Buy', 1, '0', '0'),
(190, 3, 54, 3, '2.25', '', '2013-11-25 06:57:13', 'Bid', 0, NULL, NULL),
(191, 1, 54, 5, '3.75', '', '2013-11-25 06:57:36', 'Bid', 0, NULL, NULL),
(192, 3, 0, 20, '15.00', '49B61599T6446244R', '2013-11-27 06:33:23', 'Buy', 0, NULL, NULL),
(193, 3, 0, 26, '19.50', '45182044YX568980Y', '2013-11-27 06:58:18', 'Buy', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `email_auction_watchlist_ends` tinyint(1) NOT NULL default '0',
  `email_auction_bid_end` tinyint(1) NOT NULL default '0',
  `email_win_auction` tinyint(1) NOT NULL default '0',
  `email_auction_sudden_death` tinyint(1) NOT NULL default '0',
  `email_auction_bid_doesnot_mmeets_and_sudden_death` tinyint(1) NOT NULL default '0',
  `forward_email_from_member` tinyint(1) NOT NULL default '0',
  `notify_5_min_left_auction` tinyint(1) NOT NULL default '0',
  `date_modified` date NOT NULL,
  `delete` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `email_auction_watchlist_ends`, `email_auction_bid_end`, `email_win_auction`, `email_auction_sudden_death`, `email_auction_bid_doesnot_mmeets_and_sudden_death`, `forward_email_from_member`, `notify_5_min_left_auction`, `date_modified`, `delete`) VALUES
(1, 6, 1, 1, 1, 0, 0, 0, 1, '2013-05-16', 0),
(2, 7, 0, 0, 0, 1, 0, 0, 0, '2013-05-22', 0),
(3, 8, 0, 0, 0, 0, 0, 0, 0, '2013-05-22', 0),
(4, 9, 0, 0, 0, 0, 0, 0, 0, '2013-08-22', 0),
(5, 1, 0, 0, 0, 0, 0, 0, 0, '2013-09-13', 0),
(6, 2, 0, 0, 0, 0, 0, 0, 0, '2013-09-13', 0),
(7, 3, 0, 0, 0, 0, 0, 0, 0, '2013-09-13', 0),
(8, 4, 0, 0, 0, 0, 0, 0, 0, '2013-11-22', 0),
(9, 5, 0, 0, 0, 0, 0, 0, 0, '2013-11-22', 0),
(10, 6, 0, 0, 0, 0, 0, 0, 0, '2013-11-22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `watchlist`
--

CREATE TABLE `watchlist` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `auction_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `watchlist`
--

INSERT INTO `watchlist` (`id`, `user_id`, `auction_id`, `create_date`, `status`) VALUES
(1, 1, 6, '2013-09-18 09:09:35', 1),
(2, 2, 9, '2013-09-23 09:57:09', 1),
(3, 3, 9, '2013-09-23 11:48:30', 1),
(4, 2, 8, '2013-09-24 10:17:42', 1),
(5, 3, 8, '2013-10-07 10:11:27', 1),
(6, 2, 18, '2013-10-16 08:30:55', 1),
(7, 1, 18, '2013-10-16 08:31:14', 1),
(8, 3, 35, '2013-11-12 06:58:21', 1),
(9, 3, 34, '2013-11-12 06:58:34', 1),
(10, 3, 33, '2013-11-14 11:20:18', 1);
